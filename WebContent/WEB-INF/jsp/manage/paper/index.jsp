<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>期刊杂志</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
		<div region="west" border="true">


            <table id="grid" style="width: 900px" title="期刊/杂志列表" iconcls="icon-view">            
            </table>


		</div>
	</div>
	<div id="mm" class="easyui-menu" style="width:120px;">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新</div>
	    <div class="menu-sep"></div>
	    <div onClick="upload()" data-options="iconCls:'icon-upload'">传图</div>
	    <div onClick="view()" data-options="iconCls:'icon-view'">预览</div>
	</div>
	<div id="addDialog" class="easyui-window"  title="" collapsible="false" minimizable="false"
        maximizable="false" icon="icon-save"  style="width: 800px; height: 400px; padding: 5px;
        background: #fafafa;">
		<jsp:include page="/manage/paper/add.jsp"></jsp:include>
	</div>
	<div id="uploadDialog" class="easyui-window"  title="" collapsible="false" minimizable="false"
        maximizable="false" icon="icon-save"  style="width: 600px; height: 400px; padding: 5px;
        background: #fafafa;">
		<jsp:include page="/manage/paper/upload.jsp"></jsp:include>
	</div>
	
	<script type="text/javascript">
	//实现对DataGird控件的绑定操作
        function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>manage/paper/list.do',   //指向后台的Action来获取当前菜单的信息的Json格式的数据
                title: '期刊杂志列表',
                iconCls: 'icon-grid',
                singleSelect:false,
                height: 650,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: true,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
				    $('#mm').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
   				
                columns: [[
                    { field: 'ck', checkbox: true },   //选择
                     { title: '期刊/杂志名称', field: 'paperName', width: 200, sortable:true },
                     { title: '类别', field: 'type', width: 150, sortable:true,formatter: function (val, rowdata, index){if(val=='1'){return '期刊'}else{return '杂志'}} },
                     { title: '排序', field: 'order', width: 100, sortable:true },
                     { title: 'ID', field: 'id', sortable:true },
                  	 { title: '创建时间', field: 'createTime', width: 100,sortable:true,formatter: function (val, rowdata, index){var _s=eval(val);return (1900+_s.year)+"-"+_s.month+"-"+_s.day;} },
                  	 { title: '更新时间', field: 'updateTime', width: 100, sortable:true,formatter: function (val, rowdata, index){var _s=eval(val);return (1900+_s.year)+"-"+_s.month+"-"+_s.day;} },
                  	 { title: '发布目录', field: 'publishDir', width: 200, sortable:true },
                     { title: '说明', field: 'intro', width: 80, sortable:true,hidden:true }
               ]], 
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditOrViewDialog();//实现修改记录的方法
                    }
                }, '-', {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                },'-', {
                    id: 'btnUpload',
                    text: '传图',
                    iconCls: 'icon-upload',
                    handler: function () {
                        upload();
                    }
                },'-', {
                    id: 'btnView',
                    text: '预览',
                    iconCls: 'icon-view',
                    handler: function () {
                        view();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    ShowEditOrViewDialog();
                }
            })
        };
        function reload(){
        	$("#grid").datagrid("reload");
        };
        function ShowAddDialog(){
        	setFieldValue('','','','','','');
        	$('#addDialog').window('open');
        };
        function upload(){
        	 var row = $('#grid').datagrid('getSelected');
			if (row){	
			$("#upload-main .fm .left iframe").attr("src","<%=basePath %>manage/upload/index.do?s_type=periodical&s_dir="+row.publishDir+"&s_index=1");
				//setFieldValue(row.id,row.paperName,row.type,row.order,row.publishDir,row.intro);
				$('#uploadDialog').window('open');
			}else{
				$.messager.alert('提示','请选择记录后再操作');
			}
        };
        function view(){
       	 var row = $('#grid').datagrid('getSelected');
			if (row){	
				window.open("<%=basePath %>publish/"+row.publishDir+"/index.html");
			}else{
				$.messager.alert('提示','请选择记录后再操作');
			}
        };
        function setFieldValue(id,paperName,type,order,publishDir,intro){
        
        		$("#id").val(id);
        		$("#paperName").val(paperName);
				$("#type").val(type);
				$("#order").val(order);
				$("#publishDir").val(publishDir); 
				$("#intro").val(intro); 
        };
        function ShowEditOrViewDialog(){
        	 var row = $('#grid').datagrid('getSelected');
			if (row){	
			$("#fm").attr("action","<%=basePath%>manage/paper/doEdit.do");
				setFieldValue(row.id,row.paperName,row.type,row.order,row.publishDir,row.intro);
				$('#addDialog').window('open');
			}else{
				$.messager.alert('提示','请选择记录后再操作');
			}
        };
        function del(){
	        var row = $('#grid').datagrid('getSelections');
	        if(row.length>0){
	        	$.messager.confirm('提示','确定删除?',function(r){
		        	if(r){
			        	var ids="";
			        	$.each(row, function (index, item) {
		            		ids += item.id + ",";
		        		});
		        		ids=ids.substring(0,ids.length-1);
			        	location.href="<%=basePath %>manage/paper/doDel.do?ids="+ids+"&modelId=${param.modelId}";
		        	}
	        	});
	        }else{
	        	$.messager.alert('提示','没有选中记录');
	        }
        };
        $(function(){
        	InitGrid("");//中间设置查询参数，该参数需要从request的流中读取
        	$('#addDialog').window({
                title: '添加新的字段',
                width: 800,
                modal: true,
                shadow: true,
                closed: true,
                height: 400,
                resizable:false
            });
        	$('#addDialog').window('close');
        	$('#uploadDialog').window({
                title: '传图片',
                width: 600,
                modal: true,
                shadow: true,
                closed: true,
                height: 400,
                resizable:false
            });
        	$('#uploadDialog').window('close');
        });
	</script>
	</body>
</html>
