<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
    <div id="contentview" class="easyui-panel" fit="true" style="background: #eee; overflow-y:hidden">
	 	<table id="grid" style="width: 900px;height:auto;" title="商品管理" iconcls="icon-view">            
	    </table>
    </div>

	<div id="nodeCtxMenu" class="easyui-menu" style="width:130px;">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑该栏目</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除该栏目</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新栏目列表</div>
	    <div onClick="publish_node()" data-options="iconCls:'icon-publish-node'">发布该栏目首页</div>
	    <div onClick="publish_content()" data-options="iconCls:'icon-publish-content'">发布该栏目内容</div>
	    <div onClick="publish_tree()" data-options="iconCls:'icon-publish-tree'">增量发布</div>
	    <div onClick="closeNode()" data-options="iconCls:'icon-closeNode'">开启/关闭该栏目</div>
	    <div onClick="hiddenToNav()" data-options="iconCls:'icon-hiddenToNav'">导航显示显示/隐藏</div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//initTabs();
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }

	function hiddenToNav(){
		var _setNode=$('#grid').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Product/navi.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","执行成功.","warning");
						reload();
					}
				);

	};
	//not used function
	function status(s){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
			$.post(
				basePath+"/Manage/Product/status.do",
				{ids:_ids,status:s},
				function(result){
					if(result.indexOf("msg")!=-1){
						var _ss=eval("("+result+")");
						if(_ss.status=='99'){
							msgShow("提示","执行成功.","warning");
							reload();
						}else{
							msgShow("提示",_ss.msg,"warning");
						}
					}
					
				}
			);
		}
	};
	function reload(){
		$('#grid').datagrid("reload");
	};
	function del(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/Product/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Product/list.do?id=${cataId }&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },
					{ title: 'id', field: 'id', width: 20, sortable:true,hidden:true },
					{ title: '商品名称', field: 'name', width: 60, sortable:true },
					{ title: '商品标题', field: 'title', width: 60, sortable:true,hidden:true },
					{ title: '商品介绍', field: 'introduce', width: 40, sortable:true,hidden:true },
					{ title: '原售价', field: 'price', width: 40, sortable:true },
					{ title: '现售价', field: 'nowPrice', width: 40, sortable:true },
					{ title: '商品图片', field: 'picture', width: 40, sortable:true,hidden:true },
					{ title: '商品大图', field: 'maxPicture', width: 40, sortable:true,hidden:true },
					{ title: '是否新品', field: 'isnew', width: 30, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}else if(val==true){return "是";}else{return "否";}} },
					{ title: '是否促销', field: 'isTimePromotion', width: 30, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}else if(val==true){return "是";}else{return "否";}} },
					{ title: '是否打折', field: 'sale', width: 30, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}else if(val==true){return "是";}else{return "否";}} },
					{ title: '库存', field: 'stock', width: 40, sortable:true },
					{ title: '检索关键字', field: 'searchKey', width: 50, sortable:true,hidden:true },
					{ title: '浏览量', field: 'hit', width: 40, sortable:true },
					{ title: '销量', field: 'sellcount', width: 40, sortable:true },
					{ title: '积分', field: 'score', width: 40, sortable:true,hidden:true },
					{ title: '商品状态', field: 'status', width: 40, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}else if(val=='99'){return "上架";}else{return "下架";}} },
					{ title: '单位', field: 'unit', width: 30, sortable:true,hidden:true },
					{ title: '所属商户ID', field: 'tenantId', width: 40, sortable:true,hidden:true },
					{ title: '录入时间', field: 'createtime', width: 50, sortable:true,hidden:true },
					{ title: '录入用户', field: 'createAccount', width: 40, sortable:true,hidden:true },
					{ title: '更新时间', field: 'updatetime', width: 50, sortable:true,hidden:true },
					{ title: '更新用户', field: 'updateAccount', width: 40, sortable:true,hidden:true },
					{ title: '所属类别', field: 'catalogId', width: 40, sortable:true,hidden:true }
				]], 
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditDialog();//实现修改记录的方法
                    }
                }, '-', {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                },'-', {
                    id: 'btnPass',
                    text: '上架',
                    iconCls: 'icon-pass',
                    handler: function () {
                        status("pass");//
                    }
                }, {
                    id: 'btnUnPass',
                    text: '下架',
                    iconCls: 'icon-unpass',
                    handler: function () {
                        status("unpass");//
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        function ShowEditDialog(nId){
			var nodeId="-1";
			if(nId==null){
				var node=$('#grid').datagrid("getSelected");
				if(node){
					nodeId=node.id;
				}else{
					msgShow("提示","未选中行.","warning");
					return;
				}
			}else{
				nodeId=nId;
			}
			window.open(basePath+'/Manage/Product/toEdit.do?id='+nodeId+'&t='+new Date(),'编辑商品信息','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
        };
        function ShowAddDialog(nId){
			var nodeId="-1";
			if(nId==null){
				nodeId="${cataId}";
			}else{
				nodeId=nId;
			}
			window.open(basePath+'/Manage/Product/toAdd.do?catalogId='+nodeId+'&t='+new Date(),'添加商品','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
		};
        //下面是用来测试属性配置部分的
        function loadProps(id){
			//alert("d");
			$("#propsPanel",parent.document).attr("src","<%=path %>/Manage/Product/props.do?id="+id+"&t="+new Date());
        };
			
     </script>
	</body>
</html>
