<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="bottom" class="easyui-tabs"  fit="true" border="false" >
			<div title="APP商城管理" style="padding:0;overflow:hidden; color:red; " >
			     <table id="grid" style="width: 900px;height:auto;" title="APP商城信息" iconcls="icon-view">            
           		 </table>
			</div>
		</div>
    </div>
   <div region="east" hide="true" split="false" title="属性配置" style="width:230px;" id="west">
	    <iframe id="propsPanel" onreadystatechange="resize()"  src="<%=path %>/Manage/MallHomePage/props.do?id=-1" scrolling="auto"  frameborder="0" style="width:100%;height:100%;"></iframe>
    </div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//initTabs();
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }
	function reload(){
		$('#grid').datagrid("reload");
	};
	function del(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/MallHomePage/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/MallHomePage/list.do?type=${type }&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: false,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
				
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },
					{ title: 'ID', field: 'id', width: 40, sortable:true },
					{ title: '主题', field: 'title', width: 150, sortable:true },
					{ title: '商品ID', field: 'productId', width: 100, sortable:true },
					{ title: '商品名称', field: 'productName', width: 120, sortable:true },
					{ title: '添加时间', field: 'addDate', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '排序', field: 'order', width: 50, sortable:true }
					]],
				
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                }//,{
                  //  id: 'btnEdit',
                  //  text: '修改',
                   // iconCls: 'icon-edit',
                   // handler: function () {
                   //     ShowEditDialog();//实现修改记录的方法
                   // }
                //}
				,  {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                },'-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
               onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        function ShowEditDialog(nId){
        var editDialog = $('<div id="editNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        var node=$('#grid').datagrid("getSelected");
	        nodeId=node.id;
        }else{
        	nodeId=nId;
        }
        $(editDialog).dialog({
        	modal:true,
        	title:'更新',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editDialog).dialog("close");
	                        $(editDialog).remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(editDialog).dialog("close");
	                     $(editDialog).remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:auto;" src="'+basePath+'/Manage/MallHomePage/toEdit.do?id='+nodeId+'&type='+getType()+'&t='+new Date()+'"></iframe>'
        });
        $(editDialog).dialog("open");
        };
        function ShowAddDialog(nId){
        var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        nodeId="<%=request.getParameter("id") %>";
        }else{
        	nodeId=nId;
        }
        $(addDialog).dialog({
        	modal:true,
        	title:'添加',
        	shadow:true,
        	iconCls:'icon-add',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#addiframe")[0].contentWindow.submit()){
	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(addDialog).dialog("close");
	                     $(addDialog).remove();
                    }
                }],
        	content:'<iframe id="addiframe" width="585px" height="415px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/MallHomePage/toAdd.do?t='+new Date()+'&type='+getType()+'&"></iframe>'
        });
        $(addDialog).dialog("open");
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel").attr("src","<%=path %>/Manage/MallHomePage/props.do?id="+id+"&type="+getType()+"&t="+new Date());
        };
		function getType(){
			var s="${type}";
			if(s=="pop"){
				return "0";
			}else if(s=="jrtj"){
				return "1";
			}else if(s=="rmsc"){
				return "2";
			}else if(s=="hyzm"){
				return "3";
			}
		};
     </script>
	</body>
</html>
