<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>样式模板内容编辑</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		
			
		<style type="text/css">

		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/entity/node.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;">
	
	<div class="easyui-layout" fit="true">
	<div region="north">
		<a id="btn_save" class="easyui-linkbutton" style="margin-left:10px;" data-options="plain:true,iconCls:'icon-ok'">保存模板</a>
		<a id="btn_cancel" class="easyui-linkbutton" style="margin-left:10px;" data-options="plain:true,iconCls:'icon-cancel'">取消并关闭</a>
	</div>
   <div region="center" hide="true" split="false" fit="true" id="center">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="内容编辑">
    	<input type="hidden" mthod="smt" id="pageTemplateId" name="id" value="${pageTemplateId }"/>
    	<input type="hidden" mthod="smt" id="styleTemplateId" name="id" value="${styleTemplateId }"/>
		<textarea id="content" width="100%" height="90%" style="width:100%;height:90%;max-height:90%">${content }</textarea>
    </div>
    <div title="文件管理">
		<iframe id="fileSelectIframe" src=""  frameborder="0" style="width:100%;height:100%;"></iframe>
    </div>
   <div title="标签说明">
    </div>
     </div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(function(){
	$("#btn_save").click(function(){submit();});
	$("#btn_cancel").click(function(){});
	$("#fm").tabs({
		onSelect:function(title){
			if(title=='文件管理'){
				if($("#styleTemplateId").val()=='-1'){
					alert("没有找到该模板的样式模板文件,请先保存并重新编辑该样式模板.");
					return;
				}
				$("#fileSelectIframe").attr("src","<%=basePath %>Manage/FileSelecter/index.do?styleTemplateId=${styleTemplateId }");
			}
		}
	});
	});
	
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	function closeCurWindow(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};
	function submit(){
		$.post(
				path+"/Manage/NodeTemplate/doEditStyle.do",
				{'styleTemplateId':$("#styleTemplateId").val(),'pageTemplateId':$("#pageTemplateId").val(),"content":$("#content").val()},
				function(result){
					var s=eval("("+result+")");
					if(s.status=="99"){
						alert(s.msg);
						 closeCurWindow();
					}else{
						alert(s.msg);
					}
				}
			);
	}
</script>