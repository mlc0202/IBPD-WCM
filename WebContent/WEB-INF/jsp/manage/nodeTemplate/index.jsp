<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>模板管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
<div class="easyui-layout" fit="true">
<div region="north" style="background: #eee; overflow-y:hidden;height:30px">
    <a id="menu_add" data-options="plain:true,iconCls:'icon-add'">添加</a>
    <a href="#" id="menu_edit" data-options="iconCls:'icon-edit'">编辑</a>
    <a href="#" id="menu_del" data-options="iconCls:'icon-del'">删除</a>
    <a href="#" id="menu_style" data-options="iconCls:'icon-edit'">样式信息</a>
	<a href="#" id="menu_reaload" data-options="iconCls:'icon-reload'">刷新</a>
	<div id="submenu_add" style="width:100px;">
       <div id="menu_sub_add"  data-options="iconCls:'icon-add'">手工录入</div>
       <div id="menu_sub_import" data-options="iconCls:'icon-import'">批量导入</div>
    </div>
	<div id="submenu_edit" style="width:100px;">
       <div id="menu_sub_base" data-options="iconCls:'icon-base'">基本信息</div>
       <div id="menu_sub_cont" data-options="iconCls:'icon-file'">模板内容</div>
    </div>
	<div id="submenu_style" style="width:140px;">
		<div id="menu_sub_editStyle" data-options="iconCls:'icon-edit'">编辑当前样式</div>
		<div id="menu_sub_newStyle" data-options="iconCls:'icon-new'">新建样式并应用</div>
		<div id="menu_sub_linkStyle" data-options="iconCls:'icon-exist'">链接已存在样式</div>
    </div>
	</div>
<div region="center" style="background: #eee; overflow-y:hidden">
	 	<table id="table_template" style="width: 900px;height:auto;" title="模板管理" iconcls="icon-view">            
	    </table>
    </div>
	</div>
	<div id="contentCtxMenu" class="easyui-menu" style="width:130px;height:130px;">
	    <div onClick="ShowAddDialog()" data-options="iconCls:'icon-add'">添加</div>
	    <div onClick="ShowEditBaseDialog()" data-options="iconCls:'icon-edit'">编辑</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新</div>
	    <div onClick="export()" data-options="iconCls:'icon-closeNode'">导出</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		InitGrid();//默认没有传参数,参数应该是querydata
		$("#menu_add").menubutton({menu:'#submenu_add'}); 
		$("#menu_edit").menubutton({menu:'#submenu_edit'}); 
		$("#menu_style").menubutton({menu:'#submenu_style'}); 
		$("#menu_del").linkbutton({plain:true,iconCls:'icon-remove'});
		$("#menu_del").click(function(){del();})
		$("#menu_reaload").linkbutton({plain:true,iconCls:'icon-reload'});
		$("#menu_reaload").click(function(){$('#table_template').datagrid("reload");})
		$("#menu_add").click(function(){ShowAddDialog();});
		$("#menu_sub_add").click(function(){ShowAddDialog();});
		$("#menu_sub_import").click(function(){alert("import");});
		$("#menu_edit").click(function(){ShowEditBaseDialog();});
		$("#menu_sub_base").click(function(){ShowEditBaseDialog();});
		$("#menu_sub_cont").click(function(){ShowEditContentDialog();});
		$("#menu_style").click(function(){showCurrentStyle();});
		$("#menu_sub_editStyle").click(function(){showCurrentStyle();});
		$("#menu_sub_newStyle").click(function(){showNewStyle();});
		$("#menu_sub_linkStyle").click(function(){alert("menu_sub_linkStyle");});
	});
	function showCurrentStyle(nId){
	    var templateId="-1";
	    if(nId==null){
	    var node=$('#table_template').datagrid("getSelected");
		   templateId=node.id;
	    }else{
	    	templateId=nId;
	    }
		window.open(basePath+'/Manage/NodeTemplate/toEditStyle.do?pageTemplateId='+templateId+'&t='+new Date(),'编辑样式模板','toolbar=no,menubar=no,resizable=no,location=no,status=no');
	};
	function showNewStyle(nId){
	    var templateId="-1";
	    if(nId==null){
	    var node=$('#table_template').datagrid("getSelected");
		   templateId=node.id;
	    }else{
	    	templateId=nId;
	    }
		window.open(basePath+'/Manage/NodeTemplate/toNewStyle.do?pageTemplateId='+templateId+'&t='+new Date(),'新建样式模板','toolbar=no,menubar=no,resizable=no,location=no,status=no');
	};
	function linkStyle(){
	};
	function ShowEditBaseDialog(){
        var editTempDialog = $('<div id="editDiv"/>').appendTo('body');
	        var templateId="-1";
		    var node=$('#table_template').datagrid("getSelected");
		    templateId=node.id;
        $(editTempDialog).dialog({
        	modal:true,
        	title:'编辑模板',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:550,
        	height:450,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editTempDialog).dialog("close");
	                        $(editTempDialog).remove();
							$("#editiframe").remove();
							$("#editDiv").remove();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
 	                    $(editTempDialog).dialog("close");
	                    $(editTempDialog).remove();
						$("#editiframe").remove();
						$("#editDiv").remove();
                     }
                }],
        	content:'<iframe id="editiframe" width="530px" height="400px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/NodeTemplate/toEditBase.do?pageTemplateId='+templateId+'&t='+new Date()+'"></iframe>'
        });
        $(editTempDialog).dialog("open");
	};
        function ShowAddDialog(){
        var addTempDialog = $('<div id="addDiv"/>').appendTo('body');
        var nodeId="${nodeId }";
        
        $(addTempDialog).dialog({
        	modal:true,
        	title:'添加新模板',
        	shadow:true,
        	iconCls:'icon-add',
        	width:550,
        	height:450,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#addiframe")[0].contentWindow.submit()){
	                        $(addTempDialog).dialog("close");
	                        $(addTempDialog).remove();
							$("#addDiv").remove();
							$("#addiframe").remove();
							setTimeout(function(){$('#table_template').datagrid("reload");},1000);
							
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
 	                        $(addTempDialog).dialog("close");
	                        $(addTempDialog).remove();
							$("#addDiv").remove();
							$("#addiframe").remove();
                    }
                }],
        	content:'<iframe id="addiframe" width="530px" height="400px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/NodeTemplate/toAdd.do?nodeId='+nodeId+'&t='+new Date()+'"></iframe>'
        });
        $(addTempDialog).dialog("open");
        };

	function reload(){
		$('#table_template').datagrid("reload");
	};
	function del(){
		var _sets=$('#table_template').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","请确认没有栏目或内容引用该模板,确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/NodeTemplate/doDelete.do",
					{ids:_ids},
					function(result){
						if(result.indexOf("msg")!=-1){
							var _s=eval("("+result+")");
							if(_s.status=='99'){
								msgShow("提示","删除成功.","warning");
								 $('#table_template').datagrid('clearSelections');
								$('#table_template').datagrid("reload");
							}else{
								msgShow("提示",_s.msg,"error");
							}
						}
						
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div style="height:300px;overflow-y:scroll"/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#table_template').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#table_template').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#table_template').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#table_template').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#table_template').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/NodeTemplate/list.do?nodeId=${nodeId }&t='+new Date(), 
                title: '模板列表',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'desc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#table_template').datagrid('uncheckAll');
                    $('#table_template').datagrid('checkRow', rowIndex);
				    $('#contentCtxMenu').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
   				rowStyler:function(index,row){
   					if(row.isDefault==true){
   					return "background-color:RGB(79,129,189);";
   					}else{
   					}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },   //选择
                     { title: 'ID', field: 'id', width: 60, sortable:true,hidden:true },
					{ title: '模板名称', field: 'title', width: 150, sortable:true },
					{ title: '文件大小', field: 'size', width: 100, sortable:true,formatter:function(val, rowdata, index){return (val/1024)+"KB";}  },
					{ title: '目录大小', field: 'folderSize', width: 120, sortable:true },
					{ title: '所属分组', field: 'group', width: 120, sortable:true},
					{ title: '是否默认', field: 'isDefault', width: 120, sortable:true,formatter:function(val, rowdata, index){if(val=="true" || val=="1"){return "是";}else{return "否";}}},
					{ title: '创建时间', field: 'createDate', width: 150, sortable:true,formatter:function(val, rowdata, index){return (1900+val.year)+"-"+(1+val.month)+"-"+(1+val.date)} },
					{ title: '更新时间', field: 'lastUpdateDate', width: 150, sortable:true,formatter:function(val, rowdata, index){return (1900+val.year)+"-"+(1+val.month)+"-"+(1+val.date)} },
					{ title: '栏目路径', field: 'templateFilePath', width: 120, sortable:true,formatter:function(val, rowdata, index){return val} },
					{ title: '默认UI模板ID', field: 'defaultStyleTemplateId', width: 120, sortable:true,hidden:true },
            		{ title: '默认UI模板', field: 'defaultStyleTemplateName', width: 120, sortable:true },
            		{ title: '使用鼠标右键', field: 'useMouseRightKey', width: 120, sortable:true,formatter:function(val, rowdata, index){if(val==true || val=="true" || val=="1"){return "允许";}else{return "禁止";}}  },
					{ title: '页面内容拷贝', field: 'enableCopyPageContent', width: 120, sortable:true,formatter:function(val, rowdata, index){if(val==true || val=="true" || val=="1"){return "允许";}else{return "禁止";}}  },
					{ title: '灰度页面', field: 'useDarkColor', width: 120, sortable:true,formatter:function(val, rowdata, index){if(val==true || val=="true" || val=="1"){return "是";}else{return "否";}}  },
					{ title: '排序', field: 'order', width: 120, sortable:true},
					{ title: '所属站点ID', field: 'subSiteId',hidden:true, width: 120, sortable:true},
					{ title: '所属站点名称', field: 'subSiteName',hidden:true, width: 120, sortable:true},
					{ title: '所属栏目ID', field: 'nodeId',hidden:true, width: 120, sortable:true},
					{ title: '所属栏目名称', field: 'nodeName',hidden:true, width: 120, sortable:true},
					{ title: '模板类别', field: 'type',hidden:true, width: 120, sortable:true,formatter:function(val, rowdata, index){if(val=="2"){return "站点模板";}else if(val=="1"){return "栏目模板";}else{return "内容模板";}}},
					{ title: '说明', field: 'remark', sortable:true }
			   ]],
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#table_template').datagrid('uncheckAll');
                    $('#table_template').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                }
            })
        };
        function ShowEditContentDialog(nId){
	      
	        var templateId="-1";
	        if(nId==null){
		        var node=$('#table_template').datagrid("getSelected");
		        templateId=node.id;
	        }else{
	        	templateId=nId;
	        }
			window.open(basePath+'/Manage/NodeTemplate/toEditContent.do?pageTemplateId='+templateId+'&t='+new Date(),'编辑模板内容','toolbar=no,menubar=no,resizable=no,location=no,status=no');
        };

        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel",parent.document).attr("src","<%=path %>/Manage/NodeTemplate/props.do?pageTemplateId="+id+"&t="+new Date());
        };
			
		   
        
     </script>
	</body>
</html>
