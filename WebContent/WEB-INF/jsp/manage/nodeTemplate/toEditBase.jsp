<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>添加模板</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:100%;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:30%;
		} 
		.easyui-tabs table tr .val{
			width:60%;
		} 
		.easyui-tabs table tr .val input{
			margin:2 3 2 3;
			width:96%;
		} 
		.easyui-tabs table tr .val select{
			margin:2 3 2 3;
			width:96%;
		} 
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/entity/node.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;"> 
	
	<div class="easyui-panel" fit="true">
    	<input type="hidden" id="templateId" name="templateId" value="${pageTemplate.id }"/>
     	<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="tit"><div class="tittext">模板名称</div></td>
				<td class="val">							
					<span class="int">
						<input type="text" mthod="smt" name="title" value="${pageTemplate.title }"/>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tit"><div class="tittext">排序</div></td>
				<td class="val">							
					<span class="int">
						<input type="text" mthod="smt" name="order" value="${pageTemplate.order }"/>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tit"><div class="tittext">所属分组</div></td>
				<td class="val">							
					<span class="int">
						<input type="text" mthod="smt" name="group" value="${pageTemplate.group }"/>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tit"><div class="tittext">使用鼠标右键</div></td>
				<td class="val">							
					<span class="int">
							<select mthod="smt" name="useMouseRightKey">
								<option value="true" <c:if test="${pageTemplate.useMouseRightKey==true }">selected</c:if>>允许</option>
								<option value="false" <c:if test="${pageTemplate.useMouseRightKey==false }">selected</c:if>>禁止</option>
							</select>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tit"><div class="tittext">页面内容拷贝</div></td>
				<td class="val">							
					<span class="int">
							<select mthod="smt" name="enableCopyPageContent">
								<option value="true" <c:if test="${pageTemplate.enableCopyPageContent==true }">selected</c:if>>允许</option>
								<option value="false" <c:if test="${pageTemplate.enableCopyPageContent==false }">selected</c:if>>禁止</option>
							</select>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tit"><div class="tittext">灰度页面</div></td>
				<td class="val">							
					<span class="int">
							<select mthod="smt" name="useDarkColor">
								<option value="true" <c:if test="${pageTemplate.useDarkColor==true }">selected</c:if>>是</option>
								<option value="false" <c:if test="${pageTemplate.useDarkColor==false }">selected</c:if>>否</option>
							</select>
					</span>
				</td>
			</tr>
		</table>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
	});
	function submit(){
		if($("#templateId").val()==""){
			alert("参数错误");
			return false;
		}
		
		
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			if(_s_tmp[i].tagName=="SELECT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";//.selectedOptions[0].value+"',";
			}else if(_s_tmp[i].tagName=="INPUT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";
			}else{
			}
		} 
		params="[{"+params+"id:'"+$("#templateId").val()+"'}]";
		
		$.post(
				path+"/Manage/NodeTemplate/doEditBase.do",
				eval(params)[0],
				function(result){
					var o=eval("("+result+")");
					if(o.status=="99"){
						return true;
					}else{
						alert(o.msg);
						return false;
					}
				}
			);
		return true;
	}
</script>