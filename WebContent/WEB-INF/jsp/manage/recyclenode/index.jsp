<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
 			     <table id="grid" style="width: 900px;height:auto;" title="栏目回收站管理" iconcls="icon-view">            
           		 </table>
    </div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//initTabs();
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }
	function reload(){
		$('#grid').datagrid("reload");
	};
	function del(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/RecycleNode/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	function restore(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","确定还原吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/RecycleNode/doRestore.do",
					{ids:_ids},
					function(result){
						msgShow("提示","还原成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/RecycleNode/list.do?id=${nodeId}&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
                   { field: 'ck', checkbox: true,title:'选择' },   //选择
                     { title: '栏目名称', field: 'text', width: 120, sortable:true },
                     { title: '类别', field: 'nodeType', width: 30, sortable:true,formatter:function(val, rowdata, index){if(val=='0'){return "普通";}else{return "转向";}} },
                     { title: '链接地址', field: 'linkUrl', width: 100, sortable:true,hidden:true },
                      { title: '栏目分组', field: 'group', width: 80, sortable:true,hidden:true },
                     { title: '状态', field: 'state', width: 60, sortable:true,formatter:function(val, rowdata, index){if(val<=0){return "锁定";}else{return "正常";}} },
                     { title: '统计单位', field: 'subContentCountUnit', width: 60, sortable:true,hidden:true },
                     { title: '搜索关键字', field: 'keywords', width: 80, sortable:true,hidden:true },
                     { title: '创建日期', field: 'createDate', width: 80, sortable:true,hidden:true },
                     { title: '创建者', field: 'createUser', width: 60, sortable:true,hidden:true },
                     { title: '管理员', field: 'manageUser', width: 60, sortable:true,hidden:true },
                     { title: '排序', field: 'order', width: 40, sortable:true,hidden:true },
                     { title: '子节点数量', field: 'childCount', width: 60, sortable:true,hidden:true },
                     { title: '内容数量', field: 'countentCount', width: 60, sortable:true,hidden:true },
                     { title: 'ID', field: 'id', sortable:true,hidden:true }
              ]], 
                toolbar: [
				<c:if test="${node_recycle_restore==true}">				
				{
                    id: 'btnRestore',
                    text: '还原',
                    iconCls: 'icon-reset',
                    handler: function () {
                        restore();//实现直接删除数据的方法
                    }
                },
				</c:if>
				<c:if test="${node_recycle_del==true}">				
				{
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                },'-', 
				</c:if>
				<c:if test="${node_recycle_reload==true}">				
				{
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }
				</c:if>
				],
               onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel",parent.document).attr("src","<%=path %>/Manage/RecycleNode/props.do?id="+id+"&t="+new Date());
        };
			
     </script>
	</body>
</html>
