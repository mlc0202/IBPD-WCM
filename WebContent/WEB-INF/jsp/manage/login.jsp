<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>${pageTitle }</title>
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<style type="text/css">
		body{
			width:100%;
			height:100%;
			padding:0;
			margin:0;
			text-align:center;
			
		}
		#bd{
			position:absolute;
			left:0;
			top:0;
			right:0;
			bottom:0;
			width:100%;
			height:100%;
			z-index:100;
		}
		#loginpanel{
			width:488px;
			height:336px;
			position:absolute;
			z-index:200;
			display:none;
		}
		#loginpanel .top{
			width:488px;
			height:107px;
			float:left;
			background:url('<%=basePath%>images/login/login-top.png') no-repeat;
			z-index:201;
		}
		#loginpanel .middle{
			width:488px;
			height:169px;
			float:left;
			z-index:201;
		}
		#loginpanel .middle .lbar{
			width:3px;
			height:169px;
			float:left;
			background:url('<%=basePath%>images/login/login-lbar.jpg') no-repeat;
		}
		#loginpanel .middle .bg{
			width:479px;
			height:169px;
			float:left;
			background:url('<%=basePath%>images/login/login-mbg.jpg') repeat-x;
		}
		#loginpanel .middle .bg .username_l{
			width:60px;
			height:22px;
			float:left;
			font-size:16px;
			font-weight:bold;
			color:#fffbff;
			margin-left:200px;
			margin-top:20px;
			vertical-align:middle;
		}
		#loginpanel .middle .bg .username_r{
			width:200px;
			float:left;
			font-size:14px;
			color:#000011;
			margin-top:20px;	
		}
		#loginpanel .middle .bg .username_r input{
			border:solid 2px #0b64a0;
			height:22px;
			padding-left:22px;
			background:#ffffff url('<%=basePath%>images/login/username-bg.jpg') no-repeat;
			width:150px;
		}
		#loginpanel .middle .bg .password_l{
			width:60px;
			height:22px;
			float:left;
			font-size:16px;
			font-weight:bold;
			color:#fffbff;
			margin-left:200px;
			margin-top:10px;
			vertical-align:middle;
		}
		#loginpanel .middle .bg .password_r{
			width:200px;
			float:left;
			font-size:14px;
			color:#000011;
			margin-top:10px;	
		}
		#loginpanel .middle .bg .password_r input{
			border:solid 2px #0b64a0;
			height:22px;
			padding-left:22px;
			background:#ffffff url('<%=basePath%>images/login/password-bg.jpg') no-repeat;
			width:150px;
		}
		#loginpanel .middle .bg .password_r select{
			border:solid 2px #0b64a0;
			height:25px;
			padding-left:22px;
			width:180px;
			background:#ffffff url('<%=basePath%>images/login/password-bg.jpg') no-repeat;
		}
		#loginpanel .middle .bg .btns{
			margin-left:200px;
			margin-top:10px;
			padding-left:20px;
			vertical-align:middle;
			float:left;	
			text-align:center;
		}
		#loginpanel .middle .bg .btns .submitbtn{
			width:65px;
			height:28px;
			background:#ffffff url('<%=basePath%>images/login/login-loginbtn.jpg') no-repeat;
			border:none;
		}
		#loginpanel .middle .bg .btns .resetbtn{
			width:65px;
			height:27px;
			background:#ffffff url('<%=basePath%>images/login/login-reset.jpg') no-repeat;
			border:none;
		}
		#loginpanel .middle .rbar{
			width:3px;
			height:169px;
			float:left;
			background:url('<%=basePath%>images/login/login-lbar.jpg') no-repeat;
		}
		#loginpanel .bottom{
			width:488px;
			height:79px;
			float:left;
			background:url('<%=basePath%>images/login/login-bottom.png') no-repeat;
			z-index:201;
		}
        #mask-bg{ display: none;  position: absolute;  top: 0%;  left: 0%;  width: 100%;  height: 100%;  background-color: black;  z-index:1001;  -moz-opacity: 0.7;  opacity:.70;  filter: alpha(opacity=70);}
        #mask-loadding{display: none;  position: absolute;  top: 45%;  left: 40%;  width: 120px;  height: 30px;  font-size:12px;  vertical-align:middle; padding-left:15px; border: 8px solid #E8E9F7;  background-color: white;  z-index:1002;  overflow: auto;}
	</style>
	<body onKeyDown="doLogin(this)">
		<div id="bd">
			<img id="loginbg" src="<%=basePath%>images/login/login-bg.jpg" width="100%" height="100%"/> 	
		</div>
		<div id="loginpanel">
			<div class="top"></div>
			<div class="middle">
				<div class="lbar"></div>
				<div class="bg">
					<div class="username_l">用户名:</div>
					<div class="username_r">
						<input id="uname" type="text"/>
					</div>
					<div class="password_l">密&nbsp;&nbsp;码:</div>
					<div class="password_r">
						<input id="psd" type="password"/>
					</div>
					<div class="password_l">登录组:</div>
					<div class="password_r">
						<select id="ltype">
							<option value="manage">管理员</option>
							<option value="tenant">商户</option>
						</select>
					</div>
					<div class="btns">
						<input id="submitbtn" class="submitbtn" type="button"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input id="resetbtn" class="resetbtn" type="button"/>
					</div>
					
				</div>
				<div class="rbar"></div>
			</div>
			<div class="bottom"></div>
		</div>
<div id="mask-bg"></div>
<div id="mask-loadding" class="datagrid-mask-msg">正在登录,请稍等... ...
</div>
	</body>
	<script type="text/javascript">
	var basePath='<%=path %>';
	$(document).ready(function(){
		$("#submitbtn").click(function(){submit();});
		res(); 
		$("#resetbtn").click(function(){resetForm();});
		$(window).resize(function(){ 
			 res();
		});
		$("#loginpanel").fadeIn(1000);
	});
	var loginEnable=true;
	function doLogin(e){
		var theEvent = window.event || arguments.callee.caller.arguments[0];
		if (theEvent.keyCode==13 && loginEnable)
			submit();
	}
	function res(){
		$("#loginbg").css("height",$(window).height());
		$('#loginpanel').css({top:'50%',left:'50%',margin:'-'+($('#loginpanel').height() / 2)+'px 0 0 -'+($('#loginpanel').width() / 2)+'px'}); 
		$('#loginpanel').css({top:'50%',left:'50%',margin:'-'+($('#loginpanel').height() / 2)+'px 0 0 -'+($('#loginpanel').width() / 2)+'px'});
	};
	function submit(){
		showMask();
		loginEnable=false;
		$.post(
			basePath+"/Manage/User/doLogin.do",
			{userName:$("#uname").val(),password:$("#psd").val(),type:$("#ltype").val()},
			function(result){
				loginEnable=true;
				hideMask();
				if(result.indexOf("msg")!=0){
					var _s=eval("("+result+")");
					if(_s.status>=0){
						location.href=basePath+"/Manage/index.do";
					}else{
						msgShow("error",_s.msg,"error");
					}
				}else{
					msgShow("error","错误","error");
				}
			}
		);
	};
	function resetForm(){
		$("#uname").val("");
		$("#psd").val("");
		$("#ltype").val("manage");
	};
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
function showMask() {        
            document.getElementById("mask-bg").style.display ="block";
            document.getElementById("mask-loadding").style.display ="block";
        }
function hideMask() {
            document.getElementById("mask-bg").style.display ='none';
            document.getElementById("mask-loadding").style.display ='none';
        }
	</script>

</html>
