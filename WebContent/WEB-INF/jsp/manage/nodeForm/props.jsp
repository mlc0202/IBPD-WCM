<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>表单设置</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		.baseInfo{
			width:210px;
			
		}
		.baseInfo tr .tit{
			font-size:10px;
			
			overflow:hidden;
			width:80px;
		}
		.baseInfo tr td{
			padding:2 2 2 2;
			width:100px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr td input {
			width:130px;
		}
		.baseInfo tr td select {
			width:130px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   <div region="east" hide="true" split="false"  style="width:230px;" id="west">
		<div id="propPanel" class="easyui-accordion" fit="true" border="false">
		<input type="hidden" id="nodeFormId" value="${nodeForm.id }"/>
		<input type="hidden" id="isDefault" value="${nodeForm.id }"/>
		<input type="hidden" id="nodeFormFieldId" value="${nodeFormField.id }"/>
		<input type="hidden" id="isEditable" value="${nodeFormField.isEditable }"/>
			<div title="基本信息">
				<table class="baseInfo" cellpadding="0" cellspacing="0">
					<tr>
						<td class="tit">排序</td>
						<td>
							<input type="text" mthod="smt" name="nodeForm_order" value="${nodeForm.order }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">表单名称</td>
						<td>
							<input type="text" mthod="smt" name="nodeForm_formName" value="${nodeForm.formName }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">表单类型</th>
						<td>
							<select mthod="smt" name="nodeForm_formType">
								<option id="0" <c:if test="${nodeForm.formType=='0' }">selected</c:if>>栏目表单</option>
								<option id="1" <c:if test="${nodeForm.formType=='1' }">selected</c:if>>内容表单</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">显示名称</th>
						<td>
							<input type="text" mthod="smt" name="displayName" value="${nodeFormField.displayName }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">说明信息</th>
						<td>
							<input type="text" mthod="smt" name="fieldDescription" value="${nodeFormField.fieldDescription }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">可选值</th>
						<td>
							<input type="text" mthod="smt" name="optionalValue" value="${nodeFormField.optionalValue }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">默认值</th>
						<td>
							<input type="text" mthod="smt" name="defaultValue" value="${nodeFormField.defaultValue }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">是否必填</th>
						<td>
							<select mthod="smt" name="isRequired">
								<option value="1" <c:if test="${nodeFormField.isRequired==true }">selected</c:if>>必填</option>
								<option value="0" <c:if test="${nodeFormField.isRequired==false }">selected</c:if>>选填</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">验证组</th>
						<td>
							<input type="text" mthod="smt" name="expGroup" value="${nodeFormField.expGroup }"/>
						</td>
					</tr>
					<tr>
						<td class="tit">分组</th>
						<td>
							<select mthod="smt" name="group">
								<option id="base" value="base" <c:if test="${nodeFormField.group=='base' }">selected</c:if>>基本信息</option>
								<option id="view" value="view" <c:if test="${nodeFormField.group=='view' }">selected</c:if>>显示设置</option>
								<option id="template" value="template" <c:if test="${nodeFormField.group=='template' }">selected</c:if>>模板设置</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">控件类型</th>
						<td>
							<select mthod="smt" name="tagType">
								<option id="0" value="0" <c:if test="${nodeFormField.tagType==0 }">selected</c:if>>静态文本框</option>
								<option id="1" value="1" <c:if test="${nodeFormField.tagType==1 }">selected</c:if>>动态文本框</option>
								<option id="2" value="2" <c:if test="${nodeFormField.tagType==2 }">selected</c:if>>多行文本框</option>
								<option id="3" value="3" <c:if test="${nodeFormField.tagType==3 }">selected</c:if>>静态下拉框</option>
								<option id="4" value="4" <c:if test="${nodeFormField.tagType==4 }">selected</c:if>>动态下拉框</option>
								<option id="5" value="5" <c:if test="${nodeFormField.tagType==5 }">selected</c:if>>树形下拉框</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">添加状态下控件是否可写</th>
						<td>
							<select mthod="smt" name="writeByAddMode">
								<option id="true" value="true" <c:if test="${nodeFormField.writeByAddMode=='true' }">selected</c:if>>可写</option>
								<option id="false" value="false" <c:if test="${nodeFormField.writeByAddMode=='false' }">selected</c:if>>不可写</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">添加状态下控件是否可见</th>
						<td>
							<select mthod="smt" name="displayByAddMode">
								<option id="true" value="true" <c:if test="${nodeFormField.displayByAddMode=='true' }">selected</c:if>>显示</option>
								<option id="false" value="false" <c:if test="${nodeFormField.displayByAddMode=='false' }">selected</c:if>>隐藏</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">编辑状态下控件是否可写</th>
						<td>
							<select mthod="smt" name="writeByEditMode">
								<option id="true" value="true" <c:if test="${nodeFormField.writeByEditMode=='true' }">selected</c:if>>可写</option>
								<option id="false" value="false" <c:if test="${nodeFormField.writeByEditMode=='false' }">selected</c:if>>不可写</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">编辑状态下控件是否可见</th>
						<td>
							<select mthod="smt" name="displayByEditMode">
								<option id="true" value="true" <c:if test="${nodeFormField.displayByEditMode=='true' }">selected</c:if>>显示</option>
								<option id="false" value="false" <c:if test="${nodeFormField.displayByEditMode=='false' }">selected</c:if>>隐藏</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">辅助控件</th>
						<td>
							<select mthod="smt" name="assistTags">
								<option value="none" <c:if test="${nodeFormField.assistTags=='none' }">selected</c:if>>-不选择-</option>
								<option value="formatter" <c:if test="${nodeFormField.assistTags=='formatter' }">selected</c:if>>格式选取</option>
								<option value="fileSelecter" <c:if test="${nodeFormField.assistTags=='fileSelecter' }">selected</c:if>>文件选取</option>
								<option value="userSelecter" <c:if test="${nodeFormField.assistTags=='userSelecter' }">selected</c:if>>用户选取</option>
								<option value="templateSelecter" <c:if test="${nodeFormField.assistTags=='templateSelecter' }">selected</c:if>>模板选取</option>
								<option value="dateSelecter" <c:if test="${nodeFormField.assistTags=='dateSelecter' }">selected</c:if>>日期选取</option>
								<option value="timeSelecter" <c:if test="${nodeFormField.assistTags=='timeSelecter' }">selected</c:if>>时间选取</option>
								<option value="dateTimeSelecter" <c:if test="${nodeFormField.assistTags=='dateTimeSelecter' }">selected</c:if>>日期时间选取</option>
								<option value="colorSelecter" <c:if test="${nodeFormField.assistTags=='colorSelecter' }">selected</c:if>>颜色选取</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tit">字段排序</th>
						<td>
							<input type="text" mthod="smt" name="order" value="${nodeFormField.order }"/>
						</td>
					</tr>
				</table>
			</div>
				
			</div>
	    </div>
    </div>
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		$("[mthod='smt']").bind("blur",function(e){
			var nodeFormFieldId=$("#nodeFormFieldId").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).children("option:selected").attr("value");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/NodeForm/saveProp.do",
				{fieldId:nodeFormFieldId,field:e.currentTarget.name,value:val},
				function(result){
					var s=eval("("+result+")");
					if(s.status!='99'){
						alert(s.msg);
					}
				}
			);
		});
	});        
     </script>
     
	</body>
</html>
