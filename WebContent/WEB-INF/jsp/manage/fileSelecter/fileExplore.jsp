<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
     
    <title>文件选择</title>
	<link href="${pageContext.request.contextPath}/css/fileSelecter/swfupload-default.css" rel="stylesheet"type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" /> 
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileSelecter/swfupload.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileSelecter/handlers.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>

<script type="text/javascript">
  var nodeId='${nodeId }';
  var pageTemplateId='${pageTemplateId }';
  var styleTemplateId='${styleTemplateId }';
  var tenantId='${tenantId }';
  var filter='${filter }';
  var fileTypeId='${fileTypeId }';
  var fileUploadPath='${fileUploadPath }';
  var contextPath="${pageContext.request.contextPath}";
  var url=contextPath+"/Manage/FileSelecter/doUpload.do"; //处理上传的servlet
  var sizeLimit="1 MB";// 文件的大小  注意: 中间要有空格
  var types="${fileTypes}"; //注意是 " ; " 分割 
  var typesdesc="${fileDesc}"; //这里可以自定义
  var uploadLimit=20;  //上传文件的 个数
	function startLoad(){
		initSwfupload(url,sizeLimit,types,typesdesc,uploadLimit,fileUploadPath);
	};
	startLoad();

</script>
<style type="text/css">
#fileUl{
	width:98%;
	height:98%;
	margin:10 5 10 5;
}
#fileUl ul{

}
#fileUl ul li{
	width:130px;
	height:200px;
	float:left;
	margin:5 5 5 5;
	list-style-type:none;
}
#fileUl ul li .file{
	border: 1px solid #dedede;
    -moz-border-radius: 10px;      /* Gecko browsers */
    -webkit-border-radius: 10px;   /* Webkit browsers */
    border-radius:10px;            /* W3C syntax */
	padding:8px;
	float:left;
}
#fileUl ul li .file .img{
}
#fileUl ul li .selected{
	background-color:#404040;
}
#fileUl ul li .file .img img{
	width:110px;
	height:150px;
	float:left;
}
#fileUl ul li .file .tit{
	width:110px;
	height:20px;
	line-height:200%;
	float:left;
	margin-top:3px;
	overflow:hidden;
}
</style>
  </head>
  
  <body>
  	<div class="easyui-layout" fit="true">
    <div region="center"> 
 		<div id="fileUl">
 			<ul id="fileUI_ul">
 			</ul>
 		</div>
    </div> 
   <div region="south">
	   IBPD文件选择工具 
    </div>
			<div region="north">
				 <div class="easyui-panel" style="padding:5px;">
					<a href="#" class="easyui-linkbutton" onclick="openUploadDialog();" data-options="iconCls:'icon-add',plain:true">添加文件</a>
					<a href="#" class="easyui-linkbutton" onclick="delFile();" data-options="iconCls:'icon-remove',plain:true">删除</a>
					<a href="#" class="easyui-linkbutton" onclick="reload();" data-options="iconCls:'icon-reload',plain:true">刷新</a>
					<a href="#" class="easyui-linkbutton" onclick="copyFile();" data-options="iconCls:'icon-copy',plain:true">复制到</a>
					<a href="#" class="easyui-linkbutton" onclick="moveFile();" data-options="iconCls:'icon-move',plain:true">移动到</a>
					<a href="#" class="easyui-linkbutton" onclick="linkCheck()" data-options="iconCls:'icon-look',plain:true">引用检测</a>
					
				 </div>
			</div>
		</div>
	<div id="uploadBody" class="ke-dialog-upload">
		<div id="uploadHead" class="ke-dialog-upload-head">
		  <div style="float: left">批量图片上传</div><div id="iconClose" class="ke-dialog-icon-close" title="关闭"></div><div style="clear: both"></div>
		</div>
			<!--content-->
		<div class="ke-dialog-content">
			<div class="ke-dialog-content-head">
			  <div id="errorMsg" class="ke-dialog-content-error"></div> 
			  <div id="swfbuttonParent" class="ke-swfupload-button"><span id="buttonPlaceholder"></span></div>
			  <div class="ke-inline-block ke-swfupload-desc">允许用户同时上传&nbsp;<font color="red" id="uploadLimit"></font>&nbsp;张图片，单张图片容量不超过<font color="red" id="sizeLimit"></font></div>
				<div class="ke-button-common ke-button-outer ke-swfupload-startupload"><input id="btnUpload" type="button" class="ke-button-common ke-button" value="开始上传"></div>
				<div style="clear: both;"></div> 
		 	</div>
			<div id="photoContainer" class="ke-swfupload-body">
			</div>
		</div>
			<!--bottom-->
		<div class="ke-dialog-footer">
			<span class="ke-button-common ke-button-outer" title="关闭窗口"><input id="btnfilecancel" class="ke-button-common ke-button" type="button" value="关闭窗口"></span>
		</div>
	</div>
	<div id="contextmenu" class="easyui-menu" style="width:120px;">
        <div class="ctxmenu-select">选择</div>
        <div class="menu-sep"></div>
        <div class="ctxmenu-move">移动到...</div>
        <div class="ctxmenu-copy">复制到...</div>
        <div class="ctxmenu-del">删除</div>
        <div class="menu-sep"></div>
        <div class="ctxmenu-link">引用检测</div>
  	</div>
	<div id="fileTip"><div>文件信息:<br />文件名:XXXXXXXXX<br/>大小:10043KB<br/>上传日期:2014-09-10 12:04:23</div></div>
  </body>
  <script type="text/javascript">
  $(document).ready(function(){
	$("#uploadFile").bind("click",function(){openUploadDialog();});
	loadFileList();
	$("#uploadLimit").text(uploadLimit);
	$("#sizeLimit").text(sizeLimit);
	$(".ctxmenu-select").click(function(e){selectFile(e);});
	$(".ctxmenu-move").click(function(e){moveFile(e);});
	$(".ctxmenu-copy").click(function(e){copyFile(e);});
	$(".ctxmenu-link").click(function(e){linkCheck(e);});
	$(".ctxmenu-del").click(function(e){delFile(e);});
 	$("#btnfilecancel").bind("click",function(){iconClose();});
 });
 
  </script>
</html>
