<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>服务器文件选择器</title>
	<link href="${pageContext.request.contextPath}/css/fileSelecter/swfupload-default.css" rel="stylesheet"type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileSelecter/swfupload.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileSelecter/handlers.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>

<script type="text/javascript">
var nodeId='${nodeId }';
var pageTemplateId='${pageTemplateId }';
var styleTemplateId='${styleTemplateId }';
var tenantId='${tenantId }';
var filter='${filter }';
$(document).ready(function(){
	loadFileTypes();
});
function loadFileTypes(){
	$.post(
		'<%=path %>/Manage/FileType/list.do',
		{},
		function(result){
			if(result.length>0){
				var s=eval("["+result+"]");
				//s[0].rows[0].fileTypeName
				$("#fileTypes").html("");
				for(i=0;i<s[0].rows.length;i++){
					$("#fileTypes").html($("#fileTypes").html()+'<li><span><img src="'+s[0].rows[i].fileTypeIcon+'"/></span><span class="tree-title"><a href="#" target="_self" class="fileTypeLink" fileTypeId="'+s[0].rows[i].id+'">'+s[0].rows[i].fileTypeName+'</a></span><input type="hidden" class="fileTypeId" value='+s[0].rows[i].id+'></li>');
				}
				bindFileTypeEvent();
			}
		}
	); 
};
function bindFileTypeEvent(){
	$(".fileTypeLink").click(function(e){
		$("#fileContent").attr("src","<%=basePath %>Manage/FileSelecter/fileExplore.do?filter="+filter+"&nodeId="+nodeId+"&pageTemplateId="+pageTemplateId+"&styleTemplateId="+styleTemplateId+"&tenantId="+tenantId+"&fileTypeId="+$(e.target).attr("fileTypeId"));
	});
};
function submit(s,f){
	
	//parent.changeFile(s,f);
	window.opener.changeFile(s,f);
};
</script>
	<style type="text/css">
	.left-ul{}
	.left-ul li{
		list-style-type:none;
		margin-top:5px;
	}
	</style>
  </head>
  
  <body>
  	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden" title="文件浏览" href="">
 		<iframe id="fileContent" src="<%=basePath %>Manage/FileSelecter/fileExplore.do?filter=${filter }&nodeId=${nodeId }&pageTemplateId=${pageTemplateId }&styleTemplateId=${styleTemplateId }&tenantId=${tenantId }" width="100%" height="100%"  frameborder="0"></iframe>
    </div>
   <div region="west" hide="true" split="false" title="文件类型" style="width:230px;" id="west">
		<div class="easyui-accordion" fit="true">
			<div title="按类型" data-options="iconCls:'icon-ok',tools:[{iconCls:'icon-reload',handler:function(){alert('reload');}}]">
			  <ul class="left-ul" id="fileTypes">
				<li><span><img src="<%=path %>/images/icon.jpg"/></span><span class="tree-title">图片类型</span></li>
				<li>Excel文档</li>
				<li>word文档</li>
				<li>PPT演示文稿</li>
				<li>PDF文档</li>
				<li>流媒体文件</li>
				<li>视频文件</li>
				<li>音频文件</li>
				<li>其他文件</li>
			  </ul>
			</div>
			<div title="按日期" data-options="iconCls:'icon-help'">
			  <ul class="left-ul">
				<li><span><img src="<%=path %>/images/icon.jpg"/></span><span class="tree-title">图片类型</span></li>
				<li>今天</li>
				<li>昨天</li>
				<li>本周</li>
				<li>上周</li>
				<li>本月</li>
				<li>上月</li>
				<li>具体日期</li>
				<li>所有文件</li>
			  </ul>
			</div>
			</div>
	   </div>
    </div>
	</div>
	
  </body>
</html>
