<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="bottom" class="easyui-tabs"  fit="true" border="false" >
			<div title="商户管理" style="padding:0;overflow:hidden; color:red; " >
			     <table id="grid" style="width: 900px;height:auto;" title="商户信息" iconcls="icon-view">            
           		 </table>
			</div>
		</div>
    </div>
   <div region="east" hide="true" split="false" title="属性配置" style="width:230px;" id="west">
	    <iframe id="propsPanel" onreadystatechange="resize()"  src="<%=path %>/Manage/Tenant/props.do?id=-1" scrolling="auto"  frameborder="0" style="width:100%;height:100%;"></iframe>
    </div>
	</div>

	<div id="nodeCtxMenu" class="easyui-menu" style="width:130px;">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑该栏目</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除该栏目</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新栏目列表</div>
	    <div onClick="publish_node()" data-options="iconCls:'icon-publish-node'">发布该栏目首页</div>
	    <div onClick="publish_content()" data-options="iconCls:'icon-publish-content'">发布该栏目内容</div>
	    <div onClick="publish_tree()" data-options="iconCls:'icon-publish-tree'">增量发布</div>
	    <div onClick="closeNode()" data-options="iconCls:'icon-closeNode'">开启/关闭该栏目</div>
	    <div onClick="hiddenToNav()" data-options="iconCls:'icon-hiddenToNav'">导航显示显示/隐藏</div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//initTabs();
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }

	function hiddenToNav(){
		var _setNode=$('#grid').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Tenant/navi.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","执行成功.","warning");
						reload();
					}
				);

	};
	//not used function
	function status(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
			$.post(
				basePath+"/Manage/Tenant/status.do",
				{ids:_ids},
				function(result){
					msgShow("提示","执行成功.","warning");
					reload();
				}
			);
		}
	};
	function reload(){
		$('#grid').datagrid("reload");
	};
	function del(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","如果商户下已经有商品,删除分类将会同时将该分类下商品全部删除,确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/Tenant/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Tenant/list.do?type=${type }&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
				<c:if test="${type=='ent'}">
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },
					{ title: 'ID', field: 'id', width: 40, sortable:true,hidden:true },
					{ title: '企业全称', field: 'entFullName', width: 100, sortable:true },
					{ title: '商户介绍', field: 'intro', width: 120, sortable:true,hidden:true },
					{ title: '商户名称', field: 'tenantName', width: 100, sortable:true},
					{ title: '营业执照', field: 'licenceFilePath', width: 40, sortable:true,hidden:true },
					{ title: '工商注册登记号', field: 'licenceCode', width: 80, sortable:true },
					{ title: '组织机构代码', field: 'orgCode', width: 80, sortable:true},
					{ title: '营业执照起始日期', field: 'licenceStartDate', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '营业执照截止日期', field: 'licenceEndDate', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '公司成立时间', field: 'foundingTime', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '注册地址', field: 'regAddress', width: 80, sortable:true,hidden:true },
					{ title: '企业法人', field: 'legalPerson', width: 40, sortable:true },
					{ title: '注册资金', field: 'regCapital', width: 30, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return (val/10000)+"万"} },
					{ title: '经营范围', field: 'scopeOfBusiness', width: 40, sortable:true,hidden:true },
					{ title: '官方网站', field: 'website', width: 40, sortable:true,hidden:true },
					{ title: '开户行', field: 'bankName', width: 40, sortable:true,hidden:true },
					{ title: '对公账户', field: 'bankCardCode', width: 80, sortable:true,hidden:true },
					{ title: '商户类型', field: 'tenantType', width: 40, sortable:true },
					{ title: '运营方式', field: 'manageType', width: 40, sortable:true },
					{ title: '商铺管理人', field: 'managerName', width: 40, sortable:true },
					{ title: '管理者EMail', field: 'managerEmail', width: 80, sortable:true,hidden:true },
					{ title: '商户负责人手机', field: 'managerTel', width: 80, sortable:true,hidden:true },
					{ title: '仓库情况', field: 'warehouse', width: 40, sortable:true,hidden:true },
					{ title: '商铺运营人数', field: 'employeesCount', width: 40, sortable:true,hidden:true },
					{ title: 'ERP情况', field: 'erpType', width: 40, sortable:true,hidden:true },
					{ title: '状态', field: 'status', width: 40, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}if(val==99){return "审核"}else{return "待审"}} },
					{ title: '公司联系地址', field: 'contactAddress', width: 40, sortable:true,hidden:true }
					]],
				</c:if>
				<c:if test="${type=='sing'}">
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },
					{ title: 'ID', field: 'id', width: 40, sortable:true,hidden:true },
					{ title: '商户全称', field: 'entFullName', width: 100, sortable:true },
					{ title: '商户名称', field: 'tenantName', width: 100, sortable:true},
					{ title: '营业执照', field: 'licenceFilePath', width: 40, sortable:true,hidden:true },
					{ title: '工商注册登记号', field: 'licenceCode', width: 80, sortable:true },
					{ title: '营业执照起始日期', field: 'licenceStartDate', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '营业执照截止日期', field: 'licenceEndDate', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '成立时间', field: 'foundingTime', width: 80, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==null){return ""}return val.year+1900+"-"+val.month+"-"+val.date} },
					{ title: '注册地址', field: 'regAddress', width: 80, sortable:true,hidden:true },
					{ title: '联系人', field: 'legalPerson', width: 40, sortable:true },
					{ title: '经营范围', field: 'scopeOfBusiness', width: 40, sortable:true,hidden:true },
					{ title: '开户行', field: 'bankName', width: 40, sortable:true,hidden:true },
					{ title: '对公账户', field: 'bankCardCode', width: 80, sortable:true,hidden:true },
					{ title: '运营方式', field: 'manageType', width: 40, sortable:true },
					{ title: '管理者EMail', field: 'managerEmail', width: 80, sortable:true,hidden:true },
					{ title: '商户负责人手机', field: 'managerTel', width: 80, sortable:true,hidden:true },
					{ title: '仓库情况', field: 'warehouse', width: 40, sortable:true,hidden:true },
					{ title: '商铺运营人数', field: 'employeesCount', width: 40, sortable:true,hidden:true },
					{ title: 'ERP情况', field: 'erpType', width: 40, sortable:true,hidden:true },
					{ title: '状态', field: 'status', width: 40, sortable:true,formatter:function(val, rowdata, index){if(val==null){return ""}if(val==99){return "审核"}else{return "待审"}} },
					{ title: '联系地址', field: 'contactAddress', width: 40, sortable:true,hidden:true }
					]],
				</c:if>
                toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditDialog();//实现修改记录的方法
                    }
                }, '-', {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                },'-', {
                    id: 'btnPass',
                    text: '审核',
                    iconCls: 'icon-pass',
                    handler: function () {
                        status();//实现直接删除数据的方法
                    }
                }, {
                    id: 'btnPass',
                    text: '设置用户',
                    iconCls: 'icon-user',
                    handler: function () {
                        setTenantUser();
                    }
                },{
                    id: 'btnView',
                    text: '查看证件',
                    iconCls: 'icon-view',
                    handler: function () {
                        viewLicence();
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
               onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
		function viewLicence(){
			var editDialog = $('<div id="initUserDiv"/>').appendTo('body');
			var nodeId="-1";
			var node=$('#grid').datagrid("getSelected");
			nodeId=node.id;
			$(editDialog).dialog({
				modal:true,
				title:'查看证件',
				shadow:true,
				iconCls:'icon-edit',
				width:600,
				height:500,
				resizable:true,
				toolbar:[{
						text:'查看完毕',
						iconCls:'icon-save',
						handler:function(){
							if($("#editiframe")[0].contentWindow.submit()){
								$(editDialog).dialog("close");
								$(editDialog).remove();
								reload();
							}else{
							}
						}
					}],
				content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Tenant/viewLicenceImage.do?id='+nodeId+'&t='+new Date()+'"></iframe>'
			});
			$(editDialog).dialog("open");
		};
		function setTenantUser(){
			var editDialog = $('<div id="initUserDiv"/>').appendTo('body');
			var nodeId="-1";
			var node=$('#grid').datagrid("getSelected");
			nodeId=node.id;
			$(editDialog).dialog({
				modal:true,
				title:'初始用户',
				shadow:true,
				iconCls:'icon-edit',
				width:600,
				height:500,
				resizable:true,
				toolbar:[{
						text:'保存',
						iconCls:'icon-save',
						handler:function(){
							if($("#editiframe")[0].contentWindow.submit()){
								$(editDialog).dialog("close");
								$(editDialog).remove();
								reload();
							}else{
							}
						}
					},'-',{
						text:'取消',
						iconCls:'icon-cancel',
						handler:function(){
							 $(editDialog).dialog("close");
							 $(editDialog).remove();
						}
					}],
				content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Tenant/initUser.do?id='+nodeId+'&t='+new Date()+'"></iframe>'
			});
			$(editDialog).dialog("open");
		};
        function ShowEditDialog(nId){
        var editDialog = $('<div id="editNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        var node=$('#grid').datagrid("getSelected");
	        nodeId=node.id;
        }else{
        	nodeId=nId;
        }
        $(editDialog).dialog({
        	modal:true,
        	title:'更新',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editDialog).dialog("close");
	                        $(editDialog).remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(editDialog).dialog("close");
	                     $(editDialog).remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:auto;" src="'+basePath+'/Manage/Tenant/toEdit.do?id='+nodeId+'&type=${type }&t='+new Date()+'"></iframe>'
        });
        $(editDialog).dialog("open");
        };
        function ShowAddDialog(nId){
        var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        nodeId="<%=request.getParameter("id") %>";
        }else{
        	nodeId=nId;
        }
        $(addDialog).dialog({
        	modal:true,
        	title:'添加',
        	shadow:true,
        	iconCls:'icon-add',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#addiframe")[0].contentWindow.submit()){
	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(addDialog).dialog("close");
	                     $(addDialog).remove();
                    }
                }],
        	content:'<iframe id="addiframe" width="585px" height="415px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Tenant/toAdd.do?t='+new Date()+'&type=${type }&"></iframe>'
        });
        $(addDialog).dialog("open");
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel").attr("src","<%=path %>/Manage/Tenant/props.do?id="+id+"&type=${type }&&t="+new Date());
        };
			
     </script>
	</body>
</html>
