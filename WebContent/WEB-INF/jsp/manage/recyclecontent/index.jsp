<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
 			     <table id="grid" style="width: 900px;height:auto;" title="内容回收站管理" iconcls="icon-view">            
           		 </table>
    </div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		//initTabs();
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }
	function reload(){
		$('#grid').datagrid("reload");
	};
	function del(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/RecycleContent/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	function restore(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.length==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","确定还原吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/RecycleContent/doRestore.do",
					{ids:_ids},
					function(result){
						msgShow("提示","还原成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/RecycleContent/list.do?id=${nodeId}&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
               columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },   //选择
                     { title: 'ID', field: 'id', width: 120, sortable:true },
					{ title: '标题', field: 'title', width: 120, sortable:true },
					{ title: '所属栏目', field: 'nodeId', width: 120, sortable:true },
					{ title: '状态', field: 'state', width: 120, sortable:true,formatter:function(val, rowdata, index){if(val==-1){return "锁定";}else{return "正常";}} },
					{ title: '分组', field: 'group', width: 120, sortable:true,hidden:true },
					{ title: '栏目ids', field: 'nodeIdPath', width: 120, sortable:true,hidden:true },
					{ title: '是否有缩略图', field: 'existPreview', width: 120, sortable:true,hidden:true },
					{ title: '预览地址', field: 'previewUrl', width: 120, sortable:true,hidden:true },
					{ title: '简介', field: 'description', width: 120, sortable:true,hidden:true },
					{ title: '文章关键字', field: 'textKeywords', width: 120, sortable:true,hidden:true },
					{ title: '相关关键字', field: 'aboutKeyword', width: 120, sortable:true,hidden:true },
					{ title: '录入员', field: 'enteringUser', width: 120, sortable:true,hidden:true },
					{ title: '创建时间', field: 'createDate', width: 120, sortable:true,formatter:function(val, rowdata, index){return 1900+val.year+"-"+val.month+1+"-"+val.date+" "+val.hours+":"+val.minutes+":"+val.seconds+" 星期"+val.day} },
					{ title: '最后更新', field: 'lastUpdateDate', width: 120, sortable:true,hidden:true },
					{ title: '状态信息', field: 'stateMessage', width: 120, sortable:true,hidden:true },
					{ title: '审核员', field: 'passedUser', width: 120, sortable:true,hidden:true },
					{ title: '审核时间', field: 'passedTime', width: 120, sortable:true,hidden:true },
					{ title: '是否推荐', field: 'isRecommend', width: 120, sortable:true,hidden:true },
					{ title: '好评数', field: 'goodCount', width: 120, sortable:true,hidden:true },
					{ title: '中评数', field: 'normalCount', width: 120, sortable:true,hidden:true },
					{ title: '差评数', field: 'wrongCount', width: 120, sortable:true,hidden:true },
					{ title: '评论数量', field: 'commentCount', width: 120, sortable:true,hidden:true },
					{ title: '来源', field: 'source', width: 120, sortable:true },
					{ title: '附件url', field: 'url', width: 120, sortable:true,hidden:true },
					{ title: '附件类型', field: 'urlType', width: 120, sortable:true,hidden:true },
					{ title: '附件大小', field: 'urlSize', width: 120, sortable:true,hidden:true },
					{ title: '作者', field: 'author', width: 120, sortable:true },
					{ title: '是否固顶', field: 'isTop', width: 120, sortable:true,hidden:true },
					{ title: '是否精华', field: 'isParson', width: 120, sortable:true,hidden:true },
					{ title: '排序', field: 'order', width: 120, sortable:true},
					{ title: '浏览量', field: 'hits', width: 120, sortable:true },
					{ title: '录入着IP地址', field: 'ip', width: 120, sortable:true,hidden:true },
					{ title: '自定义1', field: 'custom1', width: 120, sortable:true,hidden:true },
					{ title: '自定义2', field: 'custom2', width: 120, sortable:true,hidden:true },
					{ title: '自定义3', field: 'custom3', width: 120, sortable:true,hidden:true },
					{ title: '自定义4', field: 'custom4', width: 120, sortable:true,hidden:true },
					{ title: '自定义5', field: 'custom5', width: 120, sortable:true,hidden:true },
					{ title: '自定义6', field: 'custom6', width: 120, sortable:true,hidden:true },
					{ title: '自定义7', field: 'custom7', width: 120, sortable:true,hidden:true },
					{ title: '自定义8', field: 'custom8', width: 120, sortable:true,hidden:true },
					{ title: '自定义9', field: 'custom9', width: 120, sortable:true,hidden:true },
					{ title: '自定义10', field: 'custom10', width: 120, sortable:true,hidden:true },
					{ title: '自定义11', field: 'custom11', width: 120, sortable:true,hidden:true },
					{ title: '自定义12', field: 'custom12', width: 120, sortable:true,hidden:true },
					{ title: '自定义13', field: 'custom13', width: 120, sortable:true,hidden:true },
					{ title: '自定义14', field: 'custom14', width: 120, sortable:true,hidden:true },
					{ title: '自定义15', field: 'custom15', width: 120, sortable:true,hidden:true },
					{ title: '自定义16', field: 'custom16', width: 120, sortable:true,hidden:true },
					{ title: '自定义17', field: 'custom17', width: 120, sortable:true,hidden:true },
					{ title: '自定义18', field: 'custom18', width: 120, sortable:true,hidden:true },
					{ title: '自定义19', field: 'custom19', width: 120, sortable:true,hidden:true },
					{ title: '自定义20', field: 'custom20', width: 120, sortable:true,hidden:true }
               ]], 
                toolbar: [
				<c:if test="${cont_recycle_restore==true}">				
				{
                    id: 'btnRestore',
                    text: '还原',
                    iconCls: 'icon-reset',
                    handler: function () {
                        restore();//实现直接删除数据的方法
                    }
                },
				</c:if>
				<c:if test="${cont_recycle_del==true}">				
				{
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                },'-', 
				</c:if>
				<c:if test="${cont_recycle_reload==true}">				
				{
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }
				</c:if>
				],
               onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel",parent.document).attr("src","<%=path %>/Manage/RecycleContent/props.do?id="+id+"&t="+new Date());
        };
			
     </script>
	</body>
</html>
