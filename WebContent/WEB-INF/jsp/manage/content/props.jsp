<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>属性配置</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		.baseInfo{
			width:210px;
			
		}
		.baseInfo tr .tit{
			font-size:10px;
			
			overflow:hidden;
			width:60px;
		}
		.baseInfo tr td{
			padding:2 2 2 2;
			width:100px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr .val .int input {
			width:86px;
		}
		.baseInfo tr .val .int select {
			width:86px;
		}
		.baseInfo tr .val .int textarea {
			width:86px;
		}
		.baseInfo tr .val .int{
			float:left;
			
		}
		.baseInfo tr .val .assist{
			float:left;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   <div region="east" hide="true" split="false"  style="overflow:hidden;width:210px;" id="west">
		<div id="propPanel" class="easyui-accordion" fit="true" border="false" style="width:210px;left:0">
		<input type="hidden" id="contentId" value="${entity.id }"/>
			<div title="基本信息" style="overflow-y:auto;overflow-x:hidden;">
				<table class="baseInfo" cellpadding="0" cellspacing="0">
				<c:forEach items="${fields }" var="field">
					<tr <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
						<td class="tit"<c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val"><!--fieldName,displayName,fieldDescription,expGroup,optionalValue,defaultValue,tagType,isEditable,writeByAddMode,displayByAddMode,writeByEditMode,displayByEditMode,assistTags-->
							
							<c:if test="${field.tagType=='0' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${entity[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByEditMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>${entity[field.fieldName] }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${entity[field.fieldName]==v[0] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${entity[field.fieldName]==v[0] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${entity[field.fieldName]==v[0] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }">
							<span class="assist">
							</c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="checkbox" name="${field.fieldName }_fileAppend" style="width:20px"/>追加
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..." style="width:30px"/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }">
							</span>
							</c:if>
						</td>
						
					</tr>
				</c:forEach>
				</table>			
			</div>
	    </div>
    </div>
	</div>
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		bindEvents();
		<c:if test="${propSaveEnable==true}">
		$("[mthod='smt']").bind("blur",function(e){
			var contentId=$("#contentId").val();
			if(e.currentTarget.tagName=="SELECT"){
				//var val=$(e.currentTarget).children("option:selected").attr("id");
				var val=$(e.currentTarget).val();
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/Content/saveProp.do",
				{id:contentId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
		</c:if>
	});        
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){selectFile(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	var curImageId="";
	function selectFile(e){
		curImageId=$(e.target).attr("name").split("_")[0];
		window.open(path+'/Manage/FileSelecter/index.do?nodeId=${entity.nodeId }&filter='+curImageId,'文件上传','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
	};
	function changeFile(s,f){
		if(s!=""){
			s=s.substring(0,s.length-1);
		}
		if($("input[name='"+f+"_fileAppend']:checked").length>0){
			$("input[name='"+f+"']").val($("input[name='"+f+"']").val()+";"+s);
		}else{
			$("input[name='"+f+"']").val(s);
		}
		//$("input[name='"+f+"']").val(s);
		$("input[name='"+f+"']").focus();
	};
     </script>
     
	</body>
</html>
