<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>内容管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
    <div id="contentview" class="easyui-panel" fit="true" style="background: #eee; overflow-y:hidden">
	 	<table id="table_content" style="width: 900px;height:auto;" title="内容管理" iconcls="icon-view">            
	    </table>
    </div>

	<div id="contentCtxMenu" class="easyui-menu" style="width:130px;">
		<c:if test="${permissionEnable_contEdit==true}">
	    <div onClick="ShowEditDialog()" data-options="iconCls:'icon-edit'">编辑</div>
		</c:if>
		<c:if test="${permissionEnable_contDel==true}">
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除</div>
		</c:if>
		<c:if test="${permissionEnable_contReload==true}">
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新</div>
		</c:if>
		<c:if test="${permissionEnable_contMove==true}">
	    <div onClick="move()" data-options="iconCls:'icon-publish-tree'">移动位置</div>
		</c:if>
		<c:if test="${permissionEnable_contLink==true}">
	    <div onClick="link()" data-options="iconCls:'icon-publish-tree'">引用到</div>
		</c:if>
		<c:if test="${permissionEnable_contCopy==true}">
	    <div onClick="copy()" data-options="iconCls:'icon-publish-tree'">复制到</div>
		</c:if>
		<c:if test="${permissionEnable_contLock==true}">
	    <div onClick="closeNode()" data-options="iconCls:'icon-closeNode'">锁定/解锁</div>
		</c:if>
		<c:if test="${permissionEnable_contTop==true}">
	    <div onClick="top()" data-options="iconCls:'icon-closeNode'">置顶设置</div>
		</c:if>
		<c:if test="${permissionEnable_contRecommend==true}">
	    <div onClick="recommend()" data-options="iconCls:'icon-closeNode'">推荐设置</div>
		</c:if>
		<c:if test="${permissionEnable_contExport==true}">
	    <div onClick="export()" data-options="iconCls:'icon-closeNode'">导出</div>
		</c:if>
		<c:if test="${permissionEnable_contPublish==true}">
	    <div onClick="publish()" data-options="iconCls:'icon-submit'">发布</div>
		</c:if>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		InitGrid();//默认没有传参数,参数应该是querydata
	});
	function closeNode(){
		alert("尚未开发");
	};
	function reload(){
		$('#table_content').datagrid("reload");
	};
	function del(){
		var _sets=$('#table_content').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","删除后内容将暂时被移动到回收站,确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/Content/recv.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						$('#table_content').datagrid("reload");
					}
				);
			}
		});
		}
	};
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	function publish(){
var _sets=$('#table_content').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
			$.post(
			basePath+"/Manage/Content/publish.do",
			{ids:_ids},
			function(result){
				msgShow("提示","发布成功.","warning");
				$('#table_content').datagrid("reload");
			});
		}
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div style="height:300px;overflow-y:scroll"/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#table_content').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#table_content').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#table_content').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#table_content').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#table_content').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Content/list.do?nodeId=<%=request.getParameter("nodeId") %>&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'desc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#table_content').datagrid('uncheckAll');
                    $('#table_content').datagrid('checkRow', rowIndex);
				    $('#contentCtxMenu').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
   				rowStyler:function(index,row){
   					if(row.state<=0){
   					return "background-color:RGB(79,129,189);";
   					}else{
   					}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },   //选择
                     { title: 'ID', field: 'id', width: 120, sortable:true },
					{ title: '标题', field: 'title', width: 120, sortable:true },
					{ title: '所属栏目', field: 'nodeId', width: 120, sortable:true },
					{ title: '状态', field: 'state', width: 120, sortable:true,formatter:function(val, rowdata, index){if(val==-1){return "锁定";}else{return "正常";}} },
					{ title: '分组', field: 'group', width: 120, sortable:true,hidden:true },
					{ title: '栏目ids', field: 'nodeIdPath', width: 120, sortable:true,hidden:true },
					{ title: '是否有缩略图', field: 'existPreview', width: 120, sortable:true,hidden:true },
					{ title: '预览地址', field: 'previewUrl', width: 120, sortable:true,hidden:true },
					{ title: '简介', field: 'description', width: 120, sortable:true,hidden:true },
					{ title: '文章关键字', field: 'textKeywords', width: 120, sortable:true,hidden:true },
					{ title: '相关关键字', field: 'aboutKeyword', width: 120, sortable:true,hidden:true },
					{ title: '录入员', field: 'enteringUser', width: 120, sortable:true,hidden:true },
					{ title: '创建时间', field: 'createDate', width: 120, sortable:true,formatter:function(val, rowdata, index){return 1900+val.year+"-"+val.month+1+"-"+val.date+" "+val.hours+":"+val.minutes+":"+val.seconds+" 星期"+val.day} },
					{ title: '最后更新', field: 'lastUpdateDate', width: 120, sortable:true,hidden:true },
					{ title: '状态信息', field: 'stateMessage', width: 120, sortable:true,hidden:true },
					{ title: '审核员', field: 'passedUser', width: 120, sortable:true,hidden:true },
					{ title: '审核时间', field: 'passedTime', width: 120, sortable:true,hidden:true },
					{ title: '是否推荐', field: 'isRecommend', width: 120, sortable:true,hidden:true },
					{ title: '好评数', field: 'goodCount', width: 120, sortable:true,hidden:true },
					{ title: '中评数', field: 'normalCount', width: 120, sortable:true,hidden:true },
					{ title: '差评数', field: 'wrongCount', width: 120, sortable:true,hidden:true },
					{ title: '评论数量', field: 'commentCount', width: 120, sortable:true,hidden:true },
					{ title: '来源', field: 'source', width: 120, sortable:true },
					{ title: '附件url', field: 'url', width: 120, sortable:true,hidden:true },
					{ title: '附件类型', field: 'urlType', width: 120, sortable:true,hidden:true },
					{ title: '附件大小', field: 'urlSize', width: 120, sortable:true,hidden:true },
					{ title: '作者', field: 'author', width: 120, sortable:true },
					{ title: '是否固顶', field: 'isTop', width: 120, sortable:true,hidden:true },
					{ title: '是否精华', field: 'isParson', width: 120, sortable:true,hidden:true },
					{ title: '排序', field: 'order', width: 120, sortable:true},
					{ title: '浏览量', field: 'hits', width: 120, sortable:true },
					{ title: '录入着IP地址', field: 'ip', width: 120, sortable:true,hidden:true },
					{ title: '自定义1', field: 'custom1', width: 120, sortable:true,hidden:true },
					{ title: '自定义2', field: 'custom2', width: 120, sortable:true,hidden:true },
					{ title: '自定义3', field: 'custom3', width: 120, sortable:true,hidden:true },
					{ title: '自定义4', field: 'custom4', width: 120, sortable:true,hidden:true },
					{ title: '自定义5', field: 'custom5', width: 120, sortable:true,hidden:true },
					{ title: '自定义6', field: 'custom6', width: 120, sortable:true,hidden:true },
					{ title: '自定义7', field: 'custom7', width: 120, sortable:true,hidden:true },
					{ title: '自定义8', field: 'custom8', width: 120, sortable:true,hidden:true },
					{ title: '自定义9', field: 'custom9', width: 120, sortable:true,hidden:true },
					{ title: '自定义10', field: 'custom10', width: 120, sortable:true,hidden:true },
					{ title: '自定义11', field: 'custom11', width: 120, sortable:true,hidden:true },
					{ title: '自定义12', field: 'custom12', width: 120, sortable:true,hidden:true },
					{ title: '自定义13', field: 'custom13', width: 120, sortable:true,hidden:true },
					{ title: '自定义14', field: 'custom14', width: 120, sortable:true,hidden:true },
					{ title: '自定义15', field: 'custom15', width: 120, sortable:true,hidden:true },
					{ title: '自定义16', field: 'custom16', width: 120, sortable:true,hidden:true },
					{ title: '自定义17', field: 'custom17', width: 120, sortable:true,hidden:true },
					{ title: '自定义18', field: 'custom18', width: 120, sortable:true,hidden:true },
					{ title: '自定义19', field: 'custom19', width: 120, sortable:true,hidden:true },
					{ title: '自定义20', field: 'custom20', width: 120, sortable:true,hidden:true }
               ]], 
                toolbar: [
				<c:if test="${permissionEnable_contAdd==true}">
				{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contImport==true}">
				{
                    id: 'btnImport',
                    text: '导入',
                    iconCls: 'icon-import',
                    
                    handler: function () {
                        importContent();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contEdit==true}">
				{
                    id: 'btnEdit',
                    text: '修改', 
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditDialog();//实现修改记录的方法
                    }
                }, '-', 
				</c:if>
				<c:if test="${permissionEnable_contDel==true}">
				{
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                }, '-', 
				</c:if>
				<c:if test="${permissionEnable_contReload==true}">
				{
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contCopy==true}">
				{
                    id: 'btnCopy',
                    text: '复制到', 
                    iconCls: 'icon-copy',
                    handler: function () {
                        copy();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contMove==true}">
				{
                    id: 'btnMove',
                    text: '移动到',
                    iconCls: 'icon-move',
                    handler: function () {
                        move();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contLink==true}">
				{
                    id: 'btnLink',
                    text: '分发到',
                    iconCls: 'icon-link',
                    handler: function () {
                        link();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contPublish==true}">
				{
                    id: 'btnLink',
                    text: '发布',
                    iconCls: 'icon-submit',
                    handler: function () {
                        publish();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_contExport==true}">
				{
                    id: 'btnExport',
                    text: '导出',
                    iconCls: 'icon-export',
                    handler: function () {
                        exportContent();
                    }
                }, '-', 
				</c:if>
				<c:if test="${permissionEnable_contSearch==true}">
				{
                    id: 'search_text',
                    text: '输入全部或部分名称:<input type="text" id="searchText" style="width:100px;"/>', 
                    
                    handler: function () {
                       // initDefaultField();
                    }
                }
				</c:if>
				],
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#table_content').datagrid('uncheckAll');
                    $('#table_content').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#table_content').datagrid('uncheckAll');
                    //$('#table_content').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        function ShowEditDialog(nId){
	      
	        var contentId="-1";
	        if(nId==null){
		        var node=$('#table_content').datagrid("getSelected");
		        contentId=node.id;
	        }else{
	        	contentId=nId;
	        }
			window.open(basePath+'/Manage/Content/toEdit.do?contentId='+contentId+'&t='+new Date(),'编辑文档','toolbar=no,menubar=no,resizable=no,location=no,status=no');
        };
        function ShowAddDialog(nId){
	        var nodeId="-1";
	        if(nId==null){
		        nodeId='<%=request.getParameter("nodeId") %>';
	        }else{
	        	nodeId=nId;
	        }
	        //var addLink = $('<a id="addLink" href="'+basePath+'/Manage/Content/toAdd.do?nodeId='+nodeId+'&t='+new Date()+'" target="_blank">a</a>').appendTo('body');
	        //$("#addLink").click();
			if(nodeId.indexOf("site_")!=-1){
				msgShow("提示","请选择栏目","error");
				return;
			}
      		window.open(basePath+'/Manage/Content/toAdd.do?nodeId='+nodeId+'&t='+new Date(),'添加文档','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel",parent.document).attr("src","<%=path %>/Manage/Content/props.do?id="+id+"&t="+new Date());
        };
			
		   
        
     </script>
	</body>
</html>
