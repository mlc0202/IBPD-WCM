<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String id=request.getParameter("id");
	String pName="nodeId";
	if(id==null){
	id=request.getParameter("siteId");
	pName="siteId";
	}
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="bottom" class="easyui-tabs"  fit="true" border="false" >
			<div title="子栏目" style="padding:0;overflow:hidden; color:red; " >
			     <table id="table_subNode" style="width: 900px;height:auto;" title="子栏目信息" iconcls="icon-view">            
           		 </table>
			</div>
			<c:if test="${permissionEnable_contDisp==true}">
			<div title="栏目内容" style="overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:if test="${permissionEnable_tempDisp==true}">
			<div title="栏目模板" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:set value="${fn:indexOf(nodeId, '_')}" var="idx" />
			<c:if test="${idx==-1 }">
			<c:if test="${permissionEnable_nodeFormDisp==true}">
			<div title="栏目表单" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:if test="${permissionEnable_contFormDisp==true}">
			<div title="内容表单" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			</c:if>
			<c:if test="${permissionEnable_nodeRecycleDisp==true}">
			<div title="栏目回收站" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:if test="${permissionEnable_contRecycleDisp==true}">
			<div title="内容回收站" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:if test="${permissionEnable_keyDisp==true}">
			<div title="关键字" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:if test="${permissionEnable_acDisp==true}">
			<div title="访问控制" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
			<c:if test="${permissionEnable_tagDisp==true}">
			<div title="自定义标签" style="padding:5px;overflow:hidden; color:red; " >
			</div>
			</c:if>
		</div>
    </div>
   <div region="east" hide="true" split="false" title="属性配置" style="width:230px;" id="west" style="overflow-y:auto;">
	    <iframe id="propsPanel"  src="<%=path %>/Manage/Node/props.do?id=<%=request.getParameter("id") %>&rnd="+new Date() scrolling="no"  frameborder="0" style="width:100%;height:98%;"></iframe>
    </div>
	</div>

	<div id="nodeCtxMenu" class="easyui-menu" style="width:130px;">
		<c:if test="${permissionEnable_nodeEdit==true}">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑该栏目</div>
		</c:if>
		<c:if test="${permissionEnable_nodeDel==true}">
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除该栏目</div>
		</c:if>
		<c:if test="${permissionEnable_nodeReload==true}">
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新栏目列表</div>
		</c:if>
		<c:if test="${permissionEnable_nodeMove==true}">
	    <div onClick="move()" data-options="iconCls:'icon-move'">移动该栏目</div>
		</c:if>
		<c:if test="${permissionEnable_nodeOrder==true}">
	    <div onClick="orderNode()" data-options="iconCls:'icon-order'">改变排序</div>
		</c:if>
		<c:if test="${permissionEnable_nodePubThis==true}">
	    <div onClick="publish_node()" data-options="iconCls:'icon-publish-node'">发布该栏目首页</div>
		</c:if>
		<c:if test="${permissionEnable_nodePubCnt==true}">
	    <div onClick="publish_content()" data-options="iconCls:'icon-publish-content'">发布该栏目内容</div>
		</c:if>
		<c:if test="${permissionEnable_nodePubNew==true}">
	    <div onClick="publish_tree()" data-options="iconCls:'icon-publish-tree'">增量发布</div>
		</c:if>
		<c:if test="${permissionEnable_nodeOpenClose==true}">
	    <div onClick="closeNode()" data-options="iconCls:'icon-closeNode'">开启/关闭该栏目</div>
		</c:if>
		<c:if test="${permissionEnable_nodeNaviHidden==true}">
	    <div onClick="hiddenToNav()" data-options="iconCls:'icon-hiddenToNav'">导航显示/隐藏</div>
		</c:if>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
	//alert("${permissionEnable_tempDisp}");
		initTabs();
		InitNodeGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
    function publish_node(){
	var _setNode=$('#table_subNode').datagrid("getSelected");
	$.post(
					basePath+"/Manage/Node/publishNode.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","发布请求已经提交到后台.","info");
					}
				);
	}
    function publish_content(){
	var _setNode=$('#table_subNode').datagrid("getSelected");
	$.post(
					basePath+"/Manage/Node/publishContent.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","发布请求已经提交到后台.","info");
					}
				);
	}
    function publish_tree(){
	var _setNode=$('#table_subNode').datagrid("getSelected");
	$.post(
					basePath+"/Manage/Node/publishTree.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","发布请求已经提交到后台.","info");
					}
				);
	}
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
	//	alert(outHeight+" "+inHeight);
    }
	function move(){
		$.messager.alert("into","尚未开发","info");
	};
	function orderNode(){
		move();
	};
	function initTabs(){
	$("#tabs").tabs({
		onSelect:function(title){
			var url='';
			var nodeFormIframe='<iframe src="<%=path %>/Manage/NodeForm/index.do?nodeId=<%=request.getParameter("id") %>&type=node" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var contentFormIframe='<iframe src="<%=path %>/Manage/NodeForm/index.do?nodeId=<%=request.getParameter("id") %>&type=content" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var nodeTemplate='<iframe src="<%=path %>/Manage/NodeTemplate/index.do?id=<%=request.getParameter("id") %>" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var nodeRecv='<iframe src="<%=path %>/Manage/RecycleNode/index.do?nodeId=<%=request.getParameter("id") %>" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var contentRecv='<iframe src="<%=path %>/Manage/RecycleContent/index.do?nodeId=<%=request.getParameter("id") %>" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var keyword='';
			var content='<iframe src="<%=path %>/Manage/Content/index.do?nodeId=<%=request.getParameter("id") %>" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var assest='';
			var customTag='';
			if(title=='栏目表单'){
				url=nodeFormIframe;
			}else if(title=='内容表单'){
				url=contentFormIframe;
			}else if(title=='栏目内容'){
				url=content;
			}else if(title=='栏目模板'){
				url=nodeTemplate;
			}else if(title=='栏目回收站'){
				url=nodeRecv;
			}else if(title=='内容回收站'){
				url=contentRecv;
			}else if(title=='关键字'){
				url=keyword;
			}else if(title=='访问控制'){
				url=assest;
			}else if(title=='自定义标签'){
				url=customTag;
			}
			var tab = $('#tabs').tabs('getSelected');
			$("#tabs").tabs('update',{  
				tab:tab,  
				options:{  
					title:title,  
					//style:{padding:'0 0 0 0'},  
					//href:URL, // 使用href会导致页面加载两次，所以使用content代替  
					content:url,  
					closable:false,  
					fit:true,  
					selected:true  
				}
			});
		}
	});
	};
	function hiddenToNav(){
		var _setNode=$('#table_subNode').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Node/navi.do",
					{id:_setNode.id},
					function(result){
						//msgShow("提示","锁定成功.","warning");
						$('#table_subNode').datagrid("reload");
					}
				);

	};
	function closeNode(){
		var _setNode=$('#table_subNode').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Node/state.do",
					{id:_setNode.id},
					function(result){
						//msgShow("提示","锁定成功.","warning");
						$('#table_subNode').datagrid("reload");
					}
				);
	};
	function reload(){
		$('#table_subNode').datagrid("reload");
	};
	function del(){
		var _sets=$('#table_subNode').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","删除后该栏目将暂时被移动到回收站,以及栏目下的子栏目、内容等数据将被置为删除状态,确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/Node/recv.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						$('#table_subNode').datagrid("reload");
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#table_subNode').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#table_subNode').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#table_subNode').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#table_subNode').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitNodeGrid(queryData) {
            $('#table_subNode').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Node/list.do?id=<%=request.getParameter("id") %>&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			$('#table_subNode').datagrid('uncheckAll');
                    $('#table_subNode').datagrid('checkRow', rowIndex);
				    $('#nodeCtxMenu').menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
   				rowStyler:function(index,row){
   					if(row.state<=0){
   					return "background-color:RGB(79,129,189);";
   					}else{
   					}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },   //选择
                     { title: '栏目名称', field: 'text', width: 120, sortable:true },
                     { title: '类别', field: 'nodeType', width: 30, sortable:true,formatter:function(val, rowdata, index){if(val=='0'){return "普通";}else{return "转向";}} },
                     { title: '链接地址', field: 'linkUrl', width: 100, sortable:true,hidden:true },
                      { title: '栏目分组', field: 'group', width: 80, sortable:true,hidden:true },
                     { title: '状态', field: 'state', width: 60, sortable:true,formatter:function(val, rowdata, index){if(val<=0){return "锁定";}else{return "正常";}} },
                     { title: '统计单位', field: 'subContentCountUnit', width: 60, sortable:true,hidden:true },
                     { title: '搜索关键字', field: 'keywords', width: 80, sortable:true,hidden:true },
                     { title: '创建日期', field: 'createDate', width: 80, sortable:true,hidden:true },
                     { title: '创建者', field: 'createUser', width: 60, sortable:true,hidden:true },
                     { title: '管理员', field: 'manageUser', width: 60, sortable:true,hidden:true },
                     { title: '排序', field: 'order', width: 40, sortable:true,hidden:true },
                     { title: '子节点数量', field: 'childCount', width: 60, sortable:true,hidden:true },
                     { title: '内容数量', field: 'countentCount', width: 60, sortable:true,hidden:true },
                     { title: 'ID', field: 'id', sortable:true,hidden:true }
               ]], 
                toolbar: [
				<c:if test="${permissionEnable_nodeAdd==true}">
				{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_nodeEdit==true}">
				{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditDialog();//实现修改记录的方法
                    }
                }, '-', 
				</c:if>
				<c:if test="${permissionEnable_nodeDel==true}">
				{
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                }, '-', 
				</c:if>
				<c:if test="${permissionEnable_nodeReload==true}">
				{
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_nodeMove==true}">				
				{
                    id: 'btnReload',
                    text: '移动',
                    iconCls: 'icon-move',
                    handler: function () {
                        move();
                    }
                },
				</c:if>
				<c:if test="${permissionEnable_nodeOrder==true}">				
				{
                    id: 'btnReload',
                    text: '排序',
                    iconCls: 'icon-order',
                    handler: function () {
                        orderNode();
                    }
                }, '-',
				</c:if>
				<c:if test="${permissionEnable_nodeSearch==true}">				
				{
                    id: 'search_text',
                    text: '输入全部或部分栏目名称:<input type="text" id="searchNodeText" style="width:100px;"/>', 
                    
                    handler: function () {
                       // initDefaultField();
                    }
                }
				</c:if>
				],
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#table_subNode').datagrid('uncheckAll');
                    $('#table_subNode').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#table_subNode').datagrid('uncheckAll');
                    //$('#table_subNode').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        function ShowEditDialog(nId){
        var addDialog = $('<div id="editNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        var node=$('#table_subNode').datagrid("getSelected");
	        nodeId=node.id;
        }else{
        	nodeId=nId;
        }
        $(addDialog).dialog({
        	modal:true,
        	title:'更新栏目',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
							$("#editiframe").remove();
							$("#editNodeDiv").remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                        $(addDialog).dialog("close");
	                    $(addDialog).remove();
						$("#editiframe").remove();
						$("#editNodeDiv").remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Node/toEdit.do?id='+nodeId+'&t='+Math.ceil(Math.random()*999999)+'"></iframe>'
        });
        $(addDialog).dialog("open");
        };
        function ShowAddDialog(nId){
        var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        nodeId="<%=request.getParameter("id") %>";
        }else{
        	nodeId=nId;
        }
        $(addDialog).dialog({
        	modal:true,
        	title:'添加新栏目',
        	shadow:true,
        	iconCls:'icon-add',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#addiframe")[0].contentWindow.submit()){
	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
							$("#addiframe").remove();
							$("#addNodeDiv").remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
 	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
 							$("#addiframe").remove();
							$("#addNodeDiv").remove();
                  }
                }],
        	content:'<iframe id="addiframe" width="585px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Node/toAdd.do?parentId='+nodeId+'&t='+new Date()+'"></iframe>'
        });
        $(addDialog).dialog("open");
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
		$("#propsPanel").attr("src","");
        $("#propsPanel").attr("src","<%=path %>/Manage/Node/props.do?id="+id+"&t="+new Date());
        };
			
		function showFormatterDialog(e){
			$.messager.alert('^_^','尚未开发','error');
			var s=$(e.target).attr("name");
			var ss=s.split("_");
			e.view.setValue(ss[0],'设置值');
		};
		var fileSelectSource=null;
		function showFileSelecterDialog(e,nodeId){
			fileSelectSource=e;
			var fileSelectDialog = $('<div id="fileSelectDiv"/>').appendTo('body');
			$(fileSelectDialog).dialog({
				modal:true,
				title:'选择文件',
				shadow:true,
				iconCls:'icon-select',
				width:900,
				height:600,
				resizable:true,
				toolbar:[{
						text:'确定',
						iconCls:'icon-ok',
						handler:function(){
							if($("#fileSelectDialog")[0].contentWindow.submit()){
								$(fileSelectDialog).dialog("close");
								$(fileSelectDialog).remove();
							}else{
							}
						}
					},'-',{
						text:'取消',
						iconCls:'icon-cancel',
						handler:function(){
							alert('add')
						}
					}],
				content:'<iframe id="fileSelectDialog" width="865px" height="550px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/FileSelecter/index.do?nodeId='+nodeId+'&t='+new Date()+'"></iframe>'
			});
			$(fileSelectDialog).dialog("open");
		};
		function showUserSelecterDialog(e){
			showFormatterDialog(e);
		};
		function showTemplateSelecter(e){
			showFormatterDialog(e);
		};
        function changeFile(s,f){
			var _id=$(fileSelectSource.target).attr("name").split("_")[0];
			$("#propsPanel").contents().find("[name='"+_id+"']").val(s);
			$("#propsPanel").contents().find("[name='"+_id+"']").focus();
			$("#fileSelectDiv").dialog("close");
			$("#fileSelectDiv").remove();
		};
     </script>
	</body>
</html>
