<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>站点管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		body{
			overflow-y:auto;
			margin:0;
			padding:0;
		}
		.baseInfo{
			
			
		}
		.baseInfo tr .tit .tittext{
			font-size:10px;
			overflow:hidden;
			text-overflow:clip;
			width:60px;
		}
		.baseInfo tr .val{
			padding:2 2 2 2;
			width:110px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr .val input{
			width:80px;
		}
		.baseInfo tr .val  select{
			width:80px;
		}
		.baseInfo tr .val textarea{
			width:80px;
		}
		.baseInfo tr .val .assist {
			width:20px;
		}
		.baseInfo tr .val .assist input {
			width:20px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
	<div style="height:98%;width:100%;overflow-y:auto;">
		<input type="hidden" id="siteId" value="${site.id }"/>
			
				<table class="baseInfo" cellpadding="0" cellspacing="0">
					<tr>
						<tH class="tit"><div class="tittext">站点名称</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="siteName" value="${site.siteName }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">站点全称</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="fullName" value="${site.fullName }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">管理员</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="manageUser" value="${site.manageUser }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">域名</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="domainName" value="${site.domainName }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">站点开始生效时间</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="startDate" value="${site.startDate }"/>
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">站点关闭时间</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="endDate" value="${site.endDate }"/>
							</span>
						</td>
					<tr>
						<th class="tit"><div class="tittext">描述</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="description" value="${site.description }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">搜索关键字</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="keywords" value="${site.keywords }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">排序</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="order" value="${site.order }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">站点页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="sitePageTemplateId">
								<option value="-1" <c:if test="${site.sitePageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${sitePageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.sitePageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">站点样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="siteStyleTemplateId">
								<option value="-1" <c:if test="${site.siteStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${siteStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.siteStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">栏目页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="nodePageTemplateId">
								<option value="-1" <c:if test="${site.nodePageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${nodePageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.nodePageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">栏目样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="nodeStyleTemplateId">
								<option value="-1" <c:if test="${site.nodeStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${nodeStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.nodeStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">内容页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="contentpageTemplateId">
								<option value="-1" <c:if test="${site.contentpageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${contentpageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.contentpageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">内容样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="contentStyleTemplateId">
								<option value="-1" <c:if test="${site.contentStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${contentStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.contentStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">评论页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="commentpageTemplateId">
								<option value="-1" <c:if test="${site.commentpageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${commentpageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.commentpageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">评论样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="commentStyleTemplateId">
								<option value="-1" <c:if test="${site.commentStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${commentStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.commentStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">背景音乐</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="bgMusic" value="${site.bgMusic }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">logo标志</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="logo" value="${site.logo }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">图标</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="icon" value="${site.icon }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">横幅图片</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="banner" value="${site.banner }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">版权信息</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="copyright" value="${site.copyright }"/>
							</span>
						</td>
					</tr>
				</table>
</div>
	
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		bindEvents();
		$("[mthod='smt']").bind("blur",function(e){
			var siteId=$("#siteId").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).children("option:selected").attr("value");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/SubSite/saveProp.do",
				{id:siteId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
	});   
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){parent.showFileSelecterDialog(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*上级页面调用这个函数来做到设置本页 tag值的效果   参数：name为本页tag的name值，val为具体值,具体调用效果可参阅node的index.jsp页面
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
     </script>
     
	</body>
</html>
