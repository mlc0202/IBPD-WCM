<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/ibpdTag" prefix="mt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>${pageTitle }</title>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="${keywords }">
	<meta http-equiv="copyright" content="${copyright }">
	<meta http-equiv="description" content="${description }">
	
	<link rel="stylesheet" type="text/css" href="${cssPath }">
        <script type="text/javascript" src="${styleTemplatePath}jquery-1.4.4.min.js"></script>
<script src="${styleTemplatePath}ui.tab.js"></script>
<script src="${styleTemplatePath}mobile_ad.js" type="text/javascript"></script>  </head>
<script type="text/javascript">
$(document).ready(function(){
var dts=$("#dateArea");
now = new Date(),hour = now.getHours();
if(hour < 6){dts.html("凌晨好!");} 
else if (hour < 9){dts.html("早上好！");} 
else if (hour < 12){dts.html("上午好！");} 
else if (hour < 14){dts.html("中午好！");} 
else if (hour < 17){dts.html("下午好！");} 
else if (hour < 19){dts.html("傍晚好！");} 
else if (hour < 22){dts.html("晚上好！");} 
else {dts.html("夜里好!")}
dts.append("今天是 <font color='#5c5c5c'>");
var dayNames = new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
Stamp = new Date();
dts.append("" + (1900+Stamp.getYear()) + "年"+(Stamp.getMonth() + 1) +"月"+Stamp.getDate()+ "日"+ " " + dayNames[Stamp.getDay()] +"");});

</script>
<body>

<div class="wrapper">
	<div class="bgred">
		<div class="topimg"><img src="${styleTemplatePath}/banner.jpg" alt="" /></div>
	</div>
	<div class="bgyellow">
		<div class="bgnav" style="overflow:hidden;">
			<ul class="nav">
                           <mt:nodeEach nodeId="-1" pageSize="20"> 
				<li><a href="<mt:node value="${node}" field="SHORTURL"/>" target="_blank"><mt:node valueVar="node" field="text"></mt:node></a></li>
			</mt:nodeEach>
			</ul>
		</div>
	</div>
</div>
    <div class="bla5"></div>
<div class="container">
	<div class="container_1">
		
		<div class="dase">
			<div class="date">
				<div id="dateArea">

				</div>
			</div>
			<div class="weather">
				<iframe width="300" scrolling="no" height="25" frameborder="0" allowtransparency="true" src="http://i.tianqi.com/index.php?c=code&id=10&bgc=%23FFFFFF&icon=1"></iframe>
			</div>
			<div class="search">
				<form action="http://www.baidu.com/baidu" target="_blank">
					<table bgcolor="#FFFFFF">
						<tr>
							<td>
								<input name=tn type=hidden value=baidu>
								<input style="height:20px; width:188px; border:1px solid #dedede;" type=text name=word size=30>
								<input style="height:20px; width:50px; font-size:13px;border:1px solid #dedede;" type="submit" value="搜索">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<div class="bla8"></div>
		<div class="topnews">
			<div class="topline"><a href="<mt:node nodeId="109" field="SHORTURL"/>"><img src="${styleTemplatePath}/tt.jpg" alt="" /></a></div>
			<div class="topnew">
				<ul class="allnew">
					<mt:contentEach nodeId="109" itemCount="1">
					<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<dl>
<dd><mt:content valueVar="content" field="description"/><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank">[详细]</a></dd>
</dl>
</li>
</mt:contentEach>
				</ul>
			</div>
		</div>
		<div class="bla8"></div>
		<div id="box1">
			<div class="upbox">
				<div class="leftbox">
					<!--焦点图开始-->
					<div class="ccw_focus_area">
						<div id="ccw_focusImage" class="ccw_focusImage">
							<div class="ccw_focusImage-inner">
								<div class="ccw_focusImage-content">
									<ul id="ccw_focusImage-content" class="js-content">    
										<mt:contentEach nodeId="110" itemCount="6">
										<li class="ccw_current">
                                        <a target="_blank" href="<mt:content valueVar="content" field="SHORTURL"/>"><img src="<mt:content valueVar="content" field="previewUrl"/>" border="0"></a>
<div class="ccw_focusImage-title">
  <div class="ccw_focusImage-title-bg"></div>
  <h2><a target="_blank" href="<mt:content valueVar="content" field="SHORTURL"/>"><mt:content valueVar="content" field="title"/></a></h2>
</div>

                                        </li>
										</mt:contentEach>
										
									</ul>
								</div>
								<div class="ccw_focusImage-slide">
									<ul id="ccw_focusImage-tab" class="js-tab">
										<li class="ccw_current">1</li>
										<li>2</li>
										<li>3</li>
										<li>4</li>
										<li>5</li>
										<li>6</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!--焦点图结束-->
				</div>
				<div class="rightbox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="88" field="SHORTURL"/>"><mt:node nodeId="88" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="88" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla10"></div>
					<div class="news1">
						<ul class="f14px_B Sred lh30px">
						<mt:contentEach nodeId="88" itemCount="1" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						</ul>
						<div class="bla2"></div>
						<ul class="com_list_1">
							<mt:contentEach nodeId="88" itemCount="8" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						</ul>
					</div>
					<div class="bla10"></div>
					<div class="bla2"></div>
					<div>    <div id="scrollDiv3" style="width:306px; height:72px; line-height:72px; overflow:hidden;">
             <ul>     
              <li><a href="http://pingan.hebei.com.cn/sfgk/" target="_blank"><img src="http://pingan.hebei.com.cn/images/caw_pic_20140617_03.jpg" width="306" height="72" alt="" /></a></li>
                    <li><a href="http://pingan.hebei.com.cn/zfdjt/" target="_blank"><img src="http://pic.hebei.com.cn/0/10/52/78/10527874_383561.jpg" width="306" height="72" border="0"/></a>
               </li>
             </ul>
</div>	
<script type="text/javascript">
function AutoScroll(objg){
        $(objg).find("ul:first").animate({
                marginTop:"-72px"
        },1500,function(){
                $(this).css({marginTop:"0px"}).find("li:first").appendTo(this);
        });
}
$(document).ready(function(){
setInterval('AutoScroll("#scrollDiv3")',3000)
});
</script>


</div>
				</div>
			</div>
			<div class="bla10"></div>
			<div class="downbox">
				<div class="threebox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="89" field="SHORTURL"/>"><mt:node nodeId="89" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="89" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news2 com_list_1">
					<mt:contentEach nodeId="89" itemCount="6" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						
					</ul>
				</div>
				<div class="threebox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="90" field="SHORTURL"/>"><mt:node nodeId="90" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="90" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news2 com_list_1">
					<mt:contentEach nodeId="90" itemCount="6" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						
					</ul>
				</div>
				<div class="threebox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="91" field="SHORTURL"/>"><mt:node nodeId="91" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="91" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news2 com_list_1">
					<mt:contentEach nodeId="91" itemCount="6" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						
					</ul>
				</div>
			</div>
		</div>
		<div class="bla10"></div>
		<div style="width:980px; height:80px;float:left;overflow:hidden;">
<div id="scrollDiv980">
    <ul>
      <li><a href="http://pingan.hebei.com.cn/system/2015/04/20/010834970.shtml
" target="_blank"><img src="http://pic.hebei.com.cn/0/10/52/91/10529192_957736.jpg" width="980" height="80" border="0"></a></li>
      <li><a href="http://pingan.hebei.com.cn/system/2015/05/13/010848499.shtml" target="_blank"><img src="http://pic.hebei.com.cn/0/10/53/76/10537600_692180.jpg" width="980" height="80" border="0"></a></li>
    </ul>
  </div>
</div>
<script type="text/javascript" src="http://www.hebei.com.cn/images_new/topScroll980px.js"></script>


<!--

<div><a href="http://www.chinapeace.gov.cn/node_53448.htm"><img src="http://pic.hebei.com.cn/0/10/50/75/10507533_406647.jpg" alt="" /></a></div>

-->

		<div class="bla10"></div>
		<div id="box2">
			<div class="name2" style="background: rgba(0, 0, 0, 0) url('${styleTemplatePath}jz.jpg') repeat scroll 0 0;"></div>
			<div class="leftbox">
				<div class="fourbox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="94" field="SHORTURL"/>"><mt:node nodeId="94" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="94" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news3 com_list_2">
						<mt:contentEach nodeId="94" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
					</ul>
				</div>
				<div class="fourbox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="95" field="SHORTURL"/>"><mt:node nodeId="95" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="95" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news3 com_list_2">
						<mt:contentEach nodeId="95" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
					</ul>
				</div>
				<div class="bla8"></div>
				<div class="fourbox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="96" field="SHORTURL"/>"><mt:node nodeId="96" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="96" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news3 com_list_2">
						<mt:contentEach nodeId="96" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
					</ul>
				</div>
				<div class="fourbox">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="97" field="SHORTURL"/>"><mt:node nodeId="97" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="97" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="news3 com_list_2">
						<mt:contentEach nodeId="97" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
					</ul>
				</div>
				<div class="bla8"></div>
				<div class="fourbox">
					<div id="z1link_1" style="display: block">
						<div class="qh_box">
							<div class="qh_left">
								<div class="f14px_B Sred lk"><a href="<mt:node nodeId="98" field="SHORTURL"/>" target="_blank" onMouseOver="javascript:SetMenuz1(1)"><mt:node nodeId="98" field="text"/></a></div>
								<div class="qh_k f14px Sred b">|</div>
								<div class="f14px Sred jy_bg"><a href="<mt:node nodeId="99" field="SHORTURL"/>" onMouseOver="javascript:SetMenuz1(2)" target="_blank"><mt:node nodeId="99" field="text"/></a></div>
								<div class="qh_more"><a href="<mt:node nodeId="98" field="SHORTURL"/>">更多>></a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla10"></div>
						<div class="content">
							<ul class="news3 com_list_2">
		<mt:contentEach nodeId="98" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
											</ul>
						</div>
					</div>
					<div id="z1link_2" style="display: none">
						<div class="qh_box">
							<div class="qh_left">
								<div class="f14px Sred lk"><a href="<mt:node nodeId="98" field="SHORTURL"/>" onMouseOver="javascript:SetMenuz1(1)" target="_blank"><mt:node nodeId="98" field="text"/></a></div>
								<div class="qh_k f14px_B Sred">|</div>
								<div class="f14px_B Sred jy_bg"><a href="<mt:node nodeId="99" field="SHORTURL"/>" onMouseOver="javascript:SetMenuz1(2)" target="_blank"><mt:node nodeId="99" field="text"/></a></div>
								<div class="qh_more"><a href="<mt:node nodeId="99" field="SHORTURL"/>">更多>></a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla10"></div>
						<div class="content">
							<ul class="news3 com_list_2">
										<mt:contentEach nodeId="99" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
				
							</ul>
						</div>
					</div>
					<div class="clear"></div>
					<script type="text/javascript">     
						function SetMenuz1(theid){              
							if(theid){
							for (i = 1; i< 3; i++) {
								document.getElementById("z1link_"+i).style.display="none";
								if(i==theid){
								document.getElementById("z1link_"+i).style.display="block";     
									}       
								}
							}
						}
					</script>
				</div>
				<div class="fourbox">
					<div id="z2link_1" style="display: block">
						<div class="qh_box">
							<div class="qh_left">
								<div class="f14px_B Sred lk"><a href="<mt:node nodeId="100" field="SHORTURL"/>" target="_blank" onMouseOver="javascript:SetMenuz2(1)"><mt:node nodeId="100" field="text"/></a></div>
								<div class="qh_k f14px Sred b">|</div>
								<div class="f14px Sred jy_bg"><a href="<mt:node nodeId="101" field="SHORTURL"/>" onMouseOver="javascript:SetMenuz2(2)" target="_blank"><mt:node nodeId="101" field="text"/></a></div>
								<div class="qh_more"><a href="<mt:node nodeId="100" field="SHORTURL"/>">更多>></a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla10"></div>
						<div class="content">
							<ul class="news3 com_list_2">
							<mt:contentEach nodeId="100" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
										</ul>
						</div>
					</div>
					<div id="z2link_2" style="display: none">
						<div class="qh_box">
							<div class="qh_left">
								<div class="f14px Sred lk"><a href="<mt:node nodeId="100" field="SHORTURL"/>" onMouseOver="javascript:SetMenuz2(1)" target="_blank"><mt:node nodeId="100" field="text"/></a></div>
								<div class="qh_k f14px_B Sred">|</div>
								<div class="f14px_B Sred jy_bg"><a href="<mt:node nodeId="101" field="SHORTURL"/>" onMouseOver="javascript:SetMenuz2(2)" target="_blank"><mt:node nodeId="101" field="text"/></a></div>
								<div class="qh_more"><a href="<mt:node nodeId="98" field="101"/>">更多>></a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla10"></div>
						<div class="content">
							<ul class="news3 com_list_2">
							<mt:contentEach nodeId="101" itemCount="5" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
										</ul>
						</div>
					</div>
					<div class="clear"></div>
					<script type="text/javascript">     
						function SetMenuz2(theid){              
							if(theid){
							for (i = 1; i< 3; i++) {
								document.getElementById("z2link_"+i).style.display="none";
								if(i==theid){
								document.getElementById("z2link_"+i).style.display="block";     
									}       
								}
							}
						}
					</script>
				</div>
				<div class="bla8"></div>
				<div class="clear"></div>
				<div style="margin-left:19px;"><a href="http://www.chinapeace.gov.cn/node_53448.htm" target="_blank"><img src="http://pic.hebei.com.cn/0/10/52/96/10529630_517514.jpg" alt="" /></a></div>
			</div>
			<div class="rightbox">
				<div class="com_name">
					<div class="com_name_1">视  频</div>
				</div>
				<div class="clear"></div>
				<div class="video">
					<div id="z3link_1" style="display: block">
						<div class="qh_box_1">
						<div class="bla5"></div>
							<div class="qh_left_1">
								<div class="f12px_B Sred lk_1"><a rel="nofollow" href="http://pingan.hebei.com.cn/casp/" target="_blank" onMouseOver="javascript:SetMenuz3(1)">长安视频</a></div>
								<div class="f12px Sred jy_bg_1"><a href="http://heb.hebei.com.cn/hbxwlb/index.shtml" onMouseOver="javascript:SetMenuz3(2)" target="_blank">晋中新闻联播</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.hebtv.com/jingshi/jfbd/" onMouseOver="javascript:SetMenuz3(3)" target="_blank">法治晋中</a></div>
								<div class="f12px Sred lk_1"><a href="http://av.hebei.com.cn/" onMouseOver="javascript:SetMenuz3(4)" target="_blank">长城在线</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.chinapeace.org.cn/node_25108.htm" onMouseOver="javascript:SetMenuz3(5)" target="_blank">长安影视</a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla8"></div>
						<div class="content">
							<ul class="all_2">
								<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2015/02/03/010807116.shtml" title="《国家审计》" target="_blank"><img src="http://pic.hebei.com.cn/0/10/50/91/10509183_883198.png" alt="《国家审计》" border="0" /></a>
</li>
							</ul>
						</div>
					</div>
					<div id="z3link_2" style="display: none">
						<div class="qh_box_1">
						<div class="bla5"></div>
							<div class="qh_left_1">
								<div class="f12px Sred lk_1"><a href="http://video.sina.com.cn/p/news/v/2014-06-01/184763983603.html" target="_blank" onMouseOver="javascript:SetMenuz3(1)">长安视频</a></div>
								<div class="f12px_B Sred jy_bg_1"><a href="http://heb.hebei.com.cn/hbxwlb/index.shtml" onMouseOver="javascript:SetMenuz3(2)" target="_blank">晋中新闻联播</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.hebtv.com/jingshi/jfbd/" onMouseOver="javascript:SetMenuz3(3)" target="_blank">法治晋中</a></div>
								<div class="f12px Sred lk_1"><a href="http://av.hebei.com.cn/" onMouseOver="javascript:SetMenuz3(4)" target="_blank">长城在线</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.chinapeace.org.cn/node_25108.htm" onMouseOver="javascript:SetMenuz3(5)" target="_blank">长安影视</a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla8"></div>
						<div class="content_1">
							<ul class="all_2">
								<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716793.shtml" title="重点播报" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455168_492420.png" alt="长安播报" border="0" /></a>
</li>
							</ul>
						</div>
					</div>
					<div id="z3link_3" style="display: none">
						<div class="qh_box_1">
						<div class="bla5"></div>
							<div class="qh_left_1">
								<div class="f12px Sred lk_1"><a href="http://video.sina.com.cn/p/news/v/2014-06-01/184763983603.html" target="_blank" onMouseOver="javascript:SetMenuz3(1)">长安视频</a></div>
								<div class="f12px Sred jy_bg_1"><a href="http://heb.hebei.com.cn/hbxwlb/index.shtml" onMouseOver="javascript:SetMenuz3(2)" target="_blank">晋中新闻联播</a></div>
								<div class="f12px_B Sred lk_1"><a href="http://www.hebtv.com/jingshi/jfbd/" onMouseOver="javascript:SetMenuz3(3)" target="_blank">法治晋中</a></div>
								<div class="f12px Sred lk_1"><a href="http://av.hebei.com.cn/" onMouseOver="javascript:SetMenuz3(4)" target="_blank">长城在线</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.chinapeace.org.cn/node_25108.htm" onMouseOver="javascript:SetMenuz3(5)" target="_blank">长安影视</a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla8"></div>
						<div class="content_1">
							<ul class="all_2">
								<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716796.shtml" title="法治晋中" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455174_715998.png" alt="法治晋中" border="0" /></a>
</li>
							</ul>
						</div>
					</div>
					<div id="z3link_4" style="display: none">
						<div class="qh_box_1">
						<div class="bla5"></div>
							<div class="qh_left_1">
								<div class="f12px Sred lk_1"><a href="http://video.sina.com.cn/p/news/v/2014-06-01/184763983603.html" target="_blank" onMouseOver="javascript:SetMenuz3(1)">长安视频</a></div>
								<div class="f12px Sred jy_bg_1"><a href="http://heb.hebei.com.cn/hbxwlb/index.shtml" onMouseOver="javascript:SetMenuz3(2)" target="_blank">晋中新闻联播</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.hebtv.com/jingshi/jfbd/" onMouseOver="javascript:SetMenuz3(3)" target="_blank">法治晋中</a></div>
								<div class="f12px_B Sred lk_1"><a href="http://av.hebei.com.cn/" onMouseOver="javascript:SetMenuz3(4)" target="_blank">长城在线</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.chinapeace.org.cn/node_25108.htm" onMouseOver="javascript:SetMenuz3(5)" target="_blank">晋中影视</a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla8"></div>
						<div class="content_1">
							<ul class="all_2">
								<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716799.shtml" title="长城在线" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455178_060317.png" alt="长城在线" border="0" /></a>
</li>
							</ul>
						</div>
					</div>
					<div id="z3link_5" style="display: none">
						<div class="qh_box_1">
						<div class="bla5"></div>
							<div class="qh_left_1">
								<div class="f12px Sred lk_1"><a href="http://video.sina.com.cn/p/news/v/2014-06-01/184763983603.html" target="_blank" onMouseOver="javascript:SetMenuz3(1)">长安视频</a></div>
								<div class="f12px Sred jy_bg_1"><a href="http://heb.hebei.com.cn/hbxwlb/index.shtml" onMouseOver="javascript:SetMenuz3(2)" target="_blank">晋中新闻联播</a></div>
								<div class="f12px Sred lk_1"><a href="http://www.hebtv.com/jingshi/jfbd/" onMouseOver="javascript:SetMenuz3(3)" target="_blank">法治晋中</a></div>
								<div class="f12px Sred lk_1"><a href="http://av.hebei.com.cn/" onMouseOver="javascript:SetMenuz3(4)" target="_blank">长城在线</a></div>
								<div class="f12px_B Sred lk_1"><a href="http://www.chinapeace.org.cn/node_25108.htm" onMouseOver="javascript:SetMenuz3(5)" target="_blank">晋中影视</a></div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="bla8"></div>
						<div class="content_1">
							<ul class="all_2">
								<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716800.shtml" title="晋中影视" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455179_872528.png" alt="长安影视" border="0" /></a>
</li>
							</ul>
						</div>
					</div>
					<div class="clear"></div>
					<script type="text/javascript">     
						function SetMenuz3(theid){              
							if(theid){
							for (i = 1; i< 6; i++) {
								document.getElementById("z3link_"+i).style.display="none";
								if(i==theid){
								document.getElementById("z3link_"+i).style.display="block";     
									}       
								}
							}
						}
					</script>
				</div>
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="118" field="SHORTURL"/>"><mt:node nodeId="118" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="118" field="SHORTURL"/>">更多>></a></div>
				</div>
				<div class="bla15"></div>
				<!-- 专题推荐开始 -->
				<div id="ysScrollBox" class="zhuanti">
					<div id="ysScrollItem">
						<ul>
							<mt:contentEach nodeId="101" itemCount="10" startPos="0">
							<a class="img" href="<mt:content valueVar="content" field="url"/>" title="" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="" border="0" /></a>
							<li>
							</mt:contentEach>
						</ul>
					</div>
				</div>
				<!-- 专题推荐结束 -->
			</div>
		</div>
		<div class="bla15"></div>
		<div id="box3">
			<div class="name3" style="background: rgba(0, 0, 0, 0) url('${styleTemplatePath}fz.jpg') repeat scroll 0 0;"></div>
			<div class="leftbox">
				<div class="fourbox3">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="92" field="SHORTURL"/>"><mt:node nodeId="92" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="92" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla10"></div>
					<div class="allbody">
						<ul class="pictitabs">
						<mt:contentEach nodeId="92" itemCount="1" startPos="0">
							<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<dl>
<dt><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></dt>
<dd><mt:content valueVar="content" field="description"/><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank">[详细]</a></dd>
</dl>
</li>
</mt:contentEach>
						</ul>
						<div class="bla8"></div>
						<ul class="titlist3 com_list_2">
						<mt:contentEach nodeId="92" itemCount="4" startPos="1">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						</ul>
					</div>
				</div>
				<div class="fourbox3">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="119" field="SHORTURL"/>"><mt:node nodeId="119" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="119" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla10"></div>
					<div class="allbody">
						<ul class="pictitabs">
						<mt:contentEach nodeId="119" itemCount="1" startPos="0">
							<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<dl>
<dt><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></dt>
<dd><mt:content valueVar="content" field="description"/><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank">[详细]</a></dd>
</dl>
</li>
</mt:contentEach>
						</ul>
						<div class="bla8"></div>
						<ul class="titlist3 com_list_2">
						<mt:contentEach nodeId="119" itemCount="4" startPos="1">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						</ul>
					</div>
				</div>
				<div class="bla8"></div>
				<div class="fourbox3">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="104" field="SHORTURL"/>"><mt:node nodeId="104" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="104" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla10"></div>
					<div class="allbody">
						<ul class="pictitabs">
						<mt:contentEach nodeId="104" itemCount="1" startPos="0">
							<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<dl>
<dt><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></dt>
<dd><mt:content valueVar="content" field="description"/><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank">[详细]</a></dd>
</dl>
</li>
</mt:contentEach>
						</ul>
						<div class="bla8"></div>
						<ul class="titlist3 com_list_2">
						<mt:contentEach nodeId="104" itemCount="4" startPos="1">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						</ul>
					</div>
				</div>
				<div class="fourbox3">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="105" field="SHORTURL"/>"><mt:node nodeId="105" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="105" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla10"></div>
					<div class="allbody">
						<ul class="pictitabs">
						<mt:contentEach nodeId="105" itemCount="1" startPos="0">
							<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<dl>
<dt><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></dt>
<dd><mt:content valueVar="content" field="description"/><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank">[详细]</a></dd>
</dl>
</li>
</mt:contentEach>
						</ul>
						<div class="bla8"></div>
						<ul class="titlist3 com_list_2">
						<mt:contentEach nodeId="105" itemCount="4" startPos="1">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
						</ul>
					</div>
				</div>
			</div>
			<div class="rightbox">
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="103" field="SHORTURL"/>"><mt:node nodeId="103" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="103" field="SHORTURL"/>">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<div class="edu">
					<ul class="pictitabs">
						<mt:contentEach nodeId="103" itemCount="1" startPos="0">
							<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<dl>
<dt><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></dt>
<dd><mt:content valueVar="content" field="description"/><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank">[详细]</a></dd>
</dl>
</li>
</mt:contentEach>
					</ul>
					<div class="bla8"></div>
					<ul class="titlist3_1 com_list_1">
						<mt:contentEach nodeId="105" itemCount="4" startPos="1">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
					</ul>
				</div>
				<div class="bla8"></div>
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="106" field="SHORTURL"/>"><mt:node nodeId="106" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="106" field="SHORTURL"/>">更多>></a></div>
				</div>
				<div class="bla5"></div>
				<div class="edu">
					<ul class="titlist3_1 com_list_1">
						<mt:contentEach nodeId="105" itemCount="3" startPos="0">
							<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
						</mt:contentEach>
					</ul>
				</div>
				<div class="bla5"></div>
				<div class="com_name">
					<div class="com_name_1">部门网站</div>
				</div>
				<div class="bla8"></div>
				<ul class="net">
					<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716786.shtml" title="法院" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455160_789448.jpg" alt="法院" border="0" /></a>
<a class="title" href="http://pingan.hebei.com.cn/system/2014/06/21/010716786.shtml" target="_blank">法院</a>
</li>
<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716784.shtml" title="检察院" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455167_998436.jpg" alt="检察院" border="0" /></a>
<a class="title" href="http://pingan.hebei.com.cn/system/2014/06/21/010716784.shtml" target="_blank">检察院</a>
</li>
<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716783.shtml" title="公安厅" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455156_051479.jpg" alt="公安厅" border="0" /></a>
<a class="title" href="http://pingan.hebei.com.cn/system/2014/06/21/010716783.shtml" target="_blank">公安厅</a>
</li>
<li>
<a class="img" href="http://pingan.hebei.com.cn/system/2014/06/21/010716765.shtml" title="司法厅" target="_blank"><img src="http://pic.hebei.com.cn/0/10/45/51/10455155_812540.jpg" alt="司法厅" border="0" /></a>
<a class="title" href="http://pingan.hebei.com.cn/system/2014/06/21/010716765.shtml" target="_blank">司法厅</a>
</li>
				</ul>
			</div>
		</div>
		<div class="bla15"></div>
		<div id="box4">
			<div class="name4" style="background: rgba(0, 0, 0, 0) url('${styleTemplatePath}dw.jpg') repeat scroll 0 0;"></div>
			<div class="upbox4">
				<div class="threebox4">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="102" field="SHORTURL"/>"><mt:node nodeId="102" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="102" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="newslist4 com_list_2">
					<mt:contentEach nodeId="102" itemCount="5" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
				</div>
				<div class="threebox4">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="93" field="SHORTURL"/>"><mt:node nodeId="93" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="93" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="newslist4 com_list_2">
					<mt:contentEach nodeId="93" itemCount="5" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
				</div>
				<div class="threebox4">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="120" field="SHORTURL"/>"><mt:node nodeId="120" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="120" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="newslist4 com_list_2">
					<mt:contentEach nodeId="120" itemCount="5" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
				</div>
			</div>
			<div class="bla5"></div>
			<div class="downbox4">
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="112" field="SHORTURL"/>"><mt:node nodeId="112" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="112" field="SHORTURL"/>">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<div class="bla5"></div>
				<div class="leftbox4">
					<ul class="bigpic">
					<mt:contentEach nodeId="112" itemCount="1" startPos="0">
						<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" title="<mt:content valueVar="content" field="title"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="" border="0" /></a>
<a class="title" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a>
</li>
					</mt:contentEach>
					</ul>
				</div>
				<div class="rightbox4">
					<ul class="smallpic">
					<mt:contentEach nodeId="112" itemCount="6" startPos="1">
						<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" title="<mt:content valueVar="content" field="title"/>" target="_blank"><img src="<mt:content valueVar="content" field="previewUrl"/>" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
<a class="title" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a>
</li>
					</mt:contentEach>
					</ul>
				</div>
			</div>
		</div>
		<div class="bla15"></div>
		<div id="box5">
			<div class="name5"></div>
			<div class="leftbox5">
				<div class="leftbox5_1">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="113" field="SHORTURL"/>"><mt:node nodeId="113" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="113" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="allbody5 com_list_2">
					<mt:contentEach nodeId="113" itemCount="6" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
					<div class="bla8"></div>
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="116" field="SHORTURL"/>"><mt:node nodeId="116" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="116" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="allbody5 com_list_2">
					<mt:contentEach nodeId="116" itemCount="6" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
					<div class="bla8"></div>
				</div>
				<div class="rightbox5_1">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="114" field="SHORTURL"/>"><mt:node nodeId="114" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="114" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="allbody5 com_list_2">
					<mt:contentEach nodeId="114" itemCount="6" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
					<div class="bla8"></div>
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="117" field="SHORTURL"/>"><mt:node nodeId="117" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="117" field="SHORTURL"/>">更多>></a></div>
					</div>
					<div class="bla8"></div>
					<ul class="allbody5 com_list_2">
					<mt:contentEach nodeId="117" itemCount="6" startPos="0">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
				</div>
			</div>
			<div class="rightbox5">
					<div class="com_name">
						<div class="com_name_1"><a href="<mt:node nodeId="115" field="SHORTURL"/>"><mt:node nodeId="115" field="text"/></a></div>
						<div class="com_more_1"><a href="<mt:node nodeId="115" field="SHORTURL"/>">更多>></a></div>
					</div>
				<div class="bla15"></div>
				<ul class="pic5">
					<mt:contentEach nodeId="115" itemCount="1" startPos="0">
					<li>
<a class="img" href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><img src="<mt:content valueVar="content" field="pr"/>eviewUrl" alt="<mt:content valueVar="content" field="title"/>" border="0" /></a>
</li>
</mt:contentEach>
				</ul>
				<div class="bla10"></div>
				<div class="pic5">
					<ul class="list5 com_list_1">
					<mt:contentEach nodeId="115" itemCount="2" startPos="1">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
					<ul class="list5 com_list_1">
						<mt:contentEach nodeId="115" itemCount="2" startPos="3">
						<li><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/></a></li>
					</mt:contentEach>
					</ul>
				</div>
			</div>
		</div>
		<div class="bla15"></div>
		</div>
		<div class="bla15"></div>
		<div class="bla15"></div>
		<div id="q1" class="slidingBlock">
			<div class="bla15"></div>
			<ul class="qiehuantitle">
				<li class="selected" style="border-left:1px solid #E4E4E4;"><a>省综治成员单位网站</a></li>
				<li><a>中央及网站</a></li>
				<li><a>国内政法网站</a></li>
				<li style="border-right:1px solid #E4E4E4;"><a>省内网站</a></li>
			</ul>
			<div id="datalist">
				<ul class="qiehuan">
					<li><a rel="nofollow" href="http://www.hebei.gov.cn/" target="_blank">省委办公厅</a></li>
<li><a rel="nofollow" href="http://www.hebei.gov.cn/" target="_blank">省政府办公厅</a></li>
<li><a rel="nofollow" href="http://www.hbcourt.org/" target="_blank">省法院</a></li>
<li><a rel="nofollow" href="http://www.hejcy.gov.cn/" target="_blank">省检察院</a></li>
<li><a rel="nofollow" href="http://pingan.hebei.com.cn/" target="_blank">省委政法委</a></li>
<li><a rel="nofollow" href="http://www.hebga.gov.cn/" target="_blank">省公安厅</a></li>
<li><a rel="nofollow" href="http://www.hbfz.gov.cn/" target="_blank">省司法厅</a></li>
<li>省国安厅</li>
<li>武警晋中总队</li>
<li>省军区</a></li>
<li><a rel="nofollow" href="http://www.hebei.gov.cn/" target="_blank">省纪委</a></li>
<li><a rel="nofollow" href="http://www.hebdj.gov.cn/cyportal/template/site00_index.jsp" target="_blank">省委组织部</a></li>
<li><a rel="nofollow" href="http://www.hebei.com.cn/" target="_blank">省委宣传部</a></li>
<li><a rel="nofollow" href="http://www.hebfzb.gov.cn/index0.aspx" target="_blank">省法制办</a></li>
<li><a rel="nofollow" href="http://www.hee.cn/" target="_blank">省教育厅</a></li>
<li>省委防范办</li>
<li><a rel="nofollow" href="http://www.hebmzt.gov.cn/" target="_blank">省民宗厅</a></li>
<li><a rel="nofollow" href="http://www.gjxfj.gov.cn/2005-01/13/content_3559537.htm" target="_blank">省信访局</a></li>
<li><a rel="nofollow" href="http://www.he.lss.gov.cn/" target="_blank">省人保厅</a></li>
<li><a rel="nofollow" href="http://www.hbsa.gov.cn/zh/html/tab/portal0/default36.html" target="_blank">省民政厅</a></li>
<li><a rel="nofollow" href="http://www.hebcz.gov.cn/default.asp" target="_blank">省财政厅</a></li>
<li><a rel="nofollow" href="http://www.hebgt.gov.cn/index.do?templet=index" target="_blank">省国土厅</a></li>
<li><a rel="nofollow" href="http://www.hebjs.gov.cn/" target="_blank">省建设厅</a></li>
<li><a rel="nofollow" href="http://www.hbsjtt.gov.cn/" target="_blank"> 省交通运输厅</a></li>
<li><a rel="nofollow" href="http://www.ii.gov.cn/" target="_blank">省工信厅</a></li>
<li><a rel="nofollow" href="http://www.helib.net/" target="_blank">省文化厅</a></li>
<li><a rel="nofollow" href="http://www.hebwst.gov.cn/" target="_blank">省卫生厅</a></li>
<li><a rel="nofollow" href="http://www.hebgs.gov.cn/yw/yp/s-index.asp" target="_blank">省工商局</a></li>
<li><a rel="nofollow" href="http://www.hebeichuban.gov.cn/" target="_blank">省新闻出版广电局</a></li>
<li><a rel="nofollow" href="http://www.hebzjj.gov.cn/" target="_blank">省质监局</a></li>
<li><a rel="nofollow" href="http://www.hebeitour.com.cn/" target="_blank">省旅游局</a></li>
<li><a rel="nofollow" href="http://www.hebsafety.gov.cn/index.do?templet=index" target="_blank">省安监局</a></li>
<li><a rel="nofollow" href="http://shijiazhuang.customs.gov.cn/" target="_blank">晋中海关</a></li>
<li>晋中保监局</li>
<li><a rel="nofollow" href="http://www.hebgh.org/" target="_blank">省总工会</a></li>
<li><a rel="nofollow" href="http://www.54heb.com/index.do?templet=index" target="_blank">团省委</a></li>
<li><a rel="nofollow" href="http://www.hebeiwomen.org.cn/" target="_blank">省妇联</a></li>
<li><a rel="nofollow" href="http://shijiazhuang.pbc.gov.cn/" target="_blank">人行晋中支行</a></li>
<li><a rel="nofollow" href="http://www.hebeiairport.cn/" target="_blank">晋中机场</a></li>
				</ul>
				<ul class="qiehuan" style="display:none">
					<li><a rel="nofollow" href="http://www.chinapeace.org.cn/" target="_blank">中国长安网</a></li>
<li><a rel="nofollow" href="http://www.chinacourt.org" target="_blank">法院在线网</a></li>
<li><a rel="nofollow" href="http://www.wenming.cn/" target="_blank">中国文明网</a></li>
<li><a rel="nofollow" href="http://www.legalinfo.gov.cn/" target="_blank">中国普法网</a></li>
<li><a rel="nofollow" href="http://www.cnr.cn/" target="_blank">中国广播网</a></li>
<li><a rel="nofollow" href="http://www.chinanews.com/" target="_blank">中新社</a></li>
<li><a rel="nofollow" href="http://www.nlc.gov.cn/" target="_blank">中国国家图书馆</a></li>
<li><a rel="nofollow" href="http://www.cnradio.com.cn/" target="_blank">中国中央人民广播电台</a></li>
<li><a rel="nofollow" href="http://www.chinadaily.com.cn/" target="_blank">中国日报</a></li>
<li><a rel="nofollow" href="http://www.peopledaily.com.cn/" target="_blank">人民日报</a></li>
<li><a rel="nofollow" href="http://www.cntv.cn/" target="_blank">中国中央电视台</a></li>
<li><a rel="nofollow" href="http://www.legaldaily.com.cn/" target="_blank">法制日报网</a></li>
<li><a rel="nofollow" href="http://www.xinhuanet.com/legal/" target="_blank">新华法治网</a></li>
<li><a rel="nofollow" href="http://www.gov.cn/" target="_blank">中国政府上网工程</a></li>
<li><a rel="nofollow" href="http://zgrdxw.peopledaily.com.cn/" target="_blank">中国人大新闻网</a></li>
<li><a rel="nofollow" href="http://www.cppcc.gov.cn/" target="_blank">中国人民政治协商会议全国委员会办公厅</a></li>
<li><a rel="nofollow" href="http://www.court.gov.cn/" target="_blank">最高人民法院</a></li>
<li><a rel="nofollow" href="http://www.spp.gov.cn/" target="_blank">最高人民检察院</a></li>
<li><a rel="nofollow" href="http://www.mps.gov.cn/" target="_blank">公安部</a></li>
<li><a rel="nofollow" href="http://www.legalinfo.gov.cn/" target="_blank">司法部</a></li>
<li><a rel="nofollow" href="http://www.fmprc.gov.cn/mfa_chn/" target="_blank">外交部</a></li>
<li><a rel="nofollow" href="http://www.mof.gov.cn/index.htm" target="_blank">财政部</a></li>
<li><a rel="nofollow" href="http://www.moe.edu.cn/" target="_blank">教育部</a></li>
<li><a rel="nofollow" href="http://www.zjw.gov.cn/" target="_blank">监察部</a></li>
<li><a rel="nofollow" href="http://www.mca.gov.cn/" target="_blank">民政部</a></li>
				</ul>
				<ul class="qiehuan" style="display:none">
					<li><a rel="nofollow" href="http://www.sdzz.org/" target="_blank">首都综治网</a></li>
<li><a rel="nofollow" href="http://www.shzfzz.net/" target="_blank">上海政法网</a></li>
<li><a rel="nofollow" href="http://hapa.gov.cn/" target="_blank">河南平安网</a></li>
<li><a rel="nofollow" href="http://www.sxzf.org.cn/" target="_blank">山西政法网</a></li>
<li><a rel="nofollow" href="http://www.pahnw.cn/" target="_blank">平安海南网</a></li>
<li><a rel="nofollow" href="http://www.paljw.com/" target="_blank">平安龙江网</a></li>
<li><a rel="nofollow" href="http://gs.legaldaily.com.cn/" target="_blank">甘肃政法网</a></li>
<li><a rel="nofollow" href="http://www.jxzfw.gov.cn/" target="_blank">江西政法网</a></li>
<li><a rel="nofollow" href="http://www.zjol.com.cn/" target="_blank">平安浙江网</a></li>
<li><a rel="nofollow" href="http://hkpa.haikou.gov.cn/" target="_blank">海口平安网</a></li>
<li><a rel="nofollow" href="http://www.mnpaw.com/" target="_blank">闽南平安网</a></li>
<li><a rel="nofollow" href="http://www.lncypeace.org.cn/" target="_blank">朝阳平安网</a></li>
<li><a rel="nofollow" href="http://www.pacq.gov.cn/index.html" target="_blank">平安重庆网</a></li>
<li><a rel="nofollow" href="http://www.sichuanpeace.org.cn/" target="_blank">四川长安网</a></li>
<li><a rel="nofollow" href="http://www.pagx.cn/" target="_blank">平安广西网</a></li>
<li><a rel="nofollow" href="http://www.sjz.gov.cn/" target="_blank">晋中</a></li>
<li><a rel="nofollow" href="http://www.cangzhou.gov.cn/" target="_blank"> 沧州市</a></li>
<li><a rel="nofollow" href="http://www.zjk.gov.cn/" target="_blank">张家口市</a></li>
<li><a rel="nofollow" href="http://www.chengde.gov.cn/" target="_blank">承德市</a></li>
				</ul>
				<ul class="qiehuan" style="display:none">
					<li><a rel="nofollow" href="http://www.hebradio.com/" target="_blank">晋中广播网</a></li>
<li><a rel="nofollow" href="http://www.hebeihaoren.com/" target="_blank">晋中好人网</a></li>
<li><a rel="nofollow" href="http://www.hebei.com.cn/" target="_blank">长城网</a></li>
<li><a rel="nofollow" href="http://www.hebga.cn/" target="_blank">晋中警察网</a></li>
<li><a rel="nofollow" href="http://www.hbfz.gov.cn/" target="_blank">晋中法治网</a></li>
<li><a rel="nofollow" href="http://www.yzlegal.com/" target="_blank">晋中省法制网</a></li>
<li><a rel="nofollow" href="http://www.yzjs.com.cn/" target="_blank">晋中公安网</a></li>
<li><a rel="nofollow" href="http://www.he.yfw.com.cn/" target="_blank">晋中省职务犯罪预防网</a></li>
<li><a rel="nofollow" href="http://www.hbcourt.org/" target="_blank">晋中法院网</a></li>
<li><a rel="nofollow" href="http://wmb.hebei.com.cn/" target="_blank">晋中文明网</a></li>
<li><a rel="nofollow" href="http://www.hbsfxh.com/" target="_blank">晋中法学网</a></li>
<li><a rel="nofollow" href="http://www.hebeidaily.com.cn/" target="_blank">晋中新闻网</a></li>
<li><a rel="nofollow" href="http://www.hbqnb.com/" target="_blank">晋中青年报</a></li>
<li><a rel="nofollow" href="http://www.hbrd.net/" target="_blank">晋中省人大</a></li>
<li><a rel="nofollow" href="http://www.hebzx.gov.cn/default.aspx" target="_blank">晋中政协</a></li>
<li><a rel="nofollow" href="http://www.fawan.com/" target="_blank">法制晚报网</a></li>
<li><a rel="nofollow" href="http://hebei.kaiwind.com/" target="_blank">凯风网晋中频道</a></li>
<li><a rel="nofollow" href="http://www.helc.edu.cn/" target="_blank">晋中政法学院</a></li>

				</ul>
			</div>
		</div>
		<div class="bla15"></div>
	</div>
</div>

<div class="wrapper">
	<div id="box9">
		<div class="bottom9">
			
			主管单位：中共山西晋中市委政法委员会    晋中市社会管理综合治理委员会<br />
			备案序号：晋ICP备10001396号-1    技术支持：<a href="#">IBPD</a><br />
			联系我们： 0354-7393000    邮箱：jinzhongpingan@163.com 
		</div>
	</div>
	<div style="background-color:#e5e5e5;width:100%;">
		<div style="width:980px;margin:0 auto;text-align:center;">
			<script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/05/000/0000/60429979/CA050000000604299790000.js' type='text/javascript'%3E%3C/script%3E"));</script>
		</div>
	</div>
</div><!--<page cms="enorth webpublisher"  version="5.0.0 /2011101201" server_name="work.hebei.com.cn" parse_date="2015-05-06 18:29:13" cost="8" parse_result="0" input_mode="manual"></page>-->
<div id="ad3" style="bottom:5px; right:5px; position:fixed;width:235px;height:205px; z-index:2;display:none;">
	<div style="cursor:pointer;height:18px;pxfont-size:12px;" onclick="closed3();">关闭</div>
	<a href="#" target="_blank" ><img src="#" border="0" width="235" height="185" /></a>
</div>
<script>function closed3(){ var aaa=document.getElementById("ad3"); aaa.style.display="none"}</script>

<!--摄影长廊代码开始-->
<script type="text/javascript">
$(function(){
	$('#mobile_ad').slides({
		preload: false,
		play: 5000,
		pause: 2500,
		hoverPause: true
	});
});
</script>
<!--摄影长廊代码结束-->
<!--=======友情链接切换代码1开始=======-->
<SCRIPT>
$(document).ready(function(){
var tab = new $.fn.tab({
tabList:"#q1 .qiehuantitle li",
contentList:"#q1 #datalist .qiehuan",
eventType:"mouseover",
tabActiveClass:"selected"
});
});
</SCRIPT>
<!--=======友情链接切换代码1结束=======-->
<!--焦点图代码开始-->
<script type="text/javascript">
function ccw_focusImage(interval){
	var content=document.getElementById("ccw_focusImage-content").getElementsByTagName("li");
	var tab=document.getElementById("ccw_focusImage-tab").getElementsByTagName("li");
	var num = tab.length;
	var ccw_current=0;
	var intervalObj;
	function init(){
		tab[0].className='ccw_current';
		content[0].className='ccw_current';
		for(var i=0;i<tab.length;i++){
			tab[i].onclick=function(){
				tabOnClick(this);
			}
		}
		intervalObj=window.setTimeout(scrollImage,interval*1000);
	}
	function tabOnClick(liTag){
		var i=parseInt(liTag.innerHTML)-1;
		if(i==ccw_current){
			return;
		}
		clearInterval(intervalObj);
		tab[ccw_current].className='';
		content[ccw_current].className='';
		tab[i].className='ccw_current';
		content[i].className='ccw_current';
		ccw_current=i;
		intervalObj=window.setTimeout(scrollImage,interval*1000);
	}
	function scrollImage(){
		clearInterval(intervalObj);
		tab[ccw_current].className='';
		content[ccw_current].className='';
		ccw_current++;
		if(ccw_current>=num){
			ccw_current=0;
		}
		tab[ccw_current].className='ccw_current';
		content[ccw_current].className='ccw_current';
		intervalObj=window.setTimeout(scrollImage,interval*1000);
	}
	init();
}
new ccw_focusImage(6);
</script>

<!--焦点图代码结束-->

<!--推荐专题代码结束-->
<script>
function scrollObj(sContainerId,sElementId,iSpeed,iDirection){
 scrollLeft=function(){(this.sElement.offsetWidth-this.sContainer.scrollLeft<=0)?this.sContainer.scrollLeft-=(this.sElement.offsetWidth-1):this.sContainer.scrollLeft++;}
 scrollRight=function(){(this.sContainer.scrollLeft<=0)?this.sContainer.scrollLeft+=(this.sElement.offsetWidth-1):this.sContainer.scrollLeft--;}
 scrollUp=function(){(this.sElement.offsetHeight-this.sContainer.scrollTop<=0)?this.sContainer.scrollTop-=(this.sElement.offsetHeight-1):this.sContainer.scrollTop++;}
 scrollDown=function(){(this.sContainer.scrollTop<=0)?this.sContainer.scrollTop+=(this.sElement.offsetHeight-1):this.sContainer.scrollTop--;}
  this.sContainer=document.getElementById(sContainerId);
  this.sElement=document.getElementById(sElementId);
  this.iSpeed=iSpeed;
  this.sElement2=this.sElement.parentNode.appendChild(this.sElement.cloneNode(true));
  switch(iDirection){ 
  case "right":this.doScroll=scrollRight;break;
  case "up":this.doScroll=scrollUp;break;
  case "down":this.doScroll=scrollDown;break;
  default: this.doScroll=scrollLeft;
}
  var thisObj=this;
  thisObj.loopScroll=setInterval(function(){thisObj.doScroll()},thisObj.iSpeed);
  thisObj.sContainer.onmouseover=function() {window.clearInterval(thisObj.loopScroll)};
  thisObj.sContainer.onmouseout=function() {thisObj.loopScroll=window.setInterval(function(){thisObj.doScroll()},thisObj.iSpeed)};
  }
var ysScroll2=new scrollObj("ysScrollBox","ysScrollItem",50,"up");
</script>
<!--推荐专题代码结束-->
<!--返回顶部开始-->
<span class="side_fhtop" onclick="window.scrollTo('0','0')"><img src="http://www.hebei.com.cn/images_new/ccwsy_fh_top.jpg" alt="返回顶部" width="19" /></span>
<!--返回顶部结束-->
<!--通讯员之家开始-->
<script type="text/javascript" src="http://ftp.hebei.com.cn:8080/caw/chart/export_city_order.jsp"></script>
<!--通讯员之家结束-->
</body>
</html>
