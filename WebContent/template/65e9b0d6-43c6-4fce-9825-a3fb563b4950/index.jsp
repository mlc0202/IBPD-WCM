<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/ibpdTag" prefix="mt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>${pageTitle }</title>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="${keywords }">
	<meta http-equiv="copyright" content="${copyright }">
	<meta http-equiv="description" content="${description }">
	
		<link rel="stylesheet" type="text/css" href="${cssPath }">
		<link rel="stylesheet" type="text/css" href="<mt:styleTemplate templateId="7" field="SHORTURL"/>style.css">
        <script type="text/javascript" src="<mt:styleTemplate templateId="7" field="SHORTURL"/>jquery-1.4.4.min.js"></script>
<script src="<mt:styleTemplate templateId="7" field="SHORTURL"/>ui.tab.js"></script>
<script src="<mt:styleTemplate templateId="7" field="SHORTURL"/>mobile_ad.js" type="text/javascript"></script>  </head>

  </head>
  
  <body>
	<div class="wrapper">
	<div class="bgred">
		<div class="topimg"><img src="<mt:styleTemplate templateId="7" field="SHORTURL"/>banner.jpg" alt="" /></div>
	</div>
	<div class="bgyellow">
		<div class="bgnav" style="overflow:hidden;">
			<ul class="nav">
				<mt:nodeEach nodeId="-1" pageSize="20"> 
				<li><a href="<mt:node value="${node}" field="SHORTURL"/>" target="_blank"><mt:node valueVar="node" field="text"></mt:node></a></li>
			</mt:nodeEach>
			</ul>
		</div>
	</div>
</div>
    <div class="bla5"></div>
<div class="container">
	<div id="boxall">
		<div class="blank2"></div>
		<div class="blank2"></div>
		<div id="wrapper"  style="background-color:#FFFFFF;">
			<div class="mainDiv">
				<div id="pLocation">您当前的位置：<span class="navi_12">
<mt:nodeNavi nodeId="${currentNode.id}"/>
</span>
				</div>
				<div id="pTitle">
${currentNode.text}
</div>
				<div id="pContent">
					<ul>        
						<div style="border-bottom:1px dotted #cccccc;" >
						<mt:contentEach nodeId="${currentNode.id}" itemCount="50" startPos="0">
							<div style="height:25px;"><li style="line-height:200%"><a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank" ><mt:content valueVar="content" field="title"/></a><mt:content valueVar="content" field="createDate" dateFormat="yyyy-MM-dd"/></li></div>
						</mt:contentEach>
						</div>
					</ul>
   
				</div>
			</div>
			<div class="youce">
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="88" field="SHORTURL"/>" target="_blank"><mt:node nodeId="88" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="88" field="SHORTURL"/>" target="_blank">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<ul class="newsyou">
					<mt:contentEach nodeId="88" itemCount="8" startPos="0">
					<li>
						<a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/>"</a>
					</li>
					</mt:contentEach>
				</ul>
				<div class="bla15"></div>
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="90" field="SHORTURL"/>" target="_blank"><mt:node nodeId="90" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="90" field="SHORTURL"/>" target="_blank">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<ul class="newsyou">
					<mt:contentEach nodeId="90" itemCount="8" startPos="0">
					<li>
						<a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/>"</a>
					</li>
					</mt:contentEach>
				</ul>
				<div class="bla15"></div>
				<div class="com_name">
					<div class="com_name_1"><a href="<mt:node nodeId="94" field="SHORTURL"/>" target="_blank"><mt:node nodeId="94" field="text"/></a></div>
					<div class="com_more_1"><a href="<mt:node nodeId="94" field="SHORTURL"/>" target="_blank">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<ul class="newsyou">
					<mt:contentEach nodeId="94" itemCount="8" startPos="0">
					<li>
						<a href="<mt:content valueVar="content" field="SHORTURL"/>" target="_blank"><mt:content valueVar="content" field="title"/>"</a>
					</li>
					</mt:contentEach>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="blank2"></div>
			<div class="blank2"></div>
			<div class="blank2"></div>
		</div>
	</div>
	<div class="bla15"></div>
	<div class="bla15"></div>
</div>

<div class="wrapper">
	<div id="box9">
		<div class="bottom9">
			
			主管单位：中共山西晋中市委政法委员会    晋中市社会管理综合治理委员会<br />
			备案序号：晋ICP备10001396号-1    技术支持：<a href="#">IBPD</a><br />
			联系我们： 0354-7393000    邮箱：jinzhongpingan@163.com 
		</div>
	</div>
	<div style="background-color:#e5e5e5;width:100%;">
		<div style="width:980px;margin:0 auto;text-align:center;">
			<script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/05/000/0000/60429979/CA050000000604299790000.js' type='text/javascript'%3E%3C/script%3E"));</script>
		</div>
	</div>
</div>
  </body>
</html>
