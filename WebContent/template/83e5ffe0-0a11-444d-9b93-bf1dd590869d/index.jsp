<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/ibpdTag" prefix="mt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>${pageTitle }</title>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="${keywords }">
	<meta http-equiv="copyright" content="${copyright }">
	<meta http-equiv="description" content="${description }">
	
		<link rel="stylesheet" type="text/css" href="${cssPath }">
		<link rel="stylesheet" type="text/css" href="<mt:styleTemplate templateId="7" field="SHORTURL"/>style.css">
        <script type="text/javascript" src="<mt:styleTemplate templateId="7" field="SHORTURL"/>jquery-1.4.4.min.js"></script>
<script src="<mt:styleTemplate templateId="7" field="SHORTURL"/>ui.tab.js"></script>
<script src="<mt:styleTemplate templateId="7" field="SHORTURL"/>mobile_ad.js" type="text/javascript"></script>  </head>
  </head>
  
  <body>


<div class="wrapper">
	<div class="bgred">
		<div class="topimg"><img src="<mt:styleTemplate templateId="7" field="SHORTURL"/>banner.jpg" alt="" /></div>
	</div>
	<div class="bgyellow">
		<div class="bgnav" style="overflow:hidden;">
			<ul class="nav">
				<mt:nodeEach nodeId="-1" pageSize="20"> 
				<li><a href="<mt:node value="${node}" field="SHORTURL"/>" target="_blank"><mt:node valueVar="node" field="text"></mt:node></a></li>
			</mt:nodeEach>
			</ul>
		</div>
	</div>
</div>
    <div class="bla5"></div>
    <div style="width:955px; height:40px; padding-left:25px; line-height:40px; background:#CCCCCC; margin:0 auto;"> <span class="f14px_hei b">网群：</span><span class="f13px_hei">  <a href="http://sjzca.hebei.com.cn/" target="_blank">石家庄</a>    <a href="http://topics.gmw.cn/node_60117.htm" target="_blank">承德</a>    <a href="http://www.zjkpeace.gov.cn/" target="_blank">张家口</a>    <a href="http://qhd.hebpingan.org/" target="_blank">秦皇岛</a>    <a href="http://www.tscaw.gov.cn/" target="_blank">唐山</a>    <a href="http://www.lfcaw.org/" target="_blank">廊坊 </a>    <a href="http://www.bdcaw.org/" target="_blank">保定</a>    <a href=" http://www.czpingan.gov.cn/" target="_blank">沧州</a>    <a href="http://hs.hebpingan.org/" target="_blank">衡水</a>    <a href="http://www.xtpa.gov.cn/ " target="_blank">邢台</a>    <a href="http://hd.hebpingan.org/" target="_blank">邯郸</a>  <a href="http://www.oilhb.com/index.php?m=content&c=index&a=lists&catid=25" target="_blank">华北油田</a>  </span></div><!--<page cms="enorth webpublisher"  version="5.0.0 /2011101201" server_name="work.hebei.com.cn" parse_date="2015-06-04 10:30:08" cost="9" parse_result="0" input_mode="manual"></page>-->
<div class="container">
	<div id="boxall">
		<div class="blank2"></div>
		<div class="blank2"></div>
		<div id="wrapper"  style="background-color:#FFFFFF;">
			<div id="position">
				<div id="docLocation1" align="left" style="font-size:12px;">您当前的位置：<span class="navi_12"><mt:nodeNavi nodeId="${currentNode.id}"/>
</span></div>
				<div id="docLocation2"></div>
				<div class="clear"></div>
			</div>
			<div class="blank2"></div> 
			<div class="mainDiv">
				<div id="doc">
					<h1 id="docTitle">孟建柱：推进网上信访方便人民群众</h1>
					<div id="docDetail" style="font-size:12px;">
						<span id="pubtime_baidu">
<!--function pub_date(yyyy-MM-dd HH:mm:ss) parse begin-->
2015-06-03 14:56:49
<!--function: pub_date(yyyy-MM-dd HH:mm:ss) parse end  0ms cost! -->
</span>
						<div class="ccw_gjy_zt_bg"><div style="width:360px; height:25px; margin:0 auto; font-size:12px; color:#666666;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
     <td align="center"><span class="docSourceName">【字号：<A href="javascript:fontZoom(16)">大</A> | <A href="javascript:fontZoom(14)" docSourceName>中</A> | <A href="javascript:fontZoom(12)" >小</A>】</span></td>
     <td align="center">【背景色</td>
    <td align="center"><A href="javascript:setColor('FAFBE6')"><IMG height=10 alt=杏仁黄 src="http://www.pub1.hebei.com.cn/zygl/images/color1.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('FFF2E2')"><IMG height=10 alt=秋叶褐 src="http://www.pub1.hebei.com.cn/zygl/images/color2.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('FDE6E0')"><IMG height=10 alt=胭脂红 src="http://www.pub1.hebei.com.cn/zygl/images/color3.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('F3FFE1')"><IMG height=10 alt=芥末绿 src="http://www.pub1.hebei.com.cn/zygl/images/color4.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('DAFAFE')"><IMG height=10 alt=天蓝 src="http://www.pub1.hebei.com.cn/zygl/images/color5.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('E9EBFE')"><IMG height=10 alt=雪青 src="http://www.pub1.hebei.com.cn/zygl/images/color6.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('EAEAEF')"><IMG height=10 alt=灰 src="http://www.pub1.hebei.com.cn/zygl/images/color7.gif" width=10 border="0"></A></td>
    <td align="center"><A href="javascript:setColor('FFFFFF')"><IMG height=10 alt=银河白(默认色) src="http://www.pub1.hebei.com.cn/zygl/images/color8.gif" width=10 border="0"></A></td>
    <td align="center">】</td>
  </tr>
</table>
</div></div>
					</div>
					<div id="myTable">
						<div class="boxContent topic"></div>
						<div id="docContent">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<DIV class=Custom_UnionStyle align="left" id="fontzoom">
											<P><p style="text-align: center">　　<strong style="border-bottom: 0px; border-left: 0px; padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; border-top: 0px; border-right: 0px; padding-top: 0px">孟建柱在国家信访局调研时提出</strong></p>
<p style="text-align: center">　　<strong style="border-bottom: 0px; border-left: 0px; padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; border-top: 0px; border-right: 0px; padding-top: 0px">推进网上信访方便人民群众</strong></p>
<p style="text-align: center">　　<strong style="border-bottom: 0px; border-left: 0px; padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; border-top: 0px; border-right: 0px; padding-top: 0px">杨晶参加调研</strong></p>
<center><img src="http://pic.hebei.com.cn/0/10/54/39/10543931_073635.jpg" style="width: 500px; height: 324px" /></center>
<p>　　5月27日，中共中央政治局委员、中央政法委书记孟建柱在国家信访局调研。图为孟建柱查看国家信访信息系统运行情况。</p>
<!--advertisement code begin--><!--advertisement code end--><p>　　<strong style="border-bottom: 0px; border-left: 0px; padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; border-top: 0px; border-right: 0px; padding-top: 0px">中国长安网北京5月28日电</strong>中共中央政治局委员、中央政法委书记孟建柱27日在国家信访局调研。他指出，信访工作是党和政府密切联系群众、了解社情民意的重要渠道。要大力推进网上信访，让数据多跑路、群众少跑腿，最大限度方便人民群众。</p>
<p>　　今年初，国家信访信息系统上线运行，实现了对信访业务的全覆盖。在国家信访局投诉受理办公室，孟建柱随机调取查看网上办理的信访事项，了解操作流程。他指出，国家信访信息系统的使用，实现了对信访事项的可查询、可跟踪、可督办、可评价，方便了群众、提高了效率，密切了党和群众的联系。要优化国家信访信息系统建设，加大网上信访事项的跟踪、督办、检查、回访力度。要把更多的信访事项及时解决在当地，尽量让老百姓少奔波、少受累。要善于运用法治思维和法治方式开展信访工作，促进信访问题依法公正解决。</p>
<p>　　孟建柱指出，人民群众通过信访渠道给党和政府各项工作提出意见建议，寄托着对党和政府的信任和期望。要注意对群众信访事项的综合研判，发现带有普遍性、倾向性问题，及时提出完善制度、改进工作的意见建议，更好地服务群众、服务大局。</p>
<p>　　中共中央书记处书记、国务委员兼国务院秘书长杨晶，中央政法委秘书长、国务院副秘书长汪永清，中央政法委副秘书长、中央综治办主任陈训秋，国务院副秘书长、国家信访局局长舒晓琴参加调研。(完)</p>
</P>
										</DIV>
									</td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<DIV class=Custom_UnionStyle align="left">
											<p class="zi12"><b>关键词：</b><strong>孟建柱|网上信访</strong></P>
										</DIV>
									</td>
								</tr>
							</table>
							<div id="moreContent"><a href="http://pingan.hebei.com.cn/">&gt;&gt;&gt;更多精彩内容请进入河北长安网&lt;&lt;&lt;</a></div>
							<div id="pPageCtrl"></div>
							<div class="FX">
								<!-- Baidu Button BEGIN -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="66%" align="left"><div id="bdshare" class="bdshare_t bds_tools get-codes-bdshare"> <span class="bds_more">分享到：</span> <a class="bds_qzone"></a> <a class="bds_tsina"></a> <a class="bds_tqq"></a> <a class="bds_renren"></a> <a class="shareCount"></a> </div></td>
    <td width="21%" align="right"><a href="javascript:printPureArt()" style="font-size:13px; color:#CC0000;">打印</a></td>
    <td width="13%" align="right"><a href="javascript:AddFavor()" style="font-size:13px; color:#CC0000;">收藏本页</a></td>
  </tr>
</table>
<script type="text/javascript" id="bdshare_js" data="type=tools" ></script>
<script type="text/javascript" id="bdshell_js"></script>
<script type="text/javascript">
	document.getElementById("bdshell_js").src = "http://bdimg.share.baidu.com/static/js/shell_v2.js?cdnversion=" + new Date().getHours();
</script>
<script type="text/javascript">
function printPureArt(){//只打印id区域内的内容
	document.body.innerHTML = '<div id="myTable">' + document.getElementById('docContent').innerHTML + "</div>";
	print();	
}
</script>
<script type="text/javascript">
function AddFavor(){	
	window.external.AddFavorite(window.location.href,window.document.title);
}
</script>
<!-- Baidu Button END -->
								<div class="clear"></div>
							</div>
							<div class="blank2"></div>
							<div class="gyzb">
								<div id="docEditor"><span id="editor_baidu"><b>责任编辑：</b>张鹏宇</span></div>
								<div id="docComment"><a href=http://pnews.hebei.com.cn:7001/m_comment/comment.jsp?news_id=010872580&channel_id=14702000000000000>[发表评论]</a></div>	
							</div>
						</div>
						<div class="docBox">
							<h3 class="boxTitle">相关新闻</h3>
							<div class="boxContent xgxw"><table width="100%"  >
<tr><td>&#8226;</td><td  ><a href="http://pingan.hebei.com.cn/system/2015/05/11/010847675.shtml"   target="_blank" >孟建柱会见蒙古国家安全委员会秘书恩赫图布辛</a></td></tr>
<tr><td>&#8226;</td><td  ><a href="http://pingan.hebei.com.cn/system/2015/05/05/010841845.shtml"   target="_blank" >孟建柱：更好地肩负起社会主义法治国家建设者推动者的责任</a></td></tr>
<tr><td>&#8226;</td><td  ><a href="http://pingan.hebei.com.cn/system/2015/05/04/010841351.shtml"   target="_blank" >孟建柱：提升司法裁判社会认同和司法公信力</a></td></tr>
<tr><td>&#8226;</td><td  ><a href="http://pingan.hebei.com.cn/system/2015/05/04/010841348.shtml"   target="_blank" >孟建柱：提升司法裁判社会认同和司法公信力</a></td></tr>
</table>
</div>
						</div>
						<div class="clear"></div>
						<div class="blank2"></div>
						<div class="blank2"></div>
					</div>
				</div>
			</div>
			<link href="http://pingan.hebei.com.cn/css/hbcaw_20140619.css" rel="stylesheet" type="text/css" />
			<div class="youce">
				<div class="com_name">
					<div class="com_name_1"><a href="http://pingan.hebei.com.cn/zydt/" target="_blank">中央动态</a></div>
					<div class="com_more_1"><a href="http://pingan.hebei.com.cn/zydt/" target="_blank">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<ul class="newsyou">
					<li><a href="http://pingan.hebei.com.cn/system/2014/06/22/010716897.shtml" target="_blank">孟建柱在京会见坦桑尼亚内政部部长奇卡维</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716415.shtml" target="_blank">奚晓明在江苏调研时强调大力推进环境资源审...</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716410.shtml" target="_blank">最高人民法院通报两岸司法互助工作情况</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716407.shtml" target="_blank">周强：凝聚智慧共识 共同推动法治中国建设</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716401.shtml" target="_blank">最高人民法院举行第二届特约监督员聘任大会</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716395.shtml" target="_blank">周强：在先进典型引领下凝聚力量振奋精神</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716392.shtml" target="_blank">最高检：全力推进电子检务工程科学发展</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716388.shtml" target="_blank">全国人大常委会将审议食品安全法修订草案</a></li>
				</ul>
				<div class="bla15"></div>
				<div class="com_name">
					<div class="com_name_1"><a href="http://pingan.hebei.com.cn/cabb/" target="_blank">长安播报</a></div>
					<div class="com_more_1"><a href="http://pingan.hebei.com.cn/cabb/" target="_blank">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<ul class="newsyou">
					<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716530.shtml" target="_blank">司法厅2014年国家司法考试有关事项公告</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716526.shtml" target="_blank">我省高考顺利结束 23日前后公布成绩</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716521.shtml" target="_blank">我省高考考场6日下午开放 考试全程录像 气温...</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716511.shtml" target="_blank">2014年国家司法考试6月16日报名</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716509.shtml" target="_blank">孟建柱：促进社区服刑人员更好地融入社会</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716505.shtml" target="_blank">我国开展严打暴恐专项行动</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716500.shtml" target="_blank">2012-2013年度全省政法综治优秀新闻作品评选...</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716475.shtml" target="_blank">公安部消防局全力确保上海亚信峰会消防安全</a></li>
				</ul>
				<div class="bla15"></div>
				<div class="com_name">
					<div class="com_name_1"><a href="http://pingan.hebei.com.cn/pacj/" target="_blank">平安创建</a></div>
					<div class="com_more_1"><a href="http://pingan.hebei.com.cn/pacj/" target="_blank">更多>></a></div>
				</div>
				<div class="bla10"></div>
				<ul class="newsyou">
					<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716566.shtml" target="_blank">邢台市桥西区深入推进平安创建工作</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716565.shtml" target="_blank">兴隆县检察院把握“稳、公、廉”三个字深入...</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716562.shtml" target="_blank">赞皇县检察院五项措施提高群众满意度</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716559.shtml" target="_blank">新乐市检察院公开征求意见建议</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716557.shtml" target="_blank">晋州市检察院征求意见促整改</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716554.shtml" target="_blank">巨鹿政法委群众路线教育实践活动有声有色</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716553.shtml" target="_blank">平安邯郸县 传达正能量</a></li>
<li><a href="http://pingan.hebei.com.cn/system/2014/06/21/010716550.shtml" target="_blank">定州市检察院积极探索办理刑事和解案件新机制</a></li>
				</ul>
			</div><!--<page cms="enorth webpublisher"  version="5.0.0 /2011101201" server_name="work.hebei.com.cn" parse_date="2014-06-22 13:19:35" cost="66" parse_result="0" input_mode="manual"></page>-->
			<div class="clear"></div>
			<div class="blank2"></div>
			<div class="blank2"></div>
			<div class="blank2"></div>
		</div>
	</div>
	<div class="bla15"></div>
	<div class="bla15"></div>
</div>

<div class="wrapper">
	<div id="box9">
		<div class="bottom9">
			
			主管单位：中共山西晋中市委政法委员会    晋中市社会管理综合治理委员会<br />
			备案序号：晋ICP备10001396号-1    技术支持：<a href="#">IBPD</a><br />
			联系我们： 0354-7393000    邮箱：jinzhongpingan@163.com 
		</div>
	</div>
	<div style="background-color:#e5e5e5;width:100%;">
		<div style="width:980px;margin:0 auto;text-align:center;">
			<script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/05/000/0000/60429979/CA050000000604299790000.js' type='text/javascript'%3E%3C/script%3E"));</script>
		</div>
	</div>
</div>
  </body>
</html>
