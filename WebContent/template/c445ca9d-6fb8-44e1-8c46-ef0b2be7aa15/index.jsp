<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/ibpdTag" prefix="mt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>${pageTitle }</title>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="${keywords }">
	<meta http-equiv="copyright" content="${copyright }">
	<meta http-equiv="description" content="${description }">
	
	<link rel="stylesheet" type="text/css" href="${cssPath }">

  </head>
  
  <body>
	<mt:nodeEach nodeId="-1">
          <li class="top">
           <div style="padding-left:8px;"><a
                   href="<mt:node valueVar="node" field="SHORTURL"/>" target="_blank"><mt:node valueVar="node" field="text"/></a></div>
          </li>
</mt:nodeEach>
  </body>
</html>
