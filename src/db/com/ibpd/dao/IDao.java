package com.ibpd.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.entity.baseEntity.IBaseEntity;

public abstract interface IDao<T extends IBaseEntity>
{
  public abstract void save(T entity);
  
  public abstract void clearCache();
  
  public abstract Session getCurrentSession();

  public abstract void update(T entity);

  public abstract void delete(Class<T> clazz, Serializable pk);

  public abstract T getEntity(Class<T> clazz, Serializable pk);

  public abstract Object getProperty(String paramString1, String paramString2);

  public abstract List<Object> getPropertyList(String paramString1, String paramString2);

  public abstract int saveOrUpdate(String paramString, Class<T> paramClass);

  public abstract List<T> getEntityList(String jpql, List<HqlParameter> hqlParameterList,String orderType, Integer pageSize, Integer pageIndex);

  public abstract int createTable(Class<T> paramClass);

  public abstract List<T> getSQLResultList(String paramString, Class<T> paramClass);

  public abstract List<T> getSQLResultList(String paramString, Class<T> paramClass, Object[] paramArrayOfObject);

  public abstract int executeSqlString(String paramString, Class<T> paramClass);

  public abstract int executeSqlString(String paramString, Class<T> paramClass, Object[] paramArrayOfObject);

  public abstract void executeStoreProcedure(String paramString);

  public abstract Object getSingleValueByCallStoreProctdure(String paramString);

  public abstract List<T> getResultListByCallStoreProctdure(String paramString, Class<T> paramClass);

  public abstract Long getItemCount(String paramString1, String paramString2,List<HqlParameter> hqlParameterList);

  public abstract void batchSave(List<T> paramList);
}

/* Location:           E:\MyEclipse 8.5\henuoCMS\WebRoot\WEB-INF\lib\ibpd\ibpdDao-v1.0.jar
 * Qualified Name:     com.ibpd.dao.IDao
 * JD-Core Version:    0.5.4
 */