/*    */ package com.ibpd.dao;
/*    */ 
/*    */ import java.io.PrintStream;
/*    */ import org.hibernate.HibernateException;
/*    */ import org.hibernate.Session;
/*    */ import org.hibernate.SessionFactory;
/*    */ import org.hibernate.cfg.AnnotationConfiguration;
/*    */ import org.hibernate.cfg.Configuration;
/*    */ 
/*    */ public class IbpdSessionFactory
/*    */ {
/*  9 */   private static String CONFIG_FILE_LOCATION = "/hibernateConfig.xml";
/* 10 */   private static final ThreadLocal<Session> threadLocal = new ThreadLocal();
/* 11 */   private static Configuration configuration = new AnnotationConfiguration();
/*    */   private static SessionFactory sessionFactory;
/* 13 */   private static String configFile = CONFIG_FILE_LOCATION;
/*    */ 
/*    */   static {
/*    */     try {
/* 17 */       configuration.configure(configFile);
/* 18 */       sessionFactory = configuration.buildSessionFactory();
/*    */     } catch (Exception e) {
/* 20 */       System.err
/* 21 */         .println("%%%% Error Creating SessionFactory %%%%");
/* 22 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ 
/*    */   public static Session getSession() throws HibernateException
/*    */   {
			threadLocal.set(null);//由于同一个session中的一级缓存无法与数据库保持一致，经常出现脏数据，这里先把一级缓存禁用掉，会对性能造成影响，完了再好好研究一下这里。
/* 28 */     Session session = (Session)threadLocal.get();
/*    */ 
/* 30 */     if ((session == null) || (!session.isOpen())) {
/* 31 */       if (sessionFactory == null) {
/* 32 */         rebuildSessionFactory();
/*    */       }
/* 34 */       session = (sessionFactory != null) ? sessionFactory.openSession() : null;
/* 35 */       threadLocal.set(session);
/*    */     }
/*    */ 
/* 38 */     return session;
/*    */   }
/*    */ 
/*    */   public static void rebuildSessionFactory() {
/*    */     try {
/* 43 */       configuration.configure(configFile);
/* 44 */       sessionFactory = configuration.buildSessionFactory();
/*    */     } catch (Exception e) {
/* 46 */       System.err
/* 47 */         .println("%%%% Error Creating SessionFactory %%%%");
/* 48 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void closeSession() throws HibernateException {
/* 52 */     Session session = (Session)threadLocal.get();
/* 53 */     threadLocal.set(null);
/*    */ 
/* 55 */     if (session != null)
/* 56 */       session.close();
/*    */   }
/*    */ 
/*    */   public static SessionFactory getSessionFactory()
/*    */   {
/* 65 */     return sessionFactory;
/*    */   }
/*    */   public static void setConfigFile(String configFile) {
/* 68 */     configFile = configFile;
/* 69 */     sessionFactory = null;
/*    */   }
/*    */ 
/*    */   public static Configuration getConfiguration() {
/* 73 */     return configuration;
/*    */   }
/*    */ 
/*    */   public static void main(String[] args)
/*    */   {
/*    */   }
/*    */ }

/* Location:           E:\MyEclipse 8.5\henuoCMS\WebRoot\WEB-INF\lib\ibpd\ibpdDao-v1.0.jar
 * Qualified Name:     com.ibpd.dao.IbpdSessionFactory
 * JD-Core Version:    0.5.4
 */