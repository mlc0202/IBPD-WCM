/*    */ package com.ibpd.dao.impl;
/*    */ 
/*    */ import java.lang.reflect.InvocationHandler;
/*    */ import java.lang.reflect.Method;
/*    */ 
/*    */ public class ServiceProxy
/*    */   implements InvocationHandler
/*    */ {
/*    */   private Object delegate;
/*    */ 
/*    */   public ServiceProxy(Object obj)
/*    */   {
/* 11 */     this.delegate = obj;
/*    */   }
/*    */ 
/*    */   public Object invoke(Object proxy, Method method, Object[] args)
/*    */     throws Throwable
/*    */   {
/* 17 */     Object retObj = method.invoke(this.delegate, args);
/* 18 */     return retObj;
/*    */   }
/*    */ }

/* Location:           E:\MyEclipse 8.5\henuoCMS\WebRoot\WEB-INF\lib\ibpd\ibpdService-v1.0.jar
 * Qualified Name:     com.ibpd.service.ServiceProxy
 * JD-Core Version:    0.5.4
 */