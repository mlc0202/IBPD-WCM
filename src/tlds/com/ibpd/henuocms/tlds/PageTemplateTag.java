package com.ibpd.henuocms.tlds;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;

import org.h2.util.StringUtils;
import org.hsqldb.lib.StringUtil;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.TimeHelper;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.web.controller.web.WebSite;

public class PageTemplateTag extends SimpleBaseTag {
	private Object value;
	private String valueVar;
	private String field="";
	private String dateFormat="";
	private String escape="";
	private String templateId="";
	private PageTemplateEntity node=null;
	@Override
	public void doTag() throws JspException, IOException {
		_init();
		if(node==null){
            return ;
            }
	        getJspContext().getOut().write(getFieldValue(node,field));
	        if(getJspBody()!=null)
	        	getJspBody().invoke(null);
	}
	protected void _init() {
		if(!StringUtils.isNullOrEmpty(valueVar)){
			Object o=getJspContext().getAttribute(valueVar);
			if(o==null){
				node=null;
			}else{
				node=(PageTemplateEntity) o;
			}
		}else if(value!=null){
			node=(PageTemplateEntity) value;
		}else if(!StringUtils.isNullOrEmpty(templateId)){
			if(templateId.toLowerCase().trim().equals("OWNER")){
				templateId=getRequest().getParameter("templateId");
			}else if(StringUtils.isNumber(templateId)){
				
			}else{
				templateId="-1";
			}
			Long currentNodeId=Long.valueOf(templateId);
			if(!currentNodeId.equals(-1L)){
				IPageTemplateService nodeServ=(IPageTemplateService) ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class);
				PageTemplateEntity n=nodeServ.getEntityById(currentNodeId);
				this.node=n;
			}
		}else{
			node=null;
		}
	}
	private String getFieldValue(PageTemplateEntity tempEntity,String fieldName){
		if(tempEntity==null)
			return "";
		Object obj=tempEntity;
		if(obj==null)
			return "";
		try {
			HttpServletRequest request= (HttpServletRequest) getRequest();
			String path = request.getContextPath();
			if(!checkViewerMode()){
				path="";
			}
			if(fieldName.toUpperCase().trim().equals("FULLURL")){
				String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
				String dir=getTemplateUrlDir(tempEntity);
				return basePath+dir;
			}else if(fieldName.toUpperCase().trim().equals("SHORTURL")){
				path += "/";
				String dir=getTemplateUrlDir(tempEntity);
				return path+dir;
			}
			String getFunName = "get" + fieldName.substring(0, 1).toUpperCase()
					+ fieldName.substring(1);
			if (!existMethod(getFunName, obj)) {
				return "[error]field���ô���";
			} else {
				Method funMethod = obj.getClass().getMethod(getFunName,
						new Class[] {});
				return getReturnValue(funMethod, obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	private Boolean existMethod(String mtName,Object obj){
		Method[] mts=obj.getClass().getMethods();
		for(Method mt:mts){
			if(mt.getName().equals(mtName)){
				return true;
			}
		}
		return false;
	}
	private String getTemplateUrlDir(PageTemplateEntity temp){
		if(temp==null)
			return "";
		return "template/"+temp.getTemplateFilePath()+"/";
	}
	private String getReturnValue(Method mthod,Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Object rtn=mthod.invoke(obj, null);
		if(mthod.getReturnType().getSimpleName().equals("Date")){
			if(!StringUtil.isEmpty(dateFormat)){
				return TimeHelper.getDate((Date)rtn, dateFormat);
			}else{
				return escapeValue(rtn.toString(),this.getEscape());
			}
		}
		return escapeValue(rtn.toString(),this.getEscape());
		
	}

	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getValueVar() {
		return valueVar;
	}
	public void setValueVar(String valueVar) {
		this.valueVar = valueVar;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public String getEscape() {
		return escape;
	}
	public void setEscape(String escape) {
		this.escape = escape;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String tempId) {
		this.templateId = tempId;
	}
}