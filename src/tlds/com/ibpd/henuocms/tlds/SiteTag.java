package com.ibpd.henuocms.tlds;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;

import org.h2.util.StringUtils;
import org.hsqldb.lib.StringUtil;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.TimeHelper;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.web.controller.manage.SubSite;
import com.ibpd.henuocms.web.controller.web.WebSite;

public class SiteTag extends SimpleBaseTag {
	private Object value;
	private String valueVar;
	private String field="";
	private String dateFormat="";
	private String escape="";
	private String siteId="";
	
	private SubSiteEntity node=null;
	@Override
	public void doTag() throws JspException, IOException {
		_init();
		if(node==null){
            return ;
            }
	        getJspContext().getOut().write(getFieldValue(node,field));
	        if(getJspBody()!=null)
	        	getJspBody().invoke(null);
	}
	protected void _init() {
		if(!StringUtils.isNullOrEmpty(valueVar)){
			Object o=getJspContext().getAttribute(valueVar);
			if(o==null){
				node=null;
			}else{
				node=(SubSiteEntity) o;
			}
		}else if(value!=null){
			node=(SubSiteEntity) value;
		}else if(!StringUtils.isNullOrEmpty(siteId)){
			if(siteId.toLowerCase().trim().equals("OWNER")){
				siteId=getRequest().getParameter("siteId");
			}else if(StringUtils.isNumber(siteId)){
				
			}else{
				siteId="-1";
			}
			Long currentNodeId=Long.valueOf(siteId);
			if(!currentNodeId.equals(-1L)){
				ISubSiteService nodeServ=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
				SubSiteEntity n=nodeServ.getEntityById(currentNodeId);
				SubSiteEntity ext=n;
				node=ext;
			}
		}else{
			node=null;
		}
	}
	private String getFieldValue(SubSiteEntity siteEntity,String fieldName){
		if(siteEntity==null)
			return "";
		Object obj=siteEntity;
		if(obj==null)
			return "";
		try {
			HttpServletRequest request= (HttpServletRequest) getRequest();
			if(fieldName.toUpperCase().trim().equals("FULLURL")){
				String path = request.getContextPath();
				String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
				String dir=getSiteUrlDir(siteEntity);
				return basePath+dir;
			}else if(fieldName.toUpperCase().trim().equals("SHORTURL")){
				String path = request.getContextPath()+"/";
				String dir=getSiteUrlDir(siteEntity);
				return path+dir;
			}
			String getFunName = "get" + fieldName.substring(0, 1).toUpperCase()
					+ fieldName.substring(1);
			if (!existMethod(getFunName, obj)) {
				return "[error]field���ô���";
			} else {
				Method funMethod = obj.getClass().getMethod(getFunName,
						new Class[] {});
				return getReturnValue(funMethod, obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	private Boolean existMethod(String mtName,Object obj){
		Method[] mts=obj.getClass().getMethods();
		for(Method mt:mts){
			if(mt.getName().equals(mtName)){
				return true;
			}
		}
		return false;
	}
	private String getSiteUrlDir(SubSiteEntity site){
		if(site==null)
			return "";
		String dir="";
		String type=getRequest().getParameter("type"); 
		if(!StringUtils.isNullOrEmpty(type) && type.trim().toLowerCase().equals(WebSite.WEBPAGE_TYPE_VIEWER.toString())){
			dir="sites/";
		} 

		return dir+site.getDirectory()+"/index.html";
	}
	private String getReturnValue(Method mthod,Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Object rtn=mthod.invoke(obj, null);
		if(mthod.getReturnType().getSimpleName().equals("Date")){
			if(!StringUtil.isEmpty(dateFormat)){
				return TimeHelper.getDate((Date)rtn, dateFormat);
			}else{
				return escapeValue(rtn.toString(),this.getEscape());
			}
		}
		return escapeValue(rtn.toString(),this.getEscape());
		
	}

	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getValueVar() {
		return valueVar;
	}
	public void setValueVar(String valueVar) {
		this.valueVar = valueVar;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public String getEscape() {
		return escape;
	}
	public void setEscape(String escape) {
		this.escape = escape;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
}