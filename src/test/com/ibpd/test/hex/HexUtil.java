package com.ibpd.test.hex;

public class HexUtil {
	/** 
	 * byte转哈希 
	 * @param b 
	 * @return 
	 */  
	public static String byte2hex(byte[] b) {  
	    String hs = "";  
	    String stmp = "";  
	    for (int n = 0; n < b.length; n++) {  
	        stmp = Integer.toHexString(b[n] & 0xFF);  
	        if (stmp.length() == 1)  
	            hs += ("0" + stmp);  
	        else  
	            hs += stmp;  
	    }  
	    return hs.toUpperCase();  
	}  
	  
	/** 
	 * 哈希转byte 
	 * @param b 
	 * @return 
	 */  
	public static byte[] hex2byte(byte[] b) {  
	    if ((b.length % 2) != 0)  
	        throw new IllegalArgumentException("长度不是偶数");  
	  
	    byte[] b2 = new byte[b.length / 2];  
	  
	    for (int n = 0; n < b.length; n += 2) {  
	        String item = new String(b, n, 2);  
	        b2[n / 2] = (byte) Integer.parseInt(item, 16);  
	    }  
	    return b2;  
	}  
	public static void main(String[] args){
		String b="ThisIsATestHexCodeSample";
		String hex=HexUtil.byte2hex(b.getBytes());
		System.out.println("hex="+hex);
		byte[] bs=HexUtil.hex2byte(hex.getBytes());
		System.out.println("b="+(new String(bs)));
	}
}
