package com.ibpd.test.sort;

/**
 * 各种排序方法汇总
 * 
 * @author mg
 * 
 */
public class SortUtil {
	/**
	 * 折半排序
	 * 
	 * @param array
	 */
	public static void halfSort(int[] array) {
		int low, high, mid;
		int tmp, j;
		for (int i = 1; i < array.length; i++) {
			tmp = array[i];
			low = 0;
			high = i - 1;
			while (low <= high) {
				mid = low + (high - low) / 2;
				if (array[mid] > tmp)
					high = mid - 1;
				else
					low = mid + 1;
			}
			for (j = i - 1; j > high; j--) {
				array[j + 1] = array[j];
			}
			array[high + 1] = tmp;
		}
	}

	/**
	 * 快速排序
	 * 
	 * @param a
	 * @param low
	 * @param high
	 */
	public static void quickSortByThree(int[] a, int low, int high) {
		// if (low >= high)
		// return;
		// int mid = low + (high - low) / 2;
		// if (a[mid] < a[low])
		// Sort.swap(a, mid, low);// 交换mid和low
		// if (a[low] > a[high])
		// Sort.swap(a, low, high);
		// if (a[mid] > a[high])
		// Sort.swap(a, mid, high);
		// Sort.swap(a, mid, high - 1);
		// int pivot = a[high - 1];
		// int i, j;
		// for (i = low, j = high - 1;;) {
		// while (i < high - 1 && a[++i] < pivot)
		// ;
		// while (j > low && a[--j] > pivot)
		// ;
		// if (i >= j)
		// break;
		// Sort.swap(a, i, j);
		// }
		// Sort.swap(a, i, high - 1);
		// quickSortByThree(a, low, i - 1);
		// quickSortByThree(a, i + 1, high);
	}

	/**
	 * 快速排序
	 * 
	 * @param a
	 * @param low
	 * @param high
	 */
	public static void quickSortByMid(int[] a, int low, int high) {
		if (low >= high)
			return;
		// 分割
		int pivot = a[low];// 基准值
		int i = low, j = high;
		while (i < j) {
			while (i < j && a[j] >= pivot)
				--j;
			a[i] = a[j];
			while (i < j && a[i] <= pivot)
				++i;
			a[j] = a[i];
		}
		a[i] = pivot;
		quickSortByMid(a, low, i - 1);
		quickSortByMid(a, i + 1, high);
	}

	/**
	 * 归并排序
	 * 
	 * @param a
	 * @param tmp
	 * @param left
	 * @param right
	 */
	public static void mergeSort(int[] a, int[] tmp, int left, int right) {
		if (left < right) {
			int mid = left + (right - left) / 2;
			mergeSort(a, tmp, left, mid);// 左排序
			mergeSort(a, tmp, mid + 1, right);// 右排序
			merge(a, tmp, left, mid + 1, right);// 左右合并
		}
	}

	/**
	 * 归并排序
	 * 
	 * @param a
	 * @param tmp
	 * @param left
	 * @param rightPos
	 * @param right
	 */
	public static void merge(int[] a, int[] tmp, int left, int rightPos,
			int right) {
		int leftEnd = rightPos - 1;
		int tmpPos = left;
		int num = right - left + 1;
		while (left <= leftEnd && rightPos <= right) {
			if (a[left] < a[rightPos]) {
				tmp[tmpPos++] = a[left++];
			} else {
				tmp[tmpPos++] = a[rightPos++];
			}
		}
		while (left <= leftEnd) {
			tmp[tmpPos++] = a[left++];
		}
		while (rightPos <= right) {
			tmp[tmpPos++] = a[rightPos++];
		}
		for (int i = 0; i < num; i++, right--) {
			a[right] = tmp[right];
		}
	}

	// 带增量的插入排序
	/**
	 * 希尔排序
	 */
	public static void shellSort(int[] array) {
		// int len = array.length;
		// int h = 1;
		// while (h < len)
		// h = h * 3 + 1;
		// while (h >= 1) {
		// for (int i = 1; i < len; i++) {
		// for (int j = i; j >= h; j = j - h) {
		// if (array[j] < array[j - h]) {
		// Sort.swap(array, j, j - h);// 交换j和j-h
		// } else
		// break;
		// }
		// }
		// h = h / 3;
		// }
	}

	/**
	 * 选择排序
	 * 
	 * @param array
	 */
	public static void selectSort(int[] array) {
		// for (int i = 0; i < array.length - 1; i++) {
		// int min = i;
		// for (int j = i + 1; j < array.length; j++) {
		// if (array[j] < array[min]) {
		// min = j;
		// }
		// }
		// Sort.swap(array, i, min);// 交换i和min
		// }
	}

	/**
	 * 插入排序
	 * 
	 * @param array
	 */
	public static void insertSort(int[] array) {
		// int len = array.length;
		// for (int i = 1; i < len; i++) {
		// for (int j = i; j > 0; j--) {
		// if (array[j] < array[j - 1]) {
		// Sort.swap(array, j, j - 1);// 交换j和j-1
		// } else
		// break;
		// }
		// }
	}

	/**
	 * 冒泡排序
	 * 
	 * @param array
	 */
	public static void bubbleSort(int[] array) {
		// 下两种选择一种即可
		// /方式1
		// for (int i = array.length - 1; i > 0; i--) {
		// for (int j = 0; j < i; j++) {
		// if (array[j] > array[j + 1]) {
		// Sort.swap(array, j, j + 1);//交换j和j+1
		// }
		// }
		// }
		// /方式2
		// for(int i=0;i<array.length;i++){
		// for(int j=0;j<array.length-1-i;j++){
		// if(array[j]>array[j+1]){
		// Sort.swap(array, j, j+1);//交换j和j+1
		// }
		// }
		// }
	}
}
