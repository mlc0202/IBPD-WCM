package com.ibpd.test.ftp;
import java.io.File;  
import java.io.FileInputStream;  
import org.apache.commons.net.ftp.FTPClient;  
import org.apache.commons.net.ftp.FTPReply;  
import org.apache.commons.net.io.CopyStreamEvent;
import org.apache.commons.net.io.CopyStreamListener;

public class test {    
     
    private  FTPClient ftp;    
    /** 
     *  
     * @param path 上传到ftp服务器哪个路径下    
     * @param addr 地址 
     * @param port 端口号 
     * @param username 用户名 
     * @param password 密码 
     * @return 
     * @throws Exception 
     */  
    private  boolean connect(String path,String addr,int port,String username,String password) throws Exception {    
        boolean result = false;    
        ftp = new FTPClient();    
        int reply;    
        ftp.setCopyStreamListener(new CopyStreamListener(){

			public void bytesTransferred(CopyStreamEvent arg0) {
				System.out.println("total:"+arg0.getStreamSize()+" local:"+arg0.getTotalBytesTransferred());
				
			}

			public void bytesTransferred(long arg0, int arg1, long arg2) {
				System.out.println(arg0+" "+arg1+" "+arg2);
				
			}
        	
        });
       ftp.connect(addr,port);    
        ftp.login(username,password);    
        ftp.setControlEncoding("UTF-8");
        ftp.enterLocalPassiveMode();
         ftp.setFileType(FTPClient.BINARY_FILE_TYPE);  
         ftp.setFileTransferMode( FTPClient.STREAM_TRANSFER_MODE);
        reply = ftp.getReplyCode();    
        if (!FTPReply.isPositiveCompletion(reply)) {    
            ftp.disconnect();    
            return result;    
        }    
        ftp.changeWorkingDirectory(path);    
        result = true;    
        return result;    
    }    
    /** 
     *  
     * @param file 上传的文件或文件夹 
     * @throws Exception 
     */  
    private void upload(File file) throws Exception{    
        if(file.isDirectory()){         
            ftp.makeDirectory(file.getName());              
            ftp.changeWorkingDirectory(file.getName());    
            String[] files = file.list();           
            for (int i = 0; i < files.length; i++) {    
                File file1 = new File(file.getPath()+"\\"+files[i] );    
                if(file1.isDirectory()){    
                    upload(file1);    
                    ftp.changeToParentDirectory();    
                }else{                  
                    File file2 = new File(file.getPath()+"\\"+files[i]);    
                    FileInputStream input = new FileInputStream(file2);    
                    Boolean r=ftp.storeFile(file2.getName(), input);    
                    System.out.println("=====文件上传:"+file2.getName()+":"+r);
                    input.close();                          
                }               
            }    
        }else{    
            File file2 = new File(file.getPath());    
            FileInputStream input = new FileInputStream(file2);    
            Boolean r=ftp.storeFile(file2.getName(), input);    
            System.out.println("=====文件上传:"+file2.getName()+":"+r);
           input.close();      
        }    
    }    
   public static void main(String[] args) throws Exception{  
      test t = new test();  
      t.connect("/", "118.244.194.33", 8021, "ftpuser", "!2#4%henuojituan");  
      
      File file = new File("D:\\apache-tomcat-6.0.39\\webapps\\henuoCMS\\sites\\ssgfwssc\\bk");  
      t.upload(file);
      
   }  
}  