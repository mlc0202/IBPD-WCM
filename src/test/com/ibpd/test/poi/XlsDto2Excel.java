package com.ibpd.test.poi;
//该类主要负责向Excel(2003版)中插入数据

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
 
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
 
 
public class XlsDto2Excel {
 
    /**
     * 
     * @param xls
     *            XlsDto实体类的一个对象
     * @throws Exception
     *             在导入Excel的过程中抛出异常
     */
    public static void xlsDto2Excel(List<XlsDto> xls) throws Exception {
        // 获取总列数
        int CountColumnNum = 5;
        // 创建Excel文档
        HSSFWorkbook hwb = new HSSFWorkbook();
        XlsDto xlsDto = null;
        // sheet 对应一个工作页
        HSSFSheet sheet = hwb.createSheet("pldrxkxxmb");
        HSSFRow firstrow = sheet.createRow(0); // 下标为0的行开始
        HSSFCell[] firstcell = new HSSFCell[CountColumnNum];
        String[] names = new String[CountColumnNum];
        names[0] = "学号";
        names[1] = "姓名";
        names[2] = "学院";
        names[3] = "课程名";
        names[4] = "成绩";
        for (int j = 0; j < CountColumnNum; j++) {
            firstcell[j] = firstrow.createCell(j);
            HSSFCellStyle style = hwb.createCellStyle();
            style.setFillBackgroundColor(new HSSFColor.BLUE().getIndex());
            style.setFillForegroundColor(new HSSFColor.GREEN().getIndex());
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
//            style.setFillPattern(HSSFCellStyle.ALIGN_CENTER);
            firstcell[j].setCellStyle(style);
            firstcell[j].setCellValue(new HSSFRichTextString(names[j]));
        }
        for (int i = 0; i < xls.size(); i++) {
            // 创建一行
            HSSFRow row = sheet.createRow(i + 1);
            // 得到要插入的每一条记录
            xlsDto = xls.get(i);
            for (int colu = 0; colu <= 4; colu++) {
                // 在一行内循环
            	
                HSSFCell xh = row.createCell(0);
                if(i % 2 ==0)
                	xh.setCellStyle(getStyle(hwb));
                xh.setCellValue(xlsDto.getXh());
                
                HSSFCell xm = row.createCell(1);
                if(i % 2 ==0)
                	xm.setCellStyle(getStyle(hwb));
                xm.setCellValue(xlsDto.getXm());
                HSSFCell yxsmc = row.createCell(2);
                if(i % 2 ==0)
                	yxsmc.setCellStyle(getStyle(hwb));
                yxsmc.setCellValue(xlsDto.getYxsmc());
                HSSFCell kcm = row.createCell(3);
                if(i % 2 ==0)
                	kcm.setCellStyle(getStyle(hwb));
                kcm.setCellValue(xlsDto.getKcm());
                HSSFCell cj = row.createCell(4);
                if(i % 2 ==0)
                	cj.setCellStyle(getStyle(hwb));
                cj.setCellValue(xlsDto.getCj());
//                lxsDto.get;
            }
        }
        // 创建文件输出流，准备输出电子表格
        OutputStream out = new FileOutputStream("h://book14.xls");
        hwb.write(out);
        out.close();
        System.out.println("数据库导出成功");
    }
 
    private static HSSFCellStyle getStyle(HSSFWorkbook hwb){
        HSSFCellStyle style = hwb.createCellStyle();
        style.setFillBackgroundColor(HSSFColor.BLUE_GREY.index);
        style.setFillForegroundColor(HSSFColor.AQUA.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(HSSFCellStyle.BORDER_DOTTED);
        return style;
    }
}