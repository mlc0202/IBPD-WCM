package com.ibpd.test.oa;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.hsqldb.lib.StringUtil;
import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.TableColumn;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.htmlparser.util.SimpleNodeIterator;
public class TestConnectOA_release {
	public static void main(String[] args) {
		postForm("lh","1");
	}
	public static void postForm(String uname,String psd) {
		DefaultHttpClient httpclient=new DefaultHttpClient();		
		HttpPost httppost = new HttpPost("http://192.105.128.201/userpass.aspx?type=1");
		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
		formparams.add(new BasicNameValuePair("__VIEWSTATE", "/wEPDwUJMTI0NzA4MzkzZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAgUMSW1hZ2VCdXR0b24xBQxJbWFnZUJ1dHRvbjKSbFu4aDIE2XIIhAH93Hfs2tAHtHNUf/wOe3OMmHwizg=="));
		formparams.add(new BasicNameValuePair("__EVENTVALIDATION", "/wEWCwKNy47qBwKvruq2CALyveCRDwLSwpnTCALSwtXkAgKM54rGBgLE+s7SBAKCmc27AQKOl4KEBgLssq+LCwKalKb+BpkTLlkG8THQoaiw8k0Kb654GRrUFeZ3+lOGBFNVCKFg"));
		formparams.add(new BasicNameValuePair("UserName", uname));
		formparams.add(new BasicNameValuePair("password", psd));
		formparams.add(new BasicNameValuePair("ImageButton1.x", "29"));
		formparams.add(new BasicNameValuePair("ImageButton1.y", "10"));
		UrlEncodedFormEntity uefEntity;
		try {
			uefEntity = new UrlEncodedFormEntity(formparams, "gb2312");
			httppost.setEntity(uefEntity);
			httppost.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httppost.setHeader("Accept-Encoding","gzip, deflate");
			httppost.setHeader("Accept-Language","zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			httppost.setHeader("Connection","keep-alive");
			httppost.setHeader("Referer","http://192.105.128.201/userpass.aspx?type=1");
			httppost.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
			System.out.println("executing request " + httppost.getURI());
			CloseableHttpResponse response = httpclient.execute(httppost);
			try {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					get(httpclient.getCookieStore());
				}
			} finally {
				response.close();
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpclient.close();
		}
	}
	public static void get(CookieStore cookie) {
		DefaultHttpClient httpclient=new DefaultHttpClient();		
		httpclient.setCookieStore(cookie);
		try {
			HttpGet httpget = new HttpGet("http://192.105.128.201/desktop/MailList.aspx?FolderID=00000000000000000000000000000001&FolderName=收件箱&id=460");
			System.out.println("executing request " + httpget.getURI());
			httpget.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpget.setHeader("Accept-Encoding","gzip, deflate");
			httpget.setHeader("Accept-Language","zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			httpget.setHeader("Connection","keep-alive");
			httpget.setHeader("Referer","http://192.105.128.201/userpass.aspx?type=1");
			httpget.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
			CloseableHttpResponse response = httpclient.execute(httpget);
			try {
				HttpEntity entity = response.getEntity();
				System.out.println("--------------------------------------");
				System.out.println(response.getStatusLine());
				if (entity != null) {
					System.out.println("Response content length: " + entity.getContentLength());
					String cnt=EntityUtils.toString(entity);
					checkContent(cnt,cookie);
				}
				System.out.println("------------------------------------");
			} finally {
				response.close();
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpclient.close();
		}
	}
	private static void checkContent(String cnt,CookieStore cookie){
		Parser myParser = Parser.createParser(cnt, "GB2312");
		String filterStr = "td";
		TDFilter filter = new TDFilter(filterStr);
		
		NodeList nodeList;
		try {
			nodeList = myParser.extractAllNodesThatMatch(filter);
			System.out.println("共找到"+nodeList.size()+"个项目");
			Integer i=0;
			System.out.println(nodeList.size()/6);
			List<OAMailEntity> mailList=new ArrayList<OAMailEntity>();
			OAMailEntity mail=new OAMailEntity();
			String txt="";
			String link="";
			for(Node n:nodeList.toNodeArray()){
				link="";
				txt="";
				TableColumn c=(TableColumn)n;
				SimpleNodeIterator child=c.children();
				if(child!=null){
					if(child.hasMoreNodes()){
						Node node=child.nextNode();
						txt=node.getText();
						if(node.getClass().getSimpleName().equals("TextNode")){
							TextNode tn=(TextNode) node;
							txt=tn.getText().trim();
						}
						if(node.getClass().getSimpleName().equals("LinkTag")){
							LinkTag l=(LinkTag) node;
							link=l.getLink();
							txt=l.getLinkText().trim();
						}
					}
				}
				if(!StringUtil.isEmpty(txt)){
					if(i==0){
						mail=new OAMailEntity();
						mail.setSource(txt);
					}else if(i==1){
						mail.setTitle(txt);
						mail.setLink(link);
					}else if(i==2){
						mail.setDate(txt);
					}else if(i==3){
						mail.setBinary(txt);
					}else if(i==4){
						mail.setState(txt);
					}else if(i==5){
						mailList.add(mail);
						i=-1;
					}					
					i++;
				}
			}
			for(OAMailEntity m:mailList){
				System.out.println("=="+m.getTitle()+"\t"+m.getSource()+"\t"+m.getLink());
			}
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void download(CookieStore cookie){
		List<OAMailEntity> mailList=OAMailUtil.getMailList();
		for(OAMailEntity m:mailList){
			DefaultHttpClient httpclient=new DefaultHttpClient();		
			httpclient.setCookieStore(cookie);
			try {
				HttpGet httpget = new HttpGet("http://192.105.128.201/desktop/"+m.getSource());
				System.out.println("executing request " + httpget.getURI());
				httpget.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpget.setHeader("Accept-Encoding","gzip, deflate");
				httpget.setHeader("Accept-Language","zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
				httpget.setHeader("Connection","keep-alive");
				httpget.setHeader("Referer","http://192.105.128.201/userpass.aspx?type=1");
				httpget.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
				CloseableHttpResponse response = httpclient.execute(httpget);
				try {
					HttpEntity entity = response.getEntity();
					System.out.println("--------------------------------------");
					System.out.println(response.getStatusLine());
					if (entity != null) {
						System.out.println("Response content length: " + entity.getContentLength());
						String cnt=EntityUtils.toString(entity);
						System.out.println(cnt);
						Parser myParser = Parser.createParser(cnt, "GB2312");
						String filterStr = "span";
						SpanFilter filter = new SpanFilter(filterStr);
						
						NodeList nodeList;
						try {
							myParser.extractAllNodesThatMatch(filter);
						} catch (ParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					System.out.println("------------------------------------");
				} finally {
					response.close();
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				httpclient.close();
			}
			
		}
		
	}
}
