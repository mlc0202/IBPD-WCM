package com.ibpd.test.oa;
import java.util.List;

import org.htmlparser.Node;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;


public final class SpanFilter extends TagNameFilter {

	public SpanFilter(String filterStr) {
		super(filterStr);
	}

	@Override
	public boolean accept(Node node) {
		if(node.getText().contains("span id=\"AttachOriFileName\"")){
			return true;
		}else{
			return false;
		}
	}

}
