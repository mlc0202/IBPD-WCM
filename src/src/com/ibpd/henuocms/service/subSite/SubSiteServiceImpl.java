package com.ibpd.henuocms.service.subSite;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
@Transactional
@Service("subSiteService")
public class SubSiteServiceImpl extends BaseServiceImpl<SubSiteEntity> implements ISubSiteService {
	public SubSiteServiceImpl(){
		super();
		this.tableName="SubSiteEntity";
		this.currentClass=SubSiteEntity.class;
		this.initOK();
	} 

	public PageTemplateEntity getPageTemplate(Long siteId, Integer type) {
		Long pId=getPageTemplateId(siteId,type);
		if(pId==-1L)
			return null;
		IPageTemplateService ptServ=(IPageTemplateService) ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class);
		return ptServ.getEntityById(pId);
	}

	public Long getPageTemplateId(Long siteId, Integer type) {
		if(siteId==null)
			return -1L;
		SubSiteEntity site=this.getEntityById(siteId);
		Long tempId=-1L;
		if(type==PageTemplateEntity.TEMPLATE_TYPE_SITE){
			tempId=site.getSitePageTemplateId();
		}else if(type==PageTemplateEntity.TEMPLATE_TYPE_NODE){
			tempId=site.getNodePageTemplateId();
		}else if(type==PageTemplateEntity.TEMPLATE_TYPE_CONTENT){
			tempId=site.getContentpageTemplateId();
		}else if(type==PageTemplateEntity.TEMPLATE_TYPE_COMMENT){
			tempId=site.getCommentpageTemplateId();
		}
		return tempId;
	}
}
