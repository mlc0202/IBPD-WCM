package com.ibpd.henuocms.service.styleTemplate;

import java.util.ArrayList;
import java.util.List;

import org.hsqldb.lib.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.ext.PageTemplateExtEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
@Transactional
@Service("styleTemplateService")
public class StyleTemplateServiceImpl extends BaseServiceImpl<StyleTemplateEntity> implements IStyleTemplateService {
	public StyleTemplateServiceImpl(){
		super();
		this.tableName="StyleTemplateEntity";
		this.currentClass=StyleTemplateEntity.class;
		this.initOK();
	}

	public List<StyleTemplateEntity> getList(Long[] siteIds, Long NodeId,
			Integer pageSize, Integer pageIndex, String orderType,
			String filterString) {
		List<StyleTemplateEntity> tempExtList=new ArrayList<StyleTemplateEntity>();
		if(siteIds!=null && siteIds.length>0){
			//����վ�� ID��ģ��
			Long ids[]=siteIds;
			String hql="from "+this.getTableName()+" where ";
			for(Long id:ids){
				if(id!=null)
					hql+="subSiteId="+id.toString()+" or ";
			} 
			if(hql.endsWith(" or ")){
				hql=hql.substring(0, hql.length()-4);
				hql=hql+((filterString==null)?"":" and "+filterString);
				tempExtList=search(hql,pageSize,pageIndex,orderType);
			}
			return tempExtList;
		}else
			return null;
		}
	private List<StyleTemplateEntity> search(String hql,Integer pageSize,Integer pageIndex,String order){
		List<StyleTemplateEntity> tempExtList=this.getList(hql, null, order, pageSize, pageIndex);
		return tempExtList;
	}
}
