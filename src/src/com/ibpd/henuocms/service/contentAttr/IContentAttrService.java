package com.ibpd.henuocms.service.contentAttr;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;

public interface IContentAttrService extends IBaseService<ContentAttrEntity> {
	/**
	 * 根据内容ID检索出 内容对应的内容属性实体对象
	 * @param contentId
	 * @return
	 */
	ContentAttrEntity getContentAttr(Long contentId);
	/**
	 * 获取内容属性：复制到的内容ID
	 * @param contentId
	 * @return
	 */ 
	String getCopyToContentIds(Long contentId);
	/**
	 * 获取内容属性：标题是否加粗
	 * @param contentId
	 * @return
	 */
	Boolean getStrong(Long contentId);
	/**
	 * 获取 内容属性：标题是否斜体
	 * @param contentId
	 * @return
	 */
	Boolean  getEm(Long contentId);
	/**
	 * 获取内容属性：标题是否加下划线
	 * @param contentId
	 * @return
	 */
	Boolean getU(Long contentId);
	/**
	 * 获取标题属性 ：标题字符大小
	 * @param contentId
	 * @return
	 */
	String getSize(Long contentId);
	/**
	 * 获取标题属性：标题颜色
	 * @param contentId
	 * @return
	 */
	String getColor(Long contentId);
	/**
	 * 获取标题格式化信息
	 * @param contentId
	 * @return
	 */
	String getTitleFormat(Long contentId);
	/**
	 * 获取内容链接栏目
	 * @param contentId
	 * @return
	 */
	String getLinkTo(Long contentId);
	/**
	 * 获取内容拷贝到的栏目
	 * @param contentId
	 * @return
	 */
	String getCopyTo(Long contentId);
	
	ContentAttrEntity getContentByCopyIds(Long contentId);
	
	List<ContentAttrEntity> getAttrListByLinkToNodeId(Long nodeId);
	
}
