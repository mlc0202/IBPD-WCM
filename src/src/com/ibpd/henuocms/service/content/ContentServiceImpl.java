package com.ibpd.henuocms.service.content;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.assist.ExtContentEntity;
import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.ext.ContentExtEntity;
import com.ibpd.henuocms.service.contentAttr.ContentAttrServiceImpl;
import com.ibpd.henuocms.service.contentAttr.IContentAttrService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
@Transactional
@Service("contentService")
public class ContentServiceImpl extends BaseServiceImpl<ContentEntity> implements IContentService {
	public ContentServiceImpl(){
		super();
		this.tableName="ContentEntity";
		this.currentClass=ContentEntity.class;
		this.initOK();
	}
	

	public void InitSysConfigParams() {
		// TODO Auto-generated method stub
		
	}

	public List<ContentExtEntity> getContentList(Long currentNodeId,
			Integer currentPageSize, Integer currentPageIndex,
			String orderField, String orderType) {
		List<ContentEntity> contList=getContentListByNodeId(currentNodeId,currentPageSize,currentPageIndex,orderField,orderType);
		List<ContentExtEntity> extList=new ArrayList<ContentExtEntity>();
		IContentAttrService attrService=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);
		for(ContentEntity c:contList){
			ContentAttrEntity attr=attrService.getContentAttr(c.getId());
			ContentExtEntity ext=new ContentExtEntity();
			ext.setAttr(attr);
			ext.setContent(c);
			extList.add(ext);
		}
		return extList;
	}

	public List<ContentEntity> getContentListByNodeId(Long nodeId,
			Integer pageSize, Integer pageIndex, String orderField,
			String orderType) {
		String hql="from "+this.getTableName()+" where ";
		INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		//这里把nodeIdPath里包含该nodeId的栏目ID全取出来，然后参与内容的检索
		List<NodeEntity> nodeList=nodeService.getList("from "+nodeService.getTableName()+" where nodeIdPath like '%,"+nodeId.toString()+",%'", null, null,1000,0);
		if(nodeList!=null && nodeList.size()>0){
			for(NodeEntity n:nodeList){
				hql+="nodeId="+n.getId()+" or ";
			}
			hql=hql.substring(0,hql.length()-4);
		}else{
			hql+="nodeId="+nodeId; 
		}
		List<ContentEntity> contList=this.getList(hql, null, orderField+" "+orderType, pageSize, pageIndex);
		return contList;
	}


	public ContentExtEntity getContentExtEntity(Long contentId) {
		
		ContentEntity ce=this.getEntityById(contentId);
		if(ce==null)
			return null;
		IContentAttrService attrServ=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);
		ContentAttrEntity attr=attrServ.getContentAttr(ce.getId());
		ContentExtEntity ext=new ContentExtEntity();
		ext.setAttr(attr);
		ext.setContent(ce);
		return ext;
	}


	public List<ExtContentEntity> getContentList(Long currentSiteId,
			Long currentNodeId, Integer currentPageSize,
			Integer currentPageIndex,String filter,String orderType) {
		String chql="";
		INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		List<NodeEntity> nodeList=nodeServ.getList("from "+nodeServ.getTableName()+" where nodeIdPath like '%,"+currentNodeId+",%'", null);
		IContentAttrService attrServ=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);
		if(nodeList!=null){
			for(NodeEntity n:nodeList){
				List<ContentAttrEntity> tmpList=attrServ.getAttrListByLinkToNodeId(n.getId());
				if(tmpList!=null){
					for(ContentAttrEntity c:tmpList){
						c.getContentId();
						chql+="id="+c.getContentId()+" or ";
					}
				}
			}
		}
		
		String jpql="from "+getTableName()+" where";
		if(currentSiteId>0L)
			jpql+=" subSiteId="+currentSiteId;
		if(currentNodeId>0L){
			if(jpql.indexOf("subSiteId")!=-1){
				jpql+=" and ";
			}
			jpql+=" nodeIdPath like '%,"+currentNodeId+",%'";
		}
		if(jpql.indexOf("subSiteId")!=-1 || jpql.indexOf("nodeIdPath")!=-1){
			jpql+=" and ";
		}
		jpql+="deleted=false and state="+ContentEntity.STATE_PASS;
		if(!StringUtils.isBlank(filter)){
			jpql+=" and "+filter;
		}
		if(chql.trim().length()>0){
			jpql+=" or "+chql.substring(0,chql.length()-4)+"";
		} 
		System.out.println(jpql);
		List<ContentEntity> l=getList(jpql,null,orderType,currentPageSize,currentPageIndex);
		if(l==null || l.size()==0)
			return null;
	 	
		List<ExtContentEntity> extList=new ArrayList<ExtContentEntity>();
		for(ContentEntity c:l){
			ContentAttrEntity attr=attrServ.getContentAttr(c.getId());
			ExtContentEntity ext=new ExtContentEntity(c);
			ext.setAttrEntity(attr);
			extList.add(ext);
		}
		return extList;
	}
}
