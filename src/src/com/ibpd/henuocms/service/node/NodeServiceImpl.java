package com.ibpd.henuocms.service.node;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.ListSortUtil;
import com.ibpd.henuocms.common.Pinyin4jUtil;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.nodeForm.INodeFormService;
import com.ibpd.henuocms.service.nodeForm.NodeFormServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
@Transactional
@Service("nodeService")
public class NodeServiceImpl extends BaseServiceImpl<NodeEntity> implements INodeService {
	public NodeServiceImpl(){
		super();
		this.tableName="NodeEntity";
		this.currentClass=NodeEntity.class;
		this.initOK();
	} 
	public void initNodeDemo(){
		ISubSiteService siteService=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
		List<SubSiteEntity> siteList=siteService.getList();
		for(SubSiteEntity s:siteList){
			siteService.deleteByPK(s.getId());
		}
		List<NodeEntity> neList=this.getList();
		for(NodeEntity ne:neList){
			this.deleteByPK(ne.getId());
		}
		INodeFormService formService=(INodeFormService) ServiceProxyFactory.getServiceProxy(NodeFormServiceImpl.class);
		INodeAttrService attrService=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
		List<NodeAttrEntity> attrList=attrService.getList();
		for(NodeAttrEntity attr:attrList){
			attrService.deleteByPK(attr.getId());
		}
		String[] ss=new String[]{"和诺商业集团有限公司","XXXX官方网站"};
		for(String s:ss){
			SubSiteEntity site=new SubSiteEntity();
			site.setFullName(s);
			site.setCreateDate(new Date());
			site.setStartDate(new Date());
			site.setEndDate(new Date());
			site.setIsLocked(false);
			site.setDirectory(Pinyin4jUtil.getPinYinHeadChar(s));
			site.setSiteName(s);
			siteService.saveEntity(site);
			for(Integer i=0;i<10;i++){
				NodeEntity ne=new NodeEntity();
				ne.setText("一级栏目"+i);
				ne.setParentId(-1L);
				ne.setSubSiteId(site.getId());
				ne.setOrder(i);
				ne.setLeaf(true);
				this.saveEntity(ne);
				ne.setNodeIdPath(","+ne.getId()+",");
				this.saveEntity(ne);
				NodeAttrEntity attr=new NodeAttrEntity();
				attr.setNodeId(ne.getId());
				attr.setDirectory(Pinyin4jUtil.getPinYinHeadChar(ne.getText()));
				attrService.saveEntity(attr);
				formService.linkNodeForm(ne.getId(), -1L);
			}
			neList=this.getList();
			for(NodeEntity ne:neList){
				for(Integer i=1;i<=10;i++){
					NodeEntity ne2=new NodeEntity();
					ne2.setText("二级栏目"+i);
					ne2.setParentId(ne.getId());
					ne2.setOrder(i);
					ne2.setSubSiteId(site.getId());
					if(i % 2==0)
					ne2.setLeaf(false);
					this.saveEntity(ne2);
					ne2.setNodeIdPath(ne.getNodeIdPath()+ne2.getId()+",");
					this.saveEntity(ne2);
					NodeAttrEntity attr2=new NodeAttrEntity();
					attr2.setNodeId(ne2.getId());
					attr2.setDirectory(Pinyin4jUtil.getPinYinHeadChar(ne2.getText()));
					attrService.saveEntity(attr2);

					formService.linkNodeForm(ne2.getId(), -1L);
				}
			}
		}
	}
	/**
	 * 获取栏目的下级栏目，改函数仅仅适用于自定义标签调用
	 */
//	public List<NodeExtEntity> getChildNodeList(Long siteId,Long nodeId,Integer pageSize,Integer pageIndex,String orderField,String orderType) {
//		String jpql="from "+getTableName()+" where parentId=:nodeId and isDeleted=false and state="+NodeEntity.NODESTATE_OPEN;
//		if(siteId!=null && siteId!=-1L){
//			jpql+=" and subSiteId="+siteId;
//		}
//		List<HqlParameter> hqlParameterList=new ArrayList<HqlParameter>();
//		hqlParameterList.add(new HqlParameter("nodeId",nodeId,HqlParameter.DataType_Enum.Long));
//		if(StringUtils.isBlank(orderField)){
//			orderField=null;
//			orderType=null;
//		}else{
//			if(StringUtils.isBlank(orderType)){
//				orderType=orderField+" asc";
//			}else{
//				orderType=orderField+" "+orderType;
//			}
//		}
//		
//		List<NodeEntity> nodeList=this.getList(jpql, hqlParameterList, orderType, pageSize, pageIndex);
//		List<NodeExtEntity> extList=new ArrayList<NodeExtEntity>();
//		if(nodeList!=null){
//			for(NodeEntity node:nodeList){
//				INodeAttrService attrService=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
//				NodeAttrEntity attr=attrService.getNodeAttr(node.getId());
//				if(attr.getDisplayInNavigator()){
//					NodeExtEntity ext=new NodeExtEntity();
//					ext.setAttr(attr);
//					ext.setNode(node);
//					extList.add(ext);
//				}
//			}
//		}
//		return extList;
//	}
	public List<NodeExtEntity> getChildNodeList(Long siteId,Long nodeId,Integer pageSize,Integer pageIndex,String orderField,String orderType) {
		String jpql="from "+getTableName()+" where parentId=:nodeId and isDeleted=false and state="+NodeEntity.NODESTATE_OPEN;
		if(siteId!=null && siteId!=-1L){
			jpql+=" and subSiteId="+siteId;
		}
		List<HqlParameter> hqlParameterList=new ArrayList<HqlParameter>();
		hqlParameterList.add(new HqlParameter("nodeId",nodeId,HqlParameter.DataType_Enum.Long));
		if(StringUtils.isBlank(orderField)){
			orderField=null;
			orderType=null;
		}else{
			if(StringUtils.isBlank(orderType)){
				orderType=orderField+" asc";
			}else{
				orderType=orderField+" "+orderType;
			}
		}
		
		List<NodeEntity> nodeList=this.getList(jpql, hqlParameterList);
		List<NodeExtEntity> extList=new ArrayList<NodeExtEntity>();
		if(nodeList!=null){
			for(NodeEntity node:nodeList){
				INodeAttrService attrService=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
				NodeAttrEntity attr=attrService.getNodeAttr(node.getId());
				if(attr.getDisplayInNavigator()){
					NodeExtEntity ext=new NodeExtEntity();
					ext.setAttr(attr);
					ext.setNode(node);
					extList.add(ext);
				}
			}
		}
		extList=orderList(extList,orderField,orderType);
		List<NodeExtEntity> rtnList=new ArrayList<NodeExtEntity>();
		if(extList.size()>=pageSize*pageIndex)
			
		for(Integer i=pageSize*(pageIndex);i<pageSize*(pageIndex+1);i++){
			if(i<extList.size())
				rtnList.add(extList.get(i));
		}
		return rtnList;
	}
	private List<NodeExtEntity> orderList(List<NodeExtEntity> list,String orderField,String orderType){
		ListSortUtil<NodeExtEntity> sortList = new ListSortUtil<NodeExtEntity>(); 
		sortList.sort(list, orderField, orderType);
		return list;
	}
	public NodeExtEntity getNodeExtEntity(Long nodeId) {
		NodeEntity node=this.getEntityById(nodeId);
		if(node==null)
			return null;
		INodeAttrService attrService=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=attrService.getNodeAttr(nodeId);
		NodeExtEntity ext=new NodeExtEntity();
		ext.setAttr(attr);
		ext.setNode(node);
		return ext;
	}
}
