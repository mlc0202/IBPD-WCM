package com.ibpd.henuocms.service.node;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;

public interface INodeService extends IBaseService<NodeEntity> {
	void initNodeDemo();
	/**
	 * 获取栏目的下级栏目，改函数仅仅适用于自定义标签调用
	 */
	List<NodeExtEntity> getChildNodeList(Long siteId,Long nodeId,Integer pageSize,Integer pageIndex,String orderField,String orderType);
	NodeExtEntity getNodeExtEntity(Long nodeId);
}
 