/**
 * @author 马根
 * @version 1.0
 * @description 用户
 */
package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 验证表
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Validate")
public class ValidateEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*验证模型ID
	验证模型名称
	验证模型类型
	验证模型表达式 
	验证模型附加javascript函数、代码
	验证模型ajaxurl
	验证模型比对标签name*/
 
	/**
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 验证模型名称
	 */
	@Column(name="f_validateName",length=50,nullable=true)
	private String validateNamee=" ";
	/**
	 * 验证模型类型
	 */
	@Column(name="f_validateType",nullable=true)
	private Integer validateType=0;
	/**
	 * 验证模型表达式
	 */
	@Column(name="f_dataType",length=255,nullable=true)
	private String dataType=" ";
	/**
	 * 验证模型附加javascript函数、代码
	 */
	@Column(name="f_func",length=1024,nullable=true)
	private String func=" ";
	/**
	 * 验证模型ajaxurl
	 */
	@Column(name="f_ajaxUrl",length=255,nullable=true)
	private String ajaxUrl=" ";//安全问题的档案
	/**
	 * 验证模型比对标签name
	 */
	@Column(name="f_rechek",length=255,nullable=true)
	private String rechek=" ";
	/**
	 * 错误提示消息
	 */
	@Column(name="f_errormsg",length=255,nullable=true)
	private String errormsg=" ";
	/**
	 * 创建时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间

	/**
	 * 
	 */
	@Column(name="f_Order",nullable=true)
	private Integer Order=0;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateNamee() {
		return validateNamee;
	}

	public void setValidateNamee(String validateNamee) {
		this.validateNamee = validateNamee;
	}

	public Integer getValidateType() {
		return validateType;
	}

	public void setValidateType(Integer validateType) {
		this.validateType = validateType;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}

	public String getAjaxUrl() {
		return ajaxUrl;
	}

	public void setAjaxUrl(String ajaxUrl) {
		this.ajaxUrl = ajaxUrl;
	}

	public String getRechek() {
		return rechek;
	}

	public void setRechek(String rechek) {
		this.rechek = rechek;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getOrder() {
		return Order;
	}

	public void setOrder(Integer order) {
		Order = order;
	}

	public void setErrormsg(String errormsg) {
		this.errormsg = errormsg;
	}

	public String getErrormsg() {
		return errormsg;
	}

}
