package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 节点栏目属性
 * @author 马根
 * @version 1.0 
 */
@Entity
@Table(name="T_NodeAttr")
public class NodeAttrEntity extends IBaseEntity{
	private static final long serialVersionUID = 1L;
	
	
	
	@Id 
	@Column(name="f_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 栏目ID
	 */
	private Long nodeId=-1L;
	/**
	 * 模板库ID-
	 */
	@Column(name="f_TemplateGroupId",length=50,nullable=true)
	private Long templateGroupId=Long.parseLong("-1");
	/**
	 * 节点页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_nodePageTemplateId",nullable=true)
	private Long nodePageTemplateId=Long.parseLong("-1");
	/**
	 * 节点样式模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_nodeStyleTemplateId",nullable=true)
	private Long nodeStyleTemplateId=Long.parseLong("-1");
	/**
	 * 内容页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_contentpageTemplateId",nullable=true)
	private Long contentpageTemplateId=Long.parseLong("-1");
	/**
	 * 内容样式模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_contentStyleTemplateId",nullable=true)
	private Long contentStyleTemplateId=Long.parseLong("-1");
	/**
	 * 评论页面模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_commentpageTemplateId",nullable=true)
	private Long commentpageTemplateId=Long.parseLong("-1");
	/**
	 * 评论样式模板ID，如果模板库设置了相应的值，这里失效
	 */
	@Column(name="f_commentStyleTemplateId",nullable=true)
	private Long commentStyleTemplateId=Long.parseLong("-1");
	/**
	 * 所在目录
	 */
	private String directory="";
	/**
	 * 是否在导航中显示-
	 */
	@Column(name="f_displayInNavigator",nullable=true)
	private Boolean displayInNavigator=NodeEntity.NAVI_DISPLAY;
	/**
	 * 导航排序-
	 */
	@Column(name="f_orderByNavigator",nullable=true)
	private Integer orderByNavigator=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTemplateGroupId() {
		return templateGroupId;
	}
	public void setTemplateGroupId(Long templateGroupId) {
		this.templateGroupId = templateGroupId;
	}
	public Long getNodePageTemplateId() {
		return nodePageTemplateId;
	}
	public void setNodePageTemplateId(Long nodePageTemplateId) {
		this.nodePageTemplateId = nodePageTemplateId;
	}
	public Long getNodeStyleTemplateId() {
		return nodeStyleTemplateId;
	}
	public void setNodeStyleTemplateId(Long nodeStyleTemplateId) {
		this.nodeStyleTemplateId = nodeStyleTemplateId;
	}
	public Long getContentpageTemplateId() {
		return contentpageTemplateId;
	}
	public void setContentpageTemplateId(Long contentpageTemplateId) {
		this.contentpageTemplateId = contentpageTemplateId;
	}
	public Long getContentStyleTemplateId() {
		return contentStyleTemplateId;
	}
	public void setContentStyleTemplateId(Long contentStyleTemplateId) {
		this.contentStyleTemplateId = contentStyleTemplateId;
	}
	public Long getCommentpageTemplateId() {
		return commentpageTemplateId;
	}
	public void setCommentpageTemplateId(Long commentpageTemplateId) {
		this.commentpageTemplateId = commentpageTemplateId;
	}
	public Long getCommentStyleTemplateId() {
		return commentStyleTemplateId;
	}
	public void setCommentStyleTemplateId(Long commentStyleTemplateId) {
		this.commentStyleTemplateId = commentStyleTemplateId;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public Long getNodeId() {
		return nodeId;
	}
	public void setDisplayInNavigator(Boolean displayInNavigator) {
		this.displayInNavigator = displayInNavigator;
	}
	public Boolean getDisplayInNavigator() {
		return displayInNavigator;
	}
	public void setOrderByNavigator(Integer orderByNavigator) {
		this.orderByNavigator = orderByNavigator;
	}
	public Integer getOrderByNavigator() {
		return orderByNavigator;
	}
}
