package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 内容表单 字段
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Field")
public class FieldEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * ID
	 * 所属modelId
	 * 字段显示名称
	 * 字段表单名称
	 * 创建时间
	 * 更新时间
	 * 字段类型
	 * 验证模型ID
	 * 排序
	 * 默认值
	 * 可选值
	 * 字段类别（栏目、上传等）
	 * 字段说明
	 * 字段长度
*/

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_displayName",length=50,nullable=true)
	private String displayName=" ";
	@Column(name="f_modelId",nullable=true)
	private Long modelId=0L;
	@Column(name="f_formName",length=50,nullable=true)
	private String formName=" ";
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updateTime",nullable=true)
	private Date updateTime=new Date();//更新时间
	@Column(name="f_htmlType",nullable=true)
	private Integer htmlType=0;
	@Column(name="f_validateModelId",nullable=true)
	private Long validateModelId=0L;
	@Column(name="f_Order",nullable=true)
	private Integer Order=0;
	@Column(name="f_defaultValue",length=50,nullable=true)
	private String defaultValue=" ";
	@Column(name="f_optionValue",length=50,nullable=true)
	private String optionValue=" ";
	@Column(name="f_fieldType",nullable=true)
	private Integer fieldType=0;
	@Column(name="f_intro",length=500,nullable=true)
	private String intro;
	@Column(name="f_fieldLength",nullable=true)
	private String fieldLength;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String dispalyName) {
		this.displayName = dispalyName;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getHtmlType() {
		return htmlType;
	}
	public void setHtmlType(Integer htmlType) {
		this.htmlType = htmlType;
	}
	public Long getValidateModelId() {
		return validateModelId;
	}
	public void setValidateModelId(Long validateModelId) {
		this.validateModelId = validateModelId;
	}
	public Integer getOrder() {
		return Order;
	}
	public void setOrder(Integer order) {
		Order = order;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getOptionValue() {
		return optionValue;
	}
	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}
	public Integer getF_fieldType() {
		return fieldType;
	}
	public void setF_fieldType(Integer fFieldType) {
		fieldType = fFieldType;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public Long getModelId() {
		return modelId;
	}
	public void setFieldType(Integer fieldType) {
		this.fieldType = fieldType;
	}
	public Integer getFieldType() {
		return fieldType;
	}
	public void setFieldLength(String fieldLength) {
		this.fieldLength = fieldLength;
	}
	public String getFieldLength() {
		return fieldLength;
	}

	
}
