package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.ibpd.entity.baseEntity.IBaseEntity;

/** 
 * 节点栏目
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Node")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region="javaClassName")
public class NodeEntity extends IBaseEntity{
	public static final String NODEIDPATH_SPLIT=",";
	/**
	 * 在导航中显示
	 */
	public static final Boolean NAVI_DISPLAY=true;
	/**
	 * 不在导航中显示
	 */
	public static final Boolean NAVI_HIDDEN=false;
	/**
	 * 栏目状态：正常状态
	 */
	public static final Integer NODESTATE_OPEN=1;
	/**
	 * 栏目状态：锁定状态
	 */
	public static final Integer NODESTATE_CLOSE=-1;
	/**
	 * 默认栏目分组
	 */
	public static final String NODEGROUP_DEFAULT="default";
	/**
	 * 栏目类型：本地栏目
	 */
	public static final Integer NODETYPE_LOCAL=0;
	/**
	 * 栏目类型：转向链接
	 */
	public static final Integer NODETYPE_REDIRECT=1;
	/**
	 * 栏目下内容的统计个数单位
	 */
	public static final String CONTENTUNIT_DEFAULT="篇";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@Id 
	@Column(name="f_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 上级节点ID，如果为-1，表示此节点为根节点
	 */
	@Column(name="f_parentId",length=50,nullable=true)
	private Long parentId=Long.parseLong("-1");
	@Column(name="f_nodeIdPath",length=50,nullable=true)
	private String nodeIdPath=" ";
	/**
	 * 标题-
	 */
	@Column(name="f_text",length=50,nullable=true)
	private String text=" ";
	/**
	 * 栏目类型：本地栏目和转向链接-
	 */
	@Column(name="f_nodeType",nullable=true)
	private Integer nodeType=NODETYPE_LOCAL;
	/**
	 * 所属站点ID（）
	 */
	@Column(name="f_subSiteId",nullable=true)
	private Long subSiteId=Long.parseLong("-1");
	/**
	 * 栏目分组-
	 */
	@Column(name="f_group",length=255,nullable=true)
	private String group=NODEGROUP_DEFAULT;
	/**
	 * 链接地址，和栏目类型配合使用-
	 */
	@Column(name="f_linkUrl",length=255,nullable=true)
	private String linkUrl=" ";
	/**
	 * 包含内容的单位名称-

	 */
	@Column(name="f_subContentCountUnit",length=50,nullable=true)
	private String subContentCountUnit=CONTENTUNIT_DEFAULT;
	/**
	 * 描述-
	 */
	@Column(name="f_description",length=50,nullable=true)
	private String description = " ";
	/**
	 * 搜索关键字-

	 */
	@Column(name="f_keywords",length=50,nullable=true)
	private String keywords = " ";
	/**
	 * 是否包含子节点

	 */
	@Column(name="f_leaf",nullable=true)
	private Boolean leaf=false;
	/***
	 * 节点状态：-
	 */
	@Column(name="f_state",nullable=true)
	private Integer state=NODESTATE_OPEN;
	/**
	 * 创建日期
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	/**
	 * 创建者
	 */
	@Column(name="f_createUser",length=50,nullable=true)
	private String createUser=" ";
	/**
	 * 管理员-
	 */
	@Column(name="f_manageUser",length=50,nullable=true)
	private String manageUser=" ";
	/**
	 * 排序-
	 */
	@Column(name="f_order",length=50,nullable=true)
	private Integer order=0;
	/**
	 * 级别深度,自动生成,等于父亲depath+1
	 */
	@Column(name="f_depth",nullable=true)
	private Integer depth = 0;
	/**
	 * 子节点数量,自动
	 */
	@Column(name="f_childCount",nullable=true)
	private Integer childCount = 0; 
	/**
	 * 内容数量
	 */
	@Column(name="f_countentCount",nullable=true)
	private Integer countentCount=0;
	/**
	 * 背景音乐,-1表示默认继承父级的

	 */
	@Column(name="f_bgMusic",length=255,nullable=true)
	private String bgMusic = " ";
	/**
	 * 标志,-1表示默认继承父级的-

	 */
	@Column(name="f_logo",length=255,nullable=true)
	private String logo = " ";
	/**
	 * 图标-
	 */
	@Column(name="f_icon",length=255,nullable=true)
	private String icon = " ";
	/**
	 * 代表性图片，主要用于栏目页面中的栏目图片显示
	 */
	@Column(name="f_map",length=255,nullable=true)
	private String map = " ";
	/**
	 * 横幅图片,-1表示默认继承父级的-

	 */
	@Column(name="f_banner",length=255,nullable=true)
	private String banner = " ";
	/**
	 * 版权信息,-1表示默认继承父级的-

	 */
	@Column(name="f_copyright",length=255,nullable=true)
	private String copyright = " ";
	/**
	 * 栏目是否已经删除
	 */
	@Column(name="f_isDeleted",nullable=true)
	private Boolean isDeleted=false;
	
	private String custom1=" ";
	private String custom2=" ";
	private String custom3=" ";
	private String custom4=" ";
	private String custom5=" ";
	private String custom6=" ";
	private String custom7=" ";
	private String custom8=" ";
	private String custom9=" ";
	private String custom10=" ";
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}
	public String getCustom5() {
		return custom5;
	}
	public void setCustom5(String custom5) {
		this.custom5 = custom5;
	}
	public String getCustom6() {
		return custom6;
	}
	public void setCustom6(String custom6) {
		this.custom6 = custom6;
	}
	public String getCustom7() {
		return custom7;
	}
	public void setCustom7(String custom7) {
		this.custom7 = custom7;
	}
	public String getCustom8() {
		return custom8;
	}
	public void setCustom8(String custom8) {
		this.custom8 = custom8;
	}
	public String getCustom9() {
		return custom9;
	}
	public void setCustom9(String custom9) {
		this.custom9 = custom9;
	}
	public String getCustom10() {
		return custom10;
	}
	public void setCustom10(String custom10) {
		this.custom10 = custom10;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getNodeIdPath() {
		return nodeIdPath;
	}
	public void setNodeIdPath(String nodeIdPath) {
		this.nodeIdPath = nodeIdPath;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getNodeType() {
		return nodeType;
	}
	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}
	public Long getSubSiteId() {
		return subSiteId;
	}
	public void setSubSiteId(Long subSiteId) {
		this.subSiteId = subSiteId;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	public String getSubContentCountUnit() {
		return subContentCountUnit;
	}
	public void setSubContentCountUnit(String subContentCountUnit) {
		this.subContentCountUnit = subContentCountUnit;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Boolean getLeaf() {
		return leaf;
	}
	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getManageUser() {
		return manageUser;
	}
	public void setManageUser(String manageUser) {
		this.manageUser = manageUser;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Integer getDepth() {
		return depth;
	}
	public void setDepth(Integer depth) {
		this.depth = depth;
	}
	public Integer getChildCount() {
		return childCount;
	}
	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}
	public Integer getCountentCount() {
		return countentCount;
	}
	public void setCountentCount(Integer countentCount) {
		this.countentCount = countentCount;
	}
	public String getBgMusic() {
		return bgMusic;
	}
	public void setBgMusic(String bgMusic) {
		this.bgMusic = bgMusic;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public String getBanner() {
		return banner;
	}
	public void setBanner(String banner) {
		this.banner = banner;
	}
	public String getCopyright() {
		return copyright;
	}
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
