package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 内容 
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Content")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region="javaClassName")
public class ContentEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Integer STATE_PASS=1;
	/**
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 标题
	 */
	@Column(name="f_title",length=255,nullable=true)
	private String title=" ";
	/**
	 * 所属站点ID
	 */
	@Column(name="f_subSiteId",nullable=true)
	private Long subSiteId=Long.parseLong("-1");
	/**
	 * 栏目ID
	 */
	@Column(name="f_nodeId",nullable=true)
	private Long nodeId=Long.parseLong("-1");
	/**
	 * 栏目IDs，检索方便考虑
	 */
	@Column(name="f_nodeIdPath",length=255,nullable=true)
	private String nodeIdPath=" ";
	/**
	 * 是否是转向另外url地址的内容

	 */
	@Column(name="f_redirectUrl",length=255,nullable=true)
	private String redirectUrl="";
	/**
	 * 是否有缩略图

	 */
	@Column(name="f_existPreview",nullable=true)
	private Boolean existPreview=false;
	/**
	 * 缩略图地址
	 */
	@Column(name="f_previewUrl",length=2550,nullable=true)
	private String previewUrl=" ";
	/**
	 * 文章简介/文章段内容

	 */
	@Column(name="f_description",length=1024,nullable=true)
	private String description=" ";
	/**
	 * 文章关键字

	 */
	@Column(name="f_textKeywords",length=50,nullable=true)
	private String textKeywords=" ";
	/**
	 * 相关内容检索关键字，相关内容走这里
	 */
	@Column(name="f_aboutKeyword",length=50,nullable=true)
	private String aboutKeyword=" ";
	/**
	 * 分组
	 */
	@Column(name="f_group",length=50,nullable=true)
	private String group=" ";
	/**
	 * 来源
	 */
	@Column(name="f_source",length=255,nullable=true)
	private String source=" ";
	/**
	 * 附件等的url地址
	 */
	@Column(name="url",length=255,nullable=true)
	private String url=" ";
	/**
	 * url指向目标的目标类型

	 */
	@Column(name="f_urlType",length=255,nullable=true)
	private String urlType=" ";
	/**
	 * url目标的大小（如果目标内容值得知道大小的话）

	 */
	@Column(name="f_urlSize",nullable=true)
	private Integer urlSize=0;
	/**
	 * 作者

	 */
	@Column(name="f_author",length=50,nullable=true)
	private String author=" ";
	/**
	 * 创建者

	*/
	@Column(name="f_enteringUser",length=50,nullable=true)
	private String enteringUser=" ";
	/**
	 * 创建时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	/**
	 * 最后一次更新时间

	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_lastUpdateDate")
	private Date lastUpdateDate=new Date();
	/**
	 * 正文内容 大字段 延时加载 需要为这种特殊情况做特殊处理，在dao或者service中实现这种效果

	 */
	@Column(name="f_Text")
	@Lob
	@Basic(fetch=FetchType.LAZY)
	private String text="<p></p>";
	/**
	 * 是否删除（到回收站）
	 */
	@Column(name="f_deleted",nullable=true)
	private Boolean deleted=false;
	/**
	 * 内容状态（0代表草稿，1代表未审核，2代表退稿，99代表审核通过）

	 */
	@Column(name="f_state",nullable=true)
	private Integer state=-1;
	/**
	 * 审核评语（例如审核失败（退稿）的原因）
	 */
	@Column(name="f_stateMessage",length=50,nullable=true)
	private String stateMessage=" ";
	/**
	 * 审核用户名称
	 */
	@Column(name="f_passedUser",length=50,nullable=true)
	private String passedUser=" ";
	/**
	 * 审核状态发生变更的时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_passedTime")
	private Date passedTime=new Date();
	/**
	 * 是否固顶

	 */
	@Column(name="f_isTop",nullable=true)
	private Boolean isTop=false;
	/**
	 * 是否推荐
	 */
	@Column(name="f_isRecommend",nullable=true)
	private Boolean isRecommend=false;
	/**
	 * 是否精华
	 */
	@Column(name="f_isParson",nullable=true)
	private Boolean isParson=false;
	/**
	 * 排序
	 */
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	/**
	 * 点击率

	 */
	@Column(name="f_hits",length=50,nullable=true)
	private Long hits=Long.parseLong("0");
	/**
	 * 文章发表客户端ip地址
	 */
	@Column(name="f_ip",length=50,nullable=true)
	private String ip=" ";
	/**
	 * 好评数 用enum来实现各种不同评论级别

	 */
	@Column(name="f_goodCount",nullable=true)
	private Integer goodCount=0;
	/**
	 * 中评数

	 */
	@Column(name="f_normalCount",nullable=true)
	private Integer normalCount=0;
	/**
	 * 差评数

	 */
	@Column(name="f_wrongCount",nullable=true)
	private Integer wrongCount=0;
	/**
	 * 评论数

	 */
	@Column(name="f_commentCount",nullable=true)
	private Integer commentCount=0;
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom1",length=255,nullable=true)
	private String custom1=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom2",length=255,nullable=true)
	private String custom2=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom3",length=255,nullable=true)
	private String custom3=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom4",length=255,nullable=true)
	private String custom4=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom5",length=255,nullable=true)
	private String custom5=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom6",length=255,nullable=true)
	private String custom6=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom7",length=255,nullable=true)
	private String custom7=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom8",length=255,nullable=true)
	private String custom8=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom9",length=255,nullable=true)
	private String custom9=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom10",length=255,nullable=true)
	private String custom10=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom11",length=255,nullable=true)
	private String custom11=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom12",length=255,nullable=true)
	private String custom12=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom13",length=255,nullable=true)
	private String custom13=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom14",length=255,nullable=true)
	private String custom14=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom15",length=255,nullable=true)
	private String custom15=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom16",length=255,nullable=true)
	private String custom16=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom17",length=255,nullable=true)
	private String custom17=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom18",length=255,nullable=true)
	private String custom18=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom19",length=255,nullable=true)
	private String custom19=" ";
	/**
	 * 自定义字段（冗余字段，扩展字段）
	 */
	@Column(name="f_custom20",length=255,nullable=true)
	private String custom20=" ";
	@Column(name="f_bakText")
	@Lob
	@Basic(fetch=FetchType.LAZY)
	private String bakText=" ";
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getSubSiteId() {
		return subSiteId;
	}
	public void setSubSiteId(Long subSiteId) {
		this.subSiteId = subSiteId;
	}
	public Long getNodeId() {
		return nodeId;
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public String getNodeIdPath() {
		return nodeIdPath;
	}
	public void setNodeIdPath(String nodeIdPath) {
		this.nodeIdPath = nodeIdPath;
	}
	public Boolean getExistPreview() {
		return existPreview;
	}
	public void setExistPreview(Boolean existPreview) {
		this.existPreview = existPreview;
	}
	public String getPreviewUrl() {
		return previewUrl;
	}
	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTextKeywords() {
		return textKeywords;
	}
	public void setTextKeywords(String textKeywords) {
		this.textKeywords = textKeywords;
	}
	public String getAboutKeyword() {
		return aboutKeyword;
	}
	public void setAboutKeyword(String aboutKeyword) {
		this.aboutKeyword = aboutKeyword;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrlType() {
		return urlType;
	}
	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}
	public Integer getUrlSize() {
		return urlSize;
	}
	public void setUrlSize(Integer urlSize) {
		this.urlSize = urlSize;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEnteringUser() {
		return enteringUser;
	}
	public void setEnteringUser(String enteringUser) {
		this.enteringUser = enteringUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getStateMessage() {
		return stateMessage;
	}
	public void setStateMessage(String stateMessage) {
		this.stateMessage = stateMessage;
	}
	public String getPassedUser() {
		return passedUser;
	}
	public void setPassedUser(String passedUser) {
		this.passedUser = passedUser;
	}
	public Date getPassedTime() {
		return passedTime;
	}
	public void setPassedTime(Date passedTime) {
		this.passedTime = passedTime;
	}
	public Boolean getIsTop() {
		return isTop;
	}
	public void setIsTop(Boolean isTop) {
		this.isTop = isTop;
	}
	public Boolean getIsRecommend() {
		return isRecommend;
	}
	public void setIsRecommend(Boolean isRecommend) {
		this.isRecommend = isRecommend;
	}
	public Boolean getIsParson() {
		return isParson;
	}
	public void setIsParson(Boolean isParson) {
		this.isParson = isParson;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Long getHits() {
		return hits;
	}
	public void setHits(Long hits) {
		this.hits = hits;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getGoodCount() {
		return goodCount;
	}
	public void setGoodCount(Integer goodCount) {
		this.goodCount = goodCount;
	}
	public Integer getNormalCount() {
		return normalCount;
	}
	public void setNormalCount(Integer normalCount) {
		this.normalCount = normalCount;
	}
	public Integer getWrongCount() {
		return wrongCount;
	}
	public void setWrongCount(Integer wrongCount) {
		this.wrongCount = wrongCount;
	}
	public Integer getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}
	public String getCustom5() {
		return custom5;
	}
	public void setCustom5(String custom5) {
		this.custom5 = custom5;
	}
	public String getCustom6() {
		return custom6;
	}
	public void setCustom6(String custom6) {
		this.custom6 = custom6;
	}
	public String getCustom7() {
		return custom7;
	}
	public void setCustom7(String custom7) {
		this.custom7 = custom7;
	}
	public String getCustom8() {
		return custom8;
	}
	public void setCustom8(String custom8) {
		this.custom8 = custom8;
	}
	public String getCustom9() {
		return custom9;
	}
	public void setCustom9(String custom9) {
		this.custom9 = custom9;
	}
	public String getCustom10() {
		return custom10;
	}
	public void setCustom10(String custom10) {
		this.custom10 = custom10;
	}
	public String getCustom11() {
		return custom11;
	}
	public void setCustom11(String custom11) {
		this.custom11 = custom11;
	}
	public String getCustom12() {
		return custom12;
	}
	public void setCustom12(String custom12) {
		this.custom12 = custom12;
	}
	public String getCustom13() {
		return custom13;
	}
	public void setCustom13(String custom13) {
		this.custom13 = custom13;
	}
	public String getCustom14() {
		return custom14;
	}
	public void setCustom14(String custom14) {
		this.custom14 = custom14;
	}
	public String getCustom15() {
		return custom15;
	}
	public void setCustom15(String custom15) {
		this.custom15 = custom15;
	}
	public String getCustom16() {
		return custom16;
	}
	public void setCustom16(String custom16) {
		this.custom16 = custom16;
	}
	public String getCustom17() {
		return custom17;
	}
	public void setCustom17(String custom17) {
		this.custom17 = custom17;
	}
	public String getCustom18() {
		return custom18;
	}
	public void setCustom18(String custom18) {
		this.custom18 = custom18;
	}
	public String getCustom19() {
		return custom19;
	}
	public void setCustom19(String custom19) {
		this.custom19 = custom19;
	}
	public String getCustom20() {
		return custom20;
	}
	public void setCustom20(String custom20) {
		this.custom20 = custom20;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aboutKeyword == null) ? 0 : aboutKeyword.hashCode());
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((commentCount == null) ? 0 : commentCount.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((custom1 == null) ? 0 : custom1.hashCode());
		result = prime * result
				+ ((custom10 == null) ? 0 : custom10.hashCode());
		result = prime * result
				+ ((custom11 == null) ? 0 : custom11.hashCode());
		result = prime * result
				+ ((custom12 == null) ? 0 : custom12.hashCode());
		result = prime * result
				+ ((custom13 == null) ? 0 : custom13.hashCode());
		result = prime * result
				+ ((custom14 == null) ? 0 : custom14.hashCode());
		result = prime * result
				+ ((custom15 == null) ? 0 : custom15.hashCode());
		result = prime * result
				+ ((custom16 == null) ? 0 : custom16.hashCode());
		result = prime * result
				+ ((custom17 == null) ? 0 : custom17.hashCode());
		result = prime * result
				+ ((custom18 == null) ? 0 : custom18.hashCode());
		result = prime * result
				+ ((custom19 == null) ? 0 : custom19.hashCode());
		result = prime * result + ((custom2 == null) ? 0 : custom2.hashCode());
		result = prime * result
				+ ((custom20 == null) ? 0 : custom20.hashCode());
		result = prime * result + ((custom3 == null) ? 0 : custom3.hashCode());
		result = prime * result + ((custom4 == null) ? 0 : custom4.hashCode());
		result = prime * result + ((custom5 == null) ? 0 : custom5.hashCode());
		result = prime * result + ((custom6 == null) ? 0 : custom6.hashCode());
		result = prime * result + ((custom7 == null) ? 0 : custom7.hashCode());
		result = prime * result + ((custom8 == null) ? 0 : custom8.hashCode());
		result = prime * result + ((custom9 == null) ? 0 : custom9.hashCode());
		result = prime * result + ((deleted == null) ? 0 : deleted.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((enteringUser == null) ? 0 : enteringUser.hashCode());
		result = prime * result
				+ ((existPreview == null) ? 0 : existPreview.hashCode());
		result = prime * result
				+ ((goodCount == null) ? 0 : goodCount.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((hits == null) ? 0 : hits.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result
				+ ((isParson == null) ? 0 : isParson.hashCode());
		result = prime * result
				+ ((isRecommend == null) ? 0 : isRecommend.hashCode());
		result = prime * result + ((isTop == null) ? 0 : isTop.hashCode());
		result = prime * result
				+ ((lastUpdateDate == null) ? 0 : lastUpdateDate.hashCode());
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		result = prime * result
				+ ((nodeIdPath == null) ? 0 : nodeIdPath.hashCode());
		result = prime * result
				+ ((normalCount == null) ? 0 : normalCount.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result
				+ ((passedTime == null) ? 0 : passedTime.hashCode());
		result = prime * result
				+ ((passedUser == null) ? 0 : passedUser.hashCode());
		result = prime * result
				+ ((previewUrl == null) ? 0 : previewUrl.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result
				+ ((stateMessage == null) ? 0 : stateMessage.hashCode());
		result = prime * result
				+ ((subSiteId == null) ? 0 : subSiteId.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result
				+ ((textKeywords == null) ? 0 : textKeywords.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((urlSize == null) ? 0 : urlSize.hashCode());
		result = prime * result + ((urlType == null) ? 0 : urlType.hashCode());
		result = prime * result
				+ ((wrongCount == null) ? 0 : wrongCount.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentEntity other = (ContentEntity) obj;
		if (aboutKeyword == null) {
			if (other.aboutKeyword != null)
				return false;
		} else if (!aboutKeyword.equals(other.aboutKeyword))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (commentCount == null) {
			if (other.commentCount != null)
				return false;
		} else if (!commentCount.equals(other.commentCount))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (custom1 == null) {
			if (other.custom1 != null)
				return false;
		} else if (!custom1.equals(other.custom1))
			return false;
		if (custom10 == null) {
			if (other.custom10 != null)
				return false;
		} else if (!custom10.equals(other.custom10))
			return false;
		if (custom11 == null) {
			if (other.custom11 != null)
				return false;
		} else if (!custom11.equals(other.custom11))
			return false;
		if (custom12 == null) {
			if (other.custom12 != null)
				return false;
		} else if (!custom12.equals(other.custom12))
			return false;
		if (custom13 == null) {
			if (other.custom13 != null)
				return false;
		} else if (!custom13.equals(other.custom13))
			return false;
		if (custom14 == null) {
			if (other.custom14 != null)
				return false;
		} else if (!custom14.equals(other.custom14))
			return false;
		if (custom15 == null) {
			if (other.custom15 != null)
				return false;
		} else if (!custom15.equals(other.custom15))
			return false;
		if (custom16 == null) {
			if (other.custom16 != null)
				return false;
		} else if (!custom16.equals(other.custom16))
			return false;
		if (custom17 == null) {
			if (other.custom17 != null)
				return false;
		} else if (!custom17.equals(other.custom17))
			return false;
		if (custom18 == null) {
			if (other.custom18 != null)
				return false;
		} else if (!custom18.equals(other.custom18))
			return false;
		if (custom19 == null) {
			if (other.custom19 != null)
				return false;
		} else if (!custom19.equals(other.custom19))
			return false;
		if (custom2 == null) {
			if (other.custom2 != null)
				return false;
		} else if (!custom2.equals(other.custom2))
			return false;
		if (custom20 == null) {
			if (other.custom20 != null)
				return false;
		} else if (!custom20.equals(other.custom20))
			return false;
		if (custom3 == null) {
			if (other.custom3 != null)
				return false;
		} else if (!custom3.equals(other.custom3))
			return false;
		if (custom4 == null) {
			if (other.custom4 != null)
				return false;
		} else if (!custom4.equals(other.custom4))
			return false;
		if (custom5 == null) {
			if (other.custom5 != null)
				return false;
		} else if (!custom5.equals(other.custom5))
			return false;
		if (custom6 == null) {
			if (other.custom6 != null)
				return false;
		} else if (!custom6.equals(other.custom6))
			return false;
		if (custom7 == null) {
			if (other.custom7 != null)
				return false;
		} else if (!custom7.equals(other.custom7))
			return false;
		if (custom8 == null) {
			if (other.custom8 != null)
				return false;
		} else if (!custom8.equals(other.custom8))
			return false;
		if (custom9 == null) {
			if (other.custom9 != null)
				return false;
		} else if (!custom9.equals(other.custom9))
			return false;
		if (deleted == null) {
			if (other.deleted != null)
				return false;
		} else if (!deleted.equals(other.deleted))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (enteringUser == null) {
			if (other.enteringUser != null)
				return false;
		} else if (!enteringUser.equals(other.enteringUser))
			return false;
		if (existPreview == null) {
			if (other.existPreview != null)
				return false;
		} else if (!existPreview.equals(other.existPreview))
			return false;
		if (goodCount == null) {
			if (other.goodCount != null)
				return false;
		} else if (!goodCount.equals(other.goodCount))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (hits == null) {
			if (other.hits != null)
				return false;
		} else if (!hits.equals(other.hits))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (isParson == null) {
			if (other.isParson != null)
				return false;
		} else if (!isParson.equals(other.isParson))
			return false;
		if (isRecommend == null) {
			if (other.isRecommend != null)
				return false;
		} else if (!isRecommend.equals(other.isRecommend))
			return false;
		if (isTop == null) {
			if (other.isTop != null)
				return false;
		} else if (!isTop.equals(other.isTop))
			return false;
		if (lastUpdateDate == null) {
			if (other.lastUpdateDate != null)
				return false;
		} else if (!lastUpdateDate.equals(other.lastUpdateDate))
			return false;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		if (nodeIdPath == null) {
			if (other.nodeIdPath != null)
				return false;
		} else if (!nodeIdPath.equals(other.nodeIdPath))
			return false;
		if (normalCount == null) {
			if (other.normalCount != null)
				return false;
		} else if (!normalCount.equals(other.normalCount))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (passedTime == null) {
			if (other.passedTime != null)
				return false;
		} else if (!passedTime.equals(other.passedTime))
			return false;
		if (passedUser == null) {
			if (other.passedUser != null)
				return false;
		} else if (!passedUser.equals(other.passedUser))
			return false;
		if (previewUrl == null) {
			if (other.previewUrl != null)
				return false;
		} else if (!previewUrl.equals(other.previewUrl))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (stateMessage == null) {
			if (other.stateMessage != null)
				return false;
		} else if (!stateMessage.equals(other.stateMessage))
			return false;
		if (subSiteId == null) {
			if (other.subSiteId != null)
				return false;
		} else if (!subSiteId.equals(other.subSiteId))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (textKeywords == null) {
			if (other.textKeywords != null)
				return false;
		} else if (!textKeywords.equals(other.textKeywords))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (urlSize == null) {
			if (other.urlSize != null)
				return false;
		} else if (!urlSize.equals(other.urlSize))
			return false;
		if (urlType == null) {
			if (other.urlType != null)
				return false;
		} else if (!urlType.equals(other.urlType))
			return false;
		if (wrongCount == null) {
			if (other.wrongCount != null)
				return false;
		} else if (!wrongCount.equals(other.wrongCount))
			return false;
		return true;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setBakText(String bakText) {
		this.bakText = bakText;
	}
	public String getBakText() {
		return bakText;
	}
}
