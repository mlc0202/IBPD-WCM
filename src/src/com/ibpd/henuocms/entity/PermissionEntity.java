package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 权限表 
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Permission")
public class PermissionEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 功能ID
	 */
	@Column(name="f_funId",nullable=true)
	private Long funId=Long.parseLong("-1");
	/**
	 * 权限生效时间
	 */
	@Temporal(value=TemporalType.DATE)
	@Column(name="f_startDate",nullable=true)
	private Date startDate=new Date();
	/**
	 * 权限结束时间
	 */
	@Temporal(value=TemporalType.DATE)
	@Column(name="f_endDate",nullable=true)
	private Date endDate=new Date();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFunId() {
		return funId;
	}
	public void setFunId(Long funId) {
		this.funId = funId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
