package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 电子海报 
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Paper")
public class PaperEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_paperName",length=100,nullable=true)
	private String paperName=" ";
	@Column(name="f_type",nullable=true)
	private Integer type=0;
	@Column(name="f_publishDir",length=500,nullable=true)
	private String publishDir=" ";
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updateTime",nullable=true)
	private Date updateTime=new Date();//更新时间
	@Column(name="f_Order",nullable=true)
	private Integer Order=0;
	@Column(name="f_intro",length=500,nullable=true)
	private String intro;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPaperName() {
		return paperName;
	}
	public void setPaperName(String paperName) {
		this.paperName = paperName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getPublishDir() {
		return publishDir;
	}
	public void setPublishDir(String publishDir) {
		this.publishDir = publishDir;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOrder() {
		return Order;
	}
	public void setOrder(Integer order) {
		Order = order;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	
}
