package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 角色-权限表 中间表
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_RolePermission")
public class RolePermissionEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ID 
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 功能ID
	 */
	@Column(name="f_funId",nullable=true)
	private Long funId=Long.parseLong("-1");
	/**
	 * 模块ID
	 */
	@Column(name="f_modelId",nullable=true)
	private Long modelId=Long.parseLong("-1");
	/**
	 * 类型ID
	 */
	@Column(name="f_typeId",nullable=true)
	private Integer typeId=-1;
	/**
	 * 角色ID
	 */
	@Column(name="f_roleId",nullable=true)
	private Long roleId=Long.parseLong("-1");
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public void setFunId(Long funId) {
		this.funId = funId;
	}
	public Long getFunId() {
		return funId;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public Long getModelId() {
		return modelId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public Integer getTypeId() {
		return typeId;
	}		

}
