/**
 * @author 马根
 * @version 1.0
 * @description 用户
 */
package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 用户
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_User")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region="javaClassName")
public class UserEntity  extends IBaseEntity{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	public static final Integer USER_STATE_NOPASS=0;
	public static final Integer USER_STATE_PASS=99;
	/** 
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 用户名
	 */
	@Column(name="f_userName",length=50,nullable=true)
	private String userName=" ";
	/**
	 * 所属站点ID
	 */
	@Column(name="f_subSiteId",nullable=true)
	private Long subSiteId=Long.parseLong("-1");
	/**
	 * 密码
	 */
	@Column(name="f_password",length=50,nullable=true)
	private String password=" ";//密码
	/**
	 * 是否允许一个用户在多个地点 同时登录
	 */
	@Column(name="f_anyAddressLogin",nullable=true)
	private Boolean anyAddressLogin=false;
	/**
	 * 密码提示问题-曲子attribute表
	 */
	@Column(name="f_question",length=50,nullable=true)
	private String question=" ";//安全问题,可以不使用啊
	/**
	 * 密码提示答案
	 */
	@Column(name="f_answer",length=50,nullable=true)
	private String answer=" ";//安全问题的档案
	/**
	 * 性别
	 */
	@Column(name="f_sex",length=10,nullable=true)
	private String sex="男";
	/**
	 * 年龄
	 */
	@Column(name="f_age",nullable=true)
	private Integer age=0;
	/**
	 * 排序
	 */
	@Column(name="f_order",nullable=true)
	private Integer order=0;

	/**
	 * email
	 */
	@Column(name="f_email",length=50,nullable=true)
	private String email=" ";//用户的邮件地址
	/**
	 * 创建时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间

	/**
	 * 登录时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_LoginTime",nullable=true)
	private Date LoginTime=new Date();//本次登陆时间
	
	/**
	 * 最后一次登录时间-上次登录时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_lastLoginTime",nullable=true)
	private Date lastLoginTime=new Date();//上次登陆时间

	/**
	 * 用户锁定
	 */
	@Column(name="f_locked",nullable=true)
	private Boolean locked=false;//是否锁定

	/**
	 * 用户状态
	 */
	@Column(name="f_state",nullable=true)
	private Integer state=0;//审核状态,默认未通过

	/**
	 * 登录次数
	 */
	@Column(name="f_loginCount",nullable=true)
	private Integer loginCount=0;//登陆次数
	/**
	 * 用户所属部门-用户组或处室
	 */
	@Column(name="f_group",length=50,nullable=true)
	private String group=" ";
	/**
	 * 用户真实姓名
	 */
	@Column(name="f_trueName",length=50,nullable=true)
	private String trueName=" ";//昵称
	/**
	 * 用户类型
	 */
	@Column(name="f_userType",length=50,nullable=true)
	private String userType=" ";
	/**
	 * 职位
	 */
	@Column(name="f_worker",length=50,nullable=true)
	private String worker=" ";
	/**
	 * 证件类型-从attribute中取值
	 */
	@Column(name="f_credentialType",length=50,nullable=true)
	private String credentialType=" ";
	/**
	 * 证件号码
	 */
	@Column(name="f_credentialNo",length=50,nullable=true)
	private String credentialNo=" ";
	
	/**
	 * 通信地址-实名制使用
	 */
	private String address;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLoginTime() {
		return LoginTime;
	}

	public void setLoginTime(Date loginTime) {
		LoginTime = loginTime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getWorker() {
		return worker;
	}

	public void setWorker(String worker) {
		this.worker = worker;
	}

	public String getCredentialType() {
		return credentialType;
	}

	public void setCredentialType(String credentialType) {
		this.credentialType = credentialType;
	}

	public String getCredentialNo() {
		return credentialNo;
	}

	public void setCredentialNo(String credentialNo) {
		this.credentialNo = credentialNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setSubSiteId(Long subSiteId) {
		this.subSiteId = subSiteId;
	}

	public Long getSubSiteId() {
		return subSiteId;
	}

	public void setAnyAddressLogin(Boolean anyAddressLogin) {
		this.anyAddressLogin = anyAddressLogin;
	}

	public Boolean getAnyAddressLogin() {
		return anyAddressLogin;
	}
}
