package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 用户-角色表 中间表
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_UserRole")
public class UserRoleEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/** 
	 * 角色ID
	 */
	@Column(name="f_roleId",nullable=true)
	private Long roleId=Long.parseLong("-1");
	/**
	 * 用户ID
	 */
	@Column(name="f_userId",nullable=true)
	private Long userId=Long.parseLong("-1");
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getRoleId() {
		return roleId;
	}		

}
