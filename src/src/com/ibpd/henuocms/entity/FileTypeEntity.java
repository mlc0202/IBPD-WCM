package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 文件类型记录 
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_FileType")
public class FileTypeEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static String FILETYPEEXT_SPLITCHAR=";";
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_fileTypeName",length=50,nullable=true)
	private String fileTypeName=" ";
	@Column(name="f_fileTypeIcon",length=50,nullable=true)
	private String fileTypeIcon="";
	@Column(name="f_fileExts",length=255,nullable=true)
	private String fileExts=" ";
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updateTime",nullable=true)
	private Date updateTime=new Date();//更新时间
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileTypeName() {
		return fileTypeName;
	}
	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}
	public String getFileTypeIcon() {
		return fileTypeIcon;
	}
	public void setFileTypeIcon(String fileTypeIcon) {
		this.fileTypeIcon = fileTypeIcon;
	}
	public String getFileExts() {
		return fileExts;
	}
	public void setFileExts(String fileExts) {
		this.fileExts = fileExts;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}

	
}
