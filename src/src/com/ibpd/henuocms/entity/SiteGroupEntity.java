package com.ibpd.henuocms.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 子站组(暂时不用)
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_SiteGroup")
public class SiteGroupEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 标题 
	 */
	@Column(name="f_title",length=50,nullable=true)
	private String title=" ";
	/**
	 * 备注
	 */
	@Column(name="f_remark",length=50,nullable=true)
	private String remark=" ";
	@Temporal(value=TemporalType.DATE)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SiteGroupEntity other = (SiteGroupEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getCreateDate() {
		return createDate;
	}
}