package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 功能模块表 包含功能模块编号和功能编号，这些编号有继承关系
 * @author 马根
 * @version 1.0
 */
@Entity 
@Table(name="T_Fun")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE,region="javaClassName")
public class FunEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 功能名称
	 */
	@Column(name="f_funName",length=50,nullable=true)
	private String funName=" ";
	/**
	 * 功能分组
	 */
	@Column(name="f_funGroup",length=50,nullable=true)
	private String funGroup=" ";
	public String getFunSubGroup() {
		return funSubGroup;
	}
	public void setFunSubGroup(String funSubGroup) {
		this.funSubGroup = funSubGroup;
	}
	/**
	 * 功能分组
	 */
	@Column(name="f_funSubGroup",length=50,nullable=true)
	private String funSubGroup=" ";
	/**
	 * 功能编号
	 */
	@Column(name="f_funNo",length=50,nullable=true)
	private String funNo=" ";
	/**
	 * 功能对应的页面
	 */
	@Column(name="f_webName",length=50,nullable=true)
	private String webName=" ";
	/**
	 * 该模块是否被冻结，如果冻结，该模块将不能分配权限
	 */
	@Column(name="f_isLocked",nullable=true)
	private Boolean isLocked=false;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFunName() {
		return funName;
	}
	public void setFunName(String funName) {
		this.funName = funName;
	}
	public String getFunNo() {
		return funNo;
	}
	public void setFunNo(String funNo) {
		this.funNo = funNo;
	}
	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}
	public Boolean getIsLocked() {
		return isLocked;
	}
	public void setWebName(String webName) {
		this.webName = webName;
	}
	public String getWebName() {
		return webName;
	}
	public void setFunGroup(String funGroup) {
		this.funGroup = funGroup;
	}
	public String getFunGroup() {
		return funGroup;
	}

}
