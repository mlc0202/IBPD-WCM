package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 角色表
 * 角色表中需要固定一些基本的角色，这些角色在建立用户之初即被用户拥有，这些角色没有时间限制，是每个实体用户所必须有的
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_Role")
public class RoleEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 角色名称
	 */
	@Column(name="f_name",length=50,nullable=true)
	private String name=" ";
	/**
	 * 角色备注
	 */
	@Column(name="f_remark",length=50,nullable=true)
	private String remark=" ";
	/**
	 * 排序
	 */
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark() {
		return remark;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Integer getOrder() {
		return order;
	}
}
