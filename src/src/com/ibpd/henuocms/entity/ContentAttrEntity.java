package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * �������� 
 * @author ����
 * @version 1.0
 */
@Entity
@Table(name="T_ContentAttr")
public class ContentAttrEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_contentId",nullable=true)
	private Long contentId=-1L;
	@Column(name="f_strong",nullable=true)
	private Boolean strong=false;
	@Column(name="f_color",length=10,nullable=true)
	private String color="";
	@Column(name="f_size",length=5,nullable=true)
	private String size="";
	@Column(name="f_em",nullable=true)
	private Boolean em=false;
	@Column(name="f_u",nullable=true)
	private Boolean u=false;
	@Column(name="f_copyTo",length=255,nullable=true)
	private String copyTo="";
	@Column(name="f_linkTo",length=255,nullable=true)
	private String linkTo="";
	@Column(name="f_copyToContentIds",length=255,nullable=true)
	private String copyToContentIds="";
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getContentId() {
		return contentId;
	}
	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}
	public Boolean getStrong() {
		return strong;
	}
	public void setStrong(Boolean strong) {
		this.strong = strong;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public Boolean getEm() {
		return em;
	}
	public void setEm(Boolean em) {
		this.em = em;
	}
	public Boolean getU() {
		return u;
	}
	public void setU(Boolean u) {
		this.u = u;
	}
	public String getCopyTo() {
		return copyTo;
	}
	public void setCopyTo(String copyTo) {
		this.copyTo = copyTo;
	}
	public String getLinkTo() {
		return linkTo;
	}
	public void setLinkTo(String linkTo) {
		this.linkTo = linkTo;
	}
	public void setCopyToContentIds(String copyToContentIds) {
		this.copyToContentIds = copyToContentIds;
	}
	public String getCopyToContentIds() {
		return copyToContentIds;
	}
	
}
