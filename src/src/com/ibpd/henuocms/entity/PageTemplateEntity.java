package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 页面模板
 * @author 马根
 * @version 1.0 
 */
@Entity
@Table(name="T_PageTemplate")
public class PageTemplateEntity extends IBaseEntity{
	public static final Integer TEMPLATE_TYPE_SITE=2;
	public static final Integer TEMPLATE_TYPE_NODE=1;
	public static final Integer TEMPLATE_TYPE_CONTENT=0;
	public static final Integer TEMPLATE_TYPE_COMMENT=-1;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 标题
	 */
	@Column(name="f_title",length=50,nullable=true)
	private String title=" ";
	/**
	 * 所属站点ID
	 */
	@Column(name="f_subSiteId",nullable=true)
	private Long subSiteId=Long.parseLong("-1");
	/**
	 * 所属栏目ID
	 */
	@Column(name="f_nodeId",nullable=true)
	private Long nodeId=Long.parseLong("-1");
	/**
	 * 模板类别
	 */
	@Column(name="f_type",nullable=true)
	private Integer type=0;
	/**
	 * 静止使用鼠标右键
	 */
	@Column(name="f_useMouseRightKey",nullable=true)
	private Boolean useMouseRightKey=true;
	/**
	 * 静止拷贝页面内容
	 */
	@Column(name="f_enableCopyPageContent",nullable=true)
	private Boolean enableCopyPageContent=false;
	/**
	 * 灰度页面
	 */
	@Column(name="f_useDarkColor",nullable=true)
	private Boolean useDarkColor=false;
	public Boolean getUseDarkColor() {
		return useDarkColor;
	}
	public void setUseDarkColor(Boolean useDarkColor) {
		this.useDarkColor = useDarkColor;
	}
	/**
	 * 文件大小
	 */
	@Column(name="f_size",nullable=true)
	private Long size=0L;
	/**
	 * 附件大小（整个文件夹大小）
	 */
	@Column(name="f_folderSize",nullable=true)
	private Long folderSize=0L;
	/**
	 * 创建时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	/**
	 * 最后更新时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_lastUpdateDate",nullable=true)
	private Date lastUpdateDate=new Date();
	/**
	 * 默认的样式模板ID
	 */
	@Column(name="f_defaultStyleTemplateId",nullable=true)
	private Long defaultStyleTemplateId=Long.parseLong("-1");
	/**
	 * 模板文件路径
	 */
	@Column(name="f_templateFilePath",length=500,nullable=true)
	private String templateFilePath=" ";
	/**
	 * 排序
	 */
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	/**
	 * 分组
	 */
	@Column(name="f_group",length=50,nullable=true)
	private String group=" ";
	/**
	 * 备注
	 */
	@Column(name="f_remark",length=50,nullable=true)
	private String remark=" ";
	/**
	 * 是否是默认模板 默认模板不允许删除需
	 */
	@Column(name="f_isDefault",nullable=true)
	private Boolean isDefault=false;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PageTemplateEntity other = (PageTemplateEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Boolean getUseMouseRightKey() {
		return useMouseRightKey;
	}
	public void setUseMouseRightKey(Boolean useMouseRightKey) {
		this.useMouseRightKey = useMouseRightKey;
	}
	public Boolean getEnableCopyPageContent() {
		return enableCopyPageContent;
	}
	public void setEnableCopyPageContent(Boolean enableCopyPageContent) {
		this.enableCopyPageContent = enableCopyPageContent;
	}
//	public Boolean getUseDarkColor() {
//		return useDarkColor;
//	}
//	public void setUseDarkColor(Boolean useDarkColor) {
//		this.useDarkColor = useDarkColor;
//	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public Long getFolderSize() {
		return folderSize;
	}
	public void setFolderSize(Long folderSize) {
		this.folderSize = folderSize;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getTemplateFilePath() {
		return templateFilePath;
	}
	public void setTemplateFilePath(String templateFilePath) {
		this.templateFilePath = templateFilePath;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setDefaultStyleTemplateId(Long defaultStyleTemplateId) {
		this.defaultStyleTemplateId = defaultStyleTemplateId;
	}
	public Long getDefaultStyleTemplateId() {
		return defaultStyleTemplateId;
	}
//	public void setSubSiteId(Long subSiteId) {
//		this.subSiteId = subSiteId;
//	}
//	public Long getSubSiteId() {
//		return subSiteId;
//	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getGroup() {
		return group;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	public Long getNodeId() {
		return nodeId;
	}
	public void setSubSiteId(Long subSiteId) {
		this.subSiteId = subSiteId;
	}
	public Long getSubSiteId() {
		return subSiteId;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getType() {
		return type;
	}
	@Override
	public String toString() {
		return "PageTemplateEntity [createDate=" + createDate
				+ ", defaultStyleTemplateId=" + defaultStyleTemplateId
				+ ", enableCopyPageContent=" + enableCopyPageContent
				+ ", folderSize=" + folderSize + ", group=" + group + ", id="
				+ id + ", isDefault=" + isDefault + ", lastUpdateDate="
				+ lastUpdateDate + ", nodeId=" + nodeId + ", order=" + order
				+ ", remark=" + remark + ", size=" + size + ", subSiteId="
				+ subSiteId + ", templateFilePath=" + templateFilePath
				+ ", title=" + title + ", type=" + type + ", useDarkColor="
				+ useDarkColor + ", useMouseRightKey=" + useMouseRightKey + "]";
	}
	
	
	

}
