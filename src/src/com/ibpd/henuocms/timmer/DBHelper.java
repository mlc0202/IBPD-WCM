package com.ibpd.henuocms.timmer;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.hsqldb.Server;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.ServerDetector;
/**
 * 数据库工具类，用于自动启动hsqldb数据库的类 
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:38:46
 */
public class DBHelper {
	 public static final Logger log = Logger.getLogger("InitJob");
	 public static final String TOKEN = "${webapp.root}";
	 
	 public static final int WAIT_TIME = 2000;
	 
	 private String url;
	 
	 private String username;
	 
	 private String password;
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
         System.out.println("===="+arg0.getJobRunTime());
 
    }
    public void execute() throws JobExecutionException, InterruptedException, UnsupportedEncodingException {
 
//        System.out.println("web容器启动成功，即将初始化系统...");
//        System.out.println("web容器为:"+ServerDetector.getServerId());
        startHql();
     }
    private static Server server=null;
    public static void stopServer(){
    	System.out.println("关闭数据库");
    	if(server!=null)
    		server.stop();
    }
    private void startHql() throws InterruptedException, UnsupportedEncodingException{
    	String dbType=IbpdCommon.getInterface().getPropertiesValue("dbType");
    	if(dbType==null){
    		throw new InterruptedException("数据库类型加载错误");
    	}
    	if(!dbType.trim().toLowerCase().equals("hsqldb")){
    		System.out.println("使用外置数据库系统，选择数据库:"+dbType);
    		return;
    	}
    	System.out.println("使用内置hsqldb数据库系统，即将启动数据库系统... ... ");
        username = "root";
        password = "root"; 
        String databaseName = "henuoCMS";
        int port = 9099; 
        String hsqlPath = TOKEN+"/WEB-INF/hsqldb";

          // FIXME: 因为要用到getRealPath方法获得路径，在使用war包发布的时候会出现问题 
          if (hsqlPath.startsWith(TOKEN)) { 
        	  WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();    
//              ServletContext servletContext = webApplicationContext.getServletContext();  
              String realPath=new File(this.getClass().getResource("/").getPath()).getParentFile().getParentFile().getAbsolutePath();
//              String webappRoot = servletContext.getRealPath("/").replace("\\", "/"); 
              String webappRoot=realPath.replace("\\", "/");
              hsqlPath = hsqlPath.substring(TOKEN.length()); 
              hsqlPath = webappRoot + hsqlPath; 
          }
          String databasePath = hsqlPath + "/" + databaseName; 
          url = "jdbc:hsqldb:hsql://localhost:" + port + "/" + databaseName; 
          server = new Server(); 
          server.setDatabaseName(0, databaseName); 
          server.setDatabasePath(0, databasePath); 
          server.setPort(port);  
          server.setSilent(true);
          server.start();
          Thread.sleep(WAIT_TIME); 
          log.info("Hsqldb启动成功！");


    }

}
