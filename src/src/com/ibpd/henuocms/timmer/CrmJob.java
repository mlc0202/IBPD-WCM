package com.ibpd.henuocms.timmer;

import java.sql.Connection;
import java.sql.SQLException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ibpd.test.SQLDBUtil;
/*
              _ooOoo_
             o8888888o
             88" . "88
             (| -_- |)
             O\  =  /O
          ____/`---'\____
        .'  \\|     |//  `.
       /  \\|||  :  |||//  \
      /  _||||| -:- |||||-  \
      |   | \\\  -  /// |   |
      | \_|  ''\---/''  |   |
      \  .-\__  `-`  ___/-. /
    ___`. .'  /--.--\  `. . __
 ."" '<  `.___\_<|>_/___.'  >'"".
| | :  `- \`.;`\ _ /`;.`/ - ` : | |
\  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
`=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
佛祖保佑       永无BUG
*/
//佛曰:    
//写字楼里写字间，写字间里程序员；    
//程序人员写程序，又拿程序换酒钱。    
//酒醒只在网上坐，酒醉还来网下眠；    
//酒醉酒醒日复日，网上网下年复年。    
//但愿老死电脑间，不愿鞠躬老板前；    
//奔驰宝马贵者趣，公交自行程序员。    
//别人笑我忒疯癫，我笑自己命太贱；    
//不见满街漂亮妹，哪个归得程序员？
public class CrmJob implements Job {

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
         System.out.println("===="+arg0.getJobRunTime());
 
    }
    public void execute() throws JobExecutionException, InterruptedException {
 
		try {
	        Connection con=SQLDBUtil.getCon();
	        if(con==null){
	        	System.out.println("尚未连接sql connection");
	        } else if(con.isClosed()){
				System.out.println("sql connection链接状态为关闭");
				con=null;
	        }else{
	        	if (con != null){
	        		SQLDBUtil.CloseCon();
	        	}
	        }
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
       }
        
}