package com.ibpd.henuocms.timmer;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.hsqldb.Server;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.ServerDetector;
/*
              _ooOoo_
             o8888888o
             88" . "88
             (| -_- |)
             O\  =  /O
          ____/`---'\____
        .'  \\|     |//  `.
       /  \\|||  :  |||//  \
      /  _||||| -:- |||||-  \
      |   | \\\  -  /// |   |
      | \_|  ''\---/''  |   |
      \  .-\__  `-`  ___/-. /
    ___`. .'  /--.--\  `. . __
 ."" '<  `.___\_<|>_/___.'  >'"".
| | :  `- \`.;`\ _ /`;.`/ - ` : | |
\  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
`=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
佛祖保佑       永无BUG
*/
//佛曰:    
//写字楼里写字间，写字间里程序员；    
//程序人员写程序，又拿程序换酒钱。    
//酒醒只在网上坐，酒醉还来网下眠；    
//酒醉酒醒日复日，网上网下年复年。    
//但愿老死电脑间，不愿鞠躬老板前；    
//奔驰宝马贵者趣，公交自行程序员。    
//别人笑我忒疯癫，我笑自己命太贱；    
//不见满街漂亮妹，哪个归得程序员？
public class InitJob implements Job {
	 public static final Logger log = Logger.getLogger("InitJob");
	 public static final String TOKEN = "${webapp.root}";
	 
	 public static final int WAIT_TIME = 2000;
	 
	 private String url;
	 
	 private String username;
	 
	 private String password;
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
         System.out.println("===="+arg0.getJobRunTime());
 
    }
    public void execute() throws JobExecutionException, InterruptedException, UnsupportedEncodingException {
 
        System.out.println("web容器启动成功，即将初始化系统...");
        System.out.println("web容器为:"+ServerDetector.getServerId());
//        startHql();
     }
    private static Server server=null;
    public static Server getServer() {
		return server;
	}
	public static void setServer(Server server) {
		InitJob.server = server;
	}
	public static void stopServer(){
//    	System.out.println("关闭数据库");
//    	if(server!=null)
//    		server.stop();
    }
    /**
     * 
     * @throws InterruptedException
     * @throws UnsupportedEncodingException
     */
    private void startHql() throws InterruptedException, UnsupportedEncodingException{
    	String dbType=IbpdCommon.getInterface().getPropertiesValue("dbType");
    	if(dbType==null){
    		throw new InterruptedException("数据库类型加载错误");
    	}
    	if(!dbType.trim().toLowerCase().equals("hsqldb")){
    		System.out.println("使用外置数据库系统，选择数据库:"+dbType);
    		return;
    	}
    	System.out.println("使用内置hsqldb数据库系统，即将启动数据库系统... ... ");
        username = "root";
        password = "root"; 
        String databaseName = "henuoCMS";
        int port = 9099; 
        String hsqlPath = TOKEN+"/WEB-INF/hsqldb";

          // FIXME: 因为要用到getRealPath方法获得路径，在使用war包发布的时候会出现问题 
          if (hsqlPath.startsWith(TOKEN)) { 
        	  WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();    
//              ServletContext servletContext = webApplicationContext.getServletContext();  
              String realPath=new File(this.getClass().getResource("/").getPath()).getParentFile().getParentFile().getAbsolutePath();
//              String webappRoot = servletContext.getRealPath("/").replace("\\", "/"); 
              String webappRoot=realPath.replace("\\", "/");
              hsqlPath = hsqlPath.substring(TOKEN.length()); 
              hsqlPath = webappRoot + hsqlPath; 
          }
          String databasePath = hsqlPath + "/" + databaseName; 
          url = "jdbc:hsqldb:hsql://localhost:" + port + "/" + databaseName; 
          server = new Server(); 
          server.setDatabaseName(0, databaseName); 
          server.setDatabasePath(0, databasePath); 
          server.setPort(port);  
          server.setSilent(true);
          server.start();
          Thread.sleep(WAIT_TIME); 
          log.info("Hsqldb启动成功！");


    }
}