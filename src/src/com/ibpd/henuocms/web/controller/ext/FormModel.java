package com.ibpd.henuocms.web.controller.ext;

import java.lang.annotation.Documented;

import java.lang.annotation.ElementType;

import java.lang.annotation.Retention;

import java.lang.annotation.RetentionPolicy;

import java.lang.annotation.Target;
/**
 * 用于spring mvc传多个对象参数使用的注解 
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:45:58
 */
@Target( { ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FormModel {

	String value();

}