package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.assist.PermissionModelEntity;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;

@Controller
public class RecycleNode extends BaseController {
	@RequestMapping("Manage/RecycleNode/index.do")
	public String index(org.springframework.ui.Model model,HttpServletRequest req,String nodeId) throws IOException{
		model.addAttribute("pageTitle","栏目回收站");
		model.addAttribute("nodeId",nodeId);
		if(nodeId.indexOf("_")==-1){
			model.addAttribute("node_recycle_restore",SysUtil.checkPermissionIsEnable(req.getSession(), "node_recycle_restore", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("node_recycle_del",SysUtil.checkPermissionIsEnable(req.getSession(), "node_recycle_del", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("node_recycle_reload",SysUtil.checkPermissionIsEnable(req.getSession(), "node_recycle_reload", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_NODE));
		}else{
			nodeId=nodeId.split("_")[1];
			model.addAttribute("node_recycle_restore",SysUtil.checkPermissionIsEnable(req.getSession(), "node_recycle_restore", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("node_recycle_del",SysUtil.checkPermissionIsEnable(req.getSession(), "node_recycle_del", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("node_recycle_reload",SysUtil.checkPermissionIsEnable(req.getSession(), "node_recycle_reload", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_SITE));
		}
		return "manage/recyclenode/index";
	}
	@RequestMapping("Manage/RecycleNode/doRestore.do")
	public void doRestore(String ids,HttpServletResponse resp) throws IOException{
		INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		if(StringUtils.isBlank(ids)){
			super.printMsg(resp, "-1", "-1", "参数缺失");
			return;
		}
		String[] idss=ids.split(",");
		for(String id:idss){
			if(StringUtils.isNumeric(id)){
				NodeEntity n=nodeService.getEntityById(Long.valueOf(id));
				if(n!=null){
					n.setIsDeleted(false);
				}
			}
		}
		super.printMsg(resp, "1", "1", "保存成功");
	}
	@RequestMapping("Manage/RecycleNode/doDel.do")
	public void doDel(String ids,HttpServletResponse resp) throws IOException{
		INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		IContentService contServ=(IContentService) getService(ContentServiceImpl.class);
		if(StringUtils.isBlank(ids)){
			super.printMsg(resp, "-1", "-1", "参数缺失");
			return;
		}
		String[] idss=ids.split(",");
		for(String id:idss){
			if(StringUtils.isNumeric(id)){
				
				List<NodeEntity> nl=nodeService.getList("from "+nodeService.getTableName()+" where nodeIdPath like '%,"+id+",%'", null);
				//这里不管下面的栏目是否可见，一律删除，反正都看不见了
				if(nl!=null){
					for(NodeEntity n:nl){
						nodeService.deleteByPK(n.getId());
						List<ContentEntity>contList=contServ.getList("from "+contServ.getTableName()+" where nodeId="+n.getId(), null);
						for(ContentEntity c:contList){
							contServ.deleteByPK(c.getId());
						}
					} 
				}
			}
		}
		
		nodeService.batchDel(ids);
		super.printMsg(resp, "1", "1", "保存成功");
	}
	@RequestMapping("Manage/RecycleNode/list.do")
	public void getList(String id,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req){
		INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		String ss[]=id.split("_");
		String query="";
		if(ss.length==2){
			query="subSiteId="+ss[1]+" and parentId=-1";
		}else{
			query="parentId="+id;
		}
		if(queryString!=null && !queryString.trim().equals("")){
			query+=" and text like '%"+queryString.trim()+"%'";
		}
		query+=" and isDeleted=true"; 
//		nodeService.getDao().clearCache();
		super.getList(req, nodeService, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/RecycleNode/props.do")
	public String props(Long id,Model model) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null || id<0L){
			model.addAttribute("errorMsg","参数错误");
			return super.ERROR_PAGE;
		}
		INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		NodeEntity ce=nodeService.getEntityById(id);
		if(ce==null){
			model.addAttribute("errorMsg","没有该栏目");
			return super.ERROR_PAGE;
		}
		model.addAttribute("pageTitle","参数设置");
		model.addAttribute("htmls",super.getHtmlString(ce, "recyclenode.edit.field"));
		model.addAttribute("entity",ce);
		return "manage/recyclenode/props";
	}	
	@RequestMapping("Manage/RecycleNode/saveProp.do")
	public void saveProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		if(id!=null && id!=-1){
			INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
			NodeEntity cata=nodeService.getEntityById(id);
				if(cata!=null){
					//先确定参数类型
					Method tmpMethod=cata.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
					
					
					Method method=cata.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
					Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
					IbpdLogger.getLogger(this.getClass()).info("value="+val);
					method.invoke(cata, val);
					nodeService.saveEntity(cata);
//					nodeService.getDao().clearCache();
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");		
				}else{
					super.printMsg(resp, "-1", "", "没有该类别");
				}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");			
		}
	}
}
