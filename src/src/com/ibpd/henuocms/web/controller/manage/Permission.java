package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import sun.security.acl.PermissionImpl;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.TxtUtil;
import com.ibpd.henuocms.entity.FunEntity;
import com.ibpd.henuocms.entity.PermissionEntity;
import com.ibpd.henuocms.service.fun.FunServiceImpl;
import com.ibpd.henuocms.service.fun.IFunService;
import com.ibpd.henuocms.service.permission.IPermissionService;
import com.ibpd.henuocms.service.permission.PermissionServiceImpl;

@Controller
public class Permission extends BaseController {
	@RequestMapping("Manage/Permission/init.do")
	public void init(HttpServletRequest req,HttpServletResponse resp) throws IOException{
		String s=TxtUtil.readTxtFile("h:\\22.txt");
		s=s.replace("\r", "");
		//s=s.replace("\n", "");
		if(s!=null){
			String[] ss=s.split("\n");
			IFunService fs=(IFunService) ServiceProxyFactory.getServiceProxy(FunServiceImpl.class);
			IPermissionService ps=(IPermissionService) ServiceProxyFactory.getServiceProxy(PermissionServiceImpl.class);
			List<FunEntity> fl=fs.getList();
			if(fl!=null){
				for(FunEntity f:fl){
					fs.deleteByPK(f.getId());
				}
			}
			List<PermissionEntity> pl=ps.getList();
			if(pl!=null){
				for(PermissionEntity p:pl){
					ps.deleteByPK(p.getId());
				}
			}
			for(String d:ss){
				IbpdLogger.getLogger(this.getClass()).info(d);
				String[] dd=d.split(",");
				FunEntity f=new FunEntity();
				f.setFunName(dd[0]);
				f.setFunNo(dd[2]);
				f.setWebName(dd[1]);
				f.setFunGroup(dd[3]);
				f.setFunSubGroup(dd[4]);
				f.setIsLocked(false);
				fs.saveEntity(f);
				PermissionEntity p=new PermissionEntity();
				Date startDate=Date.valueOf("2015-01-01");
				Date endDate=Date.valueOf("2999-12-31");
				p.setEndDate(endDate);
				p.setFunId(f.getId());
				p.setStartDate(startDate);
				ps.saveEntity(p);
			}
			List l=fs.getList();
			super.sendJsonByList(resp, l, Long.valueOf(l.size()));
		}else{
			super.printMsg(resp, "-1", "-1", "��ȡ����");
		}
	}	
	public static void main(String[] a) throws IOException{
		String s=TxtUtil.readTxtFile("h:\\22.txt");
		s=s.replace("\r", "");
		//s=s.replace("\n", "");
		if(s!=null){
			String[] ss=s.split("\n");
			IPermissionService ps=(IPermissionService) ServiceProxyFactory.getServiceProxy(PermissionServiceImpl.class);
			IFunService fs=(IFunService) ServiceProxyFactory.getServiceProxy(FunServiceImpl.class);
			for(String d:ss){
				IbpdLogger.getLogger(Permission.class).info(d);
				String[] dd=d.split(",");
				FunEntity f=new FunEntity();
				f.setFunName(dd[0]);
				f.setFunNo(dd[2]);
				f.setWebName(dd[1]);
				f.setIsLocked(false);
				fs.saveEntity(f);
				PermissionEntity p=new PermissionEntity();
				Date startDate=Date.valueOf("2015-01-01");
				Date endDate=Date.valueOf("2999-12-31");
				p.setEndDate(endDate);
				p.setFunId(f.getId());
				p.setStartDate(startDate);
				ps.saveEntity(p);
			}
			List l=fs.getList();
			IbpdLogger.getLogger(Permission.class).info(l.size());
		}else{
			IbpdLogger.getLogger(Permission.class).info("error");
		}
	}	
}
