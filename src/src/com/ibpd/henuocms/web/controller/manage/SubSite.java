package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.ext.PageTemplateExtEntity;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;

@Controller
public class SubSite extends BaseController {

	@RequestMapping("Manage/SubSite/index.do")
	public String index(Model model,HttpServletRequest req) throws IOException{
		return "manage/subSite/index";
	}
	@RequestMapping("Manage/SubSite/list.do")
	public void list(String id,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
//		nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
//		String ss[]=id.split("_");
//		String query="";
//		if(ss.length==2){
//			query="subSiteId="+ss[1];
//		}else{
//			query="parentId="+id;
//		}
//		if(queryString!=null && !queryString.trim().equals("")){
//			query+=" and text like '%"+queryString.trim()+"%'";
//		}
//		query+=" and isDeleted=false"; 
//		System.out.println(order+"\n"+sort);
//		super.getList(req, nodeService, resp, order, page, rows, sort, query);
	}
 
	@RequestMapping("Manage/SubSite/props.do")
	public String props(String id,Model model){
		return "manage/subSite/props";
	}
	@RequestMapping("Manage/SubSite/saveProp.do")
	public void saveProps(Long id, String field, String value,
			HttpServletResponse resp) throws NoSuchMethodException,
			SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		if (id != null && id != -1) {
			ISubSiteService siteService = (ISubSiteService) getService(SubSiteServiceImpl.class);
			SubSiteEntity node = siteService.getEntityById(id);
			if (node != null) {
				// 先确定参数类型
				Method tmpMethod = node.getClass().getMethod(
						"get" + field.substring(0, 1).toUpperCase()
								+ field.substring(1), new Class[] {});
				Method method = node.getClass().getMethod(
						"set" + field.substring(0, 1).toUpperCase()
								+ field.substring(1),
						new Class[] { tmpMethod.getReturnType() });
				Object val = getValue(value, tmpMethod.getReturnType()
						.getSimpleName());
				method.invoke(node, val);
				siteService.saveEntity(node);
				this.makeStaticPage(MakeType.站点, id, null, null);
			}
		}
	}
}
