package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.dao.impl.HqlParameter.DataType_Enum;
import com.ibpd.entity.baseEntity.IBaseEntity;
import com.ibpd.henuocms.assist.PermissionModelEntity;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.NodeFormEntity;
import com.ibpd.henuocms.entity.ext.NodeFormExtEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;
import com.ibpd.henuocms.service.contentAttr.ContentAttrServiceImpl;
import com.ibpd.henuocms.service.contentAttr.IContentAttrService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeForm.INodeFormService;
import com.ibpd.henuocms.service.nodeForm.NodeFormServiceImpl;
import com.ibpd.henuocms.web.controller.manage.BaseController.MakeType;

@Controller
public class Content extends BaseController {
	IContentService contentService=null;
	@RequestMapping("Manage/Content/index.do")
	public String index(Model model,String nodeId,HttpServletRequest req) throws IOException{
		String id=nodeId;
		if(id.indexOf("_")==-1){
			model.addAttribute("permissionEnable_contAdd",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_add", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contEdit",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_edit", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contDel",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_del", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contReload",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_reload", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contMove",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_move", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contLink",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_link", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contCopy",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_copy", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contLock",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_lock", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contTop",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_top", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contRecommend",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recommend", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contExport",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_export", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contImport",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_import", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contPublish",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_publish", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("permissionEnable_contSearch",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_search", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_NODE));			
		}else{
			id=nodeId.split("_")[1];
			model.addAttribute("permissionEnable_contAdd",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_add", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contEdit",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_edit", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contDel",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_del", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contReload",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_reload", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contMove",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_move", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contLink",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_link", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contCopy",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_copy", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contLock",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_lock", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contTop",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_top", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contRecommend",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recommend", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contExport",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_export", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contImport",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_import", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contPublish",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_publish", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("permissionEnable_contSearch",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_search", Long.valueOf(id),PermissionModelEntity.MODEL_TYPE_SITE));			
			
		} 
		return "manage/content/index";
	}

	@RequestMapping("Manage/Content/list.do")
	public void list(String nodeId,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
		String ss[]=nodeId.split("_");
		String query="";
		if(ss.length==2){
			query="subSiteId="+ss[1];
		}else{
			query="nodeIdPath like '%,"+nodeId+",%'";
		}
		if(queryString!=null && !queryString.trim().equals("")){
			query+=" and title like '%"+queryString.trim()+"%'";
		}
		query+=" and deleted=false";
//		contentService.getDao().clearCache();
		super.getList(req, contentService, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Content/props.do")
	public String props(Long id,Model model,HttpServletRequest req){
		if(id!=null && id!=-1){
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			ContentEntity content=contentService.getEntityById(id);
			if(content!=null){
				model.addAttribute("entity",content);
				model.addAttribute("propSaveEnable",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_prop_save", content.getNodeId(),PermissionModelEntity.MODEL_TYPE_NODE));
			}
			//寻找表单并压入前台
			INodeFormService nodeFormService=(INodeFormService) ServiceProxyFactory.getServiceProxy(NodeFormServiceImpl.class);
			NodeFormExtEntity nodeForm=nodeFormService.getNodeForm(NodeFormEntity.FORMTYPE_CONTENTFORM, content.getNodeId(),null);
			model.addAttribute("nodeForm",nodeForm);
			model.addAttribute("fields",nodeForm.getNodeFormFieldList());
		}
		return "manage/content/props";
	}
	@RequestMapping("Manage/Content/saveProp.do")
	public void saveProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id!=null && id!=-1){
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			ContentEntity cnt=contentService.getEntityById(id);
			if(cnt!=null){
				//先确定参数类型
				Method tmpMethod=cnt.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
				
				
				Method method=cnt.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
				Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
				IbpdLogger.getLogger(this.getClass()).info("value="+val);
				method.invoke(cnt, val);
				cnt.setLastUpdateDate(new Date());
				contentService.saveEntity(cnt);
			}
		}
	}
	
	@RequestMapping("Manage/Content/state.do")
	public void state(Long id,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id!=null && id!=-1){
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			ContentEntity cnt=contentService.getEntityById(id);
			if(cnt!=null){
				cnt.setState(cnt.getState()==1?0:1);
				cnt.setLastUpdateDate(new Date());
				contentService.saveEntity(cnt);
			}
		}
	}
	@RequestMapping("Manage/Content/publish.do")
	public void publish(String ids,HttpServletResponse resp) throws IOException{
		if(StringUtils.isBlank(ids)){
			super.printMsg(resp, "-1", "-1", "参数不足");
			return;
		}
		String[] idarr=ids.split(",");
		for(String id:idarr){
			if(StringUtils.isNumeric(id)){
				this.makeStaticPage(MakeType.内容, Long.valueOf(id), null, null);
			}
		}
		super.printMsg(resp, "99", "99", "发布成功");
	}

		@RequestMapping("Manage/Content/toAdd.do")
		public String toAdd(String nodeId,Model model){
			model.addAttribute("pageTitle","添加文章");
			if(nodeId==null){
				return this.ERROR_PAGE;
			}
			String[] ss=nodeId.split("_");
			if(ss.length==2){
				model.addAttribute("msg","请选择栏目");
				return this.ERROR_PAGE;
			}
			if(!StringUtil.isNumeric(nodeId)){
				model.addAttribute("msg","参数不正确");
				return this.ERROR_PAGE;
			}
			model.addAttribute("nodeId",nodeId);
			model.addAttribute("type","add");
			//寻找表单并压入前台
			INodeFormService nodeFormService=(INodeFormService) ServiceProxyFactory.getServiceProxy(NodeFormServiceImpl.class);
			NodeFormExtEntity nodeForm=nodeFormService.getNodeForm(NodeFormEntity.FORMTYPE_CONTENTFORM, Long.parseLong(nodeId),null);
			model.addAttribute("nodeForm",nodeForm);
			model.addAttribute("fields",nodeForm.getNodeFormFieldList());
			return "manage/content/add";
		}
		@RequestMapping("Manage/Content/toEdit.do")
		public String toEdit(Long contentId,Model model){
			model.addAttribute("pageTitle","编辑文章");
			model.addAttribute("type","edit");
			if(contentId==null){
				return this.ERROR_PAGE;
			}
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			ContentEntity ne=contentService.getEntityById(contentId);
			if(ne==null){
				return this.ERROR_PAGE;
			}
			IContentAttrService caService=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);		
			model.addAttribute("entity",ne);
			model.addAttribute("type","edit");
			model.addAttribute("text",ne.getText().replace("\r", "").replace("\n",""));
			model.addAttribute("titleFormatParameters",caService.getTitleFormat(ne.getId()));
			model.addAttribute("copyTo",caService.getCopyTo(ne.getId()));
			model.addAttribute("linkTo",caService.getLinkTo(ne.getId()));
			//寻找表单并压入前台
			INodeFormService nodeFormService=(INodeFormService) ServiceProxyFactory.getServiceProxy(NodeFormServiceImpl.class);
			NodeFormExtEntity nodeForm=nodeFormService.getNodeForm(NodeFormEntity.FORMTYPE_CONTENTFORM, ne.getNodeId(),null);
			model.addAttribute("nodeForm",nodeForm);
			model.addAttribute("fields",nodeForm.getNodeFormFieldList());

			return "manage/content/add";
		}
		@RequestMapping("Manage/Content/doEdit.do")
		public void doEdit(@ModelAttribute() ContentEntity entity,HttpServletResponse resp,String titleFormatParams,String copyTo,String linkTo) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			if(entity!=null && entity.getId()!=null && entity.getId()!=-1L){
				IbpdLogger.getLogger(this.getClass()).info("titleFormatParams="+titleFormatParams+" copyTo="+copyTo+" linkTo="+linkTo);
				//titleFormatParams=color:#FF0000;strong:;em:;u:;size: copyTo= linkTo=
				contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
				ContentEntity ne=contentService.getEntityById(entity.getId());
				if(ne==null){
					String res=getJsonResult("-2",""+entity.getId(),"没有找到该内容");
					resp.getWriter().write(res);
					resp.getWriter().flush();
				}else{
					String nodeIdPath=ne.getNodeIdPath();
					Integer state=ne.getState();
					Boolean deleted=ne.getDeleted();
					Long siteId=ne.getSubSiteId();
					swap(ne,entity);
					ne.setNodeIdPath(nodeIdPath);
					ne.setState(state);
					ne.setDeleted(deleted);
					ne.setSubSiteId(siteId);
					ne.setLastUpdateDate(new Date());
					contentService.saveEntity(ne);
					IContentAttrService caService=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);
					ContentAttrEntity caEntity=caService.getContentAttr(ne.getId());
					caEntity=caEntity==null?new ContentAttrEntity():caEntity;
					Map<String,String> pams=getFormatMap(titleFormatParams);
					caEntity.setColor(pams.get("color"));
					caEntity.setContentId(entity.getId());
					caEntity.setCopyTo(":"+copyTo.replace(",", ":")+":");
					caEntity.setEm(getBoolean(pams.get("em")));
					caEntity.setLinkTo(":"+linkTo.replace(",", ":")+":");
					caEntity.setSize(pams.get("size"));
					caEntity.setStrong(getBoolean(pams.get("strong")));
					caEntity.setU(getBoolean(pams.get("u")));
					caService.saveEntity(caEntity);
					//copyTo fun
//					String[] nodeIds=copyTo.split(",");
//					if(copyTo!=null && !copyTo.trim().equals("")){
//						INodeService nodeSer=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
//						for(String nodeId:nodeIds){
//							NodeEntity node=nodeSer.getEntityById(Long.parseLong(nodeId));
//							ContentEntity ce=new ContentEntity();
//							swap(ce,entity);
//							ce.setId(null);
//							ce.setNodeId(node.getId());
//							ce.setNodeIdPath(node.getNodeIdPath());
//							contentService.saveEntity(ce);
//						}	
//					}
					String res=getJsonResult("99",""+entity.getId(),"保存成功");
					resp.getWriter().write(res);
					resp.getWriter().flush();
				}
			}else{
				String res=getJsonResult("-1","","ID为空");
				resp.getWriter().write(res);
				resp.getWriter().flush();
			}
			
		}
		private IBaseEntity swap(IBaseEntity dbEntity , IBaseEntity obEntity) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			Method[] methods=obEntity.getClass().getMethods();
			for(Method getMethod:methods){
				String getMethodName=getMethod.getName();
				if(getMethodName.substring(0,3).equals("get") && !getMethodName.equals("getClass")){
					String setMethodName="set"+getMethodName.substring(3);
					Method setMethod=dbEntity.getClass().getMethod(setMethodName, getMethod.getReturnType());
					if(setMethod!=null){
						
						setMethod.invoke(dbEntity, getMethod.invoke(obEntity, null));
					}
				}
			}
			return dbEntity;
		}
		private Map<String,String> getFormatMap(String formatString){
			Map<String,String> rtn=new HashMap<String,String>();
			if(formatString!=null || !formatString.trim().equals("")){
				String[] s=formatString.split(";");
				for(String ss:s){
					String[] sss=ss.split(":");		
					if(sss.length==2){
						rtn.put(sss[0], sss[1]);
					}
				}
			}
			return rtn;
		}
		@RequestMapping("Manage/Content/doAdd.do")
		public void doAdd(@ModelAttribute() ContentEntity entity,HttpServletRequest req,HttpServletResponse resp,String titleFormatParams,String copyTo,String linkTo) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
			IbpdLogger.getLogger(this.getClass()).info("titleFormatParams="+titleFormatParams+" copyTo="+copyTo+" linkTo="+linkTo);
			//titleFormatParams=color:#FF0000;strong:;em:;u:;size: copyTo= linkTo=
			if(entity.getNodeId()==null){
				String res=getJsonResult("-1","-1","没有设置 栏目信息,请联系管理员.");
				resp.getWriter().write(res);
				resp.getWriter().flush();
				return; 
			}
//			System.out.println("entity.text="+entity.getText());
			if(entity.getTitle().trim().equals("")){
				String res=getJsonResult("-1","-1","没有设置标题.");
				resp.getWriter().write(res);
				resp.getWriter().flush();
				return;
			}
			INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
			NodeEntity node=nodeService.getEntityById(entity.getNodeId());
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			entity.setNodeIdPath(node.getNodeIdPath());
			entity.setCreateDate(new Date());
			entity.setDeleted(false);
			entity.setEnteringUser(SysUtil.getCurrentLoginedUserName(req.getSession()));
			entity.setLastUpdateDate(new Date());
			entity.setState(1);
			entity.setSubSiteId(node.getSubSiteId());
			contentService.saveEntity(entity);	
			//保存内容属性信息 
			IContentAttrService caService=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);		
			ContentAttrEntity caEntity=new ContentAttrEntity();
			Map<String,String> pams=getFormatMap(titleFormatParams);
			caEntity.setColor(pams.get("color"));
			caEntity.setContentId(entity.getId());
			caEntity.setCopyTo(":"+copyTo.replace(",", ":")+":");
			caEntity.setEm(getBoolean(pams.get("em")));
			caEntity.setLinkTo(":"+linkTo.replace(",", ":")+":");
			caEntity.setSize(pams.get("size"));
			caEntity.setStrong(getBoolean(pams.get("strong")));
			caEntity.setU(getBoolean(pams.get("u")));
			//保存【复制到】的信息
			String[] nodeIds=copyTo.split(",");
			if(copyTo!=null && !copyTo.trim().equals("")){
				INodeService nodeSer=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
				for(String nodeId:nodeIds){
					NodeEntity tmpNode=nodeSer.getEntityById(Long.parseLong(nodeId));
					ContentEntity ce=new ContentEntity();
					swap(ce,entity);
					ce.setId(null);
					ce.setNodeId(tmpNode.getId());
					ce.setNodeIdPath(tmpNode.getNodeIdPath());
					ce.setSubSiteId(tmpNode.getSubSiteId());
					contentService.saveEntity(ce);
					caEntity.setCopyToContentIds(caEntity.getCopyToContentIds()+","+ce.getId()+",");
				}	
			}
			//将内容attr信息保存起来，包括已经复制过去的信息
			caService.saveEntity(caEntity);

			String res=getJsonResult("99",""+entity.getId(),"保存成功");
			resp.getWriter().write(res);
			resp.getWriter().flush();
			
		}
		private Boolean getBoolean(String v){
			//titleFormatParams=color:#FF0000;strong:1;em:1;u:1;size:22px copyTo=12,13 linkTo=32,150
			if(v==null) 
				return false;
			if(v.trim().toLowerCase().equals("1")){
				return true;
			}else{
				return false;
			}
		}
		@RequestMapping("Manage/Content/saveText.do")
		public void saveText(String id,String content,HttpServletResponse resp) throws IOException{
			if(id==null || id.trim().equals("")){
				String res=getJsonResult("-1","-1","没有内容ID，内容基本信息没有保存成功.");
				resp.getWriter().write(res);
				resp.getWriter().flush();
				return;
			}
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			ContentEntity c=contentService.getEntityById(Long.parseLong(id));
			if(c==null){
				String res=getJsonResult("-2","-1","没有找到相应内容.");
				resp.getWriter().write(res);
				resp.getWriter().flush();
			}else{
				IContentAttrService ca=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);
				String cids=ca.getCopyToContentIds(c.getId());
				
				c.setText(content);
				//开始设置复制到的内容text信息
				if(cids!=null && !cids.trim().equals("")){
					for(String cid:cids.split(",")){
						if(cid.equals(""))
							continue;
						ContentEntity tc=contentService.getEntityById(Long.parseLong(cid));
						if(tc!=null){
							tc.setText(content);
							contentService.saveEntity(tc);
						}
					}
				}
				
				contentService.saveEntity(c);
//				String res=getJsonResult("99",""+c.getId(),"保存成功.");
//				resp.getWriter().write(res);
				super.printJsonData(resp, getJsonResult("99",""+c.getId(),"保存成功."));
//				resp.getWriter().flush();
			}
		}
		@RequestMapping("Manage/Content/formatTitle.do")
		public String formatTitle(HttpServletRequest req,HttpServletResponse resp,Model model) throws UnsupportedEncodingException{
			if(req.getParameter("txt")==null)
				return ERROR_PAGE;
			String txt=new String(req.getParameter("txt").getBytes("ISO-8859-1"),"utf-8");
			IbpdLogger.getLogger(this.getClass()).info("sss="+txt); 
			model.addAttribute("txt",txt);
			return "manage/content/formatTitle";
		}
		
//		@RequestMapping("Manage/Content/saveTitleFormat.do")
//		public void formatTitle(String strong,String em,String u,String curColor){
//			System.out.println("color:"+curColor+" strong:"+strong+" em:"+em+" u:"+u);
//		}
		/**
		 * 本来是计划用来更新字段值来着，结果也就用来 做更新“是否已经删除”字段的值了
		 * @param ids
		 * @param resp
		 * @throws NoSuchMethodException
		 * @throws SecurityException
		 * @throws IllegalAccessException
		 * @throws IllegalArgumentException
		 * @throws InvocationTargetException
		 */
	@RequestMapping("Manage/Content/recv.do")
	public void props(String ids,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(ids!=null){
			String whereString=" where ";
			List<HqlParameter> pList=new ArrayList<HqlParameter>();
			if(ids.substring(ids.trim().length()-1).equals(",")){
				String[] id=ids.split(",");
				for(Integer i=0;i<id.length;i++){
					pList.add(new HqlParameter("id"+i,Long.parseLong(id[i]),DataType_Enum.Long));
					whereString+="id=:id"+i+" or ";
				}
			}
			whereString=whereString.substring(0,whereString.length()-4);
			contentService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			List<ContentEntity> contentList=contentService.getList("from "+contentService.getTableName()+whereString,pList);
			if(contentList!=null){
				List<HqlParameter> tList=new ArrayList<HqlParameter>();
				ContentEntity simpNode=null;
				for(Integer i=0;i<contentList.size();i++){
					ContentEntity ne=contentList.get(i);
					ne.setDeleted(true);
					simpNode=simpNode==null?ne:simpNode;
					ne.setLastUpdateDate(new Date());
					contentService.saveEntity(ne);
				}
				IbpdLogger.getLogger(this.getClass()).info("||||||||");
//				tList.add(new HqlParameter("id",simpNode.getParentId(),DataType_Enum.Long));
//				List<ContentEntity> parentList=contentService.getList("from "+contentService.getTableName()+" where id=:id",tList);
//				for(ContentEntity pn:parentList){
//					pn.setChildCount(pn.getChildCount()>=contentList.size()?pn.getChildCount()-contentList.size():0);
//					if(pn.getChildCount()==0){
//						pn.setLeaf(false);
//					}
//					contentService.saveEntity(pn);
//				}
			}
		}
		
	}
}
