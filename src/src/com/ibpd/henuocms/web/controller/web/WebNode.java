package com.ibpd.henuocms.web.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.hsqldb.lib.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.web.controller.manage.BaseController;
@Controller
public class WebNode  extends BaseController{
	@RequestMapping("Web/Node/index.do")
	public String index(org.springframework.ui.Model model,Long nodeId,Integer pageSize,Integer pageIndex,String type,HttpServletRequest req){
		if(nodeId==null){
			model.addAttribute("error","栏目ID为空");
			return super.ERROR_PAGE;			
		}
		INodeService nodeService=(INodeService) getService(NodeServiceImpl.class);
		NodeEntity node=nodeService.getEntityById(nodeId);
		if(node==null){
			model.addAttribute("error","没有找到这个栏目");
			return super.ERROR_PAGE;
		}
		//调取模板的逻辑：先调本身的，如果本身有，那么就用本身设置的模板，否则调用上级模板，直到找到模板为止，
		//如果到站点都没有找到，就是用默认的栏目模板
		//样式模板的调用，根据页面模板调用来，调用到那个模板，就是用那个模板的样式
		Long tempId=getPageTemplateId(nodeId);
		if(tempId==-1L){
			ISubSiteService siteService=(ISubSiteService) getService(SubSiteServiceImpl.class);
			tempId=siteService.getPageTemplateId(node.getSubSiteId(), PageTemplateEntity.TEMPLATE_TYPE_SITE);
		}
		IPageTemplateService pt=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		PageTemplateEntity pageTemplate=null;
		if(tempId==-1L){
			pageTemplate=pt.getDefaultPageTemplate(PageTemplateEntity.TEMPLATE_TYPE_NODE);
		}else{
			pageTemplate=pt.getEntityById(tempId); 
		} 
		if(pageTemplate==null){
			model.addAttribute("error","没有找到页面模板");
			return super.ERROR_PAGE;
		}
		IStyleTemplateService styleTempService=(IStyleTemplateService) getService(StyleTemplateServiceImpl.class);
		Long styleTempId=getStyleTemplateId(node,pageTemplate);
		StyleTemplateEntity styleTemplate=null;
		if(styleTempId==-1L){
			styleTempId=pageTemplate.getDefaultStyleTemplateId();
		}
		styleTemplate=styleTempService.getEntityById(styleTempId);
		if(styleTemplate==null){
			model.addAttribute("error","没有找到样式模板");
			return super.ERROR_PAGE;
		}
		//以上是准备内容的过程，下面是把具体的模板路径等映射到前台的过程
		String tmpPath=pageTemplate.getTemplateFilePath();
		if(StringUtil.isEmpty(tmpPath)){
			model.addAttribute("error","模板路径为空");
			return super.ERROR_PAGE;
		}
		String vdir=getVisualPathByType(req,type);
		model.addAttribute("currentNode",node);
		model.addAttribute("pageTitle",node.getText());
		model.addAttribute("keywords",node.getKeywords());
		model.addAttribute("description",node.getDescription());
		model.addAttribute("copyright",node.getCopyright());
		model.addAttribute("pageTemplatePath",vdir+"template/"+pageTemplate.getTemplateFilePath()+"/");
		model.addAttribute("styleTemplatePath",vdir+"template/"+styleTemplate.getTemplateFilePath()+"/");
		model.addAttribute("cssPath",vdir+"template/"+styleTemplate.getTemplateFilePath()+"/style.css");
		model.addAttribute("pageSize",pageSize);
		model.addAttribute("pageIndex",pageIndex);
		return "template/"+pageTemplate.getTemplateFilePath()+"/index";
	}
	private Long getPageTemplateId(Long nodeId){
		if(nodeId==null)
			return -1L;
		INodeAttrService nodeAttrService=(INodeAttrService) getService(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=nodeAttrService.getNodeAttr(nodeId);
		if(attr==null)
			return -1L;
		if(attr.getNodePageTemplateId()!=-1L){
			return attr.getNodePageTemplateId();
		}else{
			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			NodeEntity node=ns.getEntityById(nodeId);
			if(node==null){
				return -1L;
			}else{
				return getPageTemplateId(node.getParentId());
			}
		}
	}
	private Long getStyleTemplateId(NodeEntity node,PageTemplateEntity pageTemplate){
		INodeAttrService nodeAttrService=(INodeAttrService) getService(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=nodeAttrService.getNodeAttr(node.getId());
		if(attr.getNodeStyleTemplateId()!=-1){
			return attr.getNodeStyleTemplateId();
		}else{
			//先找页面模板设置的样式模板,如果没有则找上级
			if(pageTemplate.getDefaultStyleTemplateId()!=-1){
				return pageTemplate.getDefaultStyleTemplateId();
			}else{
				INodeService ns=(INodeService) getService(NodeServiceImpl.class);
				NodeEntity n=ns.getEntityById(node.getParentId());
				return getStyleTemplateId(n,pageTemplate);
			}
		}
	}
}
