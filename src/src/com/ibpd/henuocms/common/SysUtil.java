package com.ibpd.henuocms.common;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.assist.ExtUserPermissionEntity;
import com.ibpd.henuocms.assist.PermissionModelEntity;
import com.ibpd.henuocms.entity.FunEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.RoleEntity;
import com.ibpd.henuocms.entity.RolePermissionEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.entity.UserEntity;
import com.ibpd.henuocms.service.TestService;
import com.ibpd.henuocms.service.fun.FunServiceImpl;
import com.ibpd.henuocms.service.fun.IFunService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.role.IRolePermissionService;
import com.ibpd.henuocms.service.role.IRoleService;
import com.ibpd.henuocms.service.role.RolePermissionServiceImpl;
import com.ibpd.henuocms.service.role.RoleServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.service.user.IUserRoleService;
import com.ibpd.henuocms.service.user.UserRoleServiceImpl;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.entity.TenantEntity;

/**
 * 工具类
 * @author mg by qq:349070443
 *
 */
public class SysUtil {
	/**
	    * 返回当前进程的pid
	    * 共16位
	    * */
	   public short getPid(){
	       return Short.parseShort(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);
	   }
	 
	   /**
	    * 返回本机上的mac地址
	    * 当存在多个mac地址时，采取含有eth字符串的第一个mac地址
	    * 当不存在含有eth标记的mac字符串时，采用获取到的最后一个mac地址
	    * mac一共是48bit 即6个byte
	    * */
	   public byte[] getMac() throws SocketException {
	       byte[] mac=null;
	       Enumeration<NetworkInterface> es= NetworkInterface.getNetworkInterfaces();
	       while(es.hasMoreElements()){
	           NetworkInterface n=es.nextElement();
	           if(null!=n.getHardwareAddress()){
	               mac=n.getHardwareAddress();
	           }
	           if(n.getDisplayName().contains("eth")){
	               break;
	           }
	       }
	       return mac;
	   }
//	    private static final Logger log = Logger.getLogger(ProcessUtils.class);
	    /**
	     * 检测某进程是否在运行
	     * @param proName
	     * @return
	     */
	    public static boolean isRunning(String proName) {
	        try {
	            Process process = Runtime.getRuntime().exec("tasklist");
	            Scanner in = new Scanner(process.getInputStream());
	            while (in.hasNextLine()) {
	                String p = in.nextLine();
	                if (p.contains(proName)) {
	                    return true;
	                }
	            }
	            in.close();
	        } catch (IOException e) {
//	            log.error(String.format("Check process[%s] running error: " + e.getMessage(), proName));
	        }
	         
	        return false;
	    }
	     
	    /**
	     * 根据进程名寻找进程ID
	     * @param proName
	     * @return 不存在，返回-1
	     */
	    public static int findProcessId(String proName) {
	        try {
	            Process process = Runtime.getRuntime().exec("tasklist");
	            Scanner in = new Scanner(process.getInputStream());
	            while (in.hasNextLine()) {
	                String p = in.nextLine();
	                if (p.contains(proName)) {
	                    p = p.replaceAll("\\s+", ",");
	                    IbpdLogger.getLogger(SysUtil.class).info(p);
	                    String[] arr = p.split(",");
	                    int s=(arr[1]!=null)?Integer.parseInt(arr[1]):-1;
	                    return s;
	                }
	            }
	            in.close();
	        } catch (IOException e) {
//	            log.error(String.format("Find process[%s] id error: " + e.getMessage(), proName));
	        }
	         
	        return -1;
	    }
	     
	    /**
	     * 关闭某进程(根据PID)
	     * @param pid
	     * @return
	     */
	    public static boolean killProcess(int pid) {
	        try {
	            Runtime.getRuntime().exec("cmd.exe /c taskkill /f /pid " + pid);
	        } catch (IOException e) {
//	            log.error(String.format("Kill process[id=%d] error: " + e.getMessage(), pid));
	            return false;
	        }
	         
	        return true;
	    }
	     
	    /**
	     * 关闭进程(根据名称)
	     * @param proName
	     * @return
	     */
	    public static boolean killProcess(String proName) {
	        int pid = findProcessId(proName);
	        if (pid == -1) return true;
	        return killProcess(pid);
	    }
	     
	     
	    /**
	     * 关闭某进程(根据名称)直到真正关闭
	     * @param proName
	     * @return
	     */
	    public static boolean killProcessBlock(String proName) {
	        int pid = findProcessId(proName);
	        if (pid == -1) return true;
	        do {
	            killProcess(pid);
	            try {
	                Thread.sleep(100);
	            } catch (InterruptedException e) {
	            }
	        } while (isRunning(proName));
	         
	        return true;
	    }
	     
	    /**
	     *  显示所有进程
	     */
	    public static void listProcess() {
	        try {
	            Process process = Runtime.getRuntime().exec("tasklist");
	         
	            Scanner in = new Scanner(process.getInputStream());
	            while (in.hasNextLine()) {
	            	IbpdLogger.getLogger(SysUtil.class).info(in.nextLine());
	            }
	            in.close();
	        } catch (IOException e) {
	        }
	    }
	     
	    public static void main(String[] args) {
	        long t1 = System.currentTimeMillis();
	        listProcess();
	        long t2 = System.currentTimeMillis();
	        IbpdLogger.getLogger(SysUtil.class).info("Used time: " + (t2 - t1));
	         
	         
	        String proName = "Foxmail.exe";
//	      int pid = findProcessId(proName);
//	      System.out.println(pid);
//	      killProcess(pid);
	          
	         boolean b = killProcessBlock(proName);
	         long t3 = System.currentTimeMillis();
	         IbpdLogger.getLogger(SysUtil.class).info("Used time: " + (t3 - t2));
	         IbpdLogger.getLogger(SysUtil.class).info(b);
	    }
	    //////下面是用户登录记录部分
	    public static final String CURRENT_LOGIN_USER_ENTTIY="currentLoginedUserEntity";
	    public static final String CURRENT_LOGIN_USER_TENANT="loginedUserTenant";
	    public static final String CURRENT_LOGIN_USER_TYPE="currentLoginedUserType";
	    public static final String CURRENT_SUBSITE_ID="currentSubSiteId";
	    public static final String CURRENT_SUBSITE_INFO="currentSubSiteInfo";
	    public static final String CURRENT_LOGINED_USER_PERMISSION="currentLoginUserPermission";
	    /**
	     * 获取当前站点名称，目前没有对多站点进行扩展，这个函数权当摆设,另外这个函数已经不适合放在这里了
	     * @param session
	     * @return
	     */
	    public static String getCurrentSiteName(HttpSession session){
	    	SubSiteEntity site=(SubSiteEntity) session.getAttribute(CURRENT_SUBSITE_INFO);
	    	if(site==null){
	    		return "";
	    	}else{
	    		return site.getSiteName();
	    	}
	    }
	    /**
	     * 获取当前站点实例
	     * 目前没有对多站点进行扩展，这个函数权当摆设,另外这个函数已经不适合放在这里了
	     * @param session
	     * @return
	     */
	    public static SubSiteEntity getCurrentSiteInfo(HttpSession session){
	    	SubSiteEntity site=(SubSiteEntity) session.getAttribute(CURRENT_SUBSITE_INFO);
	    	return site;
	    }
	    /**
	     * 获取当前可管理的站点
	     * 目前没有对多站点进行扩展，这个函数权当摆设,另外这个函数已经不适合放在这里了
	     * @return
	     */
	    public static Long[] getManagedSiteIds(){
	    	//暂时先这样的
	    	ISubSiteService s=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
	    	List<SubSiteEntity> ss=s.getList();
	    	Long[] tmp=new Long[ss.size()];
	    	for(Integer i=0;i<ss.size();i++){
	    		tmp[i]=ss.get(i).getId();
	    	}
	    	return tmp;
	    }
	    /**
	     * 设置当前登录用户的类型 是管理员或商户
	     * @param session
	     * @param type
	     */
	    public static void setCurrentLoginedUserType(HttpSession session,String type){
	    	session.setAttribute(CURRENT_LOGIN_USER_TYPE, type);
	    }
	    /**
	     * 获取当前登录用户的类型 是管理员或商户
	     * @param session
	     * @return
	     */
	    public static String getCurrentLoginedUserType(HttpSession session){
	    	Object obj=session.getAttribute(CURRENT_LOGIN_USER_TYPE);
	    	if(obj==null)
	    		return null;
	    	return (String) obj;
	    }
	    public static void removeCurrentLoginedUserType(HttpSession session){
	    	session.removeAttribute(CURRENT_LOGIN_USER_TYPE);
	    }
	    /**
	     * 设置当前登录的商户
	     * @param session
	     * @param tenant
	     */
	    public static void setCurrentLoginedUserTenant(HttpSession session,TenantEntity tenant){
	    	session.setAttribute(CURRENT_LOGIN_USER_TENANT, tenant);
	    }
	    /**
	     * 获取当前登陆的商户 可管理的商户
	     * @param session
	     * @return
	     */
	    public static TenantEntity getCurrentLoginedUserTenant(HttpSession session){
	    	TenantEntity te=(TenantEntity) session.getAttribute(CURRENT_LOGIN_USER_TENANT);
	    	return te;
	    }
	    public static void setCurrentLoginedUserInfo(HttpSession session,UserEntity user){
	    	session.setAttribute(CURRENT_LOGIN_USER_ENTTIY, user);
	    	session.setAttribute(CURRENT_SUBSITE_ID, user.getSubSiteId());
	    }
	    public static void setCurrentLoginedSiteInfo(HttpSession session,SubSiteEntity site){
	    	session.setAttribute(CURRENT_SUBSITE_INFO, site);
	    }
	    public static void getCurrentLoginedSiteInfo(HttpSession session){
	    	session.getAttribute(CURRENT_SUBSITE_INFO);
	    }
	    public static UserEntity getCurrentLoginedUserInfo(HttpSession session){
	    	return (UserEntity) session.getAttribute(CURRENT_LOGIN_USER_ENTTIY);
	    }
	    public static void removeCurrentLoginedUserInfo(HttpSession session){
	    	session.removeAttribute(CURRENT_LOGIN_USER_ENTTIY);
	    }
	    public static Long getCurrentSiteId(HttpSession session){
	    	Object l=session.getAttribute(CURRENT_SUBSITE_ID);
	    	if(l==null){
	    		return -1L;
	    	}else{
	    		if(l instanceof Long){
	    			return Long.valueOf(l.toString());
	    		}else{
	    			return -1L;
	    		}
	    	}
	    }
	    
	    public static String getCurrentLoginedUserName(HttpSession session){
	    	Object obj=session.getAttribute(CURRENT_LOGIN_USER_ENTTIY);
	    	if(obj!=null){
	    		UserEntity user=(UserEntity) obj;
	    		return user.getUserName();
	    	}
	    	return null;
	    }
	    /**
	     * 获取文件上传相对目录
	     * 目前没有扩展多站点，目录是固定的 ，是/uploadFiles
	     * @return
	     */
	    public static String getUploadFileBaseDir(){
	    	return "/uploadFiles";
	    }
	    /**
	     * 返回站点名称,函数本身没什么用处了，但由于还有依赖性，暂时还去掉
	     * @return
	     */
		public static String getSiteName() {
			// TODO Auto-generated method stub
			return "和诺商业集团有限公司";
		}
		public static void removeCurrentTenant(HttpSession session){
			session.removeAttribute(CURRENT_LOGIN_USER_TENANT);
		}
		public static void setCurrentTenant(HttpSession session,TenantEntity currentTenant) {
			
			setCurrentLoginedUserTenant(session,currentTenant);
		}
		public static List<TenantEntity> getCurrentTenant(HttpSession session) {
			Object o=getCurrentLoginedUserTenant(session);
			if(o==null)
				return null;
			return (List<TenantEntity>) o;
		} 
		public static String getTenantFileUploadDir(Long tenantId){

				return "uploadFiles";
		}
		public static String getTenantFileDir(Long tenantId){
			PathUtil p = new PathUtil();
			String root="";
			try {
				root = p.getWebRoot();
				return root+"uploadFiles";
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		}

		public static Long getCurrentTenantId(HttpSession session) {
			TenantEntity te=getCurrentLoginedUserTenant(session);
			if(te!=null)
				return te.getId();
			return null;
		}
		/**
		 * 获取用户所有的权限--内部调用方法
		 * @param userId
		 * @return
		 */
		protected static List<ExtUserPermissionEntity> getUserAllPermission(Long userId){
//			IUserService userServ=(IUserService) ServiceProxyFactory.getServiceProxy(UserServiceImpl.class);
			BaseController bc=new BaseController();
			IUserRoleService userRoleServ=(IUserRoleService) bc.getBeanFromSpringDefinedBeans("userRoleService");
			IRoleService roleServ=(IRoleService) bc.getBeanFromSpringDefinedBeans("roleService");
			IRolePermissionService rolePerServ=(IRolePermissionService) bc.getBeanFromSpringDefinedBeans("rolePermissionService");
			IFunService funServ=(IFunService) bc.getBeanFromSpringDefinedBeans("funService");
//			IUserRoleService userRoleServ=(IUserRoleService) ServiceProxyFactory.getServiceProxy(UserRoleServiceImpl.class);
//			IRoleService roleServ=(IRoleService) ServiceProxyFactory.getServiceProxy(RoleServiceImpl.class);
//			IRolePermissionService rolePerServ=(IRolePermissionService) ServiceProxyFactory.getServiceProxy(RolePermissionServiceImpl.class);
//			IFunService funServ=(IFunService) ServiceProxyFactory.getServiceProxy(FunServiceImpl.class);
			List<Long> roleIdList=userRoleServ.getRoleIdByUserId(userId);
			String roleHql="";
			for(Long roleId:roleIdList){
				RoleEntity role=roleServ.getEntityById(roleId);
				if(role!=null){
					roleHql+=role.getId()+",";
				}
			}
			if(roleHql.trim().length()>0){
				roleHql=roleHql.substring(0,roleHql.length()-1);
			}
			List<RolePermissionEntity> rolePerList=rolePerServ.getListByRoleIds(roleHql);
			List<ExtUserPermissionEntity> userPermissionList=new ArrayList<ExtUserPermissionEntity>();
			for(RolePermissionEntity rp:rolePerList){
				FunEntity fun=funServ.getEntityById(rp.getFunId());
				if(fun!=null && fun.getIsLocked()==false){
					ExtUserPermissionEntity ext=new ExtUserPermissionEntity(rp);
					ext.setFunCode(fun.getWebName());
					ext.setFunName(fun.getFunName());
					userPermissionList.add(ext);
				}
			}
			return userPermissionList;
		}
		/**
		 * 用户登陆后，获取用户所有的操作权限
		 * @param session
		 */
		public static void initUserAllPermission(HttpSession session){
			UserEntity user=SysUtil.getCurrentLoginedUserInfo(session);
			if(user==null)
				return;
			List<ExtUserPermissionEntity> lst=getUserAllPermission(user.getId());
			String debug=IbpdCommon.getInterface().getPropertiesValue("system.debug.enable");
			if(debug!=null && debug.trim().toLowerCase().equals("true")){
				for(ExtUserPermissionEntity ext:lst){
					IbpdLogger.getLogger(SysUtil.class).info("==modelId:"+ext.getModelId()+"======modelType:"+ext.getTypeId()+"=====funId:"+ext.getFunId()+"=====funName:"+ext.getFunName()+"=====funCode:"+ext.getFunCode());
				}	
			}
			session.setAttribute(CURRENT_LOGINED_USER_PERMISSION, lst);
		}
		public static void clearUserPermission(HttpSession session){
			session.removeAttribute(CURRENT_LOGINED_USER_PERMISSION);
		}
		/**
		 * 检查用户某个权限组合是否在全线列表中 .
		 * @param session
		 * @param funCode
		 * @param modelId
		 * @param modelType
		 * @return
		 */
		public static Boolean checkPermissionIsEnable(HttpSession session,String funCode,Long modelId,Integer modelType){
			Object o=session.getAttribute(CURRENT_LOGINED_USER_PERMISSION);
			Boolean rtn=false;
			if(o==null)
				return rtn;
			List<ExtUserPermissionEntity> lst=(List<ExtUserPermissionEntity>) o;
			for(ExtUserPermissionEntity e:lst){
				if(modelType.equals(PermissionModelEntity.MODEL_TYPE_FUNMODEL)){
					if(e.getFunCode().toLowerCase().trim().equals(funCode.trim().toLowerCase())){
						rtn=true;
						break;
					}
				}else if(modelType.equals(PermissionModelEntity.MODEL_TYPE_SITE)){
					if(e.getFunCode().toLowerCase().trim().equals(funCode.trim().toLowerCase()) && e.getModelId().equals(modelId) && e.getTypeId().equals(modelType)){
						rtn=true;
						break;
					}
				}else if(modelType.equals(PermissionModelEntity.MODEL_TYPE_NODE)){
					if(e.getFunCode().toLowerCase().trim().equals(funCode.trim().toLowerCase()) && e.getModelId().equals(modelId) && e.getTypeId().equals(modelType)){
						rtn=true;
						break;
					}					
				}
			}
			if(modelType.equals(PermissionModelEntity.MODEL_TYPE_NODE) && rtn==false){
				INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
				NodeEntity ne=nodeServ.getEntityById(modelId);
				if(ne==null){
					return false;
				}else{
					if(ne.getParentId()!=-1L)
						return checkPermissionIsEnable(session,funCode,ne.getParentId(),modelType);
					else
						return false;
				}
			}
			return rtn;
		}
		
}
