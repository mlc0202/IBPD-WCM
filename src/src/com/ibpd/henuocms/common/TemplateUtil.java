package com.ibpd.henuocms.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.hsqldb.lib.StringUtil;

/**
 * 模版工具类
 * @author mg by qq:349070443
 *
 */
public class TemplateUtil {
	/**
	 * 保存模版内容
	 * @param dirName
	 * @param fileName
	 * @param content
	 */
	public static void saveTemplateContent(String dirName,String fileName,String content){
		String path=getTemplateFileRealPath(dirName);
		if(!StringUtil.isEmpty(path)){
			path=path+File.separator+fileName;
			File f=new File(path);
			if(f.exists() && f.isFile()){
				try {
					TxtUtil.writeTxtFile(content, f.getAbsoluteFile());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 获取模版内容
	 * @param dirName
	 * @param fileName
	 * @return
	 */
	public static String getTemplateContent(String dirName,String fileName){
		String path=getTemplateFileRealPath(dirName);
		if(!StringUtil.isEmpty(path)){
			path=path+File.separator+fileName;
			File f=new File(path);
			if(f.exists() && f.isFile()){
				return TxtUtil.readTxtFile(f.getAbsolutePath());
			}
		}
		return "";
	}
	/**
	 * 获取模版文件的绝对路径
	 * @param dirName
	 * @return
	 */
	public static String getTemplateFileRealPath(String dirName){
		TemplateUtil tu=new TemplateUtil();
		try {
			String realPath = tu.getWebRoot()+"template"+File.separator+dirName;
			return realPath;
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * 获取模板的url路径，这里的路径不包含contextPath及其前面的部分
	 * @param dirName
	 * @return
	 */
	public static String getTemplateFileUrlPath(String dirName){
		String realPath = "/template/"+dirName;
		return realPath;
	}
	/**
	 * 获取文件大小，暂时基本是无效的
	 * @param dirName
	 * @return
	 */
	public static Long getFileSize(String dirName){
		TemplateUtil tu=new TemplateUtil();
		try {
			String realPath = tu.getWebRoot()+"template"+File.separator+dirName;
			File f=new File(realPath);
			return f.length();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0L;
	}
	  // 递归方式 计算文件的大小
	/**
	 * 计算文件的大小
	 */
    private long getTotalSizeOfFilesInDir(File file) {
        if (file.isFile())
            return file.length();
        final File[] children = file.listFiles();
        long total = 0;
        if (children != null)
            for (final File child : children)
                total += getTotalSizeOfFilesInDir(child);
        return total;
    }
    /**
     * 获取目录大小（基本无效）
     * @param dirName
     * @return
     */
    public static Long getDirSize(String dirName){
		TemplateUtil tu=new TemplateUtil();
		try {
			String realPath = tu.getWebRoot()+"template"+File.separator+dirName;
			File f=new File(realPath);
			return tu.getTotalSizeOfFilesInDir(f);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0L;
    }
    /**
     * 新建一个模版文件，不包括写入数据库
     * @param pagetemplateType
     * @return
     */
	public static String makeNewPageTemplate(Integer pagetemplateType){
		TemplateUtil tu=new TemplateUtil();
		UUID uuid = UUID.randomUUID();
		String realPath;
		try {
			realPath = tu.getWebRoot()+"template"+File.separator+uuid;
			File f=new File(realPath);
			if(!f.exists()){
				f.mkdir();
				String content=TxtUtil.readTxtFile(tu.getWebRoot()+"template"+File.separator+"template.jsp");
				makeNewFile(uuid.toString(),"index.jsp",content);
			}	
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return uuid.toString();
	}
	/**
	 * 新建一个样式模版文件，不包括写入数据库
	 * @return
	 */
	public static String makeNewStyleTemplate(){
		TemplateUtil tu=new TemplateUtil();
		UUID uuid = UUID.randomUUID();
		String realPath;
		try {
			realPath = tu.getWebRoot()+"template"+File.separator+uuid;
			File f=new File(realPath);
			if(!f.exists()){
				f.mkdir();
				String content=TxtUtil.readTxtFile(tu.getWebRoot()+"template"+File.separator+"style.css");
				makeNewFile(uuid.toString(),"style.css",content);
			}	
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return uuid.toString();
	}
	public static void makeNewFile(String dirName,String fileName,String content) throws Exception{
		TemplateUtil tu=new TemplateUtil();
		String realPath = tu.getWebRoot()+"template"+File.separator+dirName+File.separator+fileName;
		File f=new File(realPath);
		if(f.createNewFile()){
			if(f.exists()){
				TxtUtil.writeTxtFile(content, f.getAbsoluteFile());
			}
		}
	}
	public String getWebRoot() throws IllegalAccessException{
		   String path = getWebClassesPath();
		   if (path.indexOf("WEB-INF") > 0) {
		    path = path.substring(0, path.indexOf("WEB-INF/classes"));
		   } else {
		    throw new IllegalAccessException("路径获取错误");	
		   }
		   return path;
		}
	public String getWebClassesPath() {
		   String path = getClass().getProtectionDomain().getCodeSource()
		     .getLocation().getPath();
		   return path;
		   
		}

		public String getWebInfPath() throws IllegalAccessException{
		   String path = getWebClassesPath();
		   if (path.indexOf("WEB-INF") > 0) {
		    path = path.substring(0, path.indexOf("WEB-INF")+8);
		   } else {
		    throw new IllegalAccessException("路径获取错误");
		   }
		   return path;
		}
}
