package com.ibpd.henuocms.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.spi.LocationInfo;
import org.jsoup.helper.StringUtil;

import com.ibpd.common.ClassPath;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
/**
 * 一个综合性的工具类
 * @author mg by qq:349070443
 *
 */
public class IbpdCommon {
	public static IbpdCommon getInterface(){
		return new IbpdCommon();
	}
	/**
	 * 获取单个站点存放的目录名称 （是单个目录名称，不是路径）
	 * @param siteId
	 * @return
	 */
	private String getSubSiteDir(Long siteId){
		ISubSiteService siteServ=(ISubSiteService) ServiceProxyFactory.getServiceProxy(SubSiteServiceImpl.class);
		SubSiteEntity site=siteServ.getEntityById(siteId);
		if(site==null)
			return "";
		return site.getDirectory();
	}
	/**
	 * 获取站点的url路径，但不包含contextpath部分
	 * @param siteId
	 * @return
	 */
	public String getSubSiteUrlDir(Long siteId){
		String dir=getSubSiteDir(siteId);
		return "/sites/"+dir;
	}
	/**
	 * 获取站点的真实路径
	 * @param siteId
	 * @return
	 */
	public String getSubSiteRealDir(Long siteId){
		String dir=getSubSiteDir(siteId);;
		return FileUtil.getWebRootDirPath()+File.separator+"sites"+File.separator+dir;
	}
	/**
	 * 获取栏目存放目录
	 * @param nodeId
	 * @return
	 */
	private String getNodeDir(Long nodeId){
		if(nodeId==null)
			return "";
		INodeAttrService attrServ=(INodeAttrService) ServiceProxyFactory.getServiceProxy(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=attrServ.getNodeAttr(nodeId);
		if(attr==null)
			return "";
		return attr.getDirectory();
	}
	/**
	 * 获取栏目 的url地址，不包含contextpath部分
	 * @param nodeId
	 * @return
	 */
	public String getNodeUrlDir(Long nodeId){
		if(nodeId==null)
			return "";
		INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		NodeEntity ne=nodeServ.getEntityById(nodeId);
		if(ne==null)
			return "";
		String idpath=ne.getNodeIdPath();
		String[] ids=idpath.split(",");
		String url="";
		url=getSubSiteUrlDir(ne.getSubSiteId());
		for(String id:ids){
			if(StringUtil.isNumeric(id)){
				url=url+"/"+getNodeDir(Long.parseLong(id));
			}
		}
		return url;
	}
	/**
	 * 获取栏目真实路径
	 * @param nodeId
	 * @return
	 */
	public String getNodeRealDir(Long nodeId){
		if(nodeId==null)
			return "";
		INodeService nodeServ=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
		NodeEntity ne=nodeServ.getEntityById(nodeId);
		if(ne==null)
			return "";
		String idpath=ne.getNodeIdPath();
		String[] ids=idpath.split(",");
		String url="";
		url+=getSubSiteRealDir(ne.getSubSiteId());
		for(String id:ids){
			if(StringUtil.isNumeric(id)){
				url=url+File.separator+getNodeDir(Long.parseLong(id));
			}
		}
		return url;
	}
	/**
	 * 获取样式模板的url路径 不包含contextpath部分
	 * @param styleTemplateId
	 * @return
	 */
	public String  getStyleTemplateUrlDir(Long styleTemplateId){
		if(styleTemplateId==null)
			return "";
		IStyleTemplateService pts=(IStyleTemplateService) ServiceProxyFactory.getServiceProxy(StyleTemplateServiceImpl.class);
		StyleTemplateEntity pt=pts.getEntityById(styleTemplateId);
		return TemplateUtil.getTemplateFileUrlPath(pt.getTemplateFilePath());
	}
	/**
	 * 获取样式模板的真实路径
	 * @param styleTemplateId
	 * @return
	 */
	public String getStyleTemplateRealDir(Long styleTemplateId){
		if(styleTemplateId==null)
			return "";
		IStyleTemplateService pts=(IStyleTemplateService) ServiceProxyFactory.getServiceProxy(StyleTemplateServiceImpl.class);
		StyleTemplateEntity pt=pts.getEntityById(styleTemplateId);
		return TemplateUtil.getTemplateFileRealPath(pt.getTemplateFilePath());
	}
	/**
	 * 获取模板的url地址 不包含contextPath部分
	 * @param pageTemplateId
	 * @return
	 */
	public String getPageTemplateUrlDir(Long  pageTemplateId){
		if(pageTemplateId==null)
			return "";
		IPageTemplateService pts=(IPageTemplateService) ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class);
		PageTemplateEntity pt=pts.getEntityById(pageTemplateId);
		return getPageTemplateUrlDir(pt);
	}
	/**
	 * 获取模板的url地址 不包含contextpath部分
	 * @param pt
	 * @return
	 */
	public String getPageTemplateUrlDir(PageTemplateEntity pt){
		TemplateUtil tu=new TemplateUtil();
		return tu.getTemplateFileUrlPath(pt.getTemplateFilePath());
	}
	/**
	 * 获取模板的真实存放路径
	 * @param pageTemplateId
	 * @return
	 */
	public String getPageTemplateRealDir(Long pageTemplateId){
		if(pageTemplateId==null)
			return "";
		IPageTemplateService pts=(IPageTemplateService) ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class);
		PageTemplateEntity pt=pts.getEntityById(pageTemplateId);
		return getPageTemplateRealDir(pt);
	}
	/**
	 * 获取模板的真实存放路径
	 * @param pt
	 * @return
	 */
	public String getPageTemplateRealDir(PageTemplateEntity pt){
		TemplateUtil tu=new TemplateUtil();
		return tu.getTemplateFileRealPath(pt.getTemplateFilePath());
	}
	/**
	 * 根据属性名称从对象中获取对应的值
	 * @param obj
	 * @param propName
	 * @return
	 * @throws Exception
	 */
	public Object getPropertyValueByObject(Object obj,String propName) throws Exception{
		if(obj==null)
			return null;
		if(propName==null)
			return null;
		if(StringUtils.isBlank(propName))
			return null;
		String mtName="get"+propName.substring(0,1).toUpperCase()+propName.substring(1);
		Method[] mds=obj.getClass().getMethods();
		for(Method md:mds){
			if(md.getName().equals(mtName)){
				return md.invoke(obj, null);
			}
		}
		return null;
	}
	/**
	 * 根据key从properties中获取实体对应的html字段信息
	 * @param key
	 * @return
	 */
	public String getEntityHtmlFields(String key){
		try {
			return com.ibpd.common.ProperCommon.readValue(ClassPath.getRealPath("/config/entityConfig.properties"), key);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return "";
	}
	/**
	 * 根据系统配置信息中的key从properties中获取对应的值
	 * @param key /config/system.properties中的key
	 * @return
	 */
	public String getPropertiesValue(String key){
		try {
			return com.ibpd.common.ProperCommon.readValue(ClassPath.getRealPath("/config/system.properties"), key);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return "";
	}
	/**
	 * 从拼接的字符串转换为hql字符串
	 * 格式如:field:eq:value:and;field:eq:value:or;field:like:value:or;field:gt:value:and;
	 * @param query
	 * @return
	 */
	public String getOtherQueryString(String query){
		String rtn="";
		if(query==null || StringUtil.isBlank(query))
			return rtn;
		//
		String[]  pms=query.split(";");
		for(String pm:pms){
			String[] singPmArray=pm.split(":");
			if(singPmArray.length==4){
				if(singPmArray[1].equals("eq")){
					rtn+=singPmArray[0]+"='"+singPmArray[2]+"' "+singPmArray[3]+" ";
				}else if(singPmArray[1].equals("like")){
					rtn+=singPmArray[0]+" like '%"+singPmArray[2]+"%' "+singPmArray[3]+" ";
				}else if(singPmArray[1].equals("gt")){
					rtn+=singPmArray[0]+" > '"+singPmArray[2]+"' "+singPmArray[3]+" ";
		 		}else if(singPmArray[1].equals("lt")){
					rtn+=singPmArray[0]+" < '"+singPmArray[2]+"' "+singPmArray[3]+" ";
				}
			}
		}
		if(StringUtil.isBlank(rtn)){
			return rtn;
		}else{
			return rtn.substring(0,rtn.length()-4);
		}
	}

}
