package com.ibpd.henuocms.common;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import javax.imageio.ImageIO;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
/**
 * 图片处理工具类，该类主要用于生成电子版海报
 * @author mg by qq:349070443
 *
 */
public final class ImageUtils {
	//以下参数用来绘制水印，代表水印位置
	public static final Integer LeftTop=0;
	public static final Integer LeftBottom=1;
	public static final Integer RightTop=2;
	public static final Integer RightBottom=3;
	public static final Integer Center=4;
	
	private Integer position=0;
    private ImageUtils() {
    	super();
    }
    public ImageUtils(Integer pos){
    	if(pos==null)
    		position=0;
    	else{
    		position=pos;
    	}
    }
 
    /**
     * 为图片加水印
     * @param pressImg 水印图片
     * @param targetImg加水印图片
     */
    public void pressImage(String pressImg, String targetImg){
        try {
            File _file = new File(targetImg);
            if(!_file.exists()){
            	return;
            }
            Image src = ImageIO.read(_file);
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(wideth, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);
 
            File _filebiao = new File(pressImg);
            if(!_filebiao.exists()){
            	return;
            }
           Image src_biao = ImageIO.read(_filebiao);
            int wideth_biao = src_biao.getWidth(null);
            int height_biao = src_biao.getHeight(null);
            if(wideth/2<wideth_biao|| height/2<height_biao)
            	return;
            int w=0;
            int h=0;
            if(position.equals(LeftTop)){
            	w=10;
            	h=10;
            }else if(position.equals(LeftBottom)){
            	w=10;
            	h=height-height_biao-10;
            }else if(position.equals(RightTop)){
            	w=wideth-wideth_biao-10;
            	h=10;
            }else if(position.equals(RightBottom)){
            	w=wideth-wideth_biao-10;
            	h=height-height_biao-10;
            }else if(position.equals(Center)){
            	w=(wideth-wideth_biao)/2;
            	h=(height-height_biao)/2;
            }else{
            	w=10;
            	h=10;
            }
            g.drawImage(src_biao, w,
                    h, wideth_biao, height_biao, null);
            g.dispose();
            FileOutputStream out = new FileOutputStream(targetImg);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    }
    /**
     * 为图片加文字水印
     * 该方法几乎就是摆设，因为最终效果并不好看
     * @param pressText 水印文字
     * @param targetImg 加水印图片
     * @param fontName 字体名称
     * @param fontStyle 字体样式
     * @param color 字体颜色
     * @param fontSize 文字大小
     * @param x 偏移 X
     * @param y 便宜 Y
     */
    public void pressText(String pressText, String targetImg,
            String fontName, int fontStyle, int color, int fontSize, int x,
            int y) {
        try {
            File _file = new File(targetImg);
            Image src = ImageIO.read(_file);
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(wideth, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);
             
            g.setColor(Color.RED);
            g.setFont(new Font(fontName, fontStyle, fontSize));
 
            g.drawString(pressText, wideth - fontSize - x, height - fontSize
                    / 2 - y); 
            g.dispose();
            FileOutputStream out = new FileOutputStream(targetImg);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image);
            out.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
 
    public static void main(String[] args) {
    	ImageUtils u=new ImageUtils(ImageUtils.LeftBottom);
    	
        u.pressImage("H:\\logo.png","H:\\444.jpg");
    }
}