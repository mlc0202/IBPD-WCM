package com.ibpd.henuocms.common;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
  
/** 
 *  List按照指定字段排序工具类 
 *  目前支持Integer、Long、Float、Double、String这几种数据类型 
 *  @author mg
 * @param <T> 
 */  
  
public class ListSortUtil<T> {  
    /** 
     * @param targetList 目标排序List 
     * @param sortField 排序字段(实体类属性名) 
     * @param sortMode 排序方式（asc or  desc） 
     */  
    @SuppressWarnings({ "unchecked", "rawtypes" })  
    public void sort(List<T> targetList, final String sortField, final String sortMode) {  
    	IbpdLogger.getLogger(this.getClass()).info(sortMode);
    	if(targetList==null)
    		return;
        Collections.sort(targetList, new Comparator() {  
            public int compare(Object obj1, Object obj2) {   
                int retVal = 0;  
                try {  
                    //首字母转大写  
                    String newStr=sortField.substring(0, 1).toUpperCase()+sortField.replaceFirst("\\w","");   
                    String methodStr="get"+newStr;  
                      
                    Method method1 = ((T)obj1).getClass().getMethod(methodStr, null);  
                    Method method2 = ((T)obj2).getClass().getMethod(methodStr, null);  
                    Object mt1Val=method1.invoke(((T) obj1), null);
                    Object mt2Val=method2.invoke(((T) obj2), null);
                    if(mt1Val==null){
                    	mt1Val=new String();
                    }
                    if(mt1Val.getClass().getSimpleName().toLowerCase().equals("integer")){
                    	Integer mt1Int=(Integer) mt1Val;
                    	Integer mt2Int=(Integer) mt2Val;
    	                if (sortMode != null && "desc".equals(sortMode)) {  
    	                    retVal = mt2Int.compareTo(mt1Int); // 倒序  
    	                } else {  
    	                    retVal = mt1Int.compareTo(mt2Int); // 正序  
    	                }  
                    }else if(mt1Val.getClass().getSimpleName().toLowerCase().equals("long")){
                    	Long mt1Int=(Long) mt1Val;
                    	Long mt2Int=(Long) mt2Val;
    	                if (sortMode != null && "desc".equals(sortMode)) {  
    	                    retVal = mt2Int.compareTo(mt1Int); // 倒序  
    	                } else {  
    	                    retVal = mt1Int.compareTo(mt2Int); // 正序  
    	                }  
                    }else if(mt1Val.getClass().getSimpleName().toLowerCase().equals("double")){
                    	Double mt1Int=(Double) mt1Val;
                    	Double mt2Int=(Double) mt2Val;
    	                if (sortMode != null && "desc".equals(sortMode)) {  
    	                    retVal = mt2Int.compareTo(mt1Int); // 倒序  
    	                } else {  
    	                    retVal = mt1Int.compareTo(mt2Int); // 正序  
    	                }  
                    }else if(mt1Val.getClass().getSimpleName().toLowerCase().equals("float")){
                    	Float mt1Int=(Float) mt1Val;
                    	Float mt2Int=(Float) mt2Val;
    	                if (sortMode != null && "desc".equals(sortMode)) {  
    	                    retVal = mt2Int.compareTo(mt1Int); // 倒序  
    	                } else {  
    	                    retVal = mt1Int.compareTo(mt2Int); // 正序  
    	                }  
                    }else if(mt1Val.getClass().getSimpleName().toLowerCase().equals("string")){
                        String mt1Str=mt1Val==null?new String():mt1Val.toString();
                        String mt2Str=mt2Val==null?new String():mt2Val.toString();
    	                if (sortMode != null && "desc".equals(sortMode)) {  
    	                    retVal = mt2Str.compareTo(mt1Str); // 倒序   
    	                } else {  
    	                    retVal = mt1Str.compareTo(mt2Str); // 正序  
    	                }  
                    }else{
                    	String mt1Str=mt1Val==null?new String():mt1Val.toString();
                        String mt2Str=mt2Val==null?new String():mt2Val.toString();
    	                if (sortMode != null && "desc".equals(sortMode)) {  
    	                    retVal = mt2Str.compareTo(mt1Str); // 倒序  
    	                } else {  
    	                    retVal = mt1Str.compareTo(mt2Str); // 正序  
    	                }  
                    }
                 } catch (Exception e) {  
                	 e.printStackTrace();
                    throw new RuntimeException();  
                }  
                return retVal;  
            }  
        });  
    }        
}  
