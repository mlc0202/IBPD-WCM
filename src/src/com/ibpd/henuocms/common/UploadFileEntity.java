package com.ibpd.henuocms.common;

import java.util.Date;

/**
 * 不是真正的实体类，只是用来生成特定格式的附件信息的
 * @author MG 
 *
 */
public class UploadFileEntity {

	private String fileName;
	private String filePath;
	private Long fileSize;
	private Date createTime;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
