package com.ibpd.shopping.assist;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class LongJsonValueProgressor implements JsonValueProcessor {

	public Object processArrayValue(Object arg0, JsonConfig arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object processObjectValue(String arg0, Object arg1, JsonConfig arg2) {
		if(arg1 instanceof Long){
			return arg1.toString();
		}else if(arg1 instanceof Integer){
			return arg1.toString();
		}else if(arg1 instanceof Float){
			return arg1.toString();
		}else if(arg1 instanceof Double){
			return arg1.toString();
		}else if(arg1==null){
			return "";
		}else{
			return arg1.toString();
		}
	}

}
