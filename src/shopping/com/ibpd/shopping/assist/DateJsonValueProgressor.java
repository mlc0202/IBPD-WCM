package com.ibpd.shopping.assist;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class DateJsonValueProgressor implements JsonValueProcessor {
	private String formate;
	public DateJsonValueProgressor(){
		this.formate = "yyyy-MM-dd hh:mm:ss";
	}
	public DateJsonValueProgressor(String format){
		this.formate=format;
	}
	public Object processArrayValue(Object arg0, JsonConfig arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object processObjectValue(String arg0, Object arg1, JsonConfig arg2) {
		if(arg1 instanceof Date){
			String str = new SimpleDateFormat(this.formate).format((Date)arg1);
			   return str;	
		}
		return null;
	}

}
