/**
 * @author 马根
 * @version 1.0
 * @description 内容
 */
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 商户实体类
 * <br>
 * 商户的 实体类，一个商户默认只有一条记录，工商注册登记号做唯一性检查（代码实现，不走数据库，避免数据底层的错误)
 * @author 马根
 * @version 1.0
 */
@Entity
@Table(name="T_S_Tenant")
public class TenantEntity extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Integer TenantStatus_passed=99;
	public static final Integer TenantStatus_unPass=-1;
	public static final Integer TenantType_ent=0;
	public static final Integer TenantType_sing=1;
	/**
	 * ID
	 */
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 商户名称
	 */
	@Column(name="f_tenantName",length=125,nullable=true)
	private String tenantName="";
	/**
	 * 商户负责人姓名
	 */
	@Column(name="f_managerName",length=50,nullable=true)
	private String managerName="";
	/**
	 * 商户负责人手机
	 */
	@Column(name="f_managerTel",length=50,nullable=true)
	private String managerTel="";
	/**
	 * 商户负责人邮箱
	 */
	@Column(name="f_managerEmail",length=50,nullable=true)
	private String managerEmail="";
	/**
	 * 商户负责人其他联系方式
	 */
	@Column(name="f_managerOtherContact",length=100,nullable=true)
	private String managerOtherContact;
	/**
	 * 商户类型<b>企业或代理商（个人）</b>
	 */
	@Column(name="f_tenantType",nullable=true)
	private Integer tenantType=0;
	/**
	 * 企业全称
	 */
	@Column(name="f_entFullName",length=125,nullable=true)
	private String entFullName="";
	/**
	 * 企业法定代表人
	 */
	@Column(name="f_legalPerson",length=50,nullable=true)
	private String legalPerson="";
	/**
	 * 公司注册地址
	 */
	@Column(name="f_regAddress",length=255,nullable=true)
	private String regAddress="";
	/**
	 * 公司联系地址
	 */
	@Column(name="f_contactAddress",length=255,nullable=true)
	private String contactAddress="";
	/**
	 * 注册资金
	 */
	@Column(name="f_regCapital",nullable=true)
	private Float regCapital=0f;
	/**
	 * 公司成立时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_foundingTime",nullable=true)
	private Date foundingTime=new Date();
	/**
	 * 营业执照(上传文件地址）
	 */
	@Column(name="f_licenceFilePath",length=255,nullable=true)
	private String licenceFilePath="";
	/**
	 * 营业执照有效期-开始时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_licenceStartDate",nullable=true)
	private Date licenceStartDate=new Date();
	/**
	 * 营业执照有效期-结束时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_licenceEndDate",nullable=true)
	private Date licenceEndDate=new Date();
	/**
	 * 商户工商注册登记号
	 */
	@Column(name="f_licenceCode",length=125,nullable=true)
	private String licenceCode="";
	/**
	 * 商户组织机构代码
	 */
	@Column(name="f_orgCode",length=125,nullable=true)
	private String orgCode="";
	/**
	 * 商户开户行（对公）
	 */
	@Column(name="f_bankName",length=125,nullable=true)
	private String bankName="";
	/**
	 * 商户银行卡号（对公）
	 */
	@Column(name="f_bankCardCode",length=50,nullable=true)
	private String bankCardCode="";
	/**
	 * 商户介绍
	 */
	@Column(name="f_intro",length=2048,nullable=true)
	private String intro="";
	/**
	 * 营业执照经营范围
	 */
	@Column(name="f_scopeOfBusiness",length=1024,nullable=true)
	private String scopeOfBusiness="";
	/**
	 * 公司官网地址
	 */
	@Column(name="f_website",length=100,nullable=true)
	private String website="";
	/**
	 * 法人或代理人的身份证号
	 */
	@Column(name="f_idCard",length=100,nullable=true)
	private String idCard="";
	/**
	 * 上半年整体销售额
	 */
	@Column(name="f_lastYearPrice",length=20,nullable=true)
	private String lastYearPrice="0";

	/**
	 * 店铺性质
	 */
	@Column(name="f_shopType",nullable=true)
	private Integer shopType=0;
	/**
	 * 店铺运营人数-员工人数
	 */
	@Column(name="f_employeesCount",nullable=true)
	private Integer employeesCount=0;
	/**
	 * 运营方式
	 */
	@Column(name="f_manageType",nullable=true)
	private Integer manageType=0;
	/**
	 * 仓库情况
	 */
	@Column(name="f_warehouse",nullable=true)
	private Integer warehouse=0;
	/**
	 * ERP情况
	 */
	@Column(name="f_erpType",nullable=true)
	private Integer erpType=0;
	/**
	 * 审核状态
	 */
	@Column(name="f_status",nullable=true)
	private Integer status=0;
	/**
	 * 状态更新时间
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_statusChangeDate",nullable=true)
	private Date statusChangeDate=new Date();
	/**
	 * logo
	 */
	@Column(name="f_logo",length=100,nullable=true)
	private String logo="noImage";
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getManagerTel() {
		return managerTel;
	}
	public void setManagerTel(String managerTel) {
		this.managerTel = managerTel;
	}
	public String getManagerEmail() {
		return managerEmail;
	}
	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}
	public String getManagerOtherContact() {
		return managerOtherContact;
	}
	public void setManagerOtherContact(String managerOtherContact) {
		this.managerOtherContact = managerOtherContact;
	}
	public Integer getTenantType() {
		return tenantType;
	}
	public void setTenantType(Integer tenantType) {
		this.tenantType = tenantType;
	}
	public String getEntFullName() {
		return entFullName;
	}
	public void setEntFullName(String entFullName) {
		this.entFullName = entFullName;
	}
	public String getLegalPerson() {
		return legalPerson;
	}
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	public String getRegAddress() {
		return regAddress;
	}
	public void setRegAddress(String regAddress) {
		this.regAddress = regAddress;
	}
	public String getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	public Float getRegCapital() {
		return regCapital;
	}
	public void setRegCapital(Float regCapital) {
		this.regCapital = regCapital;
	}
	public Date getFoundingTime() {
		return foundingTime;
	}
	public void setFoundingTime(Date foundingTime) {
		this.foundingTime = foundingTime;
	}
	public String getLicenceFilePath() {
		return licenceFilePath;
	}
	public void setLicenceFilePath(String licenceFilePath) {
		this.licenceFilePath = licenceFilePath;
	}
	public Date getLicenceStartDate() {
		return licenceStartDate;
	}
	public void setLicenceStartDate(Date licenceStartDate) {
		this.licenceStartDate = licenceStartDate;
	}
	public Date getLicenceEndDate() {
		return licenceEndDate;
	}
	public void setLicenceEndDate(Date licenceEndDate) {
		this.licenceEndDate = licenceEndDate;
	}
	public String getLicenceCode() {
		return licenceCode;
	}
	public void setLicenceCode(String licenceCode) {
		this.licenceCode = licenceCode;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankCardCode() {
		return bankCardCode;
	}
	public void setBankCardCode(String bankCardCode) {
		this.bankCardCode = bankCardCode;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getScopeOfBusiness() {
		return scopeOfBusiness;
	}
	public void setScopeOfBusiness(String scopeOfBusiness) {
		this.scopeOfBusiness = scopeOfBusiness;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Integer getShopType() {
		return shopType;
	}
	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}
	public Integer getEmployeesCount() {
		return employeesCount;
	}
	public void setEmployeesCount(Integer employeesCount) {
		this.employeesCount = employeesCount;
	}
	public Integer getManageType() {
		return manageType;
	}
	public void setManageType(Integer manageType) {
		this.manageType = manageType;
	}
	public Integer getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Integer warehouse) {
		this.warehouse = warehouse;
	}
	public Integer getErpType() {
		return erpType;
	}
	public void setErpType(Integer erpType) {
		this.erpType = erpType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getStatusChangeDate() {
		return statusChangeDate;
	}
	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setLastYearPrice(String lastYearPrice) {
		this.lastYearPrice = lastYearPrice;
	}
	public String getLastYearPrice() {
		return lastYearPrice;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getLogo() {
		return logo;
	}
}
