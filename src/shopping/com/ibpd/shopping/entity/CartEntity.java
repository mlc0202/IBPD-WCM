
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.entity.baseEntity.IBaseEntity;
import com.ibpd.shopping.service.account.AccountServiceImpl;
import com.ibpd.shopping.service.account.IAccountService;
import com.ibpd.shopping.service.product.IProductService;
import com.ibpd.shopping.service.product.ProductServiceImpl;
import com.ibpd.shopping.service.spec.ISpecService;
import com.ibpd.shopping.service.spec.SpecServiceImpl;

@Entity
@Table(name="t_s_cart")
public class CartEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="id",nullable=true) @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_aaccountId",nullable=true)
	private Long accountId;
	@Column(name="f_accountName",length=145,nullable=true)
	private String accountName;
	@Column(name="f_productId",nullable=true)
	private Long productId;
	@Column(name="f_productName",length=145,nullable=true)
	private String productName;
	@Column(name="f_productImage",length=145,nullable=true)
	private String productImage;
	@Column(name="f_specId",nullable=true)
	private Long specId;
	@Column(name="f_specColor",length=145,nullable=true)
	private String specColor;
	@Column(name="f_specSize",length=145,nullable=true)
	private String specSize;
	@Column(name="f_specPrice",nullable=true)
	private Float specPrice;
	@Column(name="f_buyCount",nullable=true)
	private Integer buyCount;
	@Column(name="f_score",nullable=true)
	private Integer score;
	@Column(name="f_order",nullable=true)
	private Integer order;
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="addDate",nullable=true)
	private Date addDate=new Date();

	public CartEntity(){
		super();
	}

	public CartEntity(Long userId,Long productId, Long buySpecId, Integer buyCount) {
		IAccountService as=(IAccountService) ServiceProxyFactory.getServiceProxy(AccountServiceImpl.class);
		AccountEntity ae=as.getEntityById(userId);
		if(ae==null){
			return;
		}
		this.setAccountId(userId);
		this.setAccountName(ae.getAccount());
		IProductService ps=(IProductService) ServiceProxyFactory.getServiceProxy(ProductServiceImpl.class);
		ProductEntity p=ps.getEntityById(productId);
		if(p==null)
			return;
		this.setProductId(productId);
		this.setProductImage(p.getMaxPicture());
		this.setProductName(p.getName());
		this.setTenantId(p.getTenantId());
		if(buySpecId!=null && buySpecId>=0){
			ISpecService ss=(ISpecService) ServiceProxyFactory.getServiceProxy(SpecServiceImpl.class);
			SpecEntity sp=ss.getEntityById(buySpecId);
			if(sp!=null){
				this.setSpecColor(sp.getSpecColor());
				this.setSpecId(sp.getId());
				this.setSpecPrice(sp.getSpecPrice());
				this.setSpecSize(sp.getSpecSize());
			}
		}else{
			this.setSpecId(-1L);
			this.setSpecPrice(p.getNowPrice());
		}
		this.setBuyCount(buyCount);
	}

	public void resetPrice(){
		if(getSpecId()!=null && getSpecId()>=0){
			ISpecService ss=(ISpecService) ServiceProxyFactory.getServiceProxy(SpecServiceImpl.class);
			SpecEntity sp=ss.getEntityById(getSpecId());
			if(sp!=null){
				this.setSpecColor(sp.getSpecColor());
				this.setSpecId(sp.getId());
				this.setSpecPrice(sp.getSpecPrice());
				this.setSpecSize(sp.getSpecSize());
			}
		}else{
			this.setSpecId(-1L);
			IProductService ps=(IProductService) ServiceProxyFactory.getServiceProxy(ProductServiceImpl.class);
			ProductEntity p=ps.getEntityById(productId);
			if(p==null)
				throw new NullPointerException("没有找到对应的商品");
			this.setSpecPrice(p.getNowPrice());
		}
		
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getAccountId() {
		return accountId;
	}


	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}


	public String getAccountName() {
		return accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public Long getProductId() {
		return productId;
	}


	public void setProductId(Long productId) {
		this.productId = productId;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public Long getSpecId() {
		return specId;
	}


	public void setSpecId(Long specId) {
		this.specId = specId;
	}


	public String getSpecColor() {
		return specColor;
	}


	public void setSpecColor(String specColor) {
		this.specColor = specColor;
	}


	public String getSpecSize() {
		return specSize;
	}


	public void setSpecSize(String specSize) {
		this.specSize = specSize;
	}


	public Float getSpecPrice() {
		return specPrice;
	}


	public void setSpecPrice(Float specPrice) {
		this.specPrice = specPrice;
	}


	public Integer getBuyCount() {
		return buyCount;
	}


	public void setBuyCount(Integer buyCount) {
		this.buyCount = buyCount;
	}


	public Integer getScore() {
		return score;
	}


	public void setScore(Integer score) {
		this.score = score;
	}


	public Integer getOrder() {
		return order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Date getAddDate() {
		return addDate;
	}


	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}


	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		String rtn=j.toString();
		rtn=rtn.substring(1);
		rtn=rtn.substring(0,rtn.length()-1);
		return rtn;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public String getProductImage() {
		return productImage;
	}
}
