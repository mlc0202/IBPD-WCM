
package com.ibpd.shopping.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_ProductAttrValue")
public class ProductAttrValueEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="f_id",nullable=true) @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_attrId",nullable=true)
	private Long attrId;
	
	@Column(name="f_productId",nullable=true)
	private Long productId;
	
	@Column(name="f_value",length=45,nullable=true)
	private String value="";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAttrId() {
		return attrId;
	}

	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString();
	}

}
