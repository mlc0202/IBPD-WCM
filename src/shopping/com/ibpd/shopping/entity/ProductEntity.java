package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import com.ibpd.entity.baseEntity.IBaseEntity;
import com.ibpd.shopping.assist.StringJsonArrayProgressor;

@Entity
@Table(name="t_s_product")
public class ProductEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Integer Status_Pass = 99;
	public static final Integer Status_UnPass = -1;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_name",length=255,nullable=true)
	private String name=null;
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId=-1L;
	@Column(name="f_introduce",length=500,nullable=true)
	private String introduce=null;
	@Column(name="f_price",nullable=true)
	private Float price=0.0f;
	@Column(name="f_nowPrice",nullable=true)
	private Float nowPrice=0.0f;
	@Column(name="f_picture",length=2048,nullable=true)
	private String picture=null;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createtime",nullable=true)
	private Date createtime=null;
	@Column(name="f_createAccount",length=20,nullable=true)
	private String createAccount=null;
	@Column(name="f_updateAccount",length=20,nullable=true)
	private String updateAccount=null;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updatetime",nullable=true)
	private Date updatetime=null;
	@Column(name="f_isnew",nullable=true)
	private Boolean isnew=false;
	@Column(name="f_sale",nullable=true)
	private Boolean sale=false;
	@Column(name="f_hit",nullable=true)
	private Integer hit=0;
	@Column(name="f_status",nullable=true)
	private Integer status=0;
	@Column(name="f_Order",nullable=true)
	private Integer order=0;
	@Lob
	@Basic(fetch=FetchType.LAZY)
	@Column(name="f_productHTML",nullable=true)
	private String productHTML="";
	@Column(name="f_maxPicture",length=1245,nullable=true)
	private String maxPicture="";
	@Column(name="f_images",length=2000,nullable=true)
	private String images="";
	@Column(name="f_catalogId",nullable=true)
	private Long catalogId=-1L;
	@Column(name="f_sellcount",nullable=true)
	private Integer sellcount=0;
	@Column(name="f_stock",nullable=true)
	private Integer stock=0;
	@Column(name="f_searchKey",length=145,nullable=true)
	private String searchKey="";
	@Column(name="f_title",length=1255,nullable=true)
	private String title="";
	@Column(name="f_description",length=2000,nullable=true)
	private String description="";
	@Column(name="f_keywords",length=1145,nullable=true)
	private String keywords="";
	@Column(name="f_activityId",nullable=true)
	private Long activityId=-1L;
	@Column(name="f_unit",nullable=true)
	private String unit="item";
	@Column(name="f_score",nullable=true)
	private Integer score=0;
	@Column(name="f_isTimePromotion",nullable=true)
	private Boolean isTimePromotion=false;
	@Column(name="f_giftId",nullable=true)
	private Long giftId=0L;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Float getNowPrice() {
		return nowPrice;
	}
	public void setNowPrice(Float nowPrice) {
		this.nowPrice = nowPrice;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getCreateAccount() {
		return createAccount;
	}
	public void setCreateAccount(String createAccount) {
		this.createAccount = createAccount;
	}
	public String getUpdateAccount() {
		return updateAccount;
	}
	public void setUpdateAccount(String updateAccount) {
		this.updateAccount = updateAccount;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public Boolean getIsnew() {
		return isnew;
	}
	public void setIsnew(Boolean isnew) {
		this.isnew = isnew;
	}
	public Boolean getSale() {
		return sale;
	}
	public void setSale(Boolean sale) {
		this.sale = sale;
	}
	public Integer getHit() {
		return hit;
	}
	public void setHit(Integer hit) {
		this.hit = hit;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getProductHTML() {
		return productHTML;
	}
	public void setProductHTML(String productHTML) {
		this.productHTML = productHTML;
	}
	public String getMaxPicture() {
		return maxPicture;
	}
	public void setMaxPicture(String maxPicture) {
		this.maxPicture = maxPicture;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	public Long getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}
	public Integer getSellcount() {
		return sellcount;
	}
	public void setSellcount(Integer sellcount) {
		this.sellcount = sellcount;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Boolean getIsTimePromotion() {
		return isTimePromotion;
	}
	public void setIsTimePromotion(Boolean isTimePromotion) {
		this.isTimePromotion = isTimePromotion;
	}
	public Long getGiftId() {
		return giftId;
	}
	public void setGiftId(Long giftId) {
		this.giftId = giftId;
	}
	@Override
	public String toString() {
		this.productHTML="";
		JSONArray jr=JSONArray.fromObject(this);
		String rtn=jr.toString();
		rtn=rtn.substring(1);
		return rtn.substring(0,rtn.length()-1);
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Integer getOrder() {
		return order;
	}
	
}
