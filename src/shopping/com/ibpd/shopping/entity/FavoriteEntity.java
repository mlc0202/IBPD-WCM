package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_favorite")
public class FavoriteEntity  extends IBaseEntity{
	public static final Integer TYPE_PRODUCT=0;
	public static final Integer TYPE_TENANT=1;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_account",length=25,nullable=true)
	private String account=null;
	
	@Column(name="f_accId",nullable=true)
	private Long accId=-1L;
	@Column(name="f_type",nullable=true)
	private Integer type=0;
	
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId=null;
	
	@Column(name="f_productId",nullable=true)
	private Long productId=null;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createtime",nullable=true)
	private Date createtime=null;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getType() {
		return type;
	}
	public void setAccId(Long accId) {
		this.accId = accId;
	}
	public Long getAccId() {
		return accId;
	}
	
}
