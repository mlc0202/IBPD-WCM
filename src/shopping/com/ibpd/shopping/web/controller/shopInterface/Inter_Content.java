package com.ibpd.shopping.web.controller.shopInterface;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;
import com.ibpd.henuocms.service.contentAttr.ContentAttrServiceImpl;
import com.ibpd.henuocms.service.contentAttr.IContentAttrService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.assist.ExtContentEntity;
import com.ibpd.shopping.assist.ExtOrginalEntity;
import com.ibpd.shopping.assist.InterfaceUtil;
import com.ibpd.shopping.entity.AccountEntity;
/**
 * /**
 * 新闻内容接口类
 * @author 马根
 */
@Controller
public class Inter_Content  extends BaseController{
	/**
	 * 根据类别全拼（小写）获取类别下的栏目（分类）
	 * @see /getChildNodeListByType.do?type=yuanchuang
	 * @return 返回由order顺序排序的json数据列表，json数据格式以及含义可参阅{@linkplain com.ibpd.henuocms.entity.NodeEntity NodeEntity}.
	 * @param type 类别的全拼
	 * @param resp 无关参数
	 * @param req 无关参数
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@RequestMapping("getChildNodeListByType.do")
	public void getChildNodeList(String type,HttpServletResponse resp,HttpServletRequest req) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		String whereString=null;
		if(type!=null && type.trim().length()>0){
			String nodeName="";
			if(type.trim().toLowerCase().equals("zixun")){
				nodeName="资讯";
			}else if(type.trim().toLowerCase().equals("baike")){
				nodeName="百科";
			}else if(type.trim().toLowerCase().equals("yuanchuang")){
				nodeName="原创";
			}else if(type.trim().toLowerCase().equals("shouye")){
				nodeName="首页";
			}else{
				nodeName="-1";
			}
			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			List<NodeEntity> nl=ns.getList("from "+ns.getTableName()+" where text='"+nodeName+"'", null);
			if(nl!=null && nl.size()>0){
				whereString="parentId="+nl.get(0).getId();
				super.getList(req, ns, resp, "asc", 1, 1000, "order", whereString);
				return;
			}else{
				super.printMsg(resp, "-1", "-1", "type值错误");
				return;
			}
		}else{
			super.printMsg(resp, "-2", "-2", "type不能为空");
			return; 
		}
		
	}
	@RequestMapping("getChildNodeListById.do")
	public void getChildNodeList(Long id,Integer pageSize,Integer pageIndex,HttpServletResponse resp,HttpServletRequest req) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		String whereString=null;
		if(id!=null){
			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			whereString="parentId="+id+" and isDeleted=false";
			List l=ns.getList("from "+ns.getTableName()+" where "+whereString,null,"order asc",pageSize,pageIndex);
			Long rowCount=ns.getRowCount(" where "+whereString, null);
			super.sendJsonByList(resp, l, rowCount);
//			super.getList(req, ns, resp, "asc", pageIndex, pageSize, "order", whereString,true);
			return;
		}else{
			super.printMsg(resp, "-2", "-2", "ID不能为空");
			return;
		}
		
	}

	@RequestMapping("getNewsList.do")
	public void getNewsList(Integer pageSize,Integer pageIndex,String type,String orderField,String orderType,String otherQueryString,org.springframework.ui.Model model,HttpServletResponse resp,HttpServletRequest req) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		IContentService imgServ=(IContentService) getService(ContentServiceImpl.class);
		pageSize=(pageSize==null)?1000:pageSize;
		pageSize=(pageSize==-1 || pageSize==0)?1000:pageSize;
		pageIndex=(pageIndex==null)?1:pageIndex;
		pageIndex=(pageIndex<=0)?1:pageIndex;
		if(orderField==null){
			orderField="id";
			orderType="desc";
		}
		if(orderField!=null){
			orderType=(orderType==null)?"asc":orderType;
		}
//		orderType=(orderField==null)?"order asc":orderField+" "+orderType;
		String whereString=null;
		if(type!=null && type.trim().length()>0){ 
			String nodeName="";
			if(type.trim().toLowerCase().equals("zixun")){
				nodeName="资讯";
			}else if(type.trim().toLowerCase().equals("baike")){
				nodeName="百科";
			}else if(type.trim().toLowerCase().equals("yuanchuang")){
				nodeName="原创";
			}else if(type.trim().toLowerCase().equals("shouye")){
				nodeName="首页";
			}else{
				nodeName="-1";
			}
			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			List<NodeEntity> nl=ns.getList("from "+ns.getTableName()+" where text='"+nodeName+"'", null);
			if(nl!=null && nl.size()>0){
				whereString="nodeIdPath like '%,"+nl.get(0).getId()+",%' and state=1 and deleted=false";
				String q=getOtherQueryString(otherQueryString);
				if(!StringUtil.isBlank(q)){
					whereString+=" and "+q; 
				}
//				super.getList(req, imgServ, resp, orderType, pageIndex, pageSize, orderField, whereString,"text");
				String whereStr="";
				if(whereString!=null){
					whereStr=" where "+whereString;
				} 
				List<ContentEntity> etList=imgServ.getList("from "+imgServ.getTableName()+whereStr,null,orderField+" "+orderType,  pageSize,pageSize*(pageIndex-1));
				List<ExtContentEntity> extList=new ArrayList<ExtContentEntity>();
				if(etList==null){
					super.printJsonData(resp, "{rows:0}");
					return;
				}else{
					for(ContentEntity c:etList){
						ExtContentEntity ext=new ExtContentEntity(c);
						extList.add(ext);
						ext.setText("0");
					}
				}
				Long rowCount=imgServ.getRowCount(whereStr,null);
				sendJsonByList(resp,extList,rowCount);
			}
		}else{
			super.printJsonData(resp, "{rows:0}");
		}
		
	}
	@RequestMapping("getShareList.do")
	public void getShareList(Integer pageSize,Integer pageIndex,String orderField,String orderType,String otherQueryString,org.springframework.ui.Model model,HttpServletResponse resp,HttpServletRequest req) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		IContentService imgServ=(IContentService) getService(ContentServiceImpl.class);
		pageSize=(pageSize==null)?1000:pageSize;
		pageSize=(pageSize==-1 || pageSize==0)?1000:pageSize;
		pageIndex=(pageIndex==null)?1:pageIndex;
		pageIndex=(pageIndex<=0)?1:pageIndex;
		if(orderField==null){
			orderField="id";
			orderType="desc";
		}
		if(orderField!=null){
			orderType=(orderType==null)?"asc":orderType;
		}
		String whereString=null;
			String nodeName="同分享";
			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			IContentAttrService caService=(IContentAttrService) ServiceProxyFactory.getServiceProxy(ContentAttrServiceImpl.class);		
			List<NodeEntity> nl=ns.getList("from "+ns.getTableName()+" where text='"+nodeName+"' and isDeleted=false and state="+NodeEntity.NODESTATE_OPEN, null);
			if(nl!=null && nl.size()>0){
				whereString="nodeIdPath like '%,"+nl.get(0).getId()+",%' and state=1 and deleted=false";
				String q=getOtherQueryString(otherQueryString);
				if(!StringUtil.isBlank(q)){
					whereString+=" and "+q; 
				}
//				super.getList(req, imgServ, resp, orderType, pageIndex, pageSize, orderField, whereString,"text");
				String whereStr="";
				if(whereString!=null){
					whereStr=" where "+whereString;
				} 
				List<ContentEntity> etList=imgServ.getList("from "+imgServ.getTableName()+whereStr,null,orderField+" "+orderType,  pageSize,pageIndex);
				List<ExtContentEntity> extList=new ArrayList<ExtContentEntity>();
				if(etList==null){
					super.printJsonData(resp, "{rows:0}");
					return;
				}else{
					for(ContentEntity c:etList){
						ExtContentEntity ext=new ExtContentEntity(c);
						ContentAttrEntity ca=caService.getContentByCopyIds(c.getId());
						if(ca!=null){
							ContentEntity ce=imgServ.getEntityById(ca.getContentId());
							if(ce!=null){
								NodeEntity ne=ns.getEntityById(ce.getNodeId());
								if(ne!=null){
									ext.setNodeId(ne.getId());
									ext.setNodeName(ne.getText());
									ext.setNodeBackgroundImage(ne.getBanner());
									ext.setNodeDescription(ne.getCopyright());
									ext.setNodeLogo(ne.getLogo());									
								}
							}
						}
						extList.add(ext);
						ext.setText("0");
					}
				}
				Long rowCount=imgServ.getRowCount(whereStr,null);
				sendJsonByList(resp,extList,rowCount);
			}else{
				super.printJsonData(resp, "{rows:0}");
			}
		}
		
	@RequestMapping("getNewsListByNodeId.do")
	public void getNewsListByNodeId(Integer pageSize,Integer pageIndex,Long nodeId,String orderField,String orderType,String otherQueryString,org.springframework.ui.Model model,HttpServletResponse resp,HttpServletRequest req) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		IContentService imgServ=(IContentService) getService(ContentServiceImpl.class);
		pageSize=(pageSize==null)?1000:pageSize;
		pageSize=(pageSize==-1 || pageSize==0)?1000:pageSize;
		pageIndex=(pageIndex==null)?1:pageIndex;
		pageIndex=(pageIndex<=0)?1:pageIndex;
		if(orderField==null){
			orderType="desc";
			orderField="id";
		}
		if(orderField!=null){
			orderType=(orderType==null)?"asc":orderType;
		}
//		orderType=(orderField==null)?"order asc":orderField+" "+orderType;
		String whereString=null;

			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			if(nodeId!=null && nodeId>0L){
				whereString="nodeIdPath like '%,"+nodeId+",%' and state=1 and deleted=false";
				String q=getOtherQueryString(otherQueryString);
				if(!StringUtil.isBlank(q)){
					whereString+=" and "+q; 
				}
				String whereStr="";
				if(whereString!=null){
					whereStr=" where "+whereString;
				} 
				List<ContentEntity> etList=imgServ.getList("from "+imgServ.getTableName()+whereStr,null,orderField+" "+orderType,  pageSize,pageSize*(pageIndex-1));
				List<ExtContentEntity> extList=new ArrayList<ExtContentEntity>();
				if(etList==null){
					super.printJsonData(resp, "{rows:0}");
					return;
				}else{
					for(ContentEntity c:etList){
						ExtContentEntity ext=new ExtContentEntity(c);
						ext.setText("0");
						extList.add(ext);
					}
				}
				Long rowCount=imgServ.getRowCount(whereStr,null);
				sendJsonByList(resp,extList,rowCount);
			}else{
				super.printJsonData(resp, "{rows:0}");
			}
	}
	/**
	 * shopping专门定制的接口,少了连接符部分
	 * @param query
	 * @return
	 */
	private String getOtherQueryString(String query){
		String rtn="";
		if(query==null || StringUtil.isBlank(query))
			return rtn;
		//
		String[]  pms=query.split(";");
		for(String pm:pms){
			String[] singPmArray=pm.split(":");
			if(singPmArray.length==3){
				if(singPmArray[1].equals("eq")){
					rtn+=singPmArray[0]+"='"+singPmArray[2]+"' and ";
				}else if(singPmArray[1].equals("like")){
					rtn+=singPmArray[0]+" like '%"+singPmArray[2]+"%' and ";
				}else if(singPmArray[1].equals("gt")){
					rtn+=singPmArray[0]+" > '"+singPmArray[2]+"' and ";
				}else if(singPmArray[1].equals("lt")){
					rtn+=singPmArray[0]+" < '"+singPmArray[2]+"' and ";
				}
			}
		}
		if(StringUtil.isBlank(rtn)){
			return rtn;
		}else{
			return rtn.substring(0,rtn.length()-4);
		}
	}
	@RequestMapping("getNewsBaseInfo.do")
	public void getNewsById(Long id,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		if(id==null || id<=0){
			super.printMsg(resp, "-1", "-1", "ID为空");
			return;
		}
		IContentService imgServ=(IContentService) getService(ContentServiceImpl.class);
		ContentEntity news=imgServ.getEntityById(id);
		if(news==null){
			super.printMsg(resp, "-2", "-2", "没有该内容");
			return;
		}
		List<ContentEntity> newsList=new ArrayList<ContentEntity>();
		newsList.add(news);
//		super.processList(newsList, new String[]{"text"});
		List<ExtContentEntity> extList=new ArrayList<ExtContentEntity>();
		for(ContentEntity c:newsList){
			ExtContentEntity ext=new ExtContentEntity(c);
			ext.setText("0");
			extList.add(ext);
		}
		super.sendJsonByList(resp, extList, 1L);
	}
	@RequestMapping("getNewsContent.do")
	public  void getNewsContent(Long id,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		if(id==null || id<=0){
			super.printMsg(resp, "-1", "-1", "ID为空");
			return;
		}
		IContentService imgServ=(IContentService) getService(ContentServiceImpl.class);
		ContentEntity news=imgServ.getEntityById(id);
		if(news==null){
			super.printMsg(resp, "-2", "-2", "没有该内容");
			return;
		}
		super.printJsonData(resp, news.getText());
		
	}
	@RequestMapping("addNewContent.do")
	public void addNewContent(HttpServletResponse resp,HttpServletRequest req,ContentEntity content) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		Integer r=saveContent(content);
		if(r==-1){
			super.printMsg(resp, "-1", "-1", "参数错误");			
		}else if(r==1){
			super.printMsg(resp, "1", "1", "保存成功");	
		}else{
			super.printMsg(resp, r+"", "-1", "其他错误");
		}
	}
	@RequestMapping("updateContent.do")
	public void updateContent(HttpServletResponse resp,HttpServletRequest req,ContentEntity content) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		Integer r=updateContent(content);
		if(r<=0){
			super.printMsg(resp, "-1", "-1", "参数错误");			
		}else if(r==1){
			super.printMsg(resp, "1", "1", "保存成功");	
		}else{
			super.printMsg(resp, r+"", "-1", "其他错误");
		} 
	}
	@RequestMapping("getAccountOriginalList.do")
	public void getAccountOriginalList(HttpServletResponse resp,HttpServletRequest req,Integer pageSize,Integer pageIndex) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		AccountEntity acc=InterfaceUtil.getLoginedAccountInfo(req);
		if(acc==null){
			super.printMsg(resp, "-1", "-1", "尚未登录或登录已经失效");
			return; 
		}
		pageSize=(pageSize==null)?10:pageSize;
		pageSize=(pageSize<=0)?10:pageSize;
		pageIndex=(pageIndex==null)?0:pageIndex;
		pageIndex=(pageIndex<=0)?0:pageIndex;
		INodeService ns=(INodeService) getService(NodeServiceImpl.class);
		List<NodeEntity> nl=ns.getList("from "+ns.getTableName()+" where text like '原创梦'",null);
		if(nl==null){
			super.printMsg(resp, "-2", "-1", "后台无对应栏目");
			return;
		}
		if(nl.size()==0){
			super.printMsg(resp, "-3", "-1", "后台无对应栏目");
			return;
		}
		if(nl.size()>1){
			super.printMsg(resp, "-4", "-1", "存在多个同名栏目");
			return;
		}
		NodeEntity node=nl.get(0);
		IContentService cs=(IContentService) getService(ContentServiceImpl.class);
		List<ContentEntity> lst=cs.getList("from "+cs.getTableName()+" where nodeIdPath like '"+node.getNodeIdPath()+"' and state=1 and author like '"+acc.getAccount()+"'",null,"createDate desc",pageSize,pageIndex);
		Long rowCount=cs.getRowCount(" where nodeIdPath like '"+node.getNodeIdPath()+"' and state=1 and author like '"+acc.getAccount()+"'", null);
		List<ExtOrginalEntity> extList=new ArrayList<ExtOrginalEntity>();
		for(ContentEntity c:lst){
			ExtOrginalEntity ext=new ExtOrginalEntity();
			ext.setAccount(c.getAuthor());
			ext.setId(c.getId()); 
			ext.setContent(c.getText());
			ext.setImageUrl(c.getPreviewUrl());
			ext.setTitle(c.getTitle());
			ext.setTypeInt((c.getGroup()==null)?0:(c.getGroup().trim().equals("原创")?0:1));
			ext.setTypeString(c.getGroup());
			ext.setCreateDate(c.getCreateDate());
			extList.add(ext);
		}
		JSONArray j=JSONArray.fromObject(extList,super.getDefaultJsonConfig());
//		super.printJsonData(resp, "{\"rows\":"+j.toString()+"}");
		super.printJsonData(resp,"{\"total\":"+rowCount.toString()+",\"rows\":"+j.toString()+"}");
	}
	@RequestMapping("updateOriginal.do")
	public void updateOriginal(HttpServletResponse resp,HttpServletRequest req,Long id,String title,Integer type,String imageUrl,String content,Integer state) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		AccountEntity acc=InterfaceUtil.getLoginedAccountInfo(req);
		if(acc==null){
			super.printMsg(resp, "-1", "-1", "尚未登录或登录已经失效");
			return;
		}
		if(id==null){
			super.printMsg(resp, "-2", "-1", "参数错误");
			return;			
		}
		
		Integer _state=0;
		String _type="";
		IContentService cs=(IContentService) getService(ContentServiceImpl.class);
		ContentEntity ce=cs.getEntityById(id);
		if(ce==null){
			super.printMsg(resp, "-3", "-1", "没该内容");
			return;				
		}
		if(!StringUtils.isBlank(title)){
			ce.setTitle(title);
		}
		if(!StringUtils.isBlank(imageUrl)){
			ce.setPreviewUrl(imageUrl);
			ce.setExistPreview(true);
		}
		if(!StringUtils.isBlank(content)){
			ce.setText(content);
		}
		if(type!=null){
			if(type==0){
				_type="原创";
			}else{
				_type="转载";
			}
			ce.setGroup(_type);
		}
		if(state!=null){
			if(state==1){
				_state=1;
			}else{
				_state=0;
			}
			ce.setState(_state);
		}
		Integer r=this.updateContent(ce);
		if(r<=0){
			super.printMsg(resp, "-1", "-1", "参数错误");			
		}else if(r==1){
			super.printMsg(resp, "1", "1", "保存成功");	
		}else{
			super.printMsg(resp, r+"", "-1", "其他错误");
		}		
	}
	@RequestMapping("addNewOriginal.do")
	public void addNewOriginal(HttpServletResponse resp,HttpServletRequest req,String title,Integer type,String imageUrl,String content,Integer state) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		AccountEntity acc=InterfaceUtil.getLoginedAccountInfo(req);
		if(acc==null){
			super.printMsg(resp, "-1", "-1", "尚未登录或登录已经失效");
			return;
		}
		Integer _state=1;
		String _type="";
		state=(state==null)?1:state;
		type=(type==null)?0:type;
		if(state==1){
			_state=1;
		}else{
			_state=0;
		}
		if(type==0){
			_type="原创";
		}else{
			_type="转载";
		} 
		INodeService ns=(INodeService) getService(NodeServiceImpl.class);
		List<NodeEntity> nl=ns.getList("from "+ns.getTableName()+" where text like '原创梦' and state="+NodeEntity.NODESTATE_OPEN + " and isDeleted=false",null);
		if(nl==null){
			super.printMsg(resp, "-2", "-1", "后台无对应栏目");
			return;
		}
		if(nl.size()==0){
			super.printMsg(resp, "-3", "-1", "后台无对应栏目");
			return;
		}
		if(nl.size()>1){
			super.printMsg(resp, "-4", "-1", "存在多个同名栏目");
			return;
		}
		NodeEntity node=nl.get(0);
		ContentEntity c=new ContentEntity();
		c.setTitle(title);
		c.setPreviewUrl(imageUrl);
		c.setExistPreview(true);
		c.setCreateDate(new Date());
		c.setAuthor(acc.getAccount());
		c.setNodeId(node.getId());
		c.setText(content);
		c.setNodeIdPath(node.getNodeIdPath());
		c.setSubSiteId(node.getSubSiteId());
		c.setState(_state);
		c.setGroup(_type);
		Integer r=saveContent(c);
		if(r==-1){
			super.printMsg(resp, "-1", "-1", "参数错误");			
		}else if(r==1){
			super.printMsg(resp, "1", "1", "保存成功");	
		}else{
			super.printMsg(resp, r+"", "-1", "其他错误");
		}
	}
	@RequestMapping("deleteOriginal.do")
	public void deleteOriginal(HttpServletResponse resp,HttpServletRequest req,Long id) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		IContentService cs=(IContentService) getService(ContentServiceImpl.class);
		if(id!=null && id>0){
			cs.deleteByPK(id);
			super.printMsg(resp, "1", "1", "执行成功");
		}else{
			super.printMsg(resp, "-1", "-1", "参数错误");		
		}
	}
	private Integer saveContent(ContentEntity content){
		if(content.getNodeId()==null){
			return -1;
		}
		INodeService ns=(INodeService) getService(NodeServiceImpl.class);
		NodeEntity n=ns.getEntityById(content.getNodeId());
		if(n==null){
			return -1;			
		}else{
			content.setNodeIdPath(n.getNodeIdPath());
			IContentService cs=(IContentService) getService(ContentServiceImpl.class);
			cs.saveEntity(content);
			return 1;			
		}

	}
	private Integer updateContent(ContentEntity content) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(content==null){
			return -1;
		}
		if(content.getNodeId()==null || content.getNodeId()<=0){
			return -2;
		}
		if(content.getSubSiteId()==null || content.getSubSiteId()<=0){
			return -2;
		}
		if(content.getNodeIdPath()==null || content.getNodeIdPath().trim().length()==0){
			return -3;
		}
		if(content.getId()==null){
			return -4;
		}
		IContentService cs=(IContentService) getService(ContentServiceImpl.class);
		ContentEntity ce=cs.getEntityById(content.getId());
		Long nodeId=content.getNodeId();
		String nodeIdPath=content.getNodeIdPath();
		Long siteId=content.getSubSiteId();
		super.swap(content, ce);
		ce.setNodeId(nodeId);
		ce.setNodeIdPath(nodeIdPath);
		ce.setSubSiteId(siteId);
		cs.saveEntity(ce);
		return 1;
	}
}
