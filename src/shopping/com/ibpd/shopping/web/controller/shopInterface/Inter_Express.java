package com.ibpd.shopping.web.controller.shopInterface;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.assist.ExtExpressEntity;
import com.ibpd.shopping.entity.ExpressEntity;
import com.ibpd.shopping.entity.TenantEntity;
import com.ibpd.shopping.service.express.ExpressServiceImpl;
import com.ibpd.shopping.service.express.IExpressService;
import com.ibpd.shopping.service.tenant.ITenantService;
import com.ibpd.shopping.service.tenant.TenantServiceImpl;
@Controller
public class Inter_Express  extends BaseController{
	private String getTenantName(ITenantService ts,Long tId){
		if(tId==null)
			return "";
		if(tId==-1L){
			return "��Ӫ";
		}
		if(ts==null)
			return "";
		TenantEntity t=ts.getEntityById(tId);
		if(t==null)
			return "";
		return t.getTenantName();
	}
	@RequestMapping("getExpressList.do")
	public void getExpressList(HttpServletResponse resp,HttpServletRequest req,String tenantIds) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		IExpressService expServ=(IExpressService) getService(ExpressServiceImpl.class);
		tenantIds=tenantIds==null?"":tenantIds;
		if(tenantIds.trim().length()==0){
			PrintWriter out = resp.getWriter();
			out.print("{\"rows\":[]}");
			 out.flush();
			 return;
		}
		String hql="from "+expServ.getTableName()+" where tenantId=-99 or ";
		String tmp=hql;
		String[] tids=tenantIds.split(",");
		for(String id:tids){
			hql+="tenantId="+id+" or ";
		}
		hql=hql.substring(0,hql.length()-4);
		List<ExpressEntity> addList=expServ.getList(hql,null);
		Map<Long,List<ExpressEntity>> map=new HashMap<Long,List<ExpressEntity>>();
		if(addList!=null){
			for(ExpressEntity exp:addList){
				List<ExpressEntity> lst=map.get(exp.getTenantId());
				lst=(lst==null)?new ArrayList<ExpressEntity>():lst;
				lst.add(exp);
				map.put(exp.getTenantId(), lst);
			}
		}
		ITenantService ts=(ITenantService) getService(TenantServiceImpl.class);
		List<ExtExpressEntity> extList=new ArrayList<ExtExpressEntity>();
		for(Long id:map.keySet()){
			ExtExpressEntity ext=new ExtExpressEntity();
			List<ExpressEntity> lst=map.get(id);
			if(lst!=null && lst.size()>0){
				ext.setExemptPrice(lst.get(0).getExempt());
				ext.setRows(lst);
				ext.setTenantId(id);
				ext.setTenantName(getTenantName(ts,id));
				extList.add(ext);
			}
		}
//		Double exempt=0d;
//		if(addList!=null){
//			for(ExpressEntity e:addList){
//				exempt=e.getExempt();
//				ExtExpressEntity ext=new ExtExpressEntity(e);
//				ext.setExemptPrice(e.getExempt());
//				ext.setTenantName(getTenantName(ts,e.getTenantId()));
//				extList.add(ext);
//			}
//		}
//		List<Hashtable<String,String>> lst=convertToHashTable(extList);
		try {
			JsonConfig conf=super.getDefaultJsonConfig();
		    JSONArray jsonArray = JSONArray.fromObject( extList.toArray(),conf );
		    
			PrintWriter out = resp.getWriter();
//			 out.print("{\"total\":"+extList.size()+",\"exempt\":\""+exempt+"\",\"rows\":"+jsonArray+"}");
			 out.print("{\"rows\":"+jsonArray+"}");
			 out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}