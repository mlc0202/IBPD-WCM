package com.ibpd.shopping.web.controller.manage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.assist.MD5;
import com.ibpd.shopping.entity.AccountEntity;
import com.ibpd.shopping.service.account.AccountServiceImpl;
import com.ibpd.shopping.service.account.IAccountService;

@Controller
public class Account extends BaseController {
	@RequestMapping("Manage/Account/index.do")
	public String index(Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","会员管理");
		return "manage/s_account/index";
	}
	@RequestMapping("Manage/Account/toChangePassword.do")
	public String toChangePassword(String ids,Model model,HttpServletResponse resp,HttpServletRequest req){
		model.addAttribute("pageTitle","修改密码");
		model.addAttribute("ids",ids);
		return "manage/s_account/editPassword";
	}
	@RequestMapping("Manage/Account/doChangePassword.do")
	public void toChangePassword(String ids,String password,HttpServletResponse resp,HttpServletRequest req) throws IOException{
		IAccountService accServ=(IAccountService) getService(AccountServiceImpl.class);
		if(ids==null){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		String[] idarr=ids.split(",");
		for(String id:idarr){
			AccountEntity acc=accServ.getEntityById(Long.valueOf(id));
			if(acc!=null){
				acc.setPassword(MD5.md5(password));
				accServ.saveEntity(acc);
			}
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}
	@RequestMapping("Manage/Account/list.do")
	public void list(HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		IAccountService tenantServ=(IAccountService) getService(AccountServiceImpl.class);
		String query="";
//		tenantServ.getDao().clearCache();
		super.getList(req, tenantServ, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Account/props.do")
	public String props(Long id,Model model,HttpServletRequest req) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null || id<0L){
			model.addAttribute("errorMsg","参数错误");
			return super.ERROR_PAGE;
		}
		IAccountService tenantServ=(IAccountService) getService(AccountServiceImpl.class);
		AccountEntity ce=tenantServ.getEntityById(id);
		if(ce==null){
			model.addAttribute("errorMsg","没有该类别");
			return super.ERROR_PAGE;
		}
		model.addAttribute("pageTitle","参数设置");
		model.addAttribute("htmls",super.getHtmlString(ce, "account.edit.field"));
		model.addAttribute("entity",ce);
		model.addAttribute("loginedUserType",SysUtil.getCurrentLoginedUserType(req.getSession()));
		return "manage/s_account/props";
	}	
	@RequestMapping("Manage/Account/saveProp.do")
	public void saveProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		if(id!=null && id!=-1){
			IAccountService tenantServ=(IAccountService) getService(AccountServiceImpl.class);
			AccountEntity cata=tenantServ.getEntityById(id);
				if(cata!=null){
					//先确定参数类型
					Method tmpMethod=cata.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
					
					
					Method method=cata.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
					Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
					IbpdLogger.getLogger(Account.class).info("value="+val);
					method.invoke(cata, val);
					tenantServ.saveEntity(cata);
//					tenantServ.getDao().clearCache();
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");		
				}else{
					super.printMsg(resp, "-1", "", "没有该类别");
				}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");			
		}
	}
	@RequestMapping("Manage/Account/toAdd.do")
	public String toAdd(Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

		model.addAttribute("pageTitle","添加");
		model.addAttribute("htmls",super.getHtmlString(null, "account.add.field"));
		return "manage/s_account/add";
	}
	@RequestMapping("Manage/Account/toEdit.do")
	public String toEdit(Long id,Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null){
			return this.ERROR_PAGE;
		}
		IAccountService tenantServ=(IAccountService) getService(AccountServiceImpl.class);
		AccountEntity cata=tenantServ.getEntityById(id);
		if(cata==null){
			model.addAttribute("errorMsg","没有该类别");
			return this.ERROR_PAGE;
		}
		model.addAttribute("pageTitle","编辑");
		model.addAttribute("htmls",super.getHtmlString(cata, "account.edit.field"));
		model.addAttribute("entity",cata);
		return "manage/s_account/edit";
	}
	@RequestMapping("Manage/Account/doEdit.do")
	public void doEdit(AccountEntity entity,HttpServletResponse resp) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(entity!=null && entity.getId()!=null && entity.getId()!=-1L){
			
			IAccountService tenantServ=(IAccountService) getService(AccountServiceImpl.class);
//			if(tenantServ.checkAccountExist(entity.getAccount())){
//				super.printMsg(resp, "-1", "-1", "该会员名已经存在");
//				return;
//			}
			AccountEntity cata=tenantServ.getEntityById(entity.getId());
			if(cata==null){
				super.printMsg(resp, "-1", "", "没有该会员");
				return;
			}else{
				String psd=cata.getPassword();
				swap(entity,cata);
				cata.setPassword(psd);
				tenantServ.saveEntity(cata);
				super.printMsg(resp, "99", cata.getId().toString(), "保存成功");
				return;
			}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");
			return;
		}
		
	}
	@RequestMapping("Manage/Account/doAdd.do")
	public void doAdd(AccountEntity entity,HttpServletResponse resp) throws IOException{
		
		
		IAccountService tenantServ=(IAccountService) getService(AccountServiceImpl.class);
		//这里需要做一下用户名的唯一性检查
//		if(tenantServ.checkAccountExist(entity.getAccount())){
//			super.printMsg(resp, "-1", "-1", "该会员名已经存在");
//			return;
//		}
		entity.setPassword(MD5.md5(entity.getPassword()));
		tenantServ.saveEntity(entity);	
		super.printMsg(resp, "99", "-1", "保存成功");
		 
	}
	@RequestMapping("Manage/Account/doDel.do")
	public void doDel(String ids,HttpServletResponse resp) throws IOException{
		if(StringUtil.isBlank(ids)){
			super.printMsg(resp, "-1", "", "没有选择项");	
		}
		IAccountService cataServ=(IAccountService) getService(AccountServiceImpl.class);
		cataServ.batchDel(ids);
		super.printMsg(resp, "99", "", "操作成功");	
	}
}