package com.ibpd.shopping.web.controller.manage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.LinkedMap;
import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.assist.ExtOrderInfoEntity;
import com.ibpd.shopping.entity.OrderEntity;
import com.ibpd.shopping.entity.OrderShipEntity;
import com.ibpd.shopping.service.order.IOrderService;
import com.ibpd.shopping.service.order.OrderServiceImpl;
import com.ibpd.shopping.service.orderDetail.IOrderdetailService;
import com.ibpd.shopping.service.orderDetail.OrderdetailServiceImpl;
import com.ibpd.shopping.service.orderShip.IOrderShipService;
import com.ibpd.shopping.service.orderShip.OrderShipServiceImpl;
import com.ibpd.shopping.service.orderpay.IOrderpayService;
import com.ibpd.shopping.service.orderpay.OrderpayServiceImpl;

@Controller
public class Order extends BaseController {
	@RequestMapping("Manage/Order/index.do")
	public String index(String status,String paystatus,Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","订单管理");
		model.addAttribute("status",status);
		model.addAttribute("paystatus",paystatus);
		
		return "manage/s_order/index";
	}
	@RequestMapping("Manage/Order/list.do")
	public void list(String status,String paystatus,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		IOrderService tenantServ=(IOrderService) getService(OrderServiceImpl.class);
		String tp=SysUtil.getCurrentLoginedUserType(req.getSession());
		Long tenantId=SysUtil.getCurrentTenantId(req.getSession());
		String query="";
//		Boolean paramIsEffective=false;
		
		if(status!=null && !StringUtil.isBlank(status)){
//			if(status.toLowerCase().equals(OrderEntity.order_status_cancel)){
//				
//			}
			query="status='"+status+"' and paystatus='"+paystatus+"' and refundStatus=null";			
		}
		if(tenantId!=null && tp.toLowerCase().trim().equals("tenant")){
			query+=" and tenantId="+tenantId;
		}
//		tenantServ.getDao().clearCache();
		super.getList(req, tenantServ, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Order/details.do")
	public String details(Long id,Model model) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		model.addAttribute("pageTitle","订单详情");
		//这里不光要显示订单的商品，还得显示配送信息、选择的快递公司信息等
		model.addAttribute("orderId",id);
		IOrderService os=(IOrderService) getService(OrderServiceImpl.class);
		OrderEntity order=os.getEntityById(id);
		model.addAttribute("order",order);
		IOrderShipService shipServ=(IOrderShipService) getService(OrderShipServiceImpl.class);
		OrderShipEntity ship=shipServ.getOrderShipByOrderId(id);
		model.addAttribute("ship",ship);
		return "manage/s_order/details";
	}	
	@RequestMapping("Manage/Order/orderDetailsList.do")
	public void orderDetailsList(Long id,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		IOrderdetailService tenantServ=(IOrderdetailService) getService(OrderdetailServiceImpl.class);
		String query="orderID="+id;
//		tenantServ.getDao().clearCache();
		super.getList(req, tenantServ, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Order/doPass.do")
	public void doPass(String ids,String status,HttpServletResponse resp) throws IOException{
		if(ids==null || ids.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		if(status==null || status.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		String[] ss=new String[]{OrderEntity.order_status_cancel,OrderEntity.order_status_file,OrderEntity.order_status_init,OrderEntity.order_status_pass,OrderEntity.order_status_send,OrderEntity.order_status_sign};
		Boolean pmIsEff=false;
		for(String s:ss){
			if(s.toLowerCase().trim().equals(status.toLowerCase().trim())){
				pmIsEff=true;
				break;
			}
		}
		if(!pmIsEff){
			super.printMsg(resp, "-2", "-2", "参数无效");
			return;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		String[] idArray=ids.split(",");
		for(String id:idArray){
			if(StringUtil.isBlank(id)){
				continue;
			}
			if(StringUtil.isNumeric(id)){
				OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
				if(oe!=null){
					oe.setStatus(status);
					ordServ.saveEntity(oe);
				}
			}
			
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}
	@RequestMapping("Manage/Order/expCode.do")
	public String expCode(Long id,Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","输入运单号");
		model.addAttribute("orderId",id);
		
		return "manage/s_order/expCode";
	}
	@RequestMapping("Manage/Order/saveExpCode.do")
	public void saveExpCode(Long id,String code,HttpServletResponse resp) throws IOException{
		if(id==null || id<=0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		if(code==null || code.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
		if(oe!=null){
			oe.setExpressNo(code);
			ordServ.saveEntity(oe);
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}
	@RequestMapping("Manage/Order/cancelOrder.do")
	public String cancelOrder(String ids,Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","取消订单");
		model.addAttribute("orderIds",ids.replace(",", "|"));
		
		return "manage/s_order/cancelOrder";
	}
	@RequestMapping("Manage/Order/doCancelOrder.do")
	public void doCancelOrder(String ids,String comment,HttpServletResponse resp) throws IOException{
		if(ids==null || ids.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		if(comment==null || comment.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		String[] idArray=ids.split("|");
		for(String id:idArray){
			if(StringUtil.isBlank(id)){
				continue;
			}
			if(StringUtil.isNumeric(id)){
				OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
				if(oe!=null){
					oe.setStatus(OrderEntity.order_status_cancel);
					oe.setRemark(comment);
					ordServ.saveEntity(oe);
				}
			}
			
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}
	@RequestMapping("Manage/Order/updatePrice.do")
	public String updatePrice(String ids,Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","更新价格");
		model.addAttribute("orderIds",ids);
		
		return "manage/s_order/price";
	}
	@RequestMapping("Manage/Order/doUpdatePrice.do")
	public void doUpdatePrice(String ids,String price,HttpServletResponse resp) throws IOException{
		if(ids==null || ids.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		if(price==null || price.trim().length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		String[] idArray=ids.split(",");
		for(String id:idArray){
			if(StringUtil.isBlank(id)){
				continue;
			}
			if(StringUtil.isNumeric(id)){
				OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
				if(oe!=null){
					oe.setUpdateAmount("y");
					oe.setAmount(Double.valueOf(price));
					ordServ.saveEntity(oe);
				}
			}
			
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}
	@RequestMapping("Manage/Order/print.do")
	public String print(String ids,Model model,HttpServletRequest req) throws IOException{
		if(ids==null || ids.trim().length()==0){
			return "manage/s_order/print";
		}
		model.addAttribute("pageTitle","订单信息打印");
		//orderPrintHeader
		IbpdCommon c=new IbpdCommon();
		String orderPrintHeader=c.getPropertiesValue("orderPrintHeader");
		model.addAttribute("orderPrintHeader",orderPrintHeader);
		List<ExtOrderInfoEntity> lst=new ArrayList<ExtOrderInfoEntity>();
		IOrderService orderServ=(IOrderService) getService(OrderServiceImpl.class);
		IOrderdetailService detaServ=(IOrderdetailService) getService(OrderdetailServiceImpl.class);
		IOrderShipService shipServ=(IOrderShipService) getService(OrderShipServiceImpl.class);
		IOrderpayService payServ=(IOrderpayService) getService(OrderpayServiceImpl.class);
		String[] idArray=ids.split(",");
		for(String id:idArray){
			Long oid=Long.valueOf(id);
			OrderEntity oe=orderServ.getEntityById(oid);
			if(oe==null){
				continue;
			}else{
				ExtOrderInfoEntity info=new ExtOrderInfoEntity(oe);
				info.setDetailList(detaServ.getListByOrderId(oid));
				info.setOrderShip(shipServ.getOrderShipByOrderId(oid));
				info.setOrderpay(payServ.getOrderpayByOrderId(oid));
				lst.add(info);
			}
		}
		model.addAttribute("lst",lst);
		return "manage/s_order/print";
	}
	@RequestMapping("Manage/Order/refund.do")
	public String refund(Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","退款退货");
		return "manage/s_order/refund";
	}
	@RequestMapping("Manage/Order/refundList.do")
	public void refundList(HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		IOrderService tenantServ=(IOrderService) getService(OrderServiceImpl.class);
		String query="refundStatus!=null";
//		tenantServ.getDao().clearCache();
		super.getList(req, tenantServ, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Order/doRefundPass.do")
	public void doRefundPass(String ids,String status,HttpServletResponse resp) throws IOException{
		if(ids==null || ids.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		if(status==null || status.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		String[] ss=new String[]{OrderEntity.order_refund_status_finish,OrderEntity.order_refund_status_init,OrderEntity.order_refund_status_pass,OrderEntity.order_refund_status_sendPass,OrderEntity.order_refund_status_unPass};
		Boolean pmIsEff=false;
		for(String s:ss){
			if(s.toLowerCase().trim().equals(status.toLowerCase().trim())){
				pmIsEff=true;
				break;
			}
		}
		if(!pmIsEff){
			super.printMsg(resp, "-2", "-2", "参数无效");
			return;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		String[] idArray=ids.split(",");
		for(String id:idArray){
			if(StringUtil.isBlank(id)){
				continue;
			}
			if(StringUtil.isNumeric(id)){
				OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
				if(oe!=null){
					oe.setRefundStatus(status);
					ordServ.saveEntity(oe);
				}
			}
			
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}
	@RequestMapping("Manage/Order/confirmRefund.do")
	public String confirmRefund(String ids,HttpServletResponse resp,Model model) throws IOException{
		if(ids==null || ids.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return super.ERROR_PAGE;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		List<OrderEntity> orderList=new ArrayList<OrderEntity>();
		Double total=0d;
		Map<String,Double> amMap=new LinkedHashMap<String,Double>();
		String[] idArray=ids.split(",");
		for(String id:idArray){
			if(StringUtil.isBlank(id)){
				continue;
			}
			if(StringUtil.isNumeric(id)){
				OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
				if(oe!=null){
					orderList.add(oe);
					total=total+oe.getAmount();
					amMap.put(oe.getAccount(), oe.getAmount());
				}
			}
			
		}
		model.addAttribute("ids", ids);
		model.addAttribute("orderList", orderList);
		model.addAttribute("accountAmount",amMap);
		model.addAttribute("total",total);
		return "manage/s_order/confirmRefund";
	}
	@RequestMapping("Manage/Order/doConfirmRefund.do")
	public void doConfirmRefund(String ids,HttpServletResponse resp) throws IOException{
		if(ids==null || ids.length()==0){
			super.printMsg(resp, "-1", "-1", "参数错误");
			return;
		}
		IOrderService ordServ=(IOrderService) getService(OrderServiceImpl.class);
		String[] idArray=ids.split(",");
		for(String id:idArray){
			if(StringUtil.isBlank(id)){
				continue;
			}
			if(StringUtil.isNumeric(id)){
				OrderEntity oe=ordServ.getEntityById(Long.valueOf(id));
				if(oe!=null){
					oe.setStatus(OrderEntity.order_status_cancel);
					oe.setRefundStatus(OrderEntity.order_refund_status_finish);
					ordServ.saveEntity(oe);
				}
			}
		}
		super.printMsg(resp, "99", "99", "执行成功");
	}


}